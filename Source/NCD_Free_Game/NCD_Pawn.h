// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "MaterialParameters.h"
#include "NCD_Pawn.generated.h"

class UDecalComponent;
class APlayerController;
class UStaticMeshComponent;
class USpringArmComponent;
class UCameraComponent;
class USceneComponent; 
class UMaterialInterface;
class ABuildingHighlightActor;
class ABuildableActor; //DEBUG
class FViewport;
struct FBox;
class ANCD_PlayerController;

UCLASS()
class NCD_FREE_GAME_API ANCD_Pawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ANCD_Pawn();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	UFUNCTION(BlueprintCallable, Category = "Settings|Pan")
	void AutoPanToLocation(FVector pPanLocation);

	/** Returns CursorToWorld subobject **/
	FORCEINLINE UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	void SelectAsset(UStaticMesh* pMesh, FTransform pTransform, TArray<UMaterialInterface*> pMaterials, bool pTutorial /* = false */, FMaterialParameters pParams = FMaterialParameters());
	void UnselectAsset(bool pTutorial = false);

	/*Return the SelectedMesh(Tutorial) based on pTutorial*/
	UStaticMeshComponent* GetSelectedMesh(bool pTutorial = false);


	UFUNCTION(BlueprintCallable, Category = "Settings|Pan")
		void SetDragPan(bool pEnabled);
	UFUNCTION(BlueprintCallable, Category = "Settings|Pan")
	FORCEINLINE bool GetDragPan() { return bCanDragPan; }
	   
protected:
	virtual void BeginPlay() override;

	void InitializeSpringArm();

	//Recalculate the panning margins based on the percentages and the new viewport size
	void OnViewportSizeChanged(FViewport* pViewport, uint32 pSomeIntValue);

	UFUNCTION(BlueprintCallable, Category = "Settings|Pan")
		void MousePositionToGround(FVector& pGroundLocation);

	void PanEdges(bool pPanX);
	UFUNCTION(BlueprintCallable, Category = "Settings|Pan")
		void PanCamera(FVector2D pPanDirection);
	UFUNCTION(BlueprintCallable, Category = "Settings|Rotation")
		void RotateCamera(FVector2D pDirection);
	void ZoomCamera(float pDeltaTime);
	void PerformAutoPan(float pDeltaTime);
	void ClampLocationInBounds(FVector& pLocationToClamp);

	//Methods which change values for the Tick() functions
	void InputMoveRight(float pInput);
	void InputMoveForward(float pInput);
	void InputStartCameraRotation();
	void InputStopCameraRotation();
	void InputZoom(float pInput);
	void InputRotateYaw(float pInput);

	//Camera Movement
	void InputMouseX(float pInput);
	void InputMouseY(float pInput);

private:
	UPROPERTY()
		ANCD_PlayerController* PlayerController;

	//2D input directions
	FVector2D PanDirection;
	FVector2D DragPanDirection;
	FVector2D EdgePanDirection;
	FVector2D RotationDirection;
	FVector2D StartingMouseDragPosition;

	FVector StartLocation;
	FVector AutoPanLocation;
	FVector PreviousDragLocation;

	FVector2D MousePosition;
	//Edge panning
	FIntPoint ViewportSize;

	float DragPanMoveTresholdSqrd;



	bool bCanRotate = false;
	bool bZoomingIn = false;
	bool bIsAutoPanning = false;

	float PanDragDistanceSqrd;
	float PanMarginHorizontal;
	float PanMarginVertical;
	float NewArmLength;
	float CurrentArmLength;
	float ZoomPanPercentage;
	float SquaredAutoPanMargin;

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Pan")
		FBox LocationBoundingBox;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Pan")
		float LocationBoundZMargin = 1000.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Pan")
		float PanScalar = 1000.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Pan")
		float EdgePanScalar = 500.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Pan")
		float CameraLag = 3.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Pan")
		float PanMarginHorizontalPercentage = 0.05f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Pan")
		float PanMarginVerticalPercentage = 0.05f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Pan")
		float ZoomPanScalar = 3.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Pan")
		bool bCanPan = true;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Pan")
		FVector2D MinimumPanDirection;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Pan")
		FVector2D MaximumPanDirection;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Pan")
		float AutoPanSpeed = 5.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Pan")
		float AutoPanMargin = 100.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Pan")
		float DragPanScreenDirectionScalar = 10000.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Pan")
		float DragPanMoveTreshold = 100.0f;
	UPROPERTY(VisibleInstanceOnly, Category = "Settings|Pan")
		bool bDragPanTresholdReached = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Rotation")
		float MinimumPitch = -89.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Rotation")
		float MaximumPitch = -20.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Rotation")
		float CameraRotationLag = 15.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Rotation")
		float RotateScalar = 200.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Rotation")
		bool bInvertHorizontalAxis = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Rotation")
		bool bInvertVerticalAxis = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Zoom")
		float ZoomScalar = 200.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Zoom")
		float MaxArmLength = 5000.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Zoom")
		float MinArmLength = 100.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Zoom")
		float DefaultArmLength = 700.f;


protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings")
		float DefaultCameraHeight = 50.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings")
		float DefaultCameraYRotation = -20.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings")
		bool bDebug = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Pan")
		bool bEnableCameraLag = true;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Pan")
		FName PanProfileName = FName(TEXT("CameraPan"));
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Settings")
		bool bCanDragPan = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Rotation")
		bool bEnableCameraRotationLag = true;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings|Zoom")
		float ZoomLerpScalar = 9.0f;

protected:



	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Grid")
		UMaterialInterface* GridMaterial;

	/** Material for the decal */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		UMaterialInterface* DecalMaterial;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Grid")
		UStaticMesh* GridStaticMesh;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		USceneComponent* SceneComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		USpringArmComponent* SpringArm;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		ABuildingHighlightActor* HighlightMeshActor;

	UPROPERTY (VisibleAnywhere, BlueprintReadOnly)
		ABuildingHighlightActor* HighlightMeshActorTutorial;

private:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Grid", meta = (AllowPrivateAccess = "true"))
		UStaticMeshComponent* GridMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Grid", meta = (AllowPrivateAccess = "true"))
		UStaticMeshComponent* SelectedMesh;

	UPROPERTY (VisibleAnywhere, BlueprintReadOnly, Category = "Grid", meta = (AllowPrivateAccess = "true"))
		UStaticMeshComponent* SelectedMeshTutorial;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		UDecalComponent* CursorToWorld;


		
};
