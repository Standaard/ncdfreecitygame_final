// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "RoadNode.h"
struct FArraySortByNodeCost
{
public:
	bool operator() (const URoadNode& A, const URoadNode& B) const {
		return (A.CostCurrent + A.CostEstimate) < (B.CostCurrent + B.CostEstimate);
	}
};
