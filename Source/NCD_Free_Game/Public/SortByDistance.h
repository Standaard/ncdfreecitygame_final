// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
struct NCD_FREE_GAME_API FSortByDistance
{
	FSortByDistance(const FVector& InSourceLocation)
		: SourceLocation(InSourceLocation)
	{

	}

	/* The Location to use in our Sort comparision. */
	FVector SourceLocation;

	bool operator()(const FVector& A, const FVector& B) const
	{
		float DistanceA = FVector::DistSquared(SourceLocation, A);
		float DistanceB = FVector::DistSquared(SourceLocation, B);

		return DistanceA > DistanceB;
	}
};


