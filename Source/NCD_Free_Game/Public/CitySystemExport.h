// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CitySystemExportBuildings.h"
#include "CitySystemExportResources.h"
#include "GameTimeState.h"
#include "CitySystemExport.generated.h"

/**
 * Export class
 */
USTRUCT(BlueprintType)
struct FCitySystemExport
{
	GENERATED_BODY()
	UPROPERTY()
		FDateTime GameDate;

	UPROPERTY()
		FGameTimeState GameTimeState;

	UPROPERTY()
		bool HappinessCounts = false;

	UPROPERTY()
		int DayCounter = 0;

	UPROPERTY()
		int TimelapseHour = 0;

	UPROPERTY()
		FString CityName;

	//TODO: Save Active Event with time elapsed.
	//UPROPERTY()
		//Active Events

	UPROPERTY()
		TMap<FName, FNCDEventExport> GameEvents;

	UPROPERTY()
		FCitySystemExportBuildings BuildingStatistics;

	UPROPERTY()
		FCitySystemExportResources ResourceStatistics;
};
