// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "TutorialBackend.h"
#include "BuildingStats.h"
#include "Tutorial.generated.h"

class ACitySystemGameMode;
//struct FBuildingStats;

/**
 * 
 */
UCLASS(BluePrintable)
class NCD_FREE_GAME_API UTutorial : public UObject {
	GENERATED_BODY()

public:
	UTutorial ();
	~UTutorial ();

	UFUNCTION ()
		void Initialize ();

	UFUNCTION (BlueprintNativeEvent, Category = "Tutorial")
		void OnConstruct (ACitySystemGameMode* pCitySystem);
	UFUNCTION (BlueprintNativeEvent, Category = "Tutorial")
		void OnStepCompleted (ETutorialSteps pTutorialStep);
	UFUNCTION (BlueprintNativeEvent, Category = "Tutorial")
		void OnSkipTutorial ();
	UFUNCTION (BlueprintNativeEvent, Category = "Tutorial")
		void OnUpdateTutorial ();
	UFUNCTION (BlueprintNativeEvent, Category = "Tutorial")
		void OnTutorialCompleted ();
	UFUNCTION (BlueprintNativeEvent, Category = " Tutorial")
		void OnClickedBuildingTutorial (FBuildingStats pBuilding);
	UFUNCTION (BlueprintNativeEvent, Category = "Tutorial")
		void OnPlayerIdle ();
	UFUNCTION (BlueprintNativeEvent, Category = "Tutorial")
		void OnBuildEvent (FBuildingStats pBuilding);

	UFUNCTION (BlueprintCallable, Category = "Tutorial")
		void SetStepCompleted (ETutorialSteps pTutorialStep, bool pSkipEvent = false);
	UFUNCTION (BlueprintCallable, Category = "Tutorial")
		void SkipTutorial ();
	UFUNCTION (BlueprintCallable, Category = "Tutorial")
		bool HasCompletedStep (ETutorialSteps pTutorialStep);
	UFUNCTION (BlueprintCallable, Category = "Tutorial")
		bool HasCompletedUpToStep (ETutorialSteps pTutorialStep);
	UFUNCTION (BlueprintCallable, Category = "Tutorial")
		bool HasCompletedTutorial ();
	UFUNCTION (BluePrintCallable, Category = "Tutorial")
		void HighlightBuildingAtLocation (FVector pLocation, bool pRemoveHighlight = false);
	UFUNCTION (BlueprintCallable, Category = "Tutorial")
		void StartPlayerIdleTimer (int pMaxIdleSeconds, bool pStopTimer = false);

	UFUNCTION ()
		void TestTickDebug ();

#pragma region Setup
	UFUNCTION ()
	void SetupStarterCity ();
#pragma region
protected:


private:
	UPROPERTY ()
	ACitySystemGameMode* _gameMode = nullptr;
	UPROPERTY ()
	UTutorialBackend* _tutorialBackend = nullptr;
	UPROPERTY ()
	FTimerHandle _idleTimerHandle;

	UPROPERTY ()
	float _timer = 0.0f;
};
