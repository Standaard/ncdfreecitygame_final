// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "NCD_GameState.generated.h"

class ABuildingDescriptionActor;
class ABuildingDemolishActor;

UCLASS()
class NCD_FREE_GAME_API ANCD_GameState : public AGameStateBase {
	GENERATED_BODY()
public:
	UFUNCTION (BlueprintCallable)
		ABuildingDescriptionActor* GetDescriptionActor ();
	UFUNCTION (BlueprintCallable)
		ABuildingDemolishActor* GetDemolishActor ();
protected:
	UPROPERTY (VisibleAnywhere)
		ABuildingDescriptionActor* DescriptionActor;
	UPROPERTY (VisibleAnywhere)
		ABuildingDemolishActor* DemolishActor;
	UPROPERTY (EditDefaultsOnly)
		TSubclassOf<ABuildingDescriptionActor> DescriptionActorClass;
	UPROPERTY (EditDefaultsOnly)
		TSubclassOf<ABuildingDemolishActor> DemolishActorClass;


};
