// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include <GenericWindow.h>
#include "BPGraphicsSettingsLib.generated.h"

#define MIN_SCREEN_WIDTH 1024
#define MIN_SCREEN_HEIGHT 768

UENUM(BlueprintType)
enum class EFullscreenMode : uint8
{
	WM_WINDOWED					UMETA(DisplayName = "Windowed"),
	WM_FULLSCREEN				UMETA(DisplayName = "Fullscreen"),
	WM_WINDOWEDFULLSCREEN		UMETA(DisplayName = "Windowed Fullscreen")
};


/**
 * 
 */
UCLASS()
class NCD_FREE_GAME_API UBPGraphicsSettingsLib : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
private:
	// Try to get the GameUserSettings object from the engine
	static UGameUserSettings* GetGameUserSettings();
	
	// Get a list of screen resolutions supported on this machine
	UFUNCTION(BlueprintPure, Category = "Video Settings")
	static bool GetSupportedScreenResolutions(TArray<FString>& Resolutions);

	// Get currently set screen resolution
	UFUNCTION(BlueprintPure, Category = "Video Settings")
		static FString GetScreenResolution();

	// Check whether or not we are currently running in fullscreen mode
	UFUNCTION(BlueprintPure, Category = "Video Settings")
		static EWindowMode::Type IsInFullscreen();

	// Set the desired screen resolution (does not change it yet)
	UFUNCTION(BlueprintCallable, Category = "Video Settings")
		static bool SetScreenResolution(const int32 Width, const int32 Height, const EWindowMode::Type Fullscreen);

	// Set the framerate limit
	UFUNCTION(BlueprintCallable, Category = "Video Settings")
		static bool SetFramerateLimit(const float FrameRate);

	// Change the current screen resolution
	UFUNCTION(BlueprintCallable, Category = "Video Settings")
		static bool ChangeScreenResolution(const int32 Width, const int32 Height, const EWindowMode::Type Fullscreen);

	UFUNCTION(BlueprintCallable, Category = "Video Settings")
		static bool SetScreenMode(const EWindowMode::Type Mode);

	UFUNCTION(BlueprintCallable, Category = "Video Settings")
		static bool ResolutionStringToIntPoint(const FString ResolutionString, FIntPoint& ResolutionIntPoint);

	// Get the current video quality settings
	UFUNCTION(BlueprintCallable, Category = "Video Settings")
		static bool GetVideoQualitySettings(int32& AntiAliasing, int32& Effects, int32& PostProcess, int32& Resolution, int32& Shadow, int32& Texture, int32& ViewDistance);

	// Set the quality settings (not applied nor saved yet)
	UFUNCTION(BlueprintCallable, Category = "Video Settings")
		static bool SetVideoQualitySettings(const int32 AntiAliasing = 3, const int32 Effects = 3, const int32 PostProcess = 3,
			const int32 Resolution = 100, const int32 Shadow = 3, const int32 Texture = 3, const int32 ViewDistance = 3);

	// Check whether or not we have vertical sync enabled
	UFUNCTION(BlueprintPure, Category = "Video Settings")
		static bool IsVSyncEnabled();

	// Set the vertical sync flag
	UFUNCTION(BlueprintCallable, Category = "Video Settings")
		static bool SetVSyncEnabled(const bool VSync);

	// Confirm and save current video mode (resolution and fullscreen/windowed) as well as quality settings
	UFUNCTION(BlueprintCallable, Category = "Video Settings")
		static bool SaveVideoModeAndQuality();

	// Revert to original video settings
	UFUNCTION(BlueprintCallable, Category = "Video Settings")
		static bool RevertVideoMode();

};
