// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataTable.h"
#include "CoreMinimal.h"
#include "FeedbackRatioType.h"
#include "FeedbackCondition.h"
//#include "Misc/DateTime.h"
#include "FeedbackNotifications.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FFeedbackNotifications : public FTableRowBase
{
	GENERATED_BODY()
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Notifications")
	TMap<EFeedbackRatioType, FFeedbackCondition> Conditions;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Notifications")
	FDataTableRowHandle Notification;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Notifications")
		FTimespan Cooldown = FTimespan(5, 0, 0, 0);
	
	FDateTime CooldownDateEnd;
};
