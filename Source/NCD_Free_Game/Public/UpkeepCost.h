// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "UpkeepCost.generated.h"

/**
 * a
 */
USTRUCT(BlueprintType)
struct FUpkeepCost : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float MoneyCost = 0.f;

	FUpkeepCost operator+=(const FUpkeepCost& pNewUpkeepCosts) {
		FUpkeepCost tmp(*this);
		tmp.MoneyCost += MoneyCost;
		return tmp;
	}
};