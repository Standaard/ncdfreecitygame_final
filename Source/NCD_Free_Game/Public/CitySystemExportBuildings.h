// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuildingManager.h"
#include "CitySystemExportBuildings.generated.h"

/**
 * Export class
 */
USTRUCT()
struct FCitySystemExportBuildings
{
	GENERATED_BODY()
	/* Actual placed buildings */
	UPROPERTY()
		TArray<FBuildingManagerExport> BuildingManagers;

	UPROPERTY()
		bool bIsCityHallPlaced = false;
};
