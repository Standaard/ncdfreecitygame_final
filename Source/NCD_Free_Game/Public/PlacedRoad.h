// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PlacedBuilding.h"
#include "BuildableRoad.h"
#include "PlacedRoad.generated.h"

/**
 * 
 */
UCLASS()
class NCD_FREE_GAME_API APlacedRoad : public APlacedBuilding
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		UMaterialInterface* RoadStraightMaterial;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		UMaterialInterface* RoadTurnMaterial;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		UMaterialInterface* RoadTSplitMaterial;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		UMaterialInterface* RoadCrossroadMaterial;	

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		ERoadType RoadType;

	UPROPERTY()
		TArray<AActor*> IgnoreBoxTraceActors;

public:

	UFUNCTION(BlueprintCallable)
	void UpdateRoadMaterial(ERoadType pRoadType);

	UFUNCTION(BlueprintCallable)
	inline ERoadType GetRoadType() { return RoadType; }

	virtual void OnDelete () override;

protected:
	virtual void BeginPlay() override;

private:
	UFUNCTION ()
		void DestroyUtilityOnTop ();
};
