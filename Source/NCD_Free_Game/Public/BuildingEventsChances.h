// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "BuildingCategory.h"
#include "BuildingEventsChances.generated.h"

/**
 * Building event chance per category 
 */
USTRUCT(BlueprintType)
struct FBuildingEventsChances : public FTableRowBase
{
	GENERATED_BODY()
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones")
		EBuildingCategory BuildingCategory;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones")
		int ChanceForFire;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones")
		int ChanceForCrime;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones")
		int ChanceForAccident;
};
