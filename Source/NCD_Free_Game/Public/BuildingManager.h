#pragma once

#include "CoreMinimal.h"
#include "BuildingStats.h"
#include "BuildableActor.h"
#include "PlacedBuilding.h"
#include "BuildingManager.generated.h"

USTRUCT(BlueprintType)
struct FBuildingManager
{
	GENERATED_BODY()
	UPROPERTY()
		ABuildableActor* BuildableActor;
	UPROPERTY()
		TMap<FVector, APlacedBuilding*> Buildings;
	UPROPERTY()
		EBuildingType BuildingsType = EBuildingType::EB_NONE;
	UPROPERTY()
		EBuildingCategory BuildingsCategory = EBuildingCategory::EBC_NONE;

	FBuildingManager(ABuildableActor* BuildableActor) {
		this->BuildableActor = BuildableActor;
	}

	FBuildingManager() {
		BuildableActor = nullptr;
	}

	inline bool HasBuildings() {
		return Buildings.Num() > 0;
	}

	inline bool AddBuilding(APlacedBuilding* pBuilding) {
		if (!pBuilding) {
			UE_LOG(LogTemp, Warning, TEXT("[BuildingManager] Could not AddBuilding as pBuilding is nullptr."));
			return false;
		}
		if (!Buildings.Find(pBuilding->BuildingStats.Location)) {
			Buildings.Add(pBuilding->BuildingStats.Location, pBuilding);
			BuildingsType = pBuilding->BuildingStats.BuildingType;
			BuildingsCategory = pBuilding->BuildingStats.BuildingCategory;
			return true;
		}

		//UE_LOG(LogTemp, Warning, TEXT("[BuildingManager] Building %s could not be added as it was already found."), *pBuilding->BuildingStats.Name);
		return false;
	}

	inline bool RemoveBuilding(FVector pLocation) {
		bool success = Buildings.Remove(pLocation) > 0;
		return success;
	}

	inline APlacedBuilding* FindPlacedBuildingByLocation(FVector pLocation) {
		APlacedBuilding** foundBuilding = Buildings.Find(pLocation);
		return foundBuilding ? *foundBuilding : nullptr;
	}

	inline void GetAllBuildings(TArray<FBuildingStats>& pBuildingStats)
	{
		for (const auto& entry : Buildings)
		{
			if (!entry.Value) continue;
			pBuildingStats.Add(entry.Value->BuildingStats);
		}
	}

	inline void ExportAllBuildings(TMap<FVector,FBuildingStats>& pBuildingStatsExport)
	{
		for (const auto& entry : Buildings)
		{
			if (!entry.Value) continue;
			pBuildingStatsExport.Add(entry.Key,entry.Value->BuildingStats);
		}
	}

	inline bool ContainsServices() {
		return BuildingsCategory == EBuildingCategory::EBC_SERVICES;
	}

	inline bool ContainsGeneralworkPlaces() {
		return	BuildingsCategory != EBuildingCategory::EBC_ROADS || 
				BuildingsCategory != EBuildingCategory::EBC_NONE || 
				BuildingsCategory != EBuildingCategory::EBC_RESIDENCE ||
				BuildingsCategory != EBuildingCategory::EBC_SERVICES;
	}
};

USTRUCT()
struct FBuildingManagerExport 
{
	GENERATED_BODY()

	FBuildingManagerExport() {};
	FBuildingManagerExport(FBuildingManager pBuildingManager) {
		Identifier = pBuildingManager.BuildableActor->GetIdentifier();
		pBuildingManager.ExportAllBuildings(Buildings);
		BuildingsCategory = pBuildingManager.BuildingsCategory;
		BuildingsType = pBuildingManager.BuildingsType;
	}
	UPROPERTY()
		FName Identifier;
	UPROPERTY()
		EBuildingCategory BuildingsCategory;
	UPROPERTY()
		EBuildingType BuildingsType;
	UPROPERTY()
		TMap<FVector, FBuildingStats> Buildings;

	inline FBuildingStats GetFirstBuilding() {
		for (auto& Building:Buildings)
		{
			return Building.Value;
		}
		return FBuildingStats();
	}
};