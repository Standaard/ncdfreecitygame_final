// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Button.h"
#include "GameFramework/Actor.h"
#include "ScenarioExporter.generated.h"

class UDataTable;

USTRUCT()
struct FInitialValues {
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY (EditAnywhere, Category = "Initial values", meta = (ClampMin = "0", ClampMax = "1000"))
		int InitialPollution = 0;
	UPROPERTY (EditAnywhere, Category = "Initial values", meta = (ClampMin = "0", ClampMax = "100"))
		int InitialHappiness = 0; //In percentages
	UPROPERTY (EditAnywhere, Category = "Initial values", meta = (ClampMin = "0", ClampMax = "100"))
		int InitialPopulation = 0; //In percentages
	UPROPERTY (EditAnywhere, Category = "Initial values", meta = (ClampMin = "0", ClampMax = "100"))
		int InitialEmploymentRate = 0; //In percentages

	FInitialValues () {

	}

	FInitialValues (int pInitialPollution, int pInitialHappiness, int pInitialPopulation, int pInitialEmploymentRate) {
		InitialPollution = pInitialPollution;
		InitialHappiness = pInitialHappiness;
		InitialPopulation = pInitialPopulation;
		InitialEmploymentRate = pInitialEmploymentRate;
	}
};

UCLASS()
class NCD_FREE_GAME_API AScenarioExporter : public AActor {
	GENERATED_BODY()

public: 
	AScenarioExporter ();

	UFUNCTION (BlueprintCallable, Category = "Export | Export files")
		void OnClickExport ();

	UPROPERTY (EditAnywhere, Category = "Export | Folders")
		FString AssetsFolder = "";

	UPROPERTY (EditAnywhere, Category = "Export | Files | Datatables")
		UDataTable* BuildingsDT;

	UPROPERTY (EditAnywhere, Category = "Export | Files | Datatables")
		UDataTable* EventsDT;

	UPROPERTY (EditAnywhere, Category = "Export | Files | Datatables")
		UDataTable* MilestonesDT;

	UPROPERTY (EditAnywhere, Category = "Export | Files | Datatables")
		UDataTable* PoliciesDT;

	UPROPERTY (EditAnywhere, Category = "Export | Files | Datatables")
		UDataTable* VehiclesDT;

	UPROPERTY (EditAnywhere, Category = "Export | Files | Datatables")
		UDataTable* GoalsDT;

	UPROPERTY (EditAnywhere, Category = "Export | Initial values")
		FInitialValues InitialValues;

protected:
	virtual void BeginPlay () override;

private:
	UFUNCTION ()
		FString GetFileNameFromObject (UObject* pObject);
	UFUNCTION ()
		bool CheckFolders ();
	UFUNCTION ()
		void ExportJsons ();
	UFUNCTION ()
		void ExportFolder (FString pFolderName);
	UFUNCTION ()
		void ExportFile (FString pFileName);

	//UPROPERTY () -> Unrecognized type for whatever reason.
		//FPlatformFileManager* fileManager;

	UPROPERTY ()
		class UJsonExport* JsonExporter;

	UPROPERTY ()
		FString fromPathBaseString;
	UPROPERTY ()
		FString toPathBaseString;
};
