// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TrafficNode.generated.h"

UCLASS()
class NCD_FREE_GAME_API ATrafficNode : public AActor
{
	GENERATED_BODY()
private:
	int ID;
public:
	static int GlobalID;

	//A* Pathfinding parent link - not used for agents!
	UPROPERTY()
	ATrafficNode* Parent;

	//A* Pathfinding costs
	float CostEstimate;
	float CostCurrent;

public:	
	// Sets default values for this actor's properties
	ATrafficNode();

	inline bool operator==(const ATrafficNode* pOther) const
	{
		return this->ID == pOther->ID;
	}
	inline bool operator!=(const ATrafficNode* pOther) const
	{
		return this->ID != pOther->ID;
	}
public:
	UFUNCTION(BlueprintCallable, Category = "Traffic|Node")
	inline TArray<ATrafficNode*> GetNeighbours() { return Neighbours; };

	UFUNCTION(BlueprintCallable, Category = "Traffic|Node")
	inline int GetID() { return ID; };

	inline float CalculateDistance(ATrafficNode* pOther) {
		//TODO: Replace GetActorLocation with own Location
		return FVector::Distance(GetActorLocation(), pOther->GetActorLocation());
	}

public:

protected:
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "Traffic|System")
	TArray<ATrafficNode*> Neighbours;
	

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
