// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
* Type of jobs
*/
UENUM(BlueprintType)
enum class EJobType : uint8
{
	EJ_FIREFREIGHTER				UMETA(DisplayName = "Firefreighter"),
	EJ_COP							UMETA(DisplayName = "Cop"),
	EJ_TEACHER						UMETA(DisplayName = "Teacher"),
	EJ_DOCTOR						UMETA(DisplayName = "Doctor"),
	EJ_CASHIER						UMETA(DisplayName = "Cashier"),
	EJ_NONE							UMETA(DisplayName = "No job")
};
