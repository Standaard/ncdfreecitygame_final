// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * Type of pollutions
 */
UENUM(BlueprintType)
enum class EPollutionType : uint8
{
	EP_AIR				UMETA(DisplayName = "Air pollution"),
	EP_WATER			UMETA(DisplayName = "Water pollution"),
	EP_NOISE			UMETA(DisplayName = "Noise pollution"),
	EP_GROUND			UMETA(DisplayName = "Ground pollution")
};
