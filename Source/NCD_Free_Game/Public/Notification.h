// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "NotificationType.h"
#include "Notification.generated.h"

/**
 * Upgrades for buildings
 */
USTRUCT(BlueprintType)
struct FNotification : public FTableRowBase
{
	GENERATED_BODY()
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Notifications")
		FString TitleText;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Notifications")
		FString Description;
};
