// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Notification.h"
#include "NotificationHandling.generated.h"

/**
 * 
 */
UCLASS()
class NCD_FREE_GAME_API UNotificationHandling : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable)
	void AddNotification(FNotification NotificationType);
	UFUNCTION(BlueprintCallable)
	FNotification CheckForNotification();

protected:
	UPROPERTY(BlueprintReadWrite, Category = "Notifications")
	TArray<FNotification> Notifications;
};
