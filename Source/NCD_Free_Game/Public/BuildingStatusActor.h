// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ActorWidgetComponent.h"
#include <Components/SceneComponent.h>
#include "BuildingEventType.h"
#include <Camera/PlayerCameraManager.h>
#include "BuildingStatusActor.generated.h"

UCLASS()
class NCD_FREE_GAME_API ABuildingStatusActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuildingStatusActor();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void SetVisible(ESlateVisibility pVisibility = ESlateVisibility::Visible);

	void SetStatus(EBuildingEventType pStatus);
	FVector GetOffsetLocation();
	void SetLocation(FVector pLocation);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USActorWidgetComponent* ActorWidgetComp;

	USceneComponent* SceneRootComponent;
	
	UPROPERTY(BlueprintReadOnly)
		EBuildingEventType StatusBuilding;
	UPROPERTY()
		APlayerCameraManager* CameraManager;
	UPROPERTY()
		FVector OffsetLocation = FVector(0, 0, 100);
};
