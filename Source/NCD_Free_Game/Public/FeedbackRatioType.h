// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
UENUM(BlueprintType)
enum class EFeedbackRatioType : uint8
{
	EF_UNEMPLOYMENT				UMETA(DisplayName = "Unemployment Ratio"),
	EF_HOSPITALVISITOR			UMETA(DisplayName = "Hospital Coverage"),
	EF_FIREDEPARTMENTVISITOR	UMETA(DisplayName = "Fire Department Coverage"),
	EF_POLICEVISITOR			UMETA(DisplayName = "Police Coverage"),
	EF_COMMERCEVISITOR			UMETA(DisplayName = "Commerce Coverage")
};
