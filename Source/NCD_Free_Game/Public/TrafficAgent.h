// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TrafficAgentData.h"
#include "TrafficAgent.generated.h"


class URoadNode;
class UTrafficAgent;
USTRUCT(BlueprintType)
struct NCD_FREE_GAME_API FTrafficContainer
{
	GENERATED_BODY()
public:
	FTrafficContainer() {}
	FTrafficContainer(int pHISMIndex)
	{
		HISMIndex = pHISMIndex;
	}
	UPROPERTY(EditAnywhere)
		int HISMIndex;
	UPROPERTY(EditAnywhere)
		TArray<UTrafficAgent*> Agents;
};


UCLASS()
class NCD_FREE_GAME_API UTrafficAgent : public UObject
{
	GENERATED_BODY()
public:
	UTrafficAgent();

	bool Initialize();

	//returns true if turned inactive
	bool Move(float DeltaTime, const UWorld* pWorld);
	void CheckCollision(FName pCollisionProfile, const UWorld* pWorld);
	void SetBuildingLocation(FVector pLocation);
	void SetStartNode(URoadNode* pStartNode);
	void SetEndNode(URoadNode* pEndNode);
	void SetTransform(const FTransform& pTransform);
	void SetTrafficSystem(class ATrafficSystem* pSystem);
	bool GetDetectedVehicle();

public:
	UPROPERTY()
		bool bIsActive;
	//If enabled, the MaxAgentNum cap can be skipped. Useful for service vehicles
	UPROPERTY()
		bool bSkippedCap;
	//The transform used for the movement calculations
	UPROPERTY()
		FTransform Transform;
	//The transform used to draw the agent
	UPROPERTY()
		FTransform DrawTransform;

	UPROPERTY()
		FTrafficAgentData AgentData;
	UPROPERTY()
		URoadNode* StartNode;
	UPROPERTY()
		URoadNode* EndNode;
	UPROPERTY()
		URoadNode* NextNode;
	UPROPERTY()
		URoadNode* CurrentNode;
	
	UPROPERTY()
		FVector BuildingLocation;
	UPROPERTY()
		TArray<URoadNode*> Path;
	UPROPERTY()
		float PivotPoint = 0.0f;
protected:
	void GetPivotPosition(FVector& pInDirection, bool pPivotIsRight, FIntVector& pUnitDirection);

protected:

	UPROPERTY()
		bool bDetectedVehicle;

	UPROPERTY()
		bool bIsRotating;
	UPROPERTY()
		bool bIsRight;

	/*Used for the forward offset of the start point of the line trace. */
	UPROPERTY()
		float BoundingBoxExtentX = 5.0f;
	UPROPERTY()
		int PathIndex = 0;
	
	UPROPERTY()
		int RotationStartIndex = 0;
	UPROPERTY()
		int RotationDegrees = 90;
	UPROPERTY()
		int CurrentRotationIndex = 0;
	UPROPERTY()
		int RotationDegreesPerTick = 2;

	UPROPERTY()
		ATrafficSystem * TrafficSystem;
	UPROPERTY()
		FVector CurrentPivot;
};
