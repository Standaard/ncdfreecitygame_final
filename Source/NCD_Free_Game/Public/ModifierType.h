// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * Size of buildings
 */
UENUM(BlueprintType)
enum class EModifierType : uint8
{
	EMT_HAPPINESS				UMETA(DisplayName = "Happiness modifier"),
	EMT_INCOME					UMETA(DisplayName = "Tax modifier"),
	// EMT_INCOME				UMETA(DisplayName = "Income modifier"),
	EMT_INCOME_BUILDING			UMETA(DisplayName = "Income for buildings modifier"),
	EMT_DISCOUNT_BUILDING		UMETA(DisplayName = "Discounts for buildings"),
	EMT_DISCOUNT_UPGRADES		UMETA(DisplayName = "Discounts for upgrading"),
	EMT_WORK_ACCIDENTS			UMETA(DisplayName = "Work accidents modifier"),
	EMT_POLLUTIONS_INDUSTRY		UMETA(DisplayName = "Pollutions modifier Industry"),
	EMT_UPKEEP_POLICE			UMETA(DisplayName = "Police upkeep modifier")
};