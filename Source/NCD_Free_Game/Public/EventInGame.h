// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "EventInGame.generated.h"

/**
 * Policy template
 */
USTRUCT(BlueprintType)
struct FEventInGame : public FTableRowBase
{
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite)
		FName ID;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		FString Title;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		FString Description;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		bool bReOccurring = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float PenaltyHappiness;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float SuccessHappiness;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float BonusHappiness;
};