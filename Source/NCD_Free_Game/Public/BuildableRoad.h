// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuildableActor.h"
#include "PlacedBuilding.h"
#include "BuildableRoad.generated.h"

class UStaticMeshComponent;
class URoadNode;

UENUM(BlueprintType)
enum class ERoadType: uint8
{
	EB_STRAIGHT		UMETA(DisplayName = "StraightRoad"),
	EB_TURN			UMETA(DisplayName = "RoadTurn"),
	EB_TSPLIT		UMETA(DisplayName = "Tsplit"),
	EB_CROSS		UMETA(DisplayName = "CrossRoad")
};

enum class ERoadPosition : uint8
{
	EB_LEFT			UMETA(DisplayName = "LeftRoad"),
	EB_RIGHT		UMETA(DisplayName = "RightRoad"),
	EB_BOTTOM		UMETA(DisplayName = "BottomRoad"),
	EB_TOP			UMETA(DisplayName = "TopRoad"),
	EB_CENTER		UMETA(DisplayName = "CenterRoad")
};

//UENUM(BlueprintType)
//enum class ERoadType : uint8
//{
//
//};

/**
 * 
 */
UCLASS()
class NCD_FREE_GAME_API ABuildableRoad : public ABuildableActor
{
	GENERATED_BODY()

public:
	ABuildableRoad();
	~ABuildableRoad();

	URoadNode* GetNodeByLocation(FVector pLocation);


	virtual void OnPreviewPlace(FTransform pTransform, bool pSkipCollisionCheck = false, bool pIsFreeBuilding = false) override;
	virtual FVector OnBuildableActorPlace(FTransform pTransform) override;
	virtual void OnDelete(FVector pLocation) override;

protected:

	virtual void BeginPlay() override;
	virtual void CheckSpecialCollision(FVector SnappedLocation) override;

private:

	void UpdateCenterRoad(FVector pSnappedLocation);
	void UpdateRoad(FVector pSnappedLocation, bool pTop = false, bool pRight = false, bool pLeft = false, bool pBottom = false, ERoadPosition pPosition = ERoadPosition::EB_CENTER);
	void UpdateRoadMesh(ERoadType pRoadType, FVector pSnappedLocation, int pRotation, ERoadPosition pPosition = ERoadPosition::EB_CENTER);

	void SetStraightRoad(TArray<bool> Neighbours, FVector pSnappedLocation, ERoadPosition pPosition = ERoadPosition::EB_CENTER);
	void SetTurnRoad(TArray<bool> Neighbours, FVector pSnappedLocation, ERoadPosition pPosition = ERoadPosition::EB_CENTER);
	void SetTSplit(TArray<bool> Neighbours, FVector pSnappedLocation, ERoadPosition pPosition = ERoadPosition::EB_CENTER);
	void SetCrossRoad(TArray<bool> Neighbours, FVector pSnappedLocation, ERoadPosition pPosition = ERoadPosition::EB_CENTER);

	void CreateNode(FVector pLocation);
	void DeleteNode(FVector pLocation);
	void LinkNodes(URoadNode* pNode1, URoadNode* pNode2);
	URoadNode* FindRoadNodeByLocation(FVector pLocation);

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building")
		UStaticMesh* RoadStraight;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		TSubclassOf<APlacedBuilding> RoadClass = APlacedBuilding::StaticClass();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		TArray<URoadNode*> RoadNodes;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly)
		TArray<bool> HasCollision;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly)
		TArray<ERoadType> RoadTypes;

	UPROPERTY(EditInstanceOnly)
		bool bUpdateMesh = true;
private:
	TArray<FVector> checkLocations;
	TArray<FTransform> RoadTransforms;



};
