// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RoadNode.generated.h"
/**
 * 
 */
UCLASS()
class NCD_FREE_GAME_API URoadNode : public UObject
{
	GENERATED_BODY()
public:
	
	void Initialize(FVector pLocation);

public:
	inline bool operator==(const URoadNode* pOther) const
	{
		return this->ID == pOther->ID;
	}
	inline bool operator!=(const URoadNode* pOther) const
	{
		return this->ID != pOther->ID;
	}
public:
	static int GlobalID;
	//A* Pathfinding parent link - not used for agents!
	UPROPERTY()
		URoadNode* Parent;
	UPROPERTY()
		bool bEnabled = true;
	//A* Pathfinding costs
	UPROPERTY()
		float CostEstimate;
	UPROPERTY()
		float CostCurrent;

public:
	UFUNCTION()
	FVector GetLocation();
	UFUNCTION()
		void AddNeighbour(URoadNode* pNode);
	UFUNCTION()
		void RemoveNode();
	UFUNCTION()
		void RemoveFromNeighbours(URoadNode* pNode);
	UFUNCTION()
		TArray<URoadNode*> GetNeighbours();
	UFUNCTION()
		int GetID();
	UFUNCTION()
		float CalculateDistance(URoadNode* pOther);
private:
	UPROPERTY()
		TArray<URoadNode*> Neighbours;
	UPROPERTY()
		FVector Position;
	UPROPERTY()
		int ID;
};
