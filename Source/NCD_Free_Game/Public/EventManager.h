//Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include <Engine/DataTable.h>
#include "BuildingStats.h"
#include <NoExportTypes.h>
#include "Policy.h"
#include "CompletionReward.h"
#include "DateTime.h"
#include "GameEventAction.h"
#include "EventManager.generated.h"

class UEventUI;
class ACitySystemGameMode;

/**
 * 
 */
USTRUCT(BlueprintType)
struct FPoliciesDayCounter
{
	GENERATED_BODY()
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Policy")
		FString EventName;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Policy")
		FPolicy Policy;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Policy")
		int DaysActive;

		bool operator==(const FString& Other) const
		{
			return (EventName == Other);
		}
};

UCLASS()
class NCD_FREE_GAME_API AEventManager : public AActor
{
	GENERATED_BODY()

private:
	bool bCanSearch = false;				//Can look for a new event
	bool EventIsCurrentlyActive = false;

	FNCDEvent* ProposedEvent;			//Being Proposed
	FNCDEvent* CurrentEvent;			//Currently Active
	FDateTime EndDateForCurrentEvent;	//Set on accept

	TArray<FName> CompletedEvents;	//TODO: Should be saved
	TArray<FName> FailedEvents;		//TODO: Should be saved
	TArray<FName> DeclinedEvents;	//TODO: Should be saved
	TArray<FName> IgnoredEvents;	//TODO: Should be saved???

	TMap<FNCDEvent*, FDateTime> EventsOnCooldown;	//Event & CooldownDate

	//Tracking win conditions
		//Track buildings
	TMap<EBuildingType, int> BuildingsOnEventStart;

protected:
	void ProposeEvent(FNCDEvent* pEvent);
	UFUNCTION(Exec, BlueprintCallable)
	void ProposeEventByName(FName pName);
	bool CheckEventEndConditions();
	bool CheckEventSpawnConditions(FNCDEvent* pEvent);
	void HandleEndConditionActions(FNCDEvent* pEvent, EGameEventAction pEventStatus);
	void HandleEventCompletionRewardsPenalties(TMap<ECompletionReward, int> pCompletionReward);

public:
	AEventManager();
	~AEventManager();

	void InitializeEventManager(ACitySystemGameMode* pCitySystemGameMode, TSubclassOf<UEventUI> pEventUI, float pTimeInSeconds);

	void Step();

	void OnEventAccepted();
	void OnEventIgnore();
	void OnEventDeclined();
	void OnEventFailed();
	void OnEventOverTime();

	inline bool GetEventManagerActive() { return bEventManagerActive; }

private:
	void SetEndDateForCurrentEvent(FDateTime pEndDate);
	void StoreBuildingAmountsBeforeEvent(TMap<EBuildingType, int> pBuildingTypesNeeded);

	//FollowupEvent
	bool CheckForFollowUpEvent(FNCDEvent* pEvent, EGameEventAction pEventStatus);
	void StoreFollowUpEvent(FNCDEvent * pFollowupEvent, int pSecondsTillPropose);
	void PickFollowUpEvent();
	void SetQueuedEventReady();
	inline bool GetQueuedEventReady() { return bQueuedEventReady; }
	FNCDEvent* QueuedEvent;

	bool IsOverTime();

	void PickNewEvent(bool pQueuedEvent);
	TArray<FNCDEvent*> GetListOfAvailableEvents();

	void CheckIfEventsOffCooldown();
	
	void SetSearchForEventActive();

protected:
	UPROPERTY()
		ACitySystemGameMode* CitySystemGameMode;
	UPROPERTY()
		UEventUI* EventUI;
	UPROPERTY()
		bool bEventManagerActive;
	UPROPERTY(EditDefaultsOnly, Category = "Events")
		int UIEVENTZORDER;
	UPROPERTY(EditDefaultsOnly, Category = "Events")
		float NextEventSearchTimeInSecond = 0.f;
	UPROPERTY()
		FTimerHandle CanSearchForEventTimer;
	//Timer
	UPROPERTY()
		FTimerHandle QueueEventTimer;
	UPROPERTY()
	bool bQueuedEventReady;

	

public:
	UFUNCTION(BlueprintCallable)
		void SetEventManagerActive(bool pActive);
	UFUNCTION(BlueprintCallable)
		void EnableEventSearch();

	UFUNCTION(BlueprintCallable)
		void ProposeEvent(FNCDEvent pEvent);

	UFUNCTION(BlueprintCallable)
		inline bool IsEventActive() { return EventIsCurrentlyActive; }
	UFUNCTION(BlueprintCallable)
		inline FNCDEvent & GetCurrentEvent() { return *CurrentEvent; }
	UFUNCTION(BlueprintCallable)
		FDateTime GetEndDateForCurrentEvent(FDateTime pEndDate);

	UFUNCTION(BlueprintCallable)
		FORCEINLINE bool AreEqual (const FNCDEvent& pFirst, const FNCDEvent& pSecond) { return (pFirst.ID == pSecond.ID); }

	UFUNCTION(BlueprintCallable)
		TArray<FNCDEvent> GetUnlockedEventQueue();
	UFUNCTION(BlueprintCallable)
		TMap<FString, FDateTime> GetEventsOnCooldown();

	UFUNCTION(BlueprintImplementableEvent)
		void UpdateCurrentEventUI(FNCDEvent &pEvent);	// Used for Blueprint widget to set Data

};
