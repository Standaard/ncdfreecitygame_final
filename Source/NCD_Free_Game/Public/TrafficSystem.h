// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TrafficAgent.h"
#include "TrafficAgentData.h"
#include "TrafficQueueAgent.h"
#include "BuildingType.h"
#include "TrafficSystem.generated.h"

class URoadNode;
class ACitySystemGameMode;
class APlacedBuilding;
UCLASS()
class NCD_FREE_GAME_API ATrafficSystem : public AActor
{
	GENERATED_BODY()
	

public:
	static const float two_pi;
	static const float one_pi;
	static const float pi_over_two;
public:	
	// Sets default values for this actor's properties
	ATrafficSystem();

	virtual void Destroyed();
	// Called every frame
	virtual void Tick(float DeltaTime) override;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	void SetupLUT();
	void LoadAgentDataFromTable();
	void SpawnVehicleFromQueue();


	/*Gets the component by ID, if there isn't any it will create one.*/
	UHierarchicalInstancedStaticMeshComponent* GetHISMComponentByAgentData(FTrafficAgentData pAgentData);
	bool GetPath(UTrafficAgent* pAgent, TArray<URoadNode*>& pAgentPath);


public:
	void SetVehicleSpawnDelay(float pDelay);
	void SpawnAgent(URoadNode* pStartNode, URoadNode* pEndNode, FName pVehicle, FVector pEndBuildingLocation, bool pOverrideCap);
	void SpawnRandomAgentByType(URoadNode* pStartNode, URoadNode* pEndNode, EBuildingType pType = EBuildingType::EB_HOUSE, FVector pEndBuildingLocation = FVector::ZeroVector, bool pOverrideCap = false);
	void SpawnRandomAgentByType(FVector nodeLocationOne, FVector nodeLocationTwo, EBuildingType pType = EBuildingType::EB_HOUSE, FVector pEndBuildingLocation = FVector::ZeroVector, bool pOverrideCap = false);
	void SpawnRandomAgentByType(APlacedBuilding* pStartBuilding, APlacedBuilding* pEndBuilding, EBuildingType pType, bool pOverrideCap = false /*= EBuildingType::EB_HOUSE*/);
	inline ACitySystemGameMode* GetCitySystem() {
		return CitySystem;
	}
	UFUNCTION()
	UTrafficAgent* GetVehicleFromComponent(UHierarchicalInstancedStaticMeshComponent* pHISMComp, int pVehicleIndex);
	UFUNCTION()
		void SetUpdatesPerAgentPerTick(int pAmount);
public:
	/*Used for creating 360 locations in a circle*/
	UPROPERTY()
		TArray<FVector> CircleLUT;

protected:
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "Traffic|System")
	TArray<UHierarchicalInstancedStaticMeshComponent*> HISMComponents;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Traffic|Agent")
	UDataTable* VehicleDataTable;


	/*The location where the vehicles get teleported to when they turn inactive*/
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "Traffic|System")
		FVector VehicleTeleportLocation;
	/*Diagonal pivot point where both X and Y are equal and positive*/
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "Traffic|System")
		float RoadPivotPoint = 25.0f;
	/*Percentage of vehicles which get updated per tick*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Traffic|System")
		float AgentPercentageUpdatedPerTick = 0.10f;
	/*Starts from the center, goes forward/right */
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "Traffic|System")
		FName VehicleCollisionPresetName = FName("Vehicle");

	UPROPERTY(EditAnywhere, Category = "Traffic|System")
		int MaxAgentNum = 100;

	UPROPERTY(VisibleInstanceOnly, Category = "Traffic|System")
		int CurrentUpperLimit = 0;
	UPROPERTY(VisibleInstanceOnly, Category = "Traffic|System")
		int CurrentLowerLimit = 0;

	UPROPERTY(VisibleInstanceOnly, Category = "Traffic|System")
		int CurrentAgentNum = 0;

	UPROPERTY(VisibleInstanceOnly, Category = "Traffic|System")
		int AgentsUpdatedThisTick = 0;

	UPROPERTY(EditInstanceOnly, Category = "Traffic|System")
		int UpdatesPerAgentPerTick = 1;

	/*Used integers for faster lookup, keys are indices for HISMComponents*/
	UPROPERTY(EditAnywhere, Category = "Traffic|System")
		TArray<FTrafficContainer> AgentsByHISM;
private:
	UPROPERTY()
		ACitySystemGameMode * CitySystem;


	UPROPERTY()
		int CurrentAgentIndex = 0;
	UPROPERTY()
		float VehicleSpawnDelay = 1.0f;
	UPROPERTY()
		float DilatedVehicleSpawnDelay = 1.0f;
	/*Runtime AgentData Datatable*/
	UPROPERTY()
		TMap<FName, FTrafficAgentData> AgentDataMap;

	
	TQueue<FTrafficQueueAgent> AgentQueue;

	UPROPERTY()
		FTimerHandle AgentQueueHandle;

	//Pathfinding node sets
	UPROPERTY()
		TArray<URoadNode*> OpenSet;
	UPROPERTY()
		TArray<URoadNode*> ClosedSet;
	UPROPERTY()
		TArray<URoadNode*> CachedNodesSet;


};
