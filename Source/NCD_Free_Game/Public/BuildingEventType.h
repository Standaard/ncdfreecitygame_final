// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * Type of building events
 */
UENUM(BlueprintType)
enum class EBuildingEventType : uint8
{
	ET_FIRE				UMETA(DisplayName = "Fire"),
	ET_CRIME			UMETA(DisplayName = "Crime"),
	ET_ACCIDENT			UMETA(DisplayName = "Accident"),
	ET_NONE				UMETA(DisplayName = "None")			// Used when no event should spawn
};
