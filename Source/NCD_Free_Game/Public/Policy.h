// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "PollutionType.h"
#include "ModifierType.h"
#include <Engine/Texture2D.h>
#include "Policy.generated.h"

UENUM(BlueprintType)
enum class EPolicy : uint8 { None = 0, EnableTax = 1, SoundCurfew = 2, PolicePatrols = 3, SmokeDetectors = 4, TaxRelief = 5, TaxIncrease = 6, SafetyEducation = 7, CyclingPaths = 8, HealthFirst = 9, 
										    SickSupport = 10, PublicTrashCans = 11, CleanIndustries = 12, HealthyHabits = 13, HealthEducation = 14, GettingShapy = 15, MeatFreeDay = 16 };

/**
 * Policy template
 */
USTRUCT(BlueprintType)
struct FPolicy : public FTableRowBase
{
	GENERATED_BODY()
		UPROPERTY(BlueprintReadWrite)
		FName ID;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		FString Name;
	/* Description for the policy*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		FString Description;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		TMap<EModifierType, float> Modifiers;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float PolicyUpkeepCosts;
	// TODO: Make struct or something similar to store other variables like pollution modifiers and upkeep costs per type
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		int SupportCost;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		bool bUnlocked = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		bool bActive = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		bool bMilestoneReached = false;
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly)
		int DaysActive = 0;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Decoration")
		UTexture2D* PolicyImage;
	UPROPERTY (EditDefaultsOnly, BlueprintReadOnly)
		EPolicy PolicyType;
};

USTRUCT()
struct FPolicyExport {
	GENERATED_BODY()

	FPolicyExport() {}

	FPolicyExport(FPolicy pPolicy) {
		bUnlocked = pPolicy.bUnlocked;
		bActive = pPolicy.bActive;
		bMilestoneReached = pPolicy.bMilestoneReached;
	}

	UPROPERTY()
		bool bUnlocked;
	UPROPERTY()
		bool bActive;
	UPROPERTY()
		bool bMilestoneReached;
};