// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * Type of buildings
 */
UENUM(BlueprintType)
enum class EQuestGiver : uint8
{
	EQ_NOTSPECIFIED				UMETA(DisplayName = "Not specified."),
	EQ_FAMILY				UMETA(DisplayName = "Family"),
	EQ_WORKERS				UMETA(DisplayName = "Workers"),
	EQ_HEALTH				UMETA(DisplayName = "Health"),
	EQ_FIRESERVICE			UMETA(DisplayName = "FireService"),
	EQ_POLICESERVICE		UMETA(DisplayName = "PoliceService"),
	EQ_POLICY				UMETA(DisplayName = "Policy"),
	EQ_FASTFOOD				UMETA(DisplayName = "Fastfood")
};
