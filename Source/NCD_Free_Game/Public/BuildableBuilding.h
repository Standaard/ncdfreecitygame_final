// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuildableActor.h"
#include "PlacedBuilding.h"
#include "PlacedRoad.h"
#include "BuildableBuilding.generated.h"

/**
 * 
 */
UCLASS()
class NCD_FREE_GAME_API ABuildableBuilding : public ABuildableActor
{
	GENERATED_BODY()
	
protected:
	virtual void CheckSpecialCollision(FVector pSnappedLocation) override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh Instancing")
	bool bDrawConnectionLocations = false;
	
	UPROPERTY(VisibleInstanceOnly)
	FVector FrontLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<APlacedRoad> RoadClass = APlacedRoad::StaticClass();

};
