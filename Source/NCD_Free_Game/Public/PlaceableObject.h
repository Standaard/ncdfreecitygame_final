// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlaceableObject.generated.h"

class UStaticMeshComponent;

UCLASS()
class NCD_FREE_GAME_API APlaceableObject : public AActor {
	GENERATED_BODY ()

public:
	APlaceableObject ();

	UFUNCTION ()
		void SetMesh (UStaticMesh* pMesh);

protected:
	virtual void BeginPlay () override;

private:
	UPROPERTY ()
		class UStaticMeshComponent* meshComp;
	UPROPERTY ()
		class UStaticMesh* baseMesh;
};
