// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FeedbackCondition.generated.h"
/**
 * 
 */
USTRUCT(BlueprintType)
struct FFeedbackCondition
{
	GENERATED_BODY()
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Feedback Condition")
		bool bBiggerThanOne;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Feedback Condition")
		float FeedbackValue;
};