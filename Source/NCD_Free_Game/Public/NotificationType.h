// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * Type of buildings
 */
UENUM(BlueprintType)
enum class ENotificationType : uint8
{
	EN_POLICE			UMETA(DisplayName = "Police"),
	EN_FIREDEPARTMENT	UMETA(DisplayName = "FireDepartment"),
	EN_HOSPITAL			UMETA(DisplayName = "Hospital")
};
