// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuildingHotspot.generated.h"

/**
 * Stores the relative location of a spawn point in PlacedBuilding.
 */
USTRUCT(BlueprintType)
struct FBuildingHotspot
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (MakeEditWidget = true))
		TArray<FVector> Locations;
};

USTRUCT(BlueprintType)
struct FBuildingHotSpotEventParticles : public FBuildingHotspot
{
	GENERATED_BODY()

public:


};

USTRUCT(BlueprintType)
struct FBuildingHotspotRoadCollision : public FBuildingHotspot
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector CollisionBoxSize = FVector(50.f,50.f,50.f);

};

