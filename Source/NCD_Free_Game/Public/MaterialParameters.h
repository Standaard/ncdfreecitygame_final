// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MaterialParameters.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct NCD_FREE_GAME_API FMaterialParameters
{
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite)
	TMap<FName, float> FloatScalars;

	UPROPERTY(BlueprintReadWrite)
	TMap<FName, float> VectorScalars;

	FMaterialParameters() {}
};
