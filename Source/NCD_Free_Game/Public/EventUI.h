// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "EventManager.h"
#include "QuestGiver.h"
#include "EventUI.generated.h"

/**
 * 
 */
UCLASS()
class NCD_FREE_GAME_API UEventUI : public UUserWidget
{
	GENERATED_BODY()
	
	public:

		UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Window")
			void SpawnEventWindow(const FString& pTitle, const FString& pDescription, bool pAccepted, bool pIgnore, bool pDeclined, EQuestGiver pQuestGiver, FNCDEvent pEvent);
	
		UFUNCTION(BlueprintCallable, Category = "Window")
			void OnEventAnswer(bool pAccept);

		UFUNCTION(BlueprintCallable, Category = "Window")
			void OnEventIgnore();
	public:

		void SetEventManager(AEventManager* pManager);

	private:
		AEventManager* EventManager;
};
