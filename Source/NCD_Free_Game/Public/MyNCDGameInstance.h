// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "CitySaveGame.h"
#include "MyNCDGameInstance.generated.h"

/**
 * 
 */
UCLASS(config=GameUserSettings)
class NCD_FREE_GAME_API UMyNCDGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	virtual void Init() override;

	UFUNCTION()
		virtual void BeginLoadingScreen(const FString& MapName);
	UFUNCTION()
		virtual void EndLoadingScreen(UWorld* InLoadedWorld);

#pragma region Saving and loading
	UFUNCTION(BlueprintCallable, Category = "Saving and loading")
		bool LoadGameInSlot(int pSlot = 0);
	UFUNCTION(BlueprintCallable, Category = "Saving and loading")
		bool SaveGameInSlot(FCitySystemExport pData, int pSlot = 0);
	UFUNCTION(BlueprintCallable, Category = "Saving and loading")
		UCitySaveGame* GetCurrentSaveGame();
	UFUNCTION(BlueprintCallable, Category = "Saving and loading")
		void SetSaveMode(bool pMode);
	UFUNCTION(BlueprintCallable, Category = "Saving and loading")
		bool IsSaveModeOn();
	UFUNCTION(BlueprintCallable, Category = "Saving and loading")
		FString GetSaveNameWithSlot(int pSlot = 0);
	UFUNCTION(BlueprintCallable, Category = "Saving and loading")
		void ClearSaveGame();
	UFUNCTION(BlueprintCallable, Category = "Saving and loading")
		bool CheckIfSaveGameExists(int pSlot = 0);

	UFUNCTION(BlueprintCallable, Category = "Saving and loading")
		void RefreshSavegames();
#pragma endregion
	UFUNCTION(BlueprintCallable, Category = "Saving and loading")
		TArray<UCitySaveGame*> GetSaveGamesAvailable();

#pragma region ConfigRegion

	UFUNCTION(BlueprintCallable)
		void SaveToConfig();

	UPROPERTY(Config, BlueprintReadWrite)
		int32 ResolutionScale = 100;
	UPROPERTY(Config, BlueprintReadWrite)
		bool bRanAutoDetect = false;


#pragma endregion

private:
	UPROPERTY()
		bool SavingMode = false;
	UPROPERTY()
		UCitySaveGame* SaveGame;
	UPROPERTY()
		TArray<UCitySaveGame*> SaveGamesAvailable;
	UPROPERTY ()
		FString DefaultSaveName = "CitySave";
};
