// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * Type of buildings
 */
UENUM(BlueprintType)
enum class EBuildingStatus : uint8
{
	ES_NONE					UMETA(DisplayName = "None"),
	ES_ONFIRE				UMETA(DisplayName = "Fire"),
	ES_ROBBED				UMETA(DisplayName = "Crime"),
	ES_ACCIDENT				UMETA(DisplayName = "Accident")
};
