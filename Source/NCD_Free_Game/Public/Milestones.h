// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "BuildingType.h"
#include "BuildingStats.h"
#include <Engine/Texture2D.h>
#include "Milestones.generated.h"

/**
 * Upgrades for buildings
 */
USTRUCT(BlueprintType)
struct FMilestones : public FTableRowBase
{
	GENERATED_BODY()
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Info")
		FString Title;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Info")
		FString Description;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Conditions")
		bool bPopulationCondition;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Conditions")
		bool bBuildingCondition;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Conditions")
		bool bWorkingPopulationCondition;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Conditions")
		bool bEventsCondition;
	/*UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones")
		bool bMoneyCondition;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones")
		bool bHappinessCondition;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones")
		bool bPollutionCondition;*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Conditions")
		int RequiredCitizens;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Conditions")
		int AmountOfBuildingNeeded;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Conditions")
		int AmountOfWorkingPeople;
	/*UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones")
		int AmountOfMoney;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones")
		int AmountOfHappiness;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones")
		int AmountOfPollution;*/
	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones")
		//EPollutionType PollutionType;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Conditions")
		TArray<FDataTableRowHandle> EventsThatNeedToBeCompleted;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Conditions")
		TArray<EBuildingType> OfBuildingType;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Info")
		bool bUnlocked;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Rewards")
		int MoneyReward;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Rewards")
		float RawHappinessReward;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Rewards")
		TArray<FDataTableRowHandle> PoliciesToUnlock;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Rewards")
		TArray<FDataTableRowHandle> BuildingsToUnlock;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Rewards")
		TArray<FDataTableRowHandle> EventsToUnlock;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Info")
		UTexture2D* AchievementImage;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Info")
		bool bNotifyTutorialManager;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones Info")
		int TutorialStepNumber;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DEVELOP")
		bool bDeveloperUnlockMilestone;
};
