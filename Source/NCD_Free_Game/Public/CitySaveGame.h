// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "CitySystemExport.h"
#include "CitySaveGame.generated.h"

/**
 * 
 */
UCLASS()
class NCD_FREE_GAME_API UCitySaveGame : public USaveGame
{
	GENERATED_BODY()
	
public: 
	UPROPERTY(VisibleAnywhere, Category = Basic)
		FString PlayerName = "Player";

	UPROPERTY(VisibleAnywhere, Category = Basic)
		FString SaveSlotName = "CitySave";

	UPROPERTY(VisibleAnywhere, Category = Basic)
		FString CityName = "Your city";

	UPROPERTY(VisibleAnywhere, Category = Basic)
		FDateTime SaveDate;

	UPROPERTY(VisibleAnywhere, Category = Basic)
		uint32 UserIndex = 0;

	UPROPERTY()
		FCitySystemExport Data;

	UFUNCTION(BlueprintCallable, Category = "Getters")
	inline FDateTime GetDateTime() {
		return SaveDate;
	}

	UFUNCTION(BlueprintCallable, Category = "Text")
	inline FString GetSaveDatePretty() {
		return FString::FromInt(SaveDate.GetDay()).Append("-").Append(FString::FromInt(SaveDate.GetMonth()).Append("-").Append(FString::FromInt(SaveDate.GetYear())));
	}

	UFUNCTION(BlueprintCallable, Category = "Text")
	inline FString GetSaveTimePretty() {
		return FString::FromInt(SaveDate.GetHour()).Append(":").Append(FString::FromInt(SaveDate.GetMinute()));
	}

	UFUNCTION(BlueprintCallable, Category = "Text")
	inline FText GetFullDate() {
		return FText::FromString(GetSaveDatePretty() + " " + GetSaveTimePretty());
	}

	UCitySaveGame();
};
