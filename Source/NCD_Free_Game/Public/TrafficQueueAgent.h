// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RoadNode.h"

struct FTrafficQueueAgent
{
public:

	URoadNode * StartNode;
	URoadNode * EndNode;
	FName AgentName;
	FVector BuildingLocation;
	bool bOverrideMaxAgent;
	FTrafficQueueAgent(URoadNode* pStartNode, URoadNode* pEndNode, FName pAgentName, bool pOverrideCap = false, FVector pBuildingLocation = FVector::ZeroVector) {
		StartNode = pStartNode;
		EndNode = pEndNode;
		AgentName = pAgentName;
		BuildingLocation = pBuildingLocation;
		bOverrideMaxAgent = pOverrideCap;
	};
	FTrafficQueueAgent() {};
};
