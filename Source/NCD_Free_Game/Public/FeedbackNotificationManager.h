// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FeedbackNotifications.h"
#include "FeedbackCondition.h"
#include "Notification.h"
#include "FeedbackNotificationManager.generated.h"
class ACitySystemGameMode;
class UDataTable;

/**
 * 
 */
UCLASS()
class NCD_FREE_GAME_API AFeedbackNotificationManager : public AActor
{
public:
	GENERATED_BODY()

	AFeedbackNotificationManager();
	~AFeedbackNotificationManager();

public:

	void LoadConditions();
	void CheckConditions();
	void RaiseNotification(FDataTableRowHandle pHandle);


protected:
	virtual void BeginPlay() override;

	float ReturnFeedbackValue(EFeedbackRatioType pRatioType);

public:
	UFUNCTION(BlueprintCallable)
	void AddNotification(FNotification NotificationType);
	UFUNCTION(BlueprintCallable)
	FName CheckForNotification();

protected:
	UPROPERTY(BlueprintReadWrite, Category = "Notifications")
	TArray<FName> Notifications;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Feedback")
	UDataTable* NotificationDataTable;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "GameSettings|Notifications")
	class UNotificationHandling* NotificationWidget;

	TMap<FName, FFeedbackNotifications> Conditions;
private:
	ACitySystemGameMode* CitySystem;
};
