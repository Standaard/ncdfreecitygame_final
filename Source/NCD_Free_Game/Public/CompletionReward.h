// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * Type of buildings
 */
UENUM(BlueprintType)
enum class ECompletionReward : uint8
{
	ECR_NONE						UMETA(DisplayName = "None"),
	ECR_MONEY						UMETA(DisplayName = "Money"),
	ECR_HAPPINESS					UMETA(DisplayName = "Happiness"),
	ECR_PEOPLE						UMETA(DisplayName = "People"),
	ECR_POLLUTION					UMETA(DisplayName = "Pollution")
};
