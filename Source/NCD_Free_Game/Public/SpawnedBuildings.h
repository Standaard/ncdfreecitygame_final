// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuildableActor.h"
#include <Engine/DataTable.h>
#include "SpawnedBuildings.generated.h"

struct FPlacedBuildingParameters;
/**
 * 
 */
USTRUCT (BlueprintType)
struct FSpawnBuildingProps {
	GENERATED_USTRUCT_BODY ()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FTransform Transform;
	UPROPERTY (BlueprintReadWrite, EditAnywhere)
	FPlacedBuildingParameters Params;

	/*FSpawnBuildingProps () {}

	FSpawnBuildingProps (TArray<FTransform> pTransforms, FPlacedBuildingParameters pParams) {
		Transforms = pTransforms;
		Params = pParams;
	}*/
};

USTRUCT(BlueprintType)
struct NCD_FREE_GAME_API FSpawnedBuildings
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		FDataTableRowHandle Building;
	UPROPERTY (BlueprintReadWrite, EditAnywhere)
		TArray<FSpawnBuildingProps> BuildingProps;
};
