// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * Type of buildings
 */
UENUM(BlueprintType)
enum class EBuildingType : uint8
{
	EB_NONE					UMETA(DisplayName = "None"),
	EB_RESIDENCE			UMETA(DisplayName = "Residence"),
	EB_INDUSTRY				UMETA(DisplayName = "Industry"),
	EB_COMMERCIAL			UMETA(DisplayName = "Commercial"),
	EB_REWARDS				UMETA(DisplayName = "Reward"),
	EB_WINDTURBINE			UMETA(DisplayName = "Wind Turbine"),
	EB_POWERPLANT			UMETA(DisplayName = "Power Plant"),
	EB_WATER_TOWER			UMETA(DisplayName = "Water TOWER"),
	EB_ROAD					UMETA(DisplayName = "Road"),
	EB_HOUSE				UMETA(DisplayName = "House"),
	EB_CLINIC				UMETA(DisplayName = "Clinic"),
	EB_HOSPITAL				UMETA(DisplayName = "Hospital"),
	EB_POLICE				UMETA(DisplayName = "Police"),
	EB_FIRE_DEPARTMENT		UMETA(DisplayName = "Fire department"),
	EB_BASESCHOOL			UMETA(DisplayName = "Base School"),
	EB_HIGHSCHOOL			UMETA(DisplayName = "High School"),
	EB_CITY_HALL			UMETA(DisplayName = "City Hall"),
	EB_POST_OFFICE			UMETA(DisplayName = "Post Office"),
	EB_OFFICE				UMETA(DisplayName = "Office"),
	EB_NCDOFFICE			UMETA(DisplayName = "NCD Office"),
	EB_FASTFOOD				UMETA(DisplayName = "McBurger Fast Food"),
	EB_RESTAURANT			UMETA(DisplayName = "GreenEden Restaurant"),
	EB_WATERTREATMENT		UMETA(DisplayName = "Water Treatment"),
	EB_FACTORYSMALL			UMETA(DisplayName = "Factory Small"),
	EB_FACTORYMID			UMETA(DisplayName = "Factory Medium"),
	EB_FACTORYBIG			UMETA(DisplayName = "Factory Big"),
	EB_MUSICSTORE			UMETA(DisplayName = "Music Store"),
	EB_REPAIRSTORE			UMETA(DisplayName = "Repair Store"),
	EB_CLOTHESSTORE			UMETA(DisplayName = "Clothes Store"),
	EB_COFFEESTORE			UMETA(DisplayName = "Coffee Store"),
	EB_GROCERYSTORE			UMETA(DisplayName = "Grocery Store"),
	EB_SUPERMARKETSMALL		UMETA(DisplayName = "Supermarket Small"),
	EB_SUPERMARKETBIG		UMETA(DisplayName = "Supermarket Big"),
	EB_MARKETPLACE			UMETA(DisplayName = "Marketplace"),
	EB_PARK					UMETA(DisplayName = "Park"),
	EB_CEMETERY				UMETA(DisplayName = "Cemetery"),
	EB_LANDFILL				UMETA(DisplayName = "Landfill"),		//Landfill
	EB_RECYCLECENTER		UMETA(DisplayName = "Recycle Center"),
	EB_GYMSMALL				UMETA(DisplayName = "Gym Smal"),
	EB_GYMBIG				UMETA(DisplayName = "Gym Big"),
	//EB_GYMRESTAURANT		UMETA(DisplayName = "Gym Restaurant"),
	EB_FOOD					UMETA(DisplayName = "Other Food Store"),
	EB_PLAYGROUND			UMETA(DisplayName = "PLAYGROUND"),
	EB_TRASHBIN			UMETA(DisplayName = "Trash bin")
};
