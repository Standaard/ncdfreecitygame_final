// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DecorationInstances.generated.h"

class ABuildableActor;

UCLASS()
class NCD_FREE_GAME_API ADecorationInstances : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADecorationInstances();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	void UpdateDecoration(ABuildableActor* pBuildableActor, int pDecorationIndex);
	void SetDecoration(UStaticMesh* pStaticMesh, UMaterialInterface* pMaterial, FTransform pRelativeTransform);
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	class UHierarchicalInstancedStaticMeshComponent* Decorations;
	class USceneComponent* SceneRootComponent;

private:

	ABuildableActor* Parent;

	FTransform relativeTransformToParent;
	UMaterialInterface* Material;
	UStaticMesh* StaticMesh;
};
