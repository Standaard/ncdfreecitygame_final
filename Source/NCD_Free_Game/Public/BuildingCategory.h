// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
* These are the Building Categories used in the BuildingData Struct.
*/
UENUM(BlueprintType)
enum class EBuildingCategory : uint8
{
	EBC_NONE			UMETA(DisplayName = "None"),
	EBC_RESIDENCE		UMETA(DisplayName = "Residence"),
	EBC_INDUSTRY		UMETA(DisplayName = "Industry"),
	EBC_SERVICES		UMETA(DisplayName = "Services"),
	EBC_COMMERCE		UMETA(DisplayName = "Commerce"),
	EBC_SPORT			UMETA(DisplayName = "Sport"),
	EBC_ROADS			UMETA(DisplayName = "Roads"),
	EBC_UTILITIES		UMETA(DisplayName = "Utilities")
};