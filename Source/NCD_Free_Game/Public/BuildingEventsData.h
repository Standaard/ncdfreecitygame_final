// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuildingStats.h"
#include "BuildingStatusActor.h"
#include <Particles/ParticleSystemComponent.h>
#include "BuildingEventType.h"
#include "BuildingEventsData.generated.h"

/**
 * information about building event
 */
USTRUCT(BlueprintType)
struct FBuildingEventsData
{
	GENERATED_BODY()
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building Event")
		FVector BuildingLocation;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building Event")
		bool bVehicleDispatched = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building Event")
		FBuildingStats BuildingStats;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building Event")
		EBuildingEventType EventType;
	UPROPERTY(BlueprintReadOnly, Category = "Building Event")
		float TimePassed = 0.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building Event")
		float BuildingEventMaxTime = 180.0f;			// In seconds
	//UPROPERTY(BlueprintReadOnly, Category = "Building Event")
	//	ABuildingStatusActor* BuildingStatusActor;
	UPROPERTY(BlueprintReadOnly, Category = "Building Event")
		UParticleSystemComponent * ParticleComponent;
};
