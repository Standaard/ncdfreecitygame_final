// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "HappinessManager.generated.h"

#define MIN_HAPPINESS 0.0f
#define MAX_HAPPINESS 10000.0f
#define INTERNAL_HAPPINESS_MULTIPLIER 100.0f

class ACitySystemGameMode;

/**
 * 
 */
UCLASS()
class NCD_FREE_GAME_API UHappinessManager : public UObject
{
	GENERATED_BODY()

public:
	UHappinessManager ();
	~UHappinessManager ();

	UFUNCTION ()
		void Init (ACitySystemGameMode* pGameMode);
	UFUNCTION ()
		void DayPassed ();
	UFUNCTION ()
		void WeekPassed ();

	UFUNCTION (BlueprintCallable)
		void AddHappiness (float pAmountPercentage);
	UFUNCTION (BlueprintCallable)
		int GetHappinessPercentage ();

	UPROPERTY (BlueprintReadOnly, Category = "Happiness manager")
		float unemploymentWeightVal = 4.0f;
	UPROPERTY (BlueprintReadOnly, Category = "Happiness manager")
		float pollutionWeightVal = 1.5f;
	UPROPERTY (BlueprintReadOnly, Category = "Happiness manager")
		float healthcareWeightVal = 2.5f;
	UPROPERTY (BlueprintReadOnly, Category = "Happiness manager")
		float educationWeightVal = 2.5f;
	UPROPERTY (BlueprintReadOnly, Category = "Happiness manager")
		float policeWeightVal = 2.0f;
	UPROPERTY (BlueprintReadOnly, Category = "Happiness manager")
		float fireDepartmentWeightVal = 1.5f;
	UPROPERTY (BlueprintReadOnly, Category = "Happiness manager")
		float foodWeightVal = 3.0f;
protected:

private:
	UFUNCTION ()
		void InitializeWeights ();
	UFUNCTION (BlueprintCallable, Category = "Happiness manager")
		float GetJobHappiness ();
	UFUNCTION (BlueprintCallable, Category = "Happiness manager")
		float GetServiceHappiness ();
	UFUNCTION (BlueprintCallable, Category = "Happiness manager")
		float GetPollutionHappiness ();
	UFUNCTION (BlueprintCallable, Category = "Happiness manager")
		float GetFoodAndGoodsHappiness ();
	UFUNCTION ()
		float CalculateHappiness ();

	FString UnemploymentWeight = "Unemployment", PollutionWeight = "Pollution", HealthcareWeight = "HealthCare", EducationWeight = "Education", PoliceWeight = "Police", FireDepartmentWeight = "FireDepartment", FoodWeight = "Food";
	ACitySystemGameMode* gameMode = nullptr;
	TMap<FString, float> factorWeights;

	float happinessIncreaseMultiplier = 0.02f;// , pollutionScaleMultiplier = 0.1f;
	int maxPercentagePopulationWithoutHealthcare = 10, maxPercentagePopulationWithoutPolice = 4, maxPercentagePopulationWithoutFireDep = 8, maxPercentagePopulationWithoutEducation = 2, maxPercentagePopulationWithoutFood = 2, maxPollutionWithoutIssues = 100.0f;
	int happiness = 5000, weekHappinessAddition = 0; //Happiness goes from 0 to 1000 and we start halfway
	bool unlockedClinic = false, unlockedHospital = false, unlockedPoliceStation = false, unlockedFireDepartment = false, unlockedJuniorHigh = false, unlockedSchool = false, unlockedPollution = false, unlockedGroceryStore = false;
};
