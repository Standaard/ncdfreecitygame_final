// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildingAreaManager.generated.h"
class UBuildingArea;
struct FBuildingStats;
class ACitySystemGameMode;

UCLASS()
class NCD_FREE_GAME_API ABuildingAreaManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuildingAreaManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	UBuildingArea* CreateBuildingArea(APlacedBuilding* pOwner);

	UFUNCTION(BlueprintCallable)
		bool GetBuildingsByBoxTrace(FVector pLocation, FVector pExtent, TArray<APlacedBuilding*>& pOutBuildings, bool pIgnoreOwner = true);
protected:

private:
	UPROPERTY()
		TArray<AActor*> IgnoreBoxTraceActors;
	UPROPERTY()
		ACitySystemGameMode* CitySystem;

};
