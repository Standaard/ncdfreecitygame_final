// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildingHighlightActor.generated.h"

UCLASS()
class NCD_FREE_GAME_API ABuildingHighlightActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuildingHighlightActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable)
	inline UStaticMeshComponent* GetMeshComponent() {
		return StaticMeshComp;
	};
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USceneComponent * SceneComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent * StaticMeshComp;
	
	
};
