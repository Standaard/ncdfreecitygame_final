// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VisibilityBinding.h"
#include "Components/SlateWrapperTypes.h"
#include "BuildingDescriptionActor.generated.h"

class ABuildableActor;
class USActorWidgetComponent;
class ANCD_PlayerController;
UCLASS()
class NCD_FREE_GAME_API ABuildingDescriptionActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuildingDescriptionActor();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetVisible(ESlateVisibility pVisibility = ESlateVisibility::Visible);

	void SetBuildableActor(ABuildableActor* pActor);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USActorWidgetComponent* ActorWidgetComp;
	USceneComponent* SceneRootComponent;
	UPROPERTY(BlueprintReadOnly)
	ABuildableActor* BuildableActor;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	ANCD_PlayerController* PlayerController;
	
};
