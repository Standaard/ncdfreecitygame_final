﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "JobType.h"
#include "BuildingStats.h"
#include "PopulationManager.generated.h"

#define 😄 true
#define 😣 false;

USTRUCT()
struct FPopulationExport
{
	GENERATED_BODY()

		FPopulationExport() {};
	FPopulationExport(TMap<FVector, int> pPopulationResidence, TMap<FVector, int> pPopulationJobs, TMap<FVector, int> pPopulationResidenceWork,
							int pPopulationMonth, int pLeavingPeople, float pResidenceWorkPercentage, int pMaxPopulationCapacity, int pWorkCapacities) {
		PopulationResidence = pPopulationResidence;
		PopulationJobs = pPopulationJobs;
		PopulationResidenceWork = pPopulationResidenceWork;

		PopulationMonth = pPopulationMonth;
	};

	UPROPERTY()
		TMap<FVector, int> PopulationResidence;
	UPROPERTY()
		TMap<FVector, int> PopulationJobs;
	UPROPERTY()
		TMap<FVector, int> PopulationResidenceWork;

	UPROPERTY()
		int PopulationMonth = 0;
};

/**
 * 
 */
UCLASS()
class NCD_FREE_GAME_API UPopulationManager : public UObject
{
	GENERATED_BODY()

private:
	/* Population that can work per household*/
	UPROPERTY()
		float ResidenceWorkPercentage = 30.f;

	/* Population variables */
	UPROPERTY()
		int TotalPopulation = 0;
	UPROPERTY()
		int PopulationBeginningMonth = 0;
	UPROPERTY ()
		int EmployedPeople = 0;
	UPROPERTY ()
		int TotalEmployeeSpace = 0;
	UPROPERTY ()
		int TotalResidentSpace = 0;
	UPROPERTY ()
		int MaxUnemploymentRatePercentage = 10;

	UPROPERTY()
		TMap<FVector, int> WorkplacePopulationByLocation;
	UPROPERTY()
		TMap<FVector, int> ResidencePopulationByLocation;
	UPROPERTY ()
		TArray<FVector> ResidencesWithAvailableSpace;
	UPROPERTY ()
		TArray<FVector> ResidencesWithoutAvailableSpace;
	UPROPERTY()
		TMap<FVector, int> EmployeesByLocation;
	UPROPERTY ()
		TMap<FVector, int> ResidenceLocationsWithAvailablePop;
	class ACitySystemGameMode* GameMode;

public:
	UPopulationManager();
	~UPopulationManager();
	/* Adding/Removing */
	UFUNCTION(BlueprintCallable, Category = "City System|Population")
		void AddPopulation(int pPopToAdd);
	UFUNCTION(BlueprintCallable, Category = "City System|Population")
		void RemovePopulation(int pPopToRemove);
	UFUNCTION(BlueprintCallable, Category = "City System|Population")
		void AddEmployeesAtLocation(FVector pResidencePos, FVector pWorkPos, int pEmployeesToAdd);
	UFUNCTION(BlueprintCallable, Category = "City System|Population")
		void RemovePopulationAtLocation(FVector pResidencePos, int pBuildingCapacity, int pPopToRemove = 0, bool pDemolished = false);
	UFUNCTION(BlueprintCallable, Category = "City System|Population")
		void RemoveEmployeesAtLocation(FVector pWorkplacePos, EBuildingCategory pCategory, int pEmployeesToRemove = 0, bool pDemolished = false);
	//UFUNCTION(BlueprintCallable, Category = "City System|Population")
		//void FireRandomEmployee();
	UFUNCTION (BluePrintCallable, Category = "City System|Population")
		void AddAvailableResidence (FVector pResidencePos, int pPopCapacity);
	UFUNCTION (BluePrintCallable, Category = "City System|Population")
		void RemoveAvailableResidence (FVector pResidencePos);
	UFUNCTION (BluePrintCallable, Category = "City System|Population")
		void AddWorkplace (FVector pWorkplacePos);
	UFUNCTION()
		void AddAvailableResidenceValue (TArray<FVector> pResidencePositions, int pValue);
	UFUNCTION ()
		void AddResidenceWithAvailableSpace (FVector pResidencePos, bool pRemoveFromOtherList);
	UFUNCTION ()
		void RemoveResidenceWithAvailableSpace (FVector pResidencePos, bool pDemolished = false);
	UFUNCTION ()
		void AddResidenceWithoutAvailableSpace (FVector pResidencePos, bool pRemoveFromOtherList);
	UFUNCTION ()
		void RemoveResidenceWithoutAvailableSpace (FVector pResidencePos, bool pDemolished = false);
	UFUNCTION ()
		void RemoveBuildingData (FVector pResidencePos, EBuildingCategory pCategory, int pPopulationCount);
	UFUNCTION (BlueprintCallable, Category = "City System|Population")
		int AssignPopToWorkPlace (FBuildingStats pBuilding, FBuildingStats pResidence, int pPopToAdd);
	UFUNCTION (BlueprintCallable, Category = "City System|Population")
		void AddPopToLocation (FBuildingStats pBuilding, FBuildingStats pResidence, int pPopToAdd);
	UFUNCTION ()
		void RemoveEmployeesFromRandomWorkplace (int pEmployeesToRemove);

	/* Getters */
	UFUNCTION ()
		TMap<FVector, int> GetAvailableResidenceLocations ();
	UFUNCTION(BlueprintCallable, Category = "City System|Population|Getters")
		int GetPopulationMonth ();
	UFUNCTION()
		int GetAvailableResidenceSpacesForLocation (FVector pLocation);
	UFUNCTION()
		int GetAvailableJobSpacesForLocation(FVector pLocation);
	UFUNCTION(BlueprintCallable, Category = "City System|Population|Getters")
		int GetEmployeeCountByLocation(FVector pLocation);
	UFUNCTION(BlueprintCallable, Category = "City System|Population|Getters")
		int GetResidenceCountByLocation(FVector pLocation);
	UFUNCTION (BlueprintCallable, Category = "City System|Population|Getters")
		int GetPopulationCount ();
	UFUNCTION (BlueprintCallable, Category = "City System|Population|Getters")
		int GetEmployedPeopleCount ();
	UFUNCTION (BlueprintCallable, Category = "City System|Population|Getters")
		int GetUnemployedPeopleCount ();
	UFUNCTION (BlueprintCallable, Category = "City System|Population|Getters")
		int GetTotalResidentSpace ();
	UFUNCTION (BlueprintCallable, Category = "City System|Population|Getters")
		int GetTotalEmployeeSpace ();
	UFUNCTION (BlueprintCallable, Category = "City System|Population|Getters")
		int GetUnemploymentRatePercentage ();
	UFUNCTION (BlueprintCallable, Category = "City System|Population|Getters")
		int GetMaxUnemploymentRate ();

	UFUNCTION(BlueprintCallable, Category = "City System|Population|Getters")
		bool IsPopulationZero();
	UFUNCTION (BlueprintCallable, Category = "City System|Population|Getters")
		bool IsOperational (FBuildingStats pBuilding);
	UFUNCTION ()
		bool UnemploymentRateTooHigh ();

	UFUNCTION()
		void Import(FPopulationExport pExport);

	UFUNCTION ()
		void SetGameMode (ACitySystemGameMode* pGameMode);

	UFUNCTION()
		FPopulationExport Export();

	/* Setters */
		void SetPopulationCountBeginningOfMonth();
		void SetPopulationCountBeginningOfMonth(int pPopulation);
};


