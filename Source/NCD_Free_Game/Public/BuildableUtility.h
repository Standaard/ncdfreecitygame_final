// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuildableActor.h"
#include "BuildableUtility.generated.h"

struct FCollisionShape;
/**
 * 
 */
UCLASS()
class NCD_FREE_GAME_API ABuildableUtility : public ABuildableActor {
	GENERATED_BODY()
public:

protected:
	virtual void CheckSpecialCollision (FVector pSnappedLocation) override;
	virtual void CheckPlacementCollisionActor (FVector pCheckLocation, FCollisionShape pBoxShape) override;
	virtual FVector OnBuildableActorPlace(FTransform pTransform) override;

	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Mesh Instancing")
		bool bDrawDebugDirection = false;

	UPROPERTY(VisibleInstanceOnly)
		FVector HitResultImpact;
private:

};
