// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * Type of buildings
 */
UENUM(BlueprintType)
enum class EGameEventAction : uint8
{
	EGE_COMPLETED				UMETA(DisplayName = "Completed"),
	EGE_FAILED					UMETA(DisplayName = "Failed"),
	EGE_DECLINED				UMETA(DisplayName = "Declined"),
	EGE_IGNORED					UMETA(DisplayName = "Ignored")
};
