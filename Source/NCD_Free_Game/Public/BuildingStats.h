// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "BuildingCategory.h"
#include "BuildingSize.h"
#include "PollutionType.h"
#include "BuildingType.h"
#include "UpkeepCost.h"
#include "MapUtils.h"
#include "BuildingStatus.h"
#include "JobType.h"
#include "Grid.h"
#include "QuestGiver.h"
#include "CompletionReward.h"
#include <Engine/Texture2D.h>
#include "GameEventAction.h"
#include "BuildingStats.generated.h"


#pragma region Building Stats

USTRUCT(BlueprintType)
struct FDecorations
{
	GENERATED_BODY()
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Building statistics")
		class UStaticMesh* Mesh;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Building statistics")
		class UMaterialInterface* Material;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Building statistics")
		FTransform RelativeToParent = FTransform::Identity;

	FDecorations() {}
};

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EBuildableActorType : uint8
{
	BA_FreePlace 			UMETA(DisplayName = "Free Place"),
	BA_ConnectToRoad		UMETA(DisplayName = "Connect To Road"),
	BA_Road					UMETA(DisplayName = "Road"),
	BA_Utilities				UMETA(DisplayName = "Utilities")
};


USTRUCT(BlueprintType)
struct FBuildingStats : public FTableRowBase
{
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite, Category = "Building statistics")
		FName ID;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Building statistics")
		FDataTableRowHandle BuildingBlueprint;
	UPROPERTY(BlueprintReadOnly, Category = "Building statistics")
		FVector Location;
	UPROPERTY()
		FRotator Rotation;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Building statistics")
		EBuildingCategory BuildingCategory;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Building statistics")
		EBuildingType BuildingType;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Building statistics")
		EBuildableActorType BuildableActorType = EBuildableActorType::BA_ConnectToRoad;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building statistics")
		FString Name;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building statistics")
		FString Description;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building statistics")			//Leaving it in visible/editable for now to Test building events. They'll load on buildings after loading a save file
		EBuildingStatus BuildingStatus;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = " Building statistics")
		int JobCapacity;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building statistics")
		int VisitorCapacity;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building statistics")
		float Cost;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building statistics")
		int Happiness;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building statistics")
		int Health;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building statistics")
		float Income;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building statistics")
		FUpkeepCost UpkeepCost;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building statistics")
		EBuildingSize Size;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Building statistics")
		class UStaticMesh* StaticMesh;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Building statistics")
		TArray<class UMaterialInterface*> MeshMaterials;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Building statistics")
		TArray<FDecorations> Decorations;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Building statistics")
		bool bCastShadow;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Building statistics")
		bool bIsAchievementBuilding = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Building statistics")
		bool bMilestoneReached = false;
	UPROPERTY()
		bool bIsInteractable = true;
	UPROPERTY()
		bool bIsRemovable = true;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Building statistics")
		UTexture2D* UIBuildingsMenuImage;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Building statistics")
		UTexture2D* PreviewImageAboveMenu;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Building statistics")
		bool bGeneratesFood = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Building statistics")
		bool bIsHealthy = false;
	UPROPERTY()
		FVector ConnectedRoadLocation;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Building statistics")
		TArray<FDataTableRowHandle> LinkedMilestone;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building statistics")
		FVector ParticleOffsetForBuilding;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building statistics")
		bool bBigParticle = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building statistics")
		bool bCanHaveBuildingEvent = true;

	//Pollution

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pollution")
		FVector PollutionExtent = Grid::GetGridVector() * 1.5f;
	/*The amount of pollution added to the global pollution, assuming bCanPollute is true*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pollution")
		int Pollution;
	/*The amount of pollution added to other buildings with bCanBePolluted enabled*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pollution")
		int PollutionInArea;
	/*Can the building be polluted by other buildings*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Pollution")
		bool bCanBePolluted = true;
	/*Does the building add to the global pollution value*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Pollution")
		bool bCanPollute = false;
	/*Does the building pollute other buildings in an area */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Pollution")
		bool bCanPolluteInArea = false;
	/*Clamped base pollution + PollutionInArea from surrounding buildings*/
	UPROPERTY(BlueprintReadOnly, Category = "Pollution")
		int TotalPollution;
	/*Unclamped TotalPollution*/
	UPROPERTY()
		int UnclampedCurrentPollution;
	

	FBuildingStats() {}
};
#pragma endregion


#pragma region Game Events
//GAME EVENTS 

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EEventPriority : uint8
{
	VE_Main 			UMETA(DisplayName = "Main"),
	VE_Optional			UMETA(DisplayName = "Optional")
};

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EEventSpawnConditionEnum : uint8
{
	ESC_BuildingAmount 			UMETA(DisplayName = "Amount of building type needed"),
	ESC_PolicyDaysActive		UMETA(DisplayName = "Policy days active"),
	ESC_HappinessOverValue		UMETA(DisplayName = "Happiness value over threshold"),
	ESC_HappinessUnderValue		UMETA(DisplayName = "Happiness value under threshold")
};

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EConditionEnum : uint8
{
	VE_BuildingAmount 			UMETA(DisplayName = "Amount of building type needed"),
	VE_WorkingPeople			UMETA(DisplayName = "Amount of working people"),
	VE_WorkingPeoplePercentage	UMETA(DisplayName = "Percentage of Total populationthat need to work"),
	VE_People					UMETA(DisplayName = "Amount of people in your city"),
	VE_Pollution				UMETA(DisplayName = "Amount of pollution"),
	VE_PolicyDaysActive			UMETA(DisplayName = "Policy days active")
};

USTRUCT(BlueprintType)
struct FNCDEvent : public FTableRowBase
{
	GENERATED_BODY()
		UPROPERTY(BlueprintReadWrite, Category = "Event Info")
			FName ID;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Event Info")
			FString Title;
		//TRIGGER: Not a functionality inside the Event. The unlocked boolean will be set from milestones or other events themselves.	
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Event Info")
			EQuestGiver QuestGiver;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Event Info")
			FString Description;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Event Info")
			bool YesOptionText = true;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Event Info")
			bool NoOptionText = true;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Event Info")
			bool IgnoreOptionText = false;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Event Info")
			int MaxTimesYouCanIgnore = 99;
		UPROPERTY(BlueprintReadOnly, Category = "Event Info")
			int TimesIgnored = 0;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Event Info")
			int IgnoreCooldownInMonths = 6;
		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Event Info")
			FString NoteForDevelopers;
		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Event Messages")
			FString WinMessage;
		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Event Messages")
			FString FailMessage;
		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Event Rewards")
			TArray<FDataTableRowHandle> PoliciesToUnlockWhenSucceeded;
		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Event Rewards")
			TArray<FDataTableRowHandle> BuildingsToUnlockWhenSucceeded;
		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Event Rewards")
			TArray<FDataTableRowHandle> EventsToUnlockWhenSucceeded;
		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Event Rewards")
			TArray<FDataTableRowHandle> PoliciesToUnlockWhenFailed;
		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Event Rewards")
			TArray<FDataTableRowHandle> BuildingsToUnlockWhenFailed;
		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Event Rewards")
			TArray<FDataTableRowHandle> EventsToUnlockWhenFailed;
		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Event Completion")
			TMap<ECompletionReward, int> RewardsForCompleting;
		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Event Completion")
			TMap<ECompletionReward, int> PenaltiesForFailing;
		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Event Completion")
			TMap<ECompletionReward, int> PenaltiesForDisagreeing;
		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Event Info")
			int CompletionTimeLimitInMonths;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Event Info")
			bool bIgnoreCompletionTimer = false;	//Event has no max time for completion
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Event Info")
			bool bUnlockedEvent = false;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Event Info")
			bool bCompletedEvent = false;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Event Info")
			bool bRepeatableEvent = false;
		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Event Info")
			int MinimumRespawnTimeInMonths = 24;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Event Followup")
			TMap<EGameEventAction, FDataTableRowHandle> FollowUpEvent;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Event Followup")
			float TimeTillFollowUpEventInSeconds = 0.f;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Event Completion Conditions")
			TMap<EConditionEnum, int> WinCondition;
		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Event Completion Conditions")
			TMap<EBuildingType, int32> BuildingTypeNeeded;
		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Event Completion Conditions")
			TArray<FDataTableRowHandle> PolicyNameToBeActiveForDays;
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Event Spawn Conditions")
			TMap<EEventSpawnConditionEnum, int> SpawnCondition;
		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Event Spawn Conditions")
			TMap<EBuildingType, int> BuildingTypeNeededForSpawn;
		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Event Spawn Conditions")
			TArray<FDataTableRowHandle> PolicyNameToBeActiveForDaysForSpawn;


		//UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Event")
		//	EEventPriority Priority = EEventPriority::VE_Optional;
		
	FNCDEvent() {}
};

USTRUCT()
struct FNCDEventExport {			//Export unlocked and completed events. Other events will have their base values loaded and will be unlocked through other milestones/events.
	GENERATED_BODY()

	FNCDEventExport() {}

	FNCDEventExport(FNCDEvent pGameEvent) {
		bUnlocked = pGameEvent.bUnlockedEvent;
		//bActive = pGameEvent.;
		bCompleted = pGameEvent.bCompletedEvent;
	}

	UPROPERTY()
		bool bUnlocked;
	//UPROPERTY()
	//	bool bActive;
	UPROPERTY()
		bool bCompleted;
};

#pragma endregion