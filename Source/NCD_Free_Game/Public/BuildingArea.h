// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "BuildingStats.h"
#include "BuildingArea.generated.h"

class APlacedBuilding;
/**
 * 
 */
UCLASS()
class NCD_FREE_GAME_API UBuildingArea : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY()
		APlacedBuilding* Owner;
	UPROPERTY()
		TArray<APlacedBuilding*> Buildings;
	UPROPERTY()
		TArray<UBuildingArea*> AreasThatAffectOwner;

public:

	void SetOwner(APlacedBuilding* pOwner);
	bool AddBuilding(APlacedBuilding* pBuilding);
	bool AddAffectingArea(UBuildingArea* pArea);
	bool RemoveAffectingArea(UBuildingArea* pArea);
	bool RemoveBuilding(APlacedBuilding* pBuilding);
	bool RemoveBuildingByLocation(FVector pLocation);
	void RemoveAllBuildings();
};
