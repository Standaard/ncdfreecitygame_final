// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <Engine/PostProcessVolume.h>
#include <Materials/MaterialParameterCollection.h>
#include "PollutionManager.generated.h"

struct FBuildingStats;
class UBuildingArea;
class ACitySystemGameMode;
class UMaterialParameterCollection;
class UMaterialParameterCollectionInstance;
class APlacedBuilding;

UCLASS()
class NCD_FREE_GAME_API APollutionManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APollutionManager();

#pragma region Getters
	UFUNCTION(Exec, Category = "Pollution")
		void GetPollutionInConsole();

	/* Gets the maximum pollution amount. */
	UFUNCTION(BlueprintCallable, Category = "Pollution")
		inline int GetPollutionMax() { return PollutionMaxValue; }


	/*Returns clamped pollution between 0 and PollutionMaxValue. */
	UFUNCTION(BlueprintCallable, Category = "Pollution")
		inline int GetPollution() { return FMath::Clamp(Pollution,0,PollutionMaxValue); }

	UFUNCTION(BlueprintCallable, Category = "Pollution")
		inline int GetPollutionUnclamped() { return Pollution; }

	UFUNCTION(BlueprintCallable, Category = "Pollution")
		inline int GetPollutionAreaVisualMax() { return PollutionAreaVisualMax; }

	UFUNCTION(BlueprintCallable, Category = "Pollution")
		inline int GetPollutionAreaVisualMin() { return PollutionAreaVisualMin; }
#pragma endregion

#pragma region Pollution 
	bool CreatePollutionArea(APlacedBuilding* pOwner, bool pLoadGame = false);
	UBuildingArea* GetPollutionAreaByLocation(FVector pLocation);
	UBuildingArea* GetPollutionAreaByOwner(APlacedBuilding* pPlacedBuilding);
	
	UFUNCTION(Exec, BlueprintCallable, Category = "Pollution")
	void AddGlobalPollution(float pValue);
	
	void AddAreaPollutionToBuilding(APlacedBuilding* pOwner, int pValueToAdd);
	bool RemovePollutionAreaByPlacedBuilding(APlacedBuilding* pBuilding);
#pragma endregion

#pragma region Pollution Visualization
	UFUNCTION(Exec, BlueprintCallable, Category = "Pollution")
		void SetPollutionFogScalar(float pScalar);

	UFUNCTION(Exec, BlueprintCallable, Category = "Pollution")
		void SetPollutionView(bool pEnabled);

	UFUNCTION(BlueprintCallable, Category = "Pollution")
		inline bool GetPollutionViewEnabled() { return bPollutionViewEnabled; };
#pragma endregion

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	bool UpdatePollutionMeshByPlacedBuilding(APlacedBuilding* pOwner);

protected:

	UPROPERTY()
	ACitySystemGameMode* CitySystem;

	/*The global value for pollution*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Pollution")
		int Pollution;
	/*The soft-cap of pollution, use this value as the max value for calculations*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pollution")
		int PollutionMaxValue = 1000.0f;
	/*The minimum pollution value on a building area to indicate the pollution, used in PollutionAreaDecal material curve*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pollution")
		int PollutionAreaVisualMin = -10.0f;
	/*The maximum pollution value on a building area to indicate the pollution, used in PollutionAreaDecal material curve*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pollution")
		int PollutionAreaVisualMax = 10.0f;
	/*The minimum pollution value on a building to indicate the pollution, used in PollutionAreaDecal material curve*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pollution")
		int PollutionBuildingVisualMin = -10.0f;
	/*The maximum pollution value on a building to indicate the pollution,, used in PollutionAreaDecal material curve*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pollution")
		int PollutionBuildingVisualMax = 10.0f;

	UPROPERTY()
		TArray<UBuildingArea*> PollutionAreas;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pollution")
		FVector MaxPollutionAreaExtent = FVector(900, 900, 100);

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UMaterialParameterCollection* PollutionMPC;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		FName PollutionFogScalarName = FName("Fog");

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		FName PollutionEnabledScalarName = FName("PollutionEnabled");

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		bool bPollutionViewEnabled = false;

private:
	UPROPERTY()
		UMaterialParameterCollectionInstance* PollutionMPCInstance;
};
