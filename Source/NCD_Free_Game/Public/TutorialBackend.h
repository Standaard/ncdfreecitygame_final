// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "TutorialBackend.generated.h"

class UTutorial;


//Put any optional steps behind the necessary ones and DO NOT include them in TOTAL_STEP_COUNT
//////////////1234 5678
//8 bits used 0000 0000. These values are backwards from the enum (so 8 = Step1 and 1 = Step8)
UENUM (BlueprintType)									
enum class ETutorialSteps : uint8 { SetCityName = 0, ClickCityHall = 1, AcceptTaxEvent = 2, TaxesPolicyEnabled = 3, AcceptPart1CompletionReward = 4, OpenedBuildMenu = 5, OpenedCommercialTab = 6, HoveredGroceryStore = 7, ClickedGroceryStoreButton = 8, BuiltGroceryStore = 9, ClickedGroceryStore = 10, AssignedPeopleGroceryStore = 11,  AcceptedPart2CompletionReward = 12,  AcceptClinicEvent = 13, OpenedBuildMenu2 = 14, OpenedServicesTab = 15, HoveredClinic = 16, ClickedClinicButton = 17, BuiltClinic = 18, ClickedClinic = 19, AcceptMorePopEvent = 20, OpenedBuildMenu3 = 21, OpenedResidenceTab = 22, HoveredHouse = 23, ClickedHouseButton = 24, BuiltHouse = 25, ClickedClinic2 = 26, AssignedPeopleClinic = 27, AcceptedPart3CompletionReward = 28, AcceptTrashEvent = 29, OpenedBuildMenu4 = 30, OpenedRoadsTab = 31, HoveredTrashBin = 32, ClickedTrashBinButton = 33, BuiltThreeTrashBins = 34, AcceptedPart4CompletionReward = 35, ReachedFirstMilestone = 36, AcceptedExpansionEvent = 37, HoveredAnyMilestone = 38, AcceptedPart5CompletionReward = 39, TOTAL_STEP_COUNT = 40 };

UCLASS()
class NCD_FREE_GAME_API UTutorialBackend : public UObject {
	GENERATED_BODY ()

public:
	UTutorialBackend();
	~UTutorialBackend();

	UFUNCTION ()
		void Initialize ();
	UFUNCTION ()
		void SetStepCompleted (ETutorialSteps pStepCompleted, bool pTutorialSkilled = false);
	UFUNCTION ()
		void SetAllStepsCompleted ();
	UFUNCTION ()
		int TotalStepCount ();
	UFUNCTION ()
		bool HasCompleted (ETutorialSteps pTutorialStep);
	UFUNCTION ()
		bool HasCompletedUpTo (ETutorialSteps pTutorialStep);
	UFUNCTION ()
		bool FinishedTutorial ();

	UFUNCTION ()
		void TestTickDebug ();

protected:

private:
	UPROPERTY()
	TArray<int> _bitShiftOriginal;
	TArray<int>* _bitshifts;

	UPROPERTY ()
	int _stepsCompleted = 0;
	UPROPERTY ()
	int _tutorialStepCount = 0;
	UPROPERTY ()
	int _tick = 0;
	float _testhenk = 0.0f;
	bool _coen = false;
};