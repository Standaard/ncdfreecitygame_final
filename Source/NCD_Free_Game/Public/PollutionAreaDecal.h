// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/DecalComponent.h"
#include "BuildingStats.h"
#include "PollutionAreaDecal.generated.h"

/**
 * 
 */
UCLASS()
class NCD_FREE_GAME_API UPollutionAreaDecal : public UDecalComponent
{
	GENERATED_BODY()
	
public:

	UPollutionAreaDecal(const FObjectInitializer& ObjectInitializer);

	/*Set the Decal size based on pBuildingStats pollution extent*/
	UFUNCTION(BlueprintCallable)
		void SetPollutionSize(FBuildingStats& pBuildingStats);
	/*Set the color in the material based on the scale between pMinPollution and pMaxPollution*/
	UFUNCTION(BlueprintCallable)
		void SetPollutionColorFromStats(FBuildingStats& pBuildingStats, float pMinPollution, float pMaxPollution);
	/*Set the color in the material based on the scale between pMinPollution and pMaxPollution*/
	UFUNCTION(BlueprintCallable)
		void SetPollutionColor(int pPollution, float pMinPollution, float pMaxPollution);

protected:

	/*Used for scaling the decal.*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh Instancing")
		FVector NormalizedPollutionDecalSize = FVector(0.05f, 0.5f, 0.5f);

	/*The name of the variable in the master material, used for changing the colour of the decal.*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh Instancing")
		FName PollutionColorScalar = FName("ColorLerpAlpha");

	UPROPERTY(VisibleInstanceOnly)
		UMaterialInstanceDynamic* PollutionDynamicMaterialInstance;
};
