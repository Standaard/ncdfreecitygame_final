// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BuildingEventType.h"
#include "BuildingCategory.h"
#include "BuildingType.h"
#include "BuildingStats.h"
#include "BuildingEventsData.h"
#include "BuildingManager.h"
#include "BuildingEventsChances.h"
//#include "BuildingStatusActor.h"
#include <Particles/ParticleSystemComponent.h>
#include "BuildingEvents.generated.h"

class ACitySystemGameMode;
class UParticleSystem;

/**
 * 
 */
UCLASS()
class NCD_FREE_GAME_API ABuildingEvents : public AActor
{
	GENERATED_BODY()

public:
	ABuildingEvents();
	~ABuildingEvents();
	
	inline TMap<FVector, FBuildingEventsData>& GetFireEvents() { return BuildingsWithFireEvents; }
	inline TMap<FVector, FBuildingEventsData>& GetCrimeEvents() { return BuildingsWithCrimeEvents; }
	inline TMap<FVector, FBuildingEventsData>& GetAccidentEvents() { return BuildingsWithAccidentEvents; }

	void RemoveBuildingFromEventList(FVector pLocation);			// Remove building from event lists
	void LoadEventsForBuilding(EBuildingStatus pBuildingEventType, FVector pLocation, FBuildingStats pBuildingStats);
	void PushEventsAfterLoadingSave();

	void CheckUndispatchedVehicles();

public:
	UFUNCTION(BlueprintCallable)
		void SpawnBuildingEvent();					// Initial Call from CitySystem to this BuildingEventsManager

	UFUNCTION(BlueprintImplementableEvent, Category = "Event Spawning")
		void PushFireEvent(int pBuildingCategory);
	UFUNCTION(BlueprintImplementableEvent, Category = "Event Spawning")
		void PushCrimeEvent(int pBuildingCategory);
	UFUNCTION(BlueprintImplementableEvent, Category = "Event Spawning")
		void PushAccidentEvent(int pBuildingCategory);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void LoadBuildingEventChances();
	void CheckIfServicesAvailable();
	
	void SetTimerForNextEvent();
	void SpawnEvent(EBuildingCategory pBuildingCategory);

	UFUNCTION(BlueprintCallable, Category = "Event Spawning")
	bool PushEventToBuilding(EBuildingEventType pEventType, FVector pLocation, FBuildingStats pBuilding);

	EBuildingEventType CalculateTypeOfEvent(EBuildingCategory pBuildingCategory);				// Chance Based Event
	void CheckEventConditions();																// Check events that can be spawned
	bool CheckEventCapacity(EBuildingEventType pEventType);

	int GetLengthOfEventsInList(EBuildingEventType pEventType);

	//void CheckEventEndTime();
	bool CheckTimePassedOnBuilding(FBuildingEventsData BuildingData, EBuildingEventType EventType);
	void UpdateEventTimePassed();

	UParticleSystemComponent * SpawnParticleForBuilding(EBuildingEventType pBuildingEventType, FVector pLocation, FBuildingStats* pBuildingStats);

protected:
	UPROPERTY()
		ACitySystemGameMode * CitySystem;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BuildingEvent Chances")
		UDataTable* BuildingEventChancesDataTable;
	UPROPERTY()
		TMap<EBuildingCategory, FBuildingEventsChances> BuildingEventChanceList;
	UPROPERTY()
		FTimerHandle CheckForEachServiceBuildings;
	UPROPERTY()
		float CheckForServiceBuildingsDelay = 10.0f;				// Make a recalling timer function to make random timing
	
	UPROPERTY()
		FTimerHandle SpawnBuildingEventTimer;
	UPROPERTY()
		float EventTimeTillSpawn = 60.0f;							// Initial time for first event call
	UPROPERTY()
		float MINIMUMTIMEBETWEENEVENTSPAWN = 20.0f;
	UPROPERTY()
		float MAXIMUMTIMEBETWEENEVENTSPAWN = 50.0f;

	UPROPERTY()
		FTimerHandle UpdateEventTimeTimer;
	UPROPERTY()
		float UpdateEventTimeDelay = 2.0f;

	UPROPERTY()
		bool bBuildingEventsUnlocked;
	UPROPERTY()
		bool bFireEventsUnlocked;
	UPROPERTY()
		bool bAccidentEventsUnlocked;
	UPROPERTY()
		bool bCrimeEventsUnlocked;

	UPROPERTY()
		float UNBALANCETHRESHOLD = 0.9f;
	UPROPERTY()
		float MAXEVENTSMULTIPLIER = 2.0f;
	UPROPERTY()
		int EXTRAEVENTADDMODIFIER = 2;			// added to the max cap events formula
	UPROPERTY()
		int SMALLSPAWNCHANCE = 5;				// Range 0 till # in percentages of 100
	UPROPERTY()
		int CHANCEIFBALANCEDCHANCE = 15;		// Range 0 till # in percentages of 100
	UPROPERTY()
		bool bFireNotBalanced;					// These will be true if the ratio of fire stations to city is not balanced
	UPROPERTY()
		bool bCrimeNotBalanced;					// These will be true if the ratio of police stations to city is not balanced
	UPROPERTY()
		bool bHealthCareNotBalanced;			// These will be true if the ratio of health care to city is not balanced

	UPROPERTY()
		float LastTimeInSeconds;

	UPROPERTY(EditDefaultsOnly, Category = "FX")
		UParticleSystem * BuildingFireParticle;
	UPROPERTY(EditDefaultsOnly, Category = "FX")
		UParticleSystem * BigBuildingFireParticle;
	UPROPERTY(EditDefaultsOnly, Category = "FX")
		UParticleSystem * BuildingCrimeParticle;
	UPROPERTY(EditDefaultsOnly, Category = "FX")
		UParticleSystem * BigBuildingCrimeParticle;
	UPROPERTY(EditDefaultsOnly, Category = "FX")
		UParticleSystem * BuildingAccidentParticle;
	UPROPERTY(EditDefaultsOnly, Category = "FX")
		UParticleSystem * BigBuildingAccidentParticle;

	UPROPERTY()
		TMap<FVector, FBuildingEventsData> BuildingsWithFireEvents;
	UPROPERTY()
		TMap<FVector, FBuildingEventsData> BuildingsWithCrimeEvents;
	UPROPERTY()
		TMap<FVector, FBuildingEventsData> BuildingsWithAccidentEvents;

	UPROPERTY()
		TArray<FBuildingEventsData> LoadedBuildingEvents;

	/*
	UPROPERTY()
		TMap<ABuildingStatusActor*, bool> BuildingStatusActorPool;
	UPROPERTY(VisibleAnywhere)
		ABuildingStatusActor* BuildingStatusActor;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABuildingStatusActor> BuildingStatusActorClass;
	*/
protected:

	UFUNCTION(BlueprintImplementableEvent, Category = "Building Events")
		bool CheckIfServiceBuildingIsUnlocked( int pType);		// 0 = Any  1 = Fire  2 = Crime  3 = Accident

	//UFUNCTION()
	//	void SetBuildingStatusActiveStatus(FVector pLocation, bool pActive = true);
	//UFUNCTION()
	//	ABuildingStatusActor* CreateOrFindInactiveStatusWidget(FVector pLocation);
};
