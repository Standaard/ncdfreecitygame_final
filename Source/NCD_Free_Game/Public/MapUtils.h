// #pragma once
// #include "CoreMinimal.h"
#include "PollutionType.h"
#include "JobType.h"

/**
 * General functions to be used anywhere
 */
class MapUtils {
	public:
		inline static TMap<EJobType, int> MergeJobTypes(TMap<EJobType,int> pTMap1, TMap<EJobType, int> pTMap2, bool pRemoving = false) {
			for (auto JobType : pTMap2) {
				int* CurrentJobMax = pTMap1.Find(JobType.Key);
				pTMap1.Add(JobType.Key, (CurrentJobMax ? *CurrentJobMax : 0) + (JobType.Value * (pRemoving ? -1 : 1)));
			}
			return pTMap1;
		}
	private:
		MapUtils() {}
};