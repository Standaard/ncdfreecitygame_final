// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * Size of buildings
 */
UENUM(BlueprintType)
enum class EBuildingSize : uint8
{
	EBS_SMALL			UMETA(DisplayName = "Small building"),
	EBS_MEDIUM			UMETA(DisplayName = "Medium building"),
	EBS_LARGE			UMETA(DisplayName = "Large building")
};
