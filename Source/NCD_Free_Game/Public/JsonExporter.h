// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "public/ScenarioExporter.h"
#include "JsonExporter.generated.h"

class AScenarioExporter;
class AStaticMeshActor;
class ABuildableActor;
struct FInitialValues;

/**
 *
 */
UCLASS()
class NCD_FREE_GAME_API UJsonExport : public UObject {
	GENERATED_BODY()

public:
	UJsonExport ();
	~UJsonExport ();

	UFUNCTION ()
		void Initialize (AScenarioExporter* pScenarioExporter, FString pAssetPath);
	UFUNCTION ()
		void ExportBaseJSON (FString pBuildingsDTName, FString pEventsDTName, FString pMilestoneDTName, FString pPoliciesDTName, FString pVehiclesDTName, FString pGoalDTName, FInitialValues pInitialValues);
	UFUNCTION ()
		void ExportSceneJSON ();

	//Scenario data
	UPROPERTY (EditAnywhere, Category = "Scenario data")
		FString ScenarioName = "TestScenario";
	UPROPERTY (EditAnywhere, Category = "Scenario data")
		FString MapModelName = "Map.fbx";

	//Paths
	UPROPERTY (EditAnywhere, Category = "Paths")
		FString DatatableFolder = "";
	UPROPERTY (EditAnywhere, Category = "Paths")
		FString ModelFolder = "";
	UPROPERTY (EditAnywhere, Category = "Paths")
		FString TextureFolder = "";

	/*UPROPERTY (EditAnywhere, Category = "Paths")
		FString BuildingsDatatableName = "DT_Buildings";
	UPROPERTY (EditAnywhere, Category = "Paths")
		FString EventsDatatableName = "DT_Events";
	UPROPERTY (EditAnywhere, Category = "Paths")
		FString MilestonesDatatableName = "DT_Milestones";
	UPROPERTY (EditAnywhere, Category = "Paths")
		FString PoliciesDatatableName = "DT_Policies";
	UPROPERTY (EditAnywhere, Category = "Paths")
		FString VehiclesDatatableName = "DT_Vehicles";

	//Initial values
	UPROPERTY (EditAnywhere, Category = "Initial values", meta = (ClampMin = "0", ClampMax = "1000"))
		int InitialPollution = 0;
	UPROPERTY (EditAnywhere, Category = "Initial values", meta = (ClampMin = "0", ClampMax = "100"))
		int InitialHappiness = 0; //In percentages
	UPROPERTY (EditAnywhere, Category = "Initial values", meta = (ClampMin = "0", ClampMax = "100"))
		int InitialPopulation = 0; //In percentages
	UPROPERTY (EditAnywhere, Category = "Initial values", meta = (ClampMin = "0", ClampMax = "100"))
		int InitialEmploymentRate = 0; //In percentages*/

		//Goals
protected:

private:
	UFUNCTION ()
		FString GetBuildingStatsRowName (FString pModelName);

	UPROPERTY ()
		AScenarioExporter* scenarioExporter;

	UPROPERTY ()
		FString assetPath;

	TMap<FString, TArray<AStaticMeshActor*>> differingActors;
	TMap<FString, TArray<ABuildableActor*>> differingBActors;
};
