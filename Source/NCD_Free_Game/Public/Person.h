// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "JobType.h"
#include "Person.generated.h"

/**
 * Policy template
 */
USTRUCT(BlueprintType)
struct FPerson : public FTableRowBase
{
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite)
		FName ID;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		EJobType JobType;
	// UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	// 	FString Name;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		bool bEducated = false;
};