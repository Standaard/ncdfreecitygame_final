// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BuildingType.h"
#include "PollutionType.h"
#include "Policy.h"
#include "GameTimeState.h"
#include "CitySystemExport.h"
#include "FeedbackNotificationManager.h"
#include "BuildingManager.h"
#include "MilestoneManager.h"
#include "BuildingEvents.h"
#include "BuildingStatus.h"
#include "PopulationManager.h"
#include "Tutorial.h"
#include "EventManager.h"
#include "PollutionManager.h"
#include "BuildableActor.h"
#include "CitySystemGameMode.generated.h"

class ATrafficSystem;
class URoadNode;
class AEventHandler;
class UEventUI;
class AEventManager;
class ABuildableRoad;
class FDelegateHandle;
class APlacedBuilding;
//class FTimerDelegate;

DECLARE_MULTICAST_DELEGATE (FHandleTimeActions);

UENUM (BlueprintType)
enum class EUpdateFunctions : uint8 { UpdatePopulation = 0, UpdateHappiness = 1, UpdatePolicies = 2, UpdateInfoForPlayer = 3, UpdateBuildingHighlight = 4, UpdateNotificationManager = 5, UpdateMilestoneManager = 6, UpdateEventManager = 7, OnUpdateTutorial = 8, UpdateTime = 9, UpdateIdleTimerTutorial = 10 };
UENUM(BlueprintType)
enum class EDelegates : uint8 { Update = 0, TimeActions = 1 };

#pragma region DelegateHandleWrapper
USTRUCT()
struct FDelegateHandleWrap {
	GENERATED_USTRUCT_BODY ()

public:
	int32 ID;
	FDelegateHandle Handle;

	FDelegateHandleWrap () {}

	FDelegateHandleWrap (FDelegateHandle pDelegateHandle) {
		ID = FMath::RandRange (0, 2000000000);
		Handle = pDelegateHandle;
	}

	friend bool operator== (const FDelegateHandleWrap& pFirst, const FDelegateHandleWrap& pSecond) {
		return (pFirst.ID == pSecond.ID);
	}
};
#pragma endregion
#pragma region DelegateTimeHandleWrapper
USTRUCT ()
struct FDelegateTimeHandleWrap {
	GENERATED_USTRUCT_BODY ()

public:
	int32 ID;
	FHandleTimeActions* Handle;

	FDelegateTimeHandleWrap () {}

	FDelegateTimeHandleWrap (FHandleTimeActions* pDelegateHandle) {
		ID = FMath::RandRange (0, 2000000000);
		Handle = pDelegateHandle;
	}

	friend bool operator== (const FDelegateTimeHandleWrap& pFirst, const FDelegateTimeHandleWrap& pSecond) {
		return (pFirst.ID == pSecond.ID);
	}
};
#pragma endregion

/**
 * A city system game mode, this will contain most variables in the game and keep it running
 */
UCLASS(Config=Game)
class NCD_FREE_GAME_API ACitySystemGameMode : public AGameModeBase
{
	GENERATED_BODY ()
private:

#pragma region Private constant variables
	const float MINUTES_IN_HOUR = 60.f;
	const float HOURS_IN_DAY = 24.f;
	const float NIHIL_PERCENTAGE = 0.f;
	const float ONE_HUNDRED_PERCENT = 100.f;
#pragma endregion

	bool bIsLoadingSavegame = false;

public:
	UFUNCTION ()
		void LoadDatatables ();

	/* Gets the current game speed*/
	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions|Getters")
		inline int GetGameSpeed() { return CurrentGameSpeedMultiplier; }

	//inline TMap<FPolicy, int> GetPoliciesAndDays() { return PoliciesDaysActive; }
	inline bool IsPollutionTooHigh() { return GetTotalPollutionAmount() > PollutionPenaltyThreshold; }
	UFUNCTION(BlueprintCallable, Category = "City System|Public function|Getters")
	inline FDateTime GetCurrentDateTime() { return StartDate; }
	inline TMap<EBuildingType, int> GetPlacedBuildingsCounters() { return PlacedBuildingsCounters; }

	UFUNCTION (BlueprintCallable, Category = "City System|Public function|Getters")
		FBuildingStats GetBuildingByLocationBP (FVector pLocation);
	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions|Getters")
		FBuildingStats GetBuildingOnLocation(ABuildableActor* pBuildableActor, FVector pLocation);

	FBuildingStats* GetBuildingByLocation (FVector pLocation);

	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions|Getters")
		APlacedBuilding* GetPlacedBuildingByLocation(FVector pLocation);

	FPolicy* GetPolicyByName(FName pPolicyName);
	
	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Getters")
		bool IsAllowedToBuildBuilding(FBuildingStats pBuilding, bool pIsFreeBuilding = false);

	UFUNCTION (BlueprintCallable, Category = "Saving and loading")
		void SetCityName (FString pName);
	UFUNCTION (BlueprintCallable, Category = "Saving and loading")
		FString GetCityname ();
	UFUNCTION ()
		void AddBuildingIncome (FVector pLocation, float Income);
	UFUNCTION ()
		void RemoveBuildingIncome (FVector pLocation);
	UFUNCTION ()
		void AddBuildingExpense (EBuildingType pBuildingType, float pUpkeep, float pModifier);
	/* Adds or removes a building based on arguments */
	UFUNCTION ()
		void HandleBuildingValues(FBuildingStats& pBuilding, bool pRemoving, FPlacedBuildingParameters& pParameters);
	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Starter Content")
		void PlaceStarterStaticContent ();

	UFUNCTION (BlueprintCallable, Category = "City System|Public functions|Player Controller")
		ANCD_PlayerController* GetPlayerController ();
	UFUNCTION (BlueprintNativeEvent, Category = "Buildings | Initialization")
		void OnBuildingsDTInitialized ();

#pragma region Deleget
	UFUNCTION (BluePrintCallable, Category = "City System|Public functions|Update")
		void BindAllUpdates ();
	UFUNCTION (BlueprintCallable, Category = "City System|Public functions|Update")
		void BindUpdateFunction (EDelegates pInDelegate, UObject *pInUserObject, EUpdateFunctions pFunction);
	UFUNCTION (BlueprintCallable, Category = "City System|Public functions|Update")
		bool RemoveUpdateFunctionBinding (EUpdateFunctions pFunction);
	UFUNCTION ()
		void CreateDelegateBindingMap ();
	UPROPERTY ()
		TMap<EUpdateFunctions, FName> UpdateFunctionNames;
	UPROPERTY ()
		TMap<EUpdateFunctions, FDelegateHandleWrap> UpdateDelegates;
	UPROPERTY ()
		TMap<EDelegates, FDelegateTimeHandleWrap> Delegates;
#pragma endregion

	UPROPERTY()
		class ABuildingAreaManager* BuildingAreaManager;
	UPROPERTY(BlueprintReadWrite, Category = "City System|In-game variables|Pollution")
		APollutionManager* PollutionManager;

#pragma region Export functions
	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Exports")
		FCitySystemExport TotalExport();
	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Exports")
		void SaveGameInSlot(int pSlot = 0);

	FCitySystemExportBuildings BuildingsExport();
	FCitySystemExportResources ResourcesExport();
#pragma endregion

	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions|Getters")
		UPopulationManager* GetPopulationManager();
	UFUNCTION (BlueprintCallable, Category = "City System|Public functions|Getters")
		AEventManager* GetEventManager ();
	UFUNCTION (BlueprintCallable, Category = "City System|Public functions|Getters")
		AMilestoneManager* GetMilestoneManager ();

#pragma region Building functions (Buying/Selling/Upgrading)
	/* Buying buildings */
	UFUNCTION() 
		bool BuyBuilding(ABuildableActor* pBuildableActor, APlacedBuilding* pBuilding, FPlacedBuildingParameters pParams);
	/* Register building without buying */
	UFUNCTION()
		int RegisterBuilding(ABuildableActor* pBuildableActor, APlacedBuilding* pBuilding);
	/* Selling buildings by FName index that is defined in each building stats struct */
	UFUNCTION()
		bool SellBuilding(ABuildableActor* pBuildableActor, FVector pLocation, bool pMoneyBack = false);

	UFUNCTION (BlueprintCallable, Category = "City System|Protected functions|Getters")
		float GetBuildingExpensesByType (EBuildingType pBuildingType);

	UFUNCTION()
		FBuildingStats GetCurrentBuildingStats(FName pBuildingName);
#pragma endregion

#pragma region Public Tutorial
	UFUNCTION (BlueprintCallable)
		UTutorial* GetTutorial ();
	UPROPERTY (EditDefaultsOnly, BlueprintReadOnly, Category = "Tutorial")
		TSubclassOf<UTutorial> BPTutorialClass;
#pragma endregion

#pragma region Public Happiness
	UFUNCTION (BlueprintCallable)
		UHappinessManager* GetHappinessManager ();
#pragma endregion

#pragma region Scenario goals
	UFUNCTION (BlueprintCallable)
		UGoalManager* GetGoalManager ();
#pragma endregion

#pragma region Public building functions

	UFUNCTION(BlueprintCallable)
	FBuildingManager& GetBuildingManagerByID(FName pBuildingIdentifier);

	inline FBuildingManager & GetBuildingManagerByIndex(int index) { return BuildingManagers[index] ; }
	int FindIndexBuildingMananagerByBuildableActor(ABuildableActor* pBuildableActor);
	int FindIndexBuildingMananagerByBuildableActorID(FName pBuildableActorIdentifier);

	UFUNCTION(BlueprintCallable)
	static APlacedBuilding* GetBuildingBlueprintFromStats(FBuildingStats& pBuildingStats);

#pragma endregion

#pragma region Funds checking
	/* Checks whether the player has enough funds to buy the policy/building */
	bool HasSufficientFunds(FBuildingStats pBuilding);

	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Checkers")
		bool HasSufficientFundsMoney(float pMoney);
#pragma endregion

#pragma region Getters for General variables
	/* Gets the current money amount */
	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Getters")
		inline float GetMoneyAmount() { return Money; }

	/* Gets the maximum pollution amount */
	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Getters")
		inline int GetPollutionMax() { return PollutionManager->GetPollutionMax(); }

	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Getters")
		inline int GetPollutionAreaVisualMax() { return PollutionManager->GetPollutionAreaVisualMax(); }

	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Getters")
		inline int GetPollutionAreaVisualMin() { return PollutionManager->GetPollutionAreaVisualMin(); }
	/* Gets the building count of a type that is specified in the parameter */
	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Getters")
		int GetBuildingCountOfType(EBuildingType pBuildingType);

	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Getters")
		TMap<FVector, FBuildingStats> GetBuildingsByType(EBuildingType pBuildingType);
	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Getters")
		TMap<FVector, APlacedBuilding*> GetPlacedBuildingsByType(EBuildingType pBuildingType);
	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Getters")
		TMap<FVector, FBuildingStats> GetBuildingsByCategory(EBuildingCategory pBuildingCategory);
	UFUNCTION()
		FBuildingStats GetNearestBuildingByTypes(TArray<EBuildingType> pTypes, FVector pCheckLocation);
	UFUNCTION()
		FBuildingStats GetNearestBuildingByType(EBuildingType pType, FVector pCheckLocation);

	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Getters")
		bool IsCityHallPlaced();
	UFUNCTION ()
		void SetCityHallPlaced (bool pPlaced);

#pragma endregion

#pragma region Get Ratios
	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Getters")
		float HasEnoughBuildingsOfType(EBuildingType pBuildingType);

	UFUNCTION (BlueprintCallable, Category = "City System|Protected functions|Getters")
		int HasEnoughBuildingsOfTypeByInt (EBuildingType pBuildingType);

	UFUNCTION (BlueprintCallable, Category = "City System|Protected functions|Getters")
		int GetVisitorCapacityByType (EBuildingType pBuildingType);

	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Getters")
		float HasEnoughJobsForPeople();

	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Getters")
		float HasEnoughPeopleForJobs();
#pragma endregion

#pragma region Get Policies
	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Getters")
		inline TMap<FName, FPolicy> & GetPolicyList() { return Policies; }
	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Getters")
		bool GetPolicyUnlockedByMilestone( FName pId );
	UFUNCTION (BlueprintCallable, Category = "City System|Public functions|Getters")
	void UnlockNewPolicies(TArray<FDataTableRowHandle> pPoliciesToUnlock);
#pragma endregion

#pragma region Get Buildings
	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Getters")
		inline TMap<FName, FBuildingStats> & GetBuildingsList() { return Buildings; }
	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Getters")
		bool GetBuildingUnlockedByMilestone(FName pId);
	UFUNCTION (BlueprintCallable, Category = "City System|Public functions|Getters")
	void UnlockNewBuildings(TArray<FDataTableRowHandle> pBuildingsToUnlock);
#pragma endregion
	
#pragma region Milestone Functions
	UFUNCTION(Exec, BlueprintCallable, Category = "City System|Public functions")
		void RewardMoney(int pAmount);
#pragma endregion

#pragma region Building Events
	UFUNCTION(BlueprintCallable, Category = "City System|Public functions")
		inline ABuildingEvents * GetBuildingEventManager(){ return BuildingEventsManager; }
#pragma endregion

#pragma region Game Events
	UFUNCTION(BlueprintCallable, Category = "City System|Public functions")
		inline TMap<FName, FNCDEvent> & GetGameEventsList() { return GameEventsList; }
	UFUNCTION(BlueprintCallable, Category = "City System|Public functions|Getters")
		bool GetUnlockedGameEvents(FName pId);
	UFUNCTION (BlueprintCallable, Category = "City System|Public functions|Getters")
	void UnlockNewGameEvents(TArray<FDataTableRowHandle> pGameEventToUnlock);
	FNCDEvent * FindGameEventByID(FName pID);
#pragma endregion

#pragma region Public Traffic
	ATrafficSystem* GetTrafficSystem();
	UFUNCTION(Exec)
		void SpawnRandomTrafficToExit(int32 pAmount);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Traffic")
	FVector StartRoadLocation;

	UPROPERTY()
	ABuildableRoad* BuildableRoadActor;
#pragma endregion

	UPROPERTY (BlueprintReadWrite, Category = "City System|In-game variables|Population|Name")
		bool IsNewGame = true;

private:
	
	UPROPERTY()
		TArray<FPoliciesDayCounter> PoliciesDayCounters;
	UPROPERTY()
		AEventManager* EventManager;

	UPROPERTY()
		bool bIsCityHallPlaced = false;
	UPROPERTY ()
		FString CityName = "Your city";

	/*	
		If population threshold is reached, happiness should also be calculated, 
		to prevent the player from removing everything and have the same happiness ,this will stop it
	*/
	UPROPERTY()
		bool IsHappinessThresholdReached = false;

#pragma region Timer variables
	/* Timer */
	UPROPERTY()
		FTimerHandle TimeHandler;
	UPROPERTY()
		FTimerHandle TimelapseDay;

	/* Amount of times game speed should multiply by */
	UPROPERTY()
		int CurrentGameSpeedMultiplier = 1;
	UPROPERTY()
		int DayCounter = 0;

	UPROPERTY()
		int CurrentDay = 0;
	UPROPERTY()
		int CurrentMonth = 0;
	UPROPERTY()
		int CurrentYear = 0;
#pragma endregion 

#pragma region Building variables
	/* Buildings upkeep costs in money */
	UPROPERTY()
		TMap<EBuildingType, float> BuildingsExpenses;
	/* Buildings income */
	UPROPERTY()
		TMap<FVector,float> BuildingsIncomeAtLocation;
	UPROPERTY()
		TMap<FVector, float> MinimumJobsReachedWorkplace;
	/* Actual placed buildings */
	UPROPERTY()
		TArray<FBuildingManager> BuildingManagers;
	/* Placed building counter, this helps with performance */
	UPROPERTY()
		TMap<EBuildingType, int> PlacedBuildingsCounters;
	UPROPERTY()
		TMap<EBuildingType, int> PlacedBuildingsCapacityCount;

	UPROPERTY()
		int UnhealthyFoodChainCount = 0;
	UPROPERTY()
		int HealthyFoodChainCount = 0;
	UPROPERTY()
		float BuildingsPollutionHappiness = 0.f;
	UPROPERTY()
		int MaxWorkCapacity;
#pragma endregion

#pragma region Policy variables
	/* Modifiers for policies */
	UPROPERTY()
		TMap<EModifierType, float> Modifiers;
	UPROPERTY()
		TMap<FName, float> PolicyUpkeepCosts;
#pragma endregion
		
protected:
	ACitySystemGameMode();
	~ACitySystemGameMode();

#pragma region Console commands

	UFUNCTION(Exec)
		void PushFire(int pBuildingCategory);
	UFUNCTION(Exec)
		void PushCrime(int pBuildingCategory);
	UFUNCTION(Exec)
		void PushAccident(int pBuildingCategory);
	UFUNCTION (Exec)
		void RemovePop (int pPopToRemove);
	UFUNCTION (Exec)
		void TutorialPartCompleted (ETutorialSteps pTutorialStep);
	UFUNCTION (Exec)
		void TutorialHasCompleted (ETutorialSteps pTutorialStep);
	UFUNCTION (Exec)
		void TutorialHasCompletedUpTo (ETutorialSteps pTutorialStep);
	UFUNCTION (Exec)
		void AddPopulation (int pAmount);
	UFUNCTION (Exec)
		void RemovePopulation (int pAmount);

	virtual bool ProcessConsoleExec(const TCHAR* Cmd, FOutputDevice& Ar, UObject* Executor) override;


#pragma endregion


#pragma region Protected Update loop

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "City System|Protected functions|Setters")
		void SetLightAndHorizon(FGameTimeState pGameTimeState);

	/* Handles every timer tick */
	UFUNCTION()
		void HandleTimelapseDay();
	UFUNCTION()
		void HandleTimeActions();
	UFUNCTION()
		bool IsDayPassed();
	UFUNCTION()
		bool IsWeekPassed();
	UFUNCTION()
		bool IsMonthPassed();

	UPROPERTY ()
		TArray<int> boundFunctions;

#pragma region Update loops
	FHandleTimeActions Update;
	FHandleTimeActions TimeActions;
	UFUNCTION ()
		virtual void Tick (float pDeltaSeconds) override;
	UFUNCTION ()
		void UpdatePopulation ();
	UFUNCTION ()
		void UpdateTime ();
	UFUNCTION ()
		void UpdateHappiness ();
	UFUNCTION ()
		void UpdatePolicies ();
	UFUNCTION ()
		void UpdateInfoForPlayer ();
	UFUNCTION ()
		void UpdateNotificationManager ();
	UFUNCTION ()
		void UpdateMilestoneManager ();
	UFUNCTION ()
		void UpdateEventManager ();
#pragma endregion

	/* Sets the current game speed multiplier */
	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions|Setters")
		void SetGameSpeed(int pGameSpeed);


	/* Gets the date time in a specific format */
	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions|Getters")
		FString GetDateTimeInString();

#pragma endregion 

#pragma region Protected population functions
	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions|Getters")
		inline int GetMaxWorkCapacity() { return MaxWorkCapacity; }

	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions|Setters")
		void ModifyCapacityCount(FBuildingStats pBuilding, bool pRemove = false);

#pragma endregion 

#pragma region Protected pollution functions
	/* Gets the total pollution amount */
	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions|Getters")
		int GetTotalPollutionAmount();

	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions|Getters")
		float GetTotalPollutionPercentage();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game variables|Pollution")
		TSubclassOf<APollutionManager> PollutionManagerClass = APollutionManager::StaticClass();

#pragma endregion

#pragma region Getters for general variables
	/* Gets the current support count */
	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions")
		inline float GetSupportCount() { return Support; }

	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions|Getters")
		float GetBuildingsIncomeTotal();

	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions|Getters")
		float GetTaxIncome();

	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions|Getters")
		float GetTotalIncome(bool WithoutExpenses = false);

	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions|Getters")
		float GetMonthlyExpensesTotal();

	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions|Getters")
		float GetModifierByType(EModifierType pModifierType);
#pragma endregion 

#pragma region Protected policy functions
	/* Buying policies */
	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions|Policy specific")
		bool BuyPolicy(FName pId);
	/* Activating or deactivating policies by index */
	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions|Policy specific")
		bool TogglePolicy(FName pId);
	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions|Policy specific")
		FPolicy FindPolicyByID(FName pId);
	UFUNCTION(BlueprintCallable, Category = "City System|Protected functions|Getters")
		float GetCurrentPolicyUpkeepCosts(); 
#pragma endregion

	TArray<FBuildingManager> GetBuildingManagersForJobs(bool pServices = false);
	TMap<FVector, FBuildingStats> GetAllJobBuildingsExceptServices();
	TMap<FVector, FBuildingStats> GetMinimumAllJobBuildingsExceptServices();
	TMap<FVector, FBuildingStats> GetMinimumNotReachedServices();
private:

	void LoadSaveGame();
	void LoadBuildingsFromSaveGame(class UCitySaveGame* pSaveGame);
	void LoadPoliciesFromSaveGame(class UCitySaveGame* pSaveGame);
	//TODO:
	void LoadGameEventsFromSaveGame(class UCitySaveGame* pSaveGame);

	int FindOrCreateBuildingManager(ABuildableActor* pBuildableActor);

#pragma region Private Policy functions
	void RemoveUnusedPolicies();
	void InitializePolicies(FString pDTName);
#pragma endregion

#pragma region Private building functions
	void InitializeBuildings(FString pDTName);
#pragma endregion

#pragma region Private Game Events functions
	void InitializeGameEvents(FString pDTName);
#pragma endregion

#pragma region Private time functions
	/* Game time state updating */
	void SetGameTimeState(int pHour);

	/* Calculates the time interval and returns it */
	inline float GetTimeInterval() {
		return SecondsToADay / CurrentGameSpeedMultiplier;
	}

	/* Restarts the game timer based on the time interval */
	inline void RestartGameTimer() {
		GetWorld()->GetTimerManager().SetTimer(TimeHandler, this, &ACitySystemGameMode::HandleTimeActions, GetTimeInterval(), true, GetTimeInterval());
	}

	/* Restarts timelapse timer */
	inline void RestartTimelapseTimer() {
		float Interval = TimelapseDayInSeconds / HOURS_IN_DAY;
		GetWorld()->GetTimerManager().SetTimer(TimelapseDay, this, &ACitySystemGameMode::HandleTimelapseDay, Interval, true, Interval);
	}

	/* Adds a zero in front of an int in string format */
	inline FString AddZeroInFrontOfInt(int value) {
		return FString::FromInt(value).Len() == 1 ? FString("0").Append(FString::FromInt(value))
			: FString::FromInt(value);
	}

	void OnDayPassed ();
	void OnWeekPassed ();
	void OnMonthPassed ();
#pragma endregion
	virtual void BeginPlay() override;

protected:
	UPROPERTY()
		class UPopulationManager* PopulationManager;
	UPROPERTY()
		class UMyNCDGameInstance* GameInstance;
	UPROPERTY()
		class ANCD_PlayerController* PlayerController;
	UPROPERTY ()
		class UJsonImporter* JsonImporter;

#pragma region Tutorial
	UPROPERTY ()
		class UTutorial* Tutorial;
#pragma endregion

#pragma region Happiness
	UPROPERTY ()
		class UHappinessManager* HappinessManager;
#pragma endregion

#pragma region Scenario goals
	UPROPERTY ()
		class UGoalManager* GoalManager;
#pragma endregion

#pragma region Protected Lists
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Thresholds")
		int CapacitiesPollutions;

	UPROPERTY(BlueprintReadOnly, Category = "City System|GameSettings|In-game lists")
		TMap<FName,FPolicy> Policies;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game lists")
		UDataTable* PoliciesDataTable;
	
	/* Stored BuildingStats from the DataTable*/
	UPROPERTY(BlueprintReadOnly, Category = "City System|GameSettings|In-game lists")
		TMap<FName, FBuildingStats> Buildings;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game lists")
		UDataTable* BuildingsDataTable;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game lists")
		UDataTable* BuildingBlueprintsDataTable;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game lists")
		bool bSetBuildingBlueprints;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game lists")
		TMap<int, FGameTimeState> GameStates;

	UPROPERTY(Config, EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game lists")
		bool bUnlockAllBuildings;
#pragma endregion

#pragma region Protected Notification variables

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Notifications")
		TSubclassOf<AFeedbackNotificationManager> FeedBackNotificationManagerClass;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Notifications")
	AFeedbackNotificationManager* NotificationManager;
	
#pragma endregion 

#pragma region Protected Milestones variables

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Milestones")
		TSubclassOf<AMilestoneManager> MilestonesManagerClass;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Milestones")
		AMilestoneManager* MilestonesManager;

#pragma endregion 

#pragma region Protected Building Event variables

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|BuildingEvents")
		TSubclassOf<ABuildingEvents> BuildingEventsClass;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|BuildingEvents")
		ABuildingEvents* BuildingEventsManager;

	UPROPERTY()
		TMap<EBuildingStatus, FVector> BuildingWithActiveEvent;

#pragma endregion 

#pragma region Protected Traffic variables

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Traffic")
		TSubclassOf<ATrafficSystem> TrafficSystemClass;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = "City System|GameSettings|Traffic")
		ATrafficSystem* TrafficSystem;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "City System|GameSettings|Traffic")
		float VehicleSpawnDelay = 1.0f;

#pragma endregion 

#pragma region Protected Game Events stuff
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Events")
		UDataTable* GameEventsData;	//Set to DataTable to initiliaze on Load -> List variable is used for manipulation after.

	UPROPERTY(BlueprintReadOnly, Category = "City System|GameSettings|In-game lists")
		TMap<FName, FNCDEvent> GameEventsList;			// This list holds initialized events from the data table, to update for unlock and save/load from.

	UPROPERTY(EditDefaultsOnly, Category = "Events")
		float SecondsTillNextEventSearch = 180.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Events")
		TSubclassOf<UEventUI> EventUI;
#pragma endregion

#pragma region Protected time values
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Time settings")
		float SecondsToADay = 10.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Time settings")
		FDateTime StartDate = FDateTime(2018, 01, 01);
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Time settings")
		FGameTimeState GameTimeState;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Time settings")
		bool bHours24 = true;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Time settings")
		bool bShowTime = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Time settings")
		float TimelapseDayInSeconds = 60.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Time settings")
		int TimelapseHour = 0;

#pragma endregion

#pragma region Protected Happiness variables
	UPROPERTY (EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Population")
		int happinessMean = 50;
#pragma endregion

#pragma region Protected population variables
	UPROPERTY (EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Population")
		float cityGrowthModifierBase = 0.7f; //This should never be modified!
	UPROPERTY (EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Population")
		float cityGrowthModifier = 0.7f; //This will be modified at runtime to accomodate the formula to a growing/shrinking population
	UPROPERTY (EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Population")
		float populationSqrtModifier = 0.035f; //Don't change this unless you know what you're doing!
	UPROPERTY (EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Population")
		float populationFractionBuffer = 0.0f;
	UPROPERTY (EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Population")
		int daysPerPopModification = 4;
#pragma endregion

#pragma region Intervals
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|TimeIntervals|Days")
		int UpdateIntervalPollution = 1;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|TimeIntervals|Days")
		int UpdateIntervalEnoughBuildings = 2;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|TimeIntervals|Days")
		int UpdateIntervalAssigningJobs = 1;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|TimeIntervals|Days")
		int UpdateIntervalHappiness = 7;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|TimeIntervals|Days")
		int IntervalFiredPeople = 12;
#pragma endregion 

#pragma region Protected Pollution variables
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Thresholds")
		float PollutionPenaltyThreshold = 25.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Thresholds")
		int HealthyThreshold = 5;
#pragma endregion

#pragma region Protected money variables
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game variables|Currency")
		float Money = 5000;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game variables|Currency")
		float MonthlyIncomePerPerson = 1500.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game variables|Currency")
		float VatPercentage = 21.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Building values")
		float SellPercentage = 75.f; 
#pragma endregion

#pragma region Protected support variables
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game variables|Suppport")
		float Support = 0.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game variables|Suppport")
		float MinSupport = NIHIL_PERCENTAGE;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game variables|Suppport")
		float MaxSupport = 1000.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game variables|Suppport")
		float SupportAddingValue = 0.25f;
#pragma endregion 

#pragma region Protected health variables
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game variables|Health")
		float Health = 80.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game variables|Health")
		float MinHealth = NIHIL_PERCENTAGE;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game variables|Health")
		float MaxHealth = 1000.f;
#pragma endregion 
	
#pragma region Protected population variables
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game variables|Population")
		int MaxPopulationModifier = 4;
#pragma endregion

#pragma region Protected population chance variables
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game variables|Population|Chance")
		float BonusChanceSpawning = 30.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game variables|Population|Growth")
		float PopulationLeavingThreshold = 30.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game variables|Population|Growth")
		float PopulationSpawnThreshold = 65.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|In-game variables|Population|Growth")
		float PopulationSpawnThresholdBeginning = 80.f;
#pragma endregion 

#pragma region Work percentages
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Jobs")
		float MinimumWorkPercentage = 65.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Jobs")
		float ChanceToGetAJob = 75.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Jobs")
		float BonusChanceToGetAJob = 15.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "City System|GameSettings|Jobs")
		float JobsPercentageHousehold = 30.f;
#pragma endregion
};