// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "GameTimeState.generated.h"

/**
 * Game light attributes like rotation light, color, intensity, postfx options(optional)
 */
USTRUCT(BlueprintType)
struct FGameTimeState : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		FRotator LightRotation;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		FLinearColor LightColor = FLinearColor::White;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		FLinearColor HorizonColor = FLinearColor::White;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		FLinearColor OverallColor = FLinearColor::White;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		FLinearColor CloudsColor = FLinearColor::White;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		FLinearColor ZenithColor = FLinearColor::White;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float Intensity;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		FLinearColor HeightFogColor = FLinearColor::White;

	/* PostFX effects */
};