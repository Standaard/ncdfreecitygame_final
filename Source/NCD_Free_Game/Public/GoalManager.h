// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "BuildingStats.h"
#include "UObject/NoExportTypes.h"
#include "GoalManager.generated.h"

class ACitySystemGameMode;

UENUM(BlueprintType)
enum class EGoalTypes : uint8 { BuildGoal = 0, PopulationGoal = 1, PollutionGoal = 2, EmploymentGoal = 3 };
UENUM (BlueprintType)
enum class EGoalEvents : uint8 { BuildEvent = 0, PopulationEvent = 1, PollutionEvent = 2, EmploymentEvent = 3 };

USTRUCT(BlueprintType)
struct FScenarioGoal : public FTableRowBase {
	GENERATED_BODY ()

	UPROPERTY (BlueprintReadWrite, Category = "Goal Info")
		FName ID;
	UPROPERTY (EditDefaultsOnly, BlueprintReadWrite, Category = "Goal Info")
		FName Name;
	UPROPERTY (EditDefaultsOnly, BlueprintReadWrite, Category = "Goal Info")
		FString Description;
	UPROPERTY (EditDefaultsOnly, BlueprintReadWrite, Category = "Goal Info")
		EGoalTypes GoalType;
	UPROPERTY (EditDefaultsOnly, BlueprintReadWrite, Category = "Goal Info")
		EBuildingType buildingToBuild;
	UPROPERTY (EditDefaultsOnly, BlueprintReadWrite, Category = "Goal Info")
		int AmountToBuild;
	UPROPERTY (BlueprintReadWrite, Category = "Goal Info")
		int AmountBuilt = 0;
	UPROPERTY (BlueprintReadWrite, Category = "Goal Info")
		bool Completed = false;

	FScenarioGoal () {}
};

/**
 * 
 */
UCLASS()
class NCD_FREE_GAME_API UGoalManager : public UObject {
	GENERATED_BODY()
	
public:
	UGoalManager ();
	~UGoalManager ();

	UFUNCTION ()
		void Initialize (ACitySystemGameMode* pGameMode);
	UFUNCTION ()
		void LoadGoalData (FString pDTName);
	UFUNCTION ()
		void OnBuildEvent (FBuildingStats pBuilding);
	UFUNCTION (BlueprintCallable)
		TArray<FScenarioGoal> GetGoalsOfType (EGoalTypes pGoalType);
protected:

private:
	UFUNCTION ()
		void OnEventHandled ();
	UFUNCTION ()
		void SetGoalTypeActive (EGoalTypes pGoalType);
	UFUNCTION ()
		bool IsGoalTypeActive (EGoalTypes pGoalType);
	UFUNCTION ()
		bool GoalSetCompleted (TArray<FScenarioGoal> pGoalSet);

	UPROPERTY ()
		ACitySystemGameMode* gameMode;
	UPROPERTY ()
		TMap<FName, FScenarioGoal> scenarioGoals;

#pragma region Goal lists
	UPROPERTY ()
		TArray<FScenarioGoal> buildGoals;
	UPROPERTY ()
		TArray<FScenarioGoal> populationGoals;
	UPROPERTY ()
		TArray<FScenarioGoal> pollutionGoals;
	UPROPERTY ()
		TArray<FScenarioGoal> employmentGoals;
#pragma endregion

		//Will function as a way to store the goal types (booleans). Only the last few bits will be used!
		//Last bits: 3210, where 0 = buildgoal, 1 = populationgoal, 2 = pollutiongoal, 3 = employmentgoal
	UPROPERTY()
		int goalTypes = 0;
};
