// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Milestones.h"
#include "MilestoneManager.generated.h"

class ACitySystemGameMode;

UCLASS()
class NCD_FREE_GAME_API AMilestoneManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMilestoneManager();
	~AMilestoneManager();

	void CheckMilestones();			//Check all milestones against their conditions if they haven't been unlocked yet
	TArray<FName> GetUnlockedMilestonesForExport();
	void LoadMilestonesFromSavegame(TArray<FName> pUnlockedMilestonesList);			//Load all data from DataTable

	//UFUNCTION(BlueprintImplementableEvent, Category = "Achievements")
		//void ShowNotification(FMilestones pMilestone);

	void DeveloperUnlockMilestone();

protected:
	virtual void BeginPlay() override;

	void LoadMilestones();			// Load all data from DataTable
									// Called when the game starts or when spawned
	   
	void MilestoneReached(FMilestones * Milestone, bool bShowNotification = true);		//Milestone reached, triggers unlock on buildings/policies
	void HappinessReward(float pAmount);

	bool CheckAmountOfCitizens( int pAmountNeeded );
	bool CheckAmountOfBuildings(int pAmountNeeded, TArray<EBuildingType> pOfBuildingType);
	bool CheckAmountOfWorkingPeople( int pAmountNeeded );
	bool CheckCompletedGameEvent(TArray<FDataTableRowHandle> pGameEvents);

	//bool CheckAmountOfHappiness(int pAmountNeeded);
	//bool CheckAmountOfMoney(int pAmountNeeded);
	//bool CheckAmountOfPollution(int pAmountNeeded, EPollutionType pPollutionType);

protected:
	UFUNCTION(BlueprintCallable)
	void AddMilestoneNotification(FMilestones pMilestoneNotification);
	UFUNCTION(BlueprintCallable)
	FMilestones CheckForMilestoneNotification();
	UFUNCTION(BlueprintCallable)
	TMap<FName, FMilestones> GetMilestoneList();
	UFUNCTION(BlueprintCallable)
		TArray<FMilestones> GetNotificationsList();

protected:
	UPROPERTY(BlueprintReadWrite, Category = "Notifications")
	TArray<FMilestones> MilestoneNotifications;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones")
	UDataTable* MilestonesDataTable;

	UPROPERTY()
	ACitySystemGameMode* CitySystem;
	UPROPERTY()
	TMap<FName, FMilestones> MilestoneList;

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Milestones")
		bool bDeveloperUnlockActive = false;
};
