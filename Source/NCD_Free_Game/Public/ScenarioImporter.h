// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ScenarioImporter.generated.h"

UCLASS()
class NCD_FREE_GAME_API AScenarioImporter : public AActor {
	GENERATED_BODY ()

public:
	AScenarioImporter ();

	UFUNCTION (BlueprintCallable, Category = "Import | Import files")
		void OnClickImport ();
	UFUNCTION ()
		UJsonImporter* GetJsonImporter ();
	UFUNCTION ()
		class ANCD_PlayerController* GetPlayerController ();

	UPROPERTY (EditAnywhere, Category = "Import | Folders")
		FString FolderToImport = "";

protected:
	virtual void BeginPlay () override;

private:
	UFUNCTION ()
		void ImportFolder (FString pFolderName);
	UFUNCTION ()
		void ImportJsons ();
	UFUNCTION ()
		void LoadDatatables ();
	UFUNCTION ()
		void LoadDatatable (FString pFileName);

	//UPROPERTY ()
		//class FPlatformFileManager* fileManager;
	UPROPERTY ()
		class UJsonImporter* JsonImporter;
	UPROPERTY ()
		FString fromPathBaseString;
	UPROPERTY ()
		FString toPathBaseString;
	UPROPERTY ()
		ANCD_PlayerController* playerController;
};