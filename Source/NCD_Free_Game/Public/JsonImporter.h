// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"
#include "JsonImporter.generated.h"

USTRUCT ()
struct FDatatables {
	GENERATED_USTRUCT_BODY ()

public:
	UPROPERTY ()
		FString BuildingsDT;
	UPROPERTY ()
		FString EventsDT;
	UPROPERTY ()
		FString MilestonesDT;
	UPROPERTY ()
		FString PoliciesDT;
	UPROPERTY ()
		FString VehiclesDT;
	UPROPERTY ()
		FString GoalsDT;

	FDatatables () {

	}

	FDatatables (FString pBuildingsDT, FString pEventsDT, FString pMilestonesDT, FString pPoliciesDT, FString pVehiclesDT, FString pGoalsDT) {
		BuildingsDT = pBuildingsDT;
		EventsDT = pEventsDT;
		MilestonesDT = pMilestonesDT;
		PoliciesDT = pPoliciesDT;
		VehiclesDT = pVehiclesDT;
		GoalsDT = pGoalsDT;
	}
};

/**
 *
 */
UCLASS()
class NCD_FREE_GAME_API UJsonImporter : public UObject {
	GENERATED_BODY ()

public:
	// Sets default values for this actor's properties
	UJsonImporter ();
	~UJsonImporter ();

	UFUNCTION ()
		void Initialize (AScenarioImporter* pScenarioImporter);
	UFUNCTION ()
		bool ReadBaseJSON (FString pRelativePath);
	UFUNCTION ()
		bool ReadSceneJSON (FString pRelativePath);

	UPROPERTY ()
		FDatatables DatatableNames;

protected:

private:
	class AScenarioImporter* scenarioImporter;
};
