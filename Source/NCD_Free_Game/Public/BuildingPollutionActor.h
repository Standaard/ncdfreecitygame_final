// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildingStats.h"
#include "MaterialParameters.h"
#include "BuildingPollutionActor.generated.h"

class UMaterialInterface;
class UStaticMeshComponent;
class UPollutionAreaDecal;
class ACitySystemGameMode;
class UMaterialInstanceDynamic;

UCLASS()
class NCD_FREE_GAME_API ABuildingPollutionActor : public AActor
{
	GENERATED_BODY()
	
};
