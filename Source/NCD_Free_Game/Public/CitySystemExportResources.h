// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Policy.h"
#include "Milestones.h"
#include "PopulationManager.h"
#include "CitySystemExportResources.generated.h"

/**
 * Export class
 */
USTRUCT()
struct FCitySystemExportResources
{
	GENERATED_BODY()
	UPROPERTY()
		int Support;
	UPROPERTY()
		int Money;
	UPROPERTY()
		float Happiness;
	UPROPERTY()
		FPopulationExport PopulationExport;
	UPROPERTY()
		TMap<FName, FPolicyExport> Policies;
	UPROPERTY()
		TArray<FName> MilestonesList;
	
	/* This month's policies that are activated
	UPROPERTY()
		TMap<FName, float> PolicyUpkeepCosts;
	*/
};
