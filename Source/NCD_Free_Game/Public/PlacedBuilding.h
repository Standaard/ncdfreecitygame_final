// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildingHotspot.h"
#include "BuildingStats.h"
#include <Engine/DataTable.h>
#include "PlacedBuilding.generated.h"

class UStaticMeshComponent;
class UPollutionAreaDecal;
class ACitySystemGameMode;
struct FMaterialParameters;

//Use CitySystemGameMode's GetBuildingBlueprintFromStats to obtain a reference to the default object
USTRUCT(BlueprintType)
struct FBuildingBlueprints : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TSubclassOf<class APlacedBuilding> PlacedBuildingBlueprint;
};
/*
*
*/
UCLASS(BlueprintType)
class NCD_FREE_GAME_API APlacedBuilding : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlacedBuilding();
	
	UPROPERTY(BlueprintReadOnly)
	FBuildingStats BuildingStats;

	UPROPERTY(VisibleInstanceOnly)
	ACitySystemGameMode* CitySystem;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	USceneComponent* SceneComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (MakeEditWidget = true))
	FBuildingHotSpotEventParticles ParticleHotspot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FBuildingHotspotRoadCollision RoadCollisionBoxHotspot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta = (MakeEditWidget = true))
	TArray<FTransform> DecorationTransforms;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pollution")
	UPollutionAreaDecal* DecalPollutionArea;

public:
	inline USceneComponent* GetSceneComponent() {
		return SceneComponent;
	}
	inline UStaticMeshComponent* GetMeshComponent() {
		return MeshComponent;
	}
	inline FVector GetFirstParticleHotspot() {
		return ParticleHotspot.Locations.Num() > 0 ? ParticleHotspot.Locations[0] : FVector::ZeroVector;
	}
	inline FBuildingHotspotRoadCollision& GetRoadCollisionHotspot() {
		return RoadCollisionBoxHotspot;
	}

	UFUNCTION()
		virtual void OnDelete();
	/*
		Sets the value of PollutionMaterialValue in the Building Master Material
		- pCurveScalar = The horizontal value used in the Color Curve, which can be found in the Master material
		+ Returns false if the mesh has zero materials
	*/
	bool SetPollutionColor(float pCurveScalar);
	/*
	Returns the parameters set on the material.
	- pParams = The custom parameters.
*/
	void GetPollutionMeshScalars(FMaterialParameters& pParams);
	
	UFUNCTION(BlueprintCallable)
	void SetPollutionDecalVisible(bool pVisible);

	virtual void Tick(float DeltaTime) override;

	void SetBuildingStats(FBuildingStats& Building);
	void SetCitySystem(ACitySystemGameMode* CitySystem);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

protected:
	/*The name of the parameter in a building material to change the pollution*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pollution")
	FName PollutionMaterialScalarName = FName(TEXT("PollutionScalar"));
	
	/*The scalar value used to determine the color in the color curve inside the building material*/
	UPROPERTY(VisibleInstanceOnly, Category = "Pollution")
	float PollutionMaterialValue;

	/*Enable or disable the pollution decal*/
	UPROPERTY(VisibleInstanceOnly, Category = "Pollution")
	bool bEnableDecal;
};
