// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "BuildingType.h"
#include "TrafficAgentData.generated.h"

USTRUCT(BlueprintType)
struct FTrafficAgentData : public FTableRowBase
{
	GENERATED_BODY()

		FName ID;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Traffic")
		TArray<EBuildingType> Types;

	/*Make sure that the Mesh's Collision profile is set to "Vehicle"*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Traffic")
		UStaticMesh* Mesh;

	/*The material interface used for the Mesh*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Traffic")
		TArray<UMaterialInterface*> Materials;

	/*The line that's used for collision checking with other Vehicles*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Traffic")
		float LineTraceLength = 5.0f;

	/*Forward offset for the line trace.*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Traffic")
		float LineTraceOffsetFromExtent = 5.0f;
	/*Horizontal offset from the center, used to make the vehicles drive on the correct lane*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Traffic")
		float HorizontalLocationOffset = 15.0f;
	
	/*Vehicle speed per second.*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Traffic")
		float TranslationSpeed = 50.0f;
};
