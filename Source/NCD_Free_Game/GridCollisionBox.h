// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/BoxComponent.h"
#include "GridCollisionBox.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class NCD_FREE_GAME_API UGridCollisionBox : public UBoxComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGridCollisionBox();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	int GetInstanceMeshIndex();
	void SetInstanceMeshIndex(int pIndex);

private:
	int InstanceMeshIndex = 0;


		
	
};
