// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildingStats.h"
#include "DecorationInstances.h"
#include "PollutionAreaDecal.h"
#include "BuildableActor.generated.h"

class ACitySystemGameMode;
class ADecalActor;
class ANCD_PlayerController;
class UGridCollisionBox;
class UHierarchicalInstancedStaticMeshComponent;
class UInstancedStaticMeshComponent;
class USceneComponent;
class UStaticMesh;
class UStaticMeshComponent;
class APlacedBuilding;


USTRUCT(BlueprintType)
struct FPlacedBuildingParameters
{
	GENERATED_BODY()
public:
	/*Does the building require money to be placed?*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsFree = false;

	/*Is the building loaded in from a savegame?*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsRemovable = true;

	/*Do we need to run the placement collision?*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bSkipCollision = false;

	/*Do we skip using the rotated offset?*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bSkipLocationOffset = false;
	
	/*Is the placed building interactable?*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsInteractable = true;

	/*Is the building loaded from a savegame?*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bLoadFromSave = false;

	/*Is the building loaded from a scenario?*/
	UPROPERTY (EditAnywhere, BlueprintReadWrite)
		bool bLoadFromScenario = false;

};

UCLASS()
class NCD_FREE_GAME_API ABuildableActor : public AActor
{
	GENERATED_BODY()
	
public:	
	ABuildableActor();


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void OnPreviewPlace(FTransform pTransform, bool pSkipCollisionCheck = false, bool pIsFreeBuilding = false);
	virtual FVector OnBuildableActorPlace(FTransform pTransform);
	virtual void CheckPlacementCollision(FVector pSnappedLocation);
	virtual void CheckPlacementCollisionActor (FVector pCheckLocation, FCollisionShape pBoxShape);
	//Custom collision 
	virtual void CheckSpecialCollision(FVector SnappedLocation);

	virtual void OnDelete(FVector pLocation);
	
	AActor* SweepForActorInScene(FVector pSnappedLocation, UClass* pRoadClass);

	void UpdateDecorations();

	inline bool CanPlaceBuilding() { return bAllowedToBuild; }
	FVector GetRotatedSnapLocation(FVector pLocation);
	void SetRotation(int pRotation);
	int GetRotation();
	void SetSpawnOffset();
	void ClearPreviewMesh();

	UFUNCTION ()
		UStaticMeshComponent* GetStaticMeshComponent ();

public:

	UFUNCTION(BlueprintCallable, Category = "Mesh Instancing")
	FName GetIdentifier();

	UFUNCTION(BlueprintCallable, Category = "Mesh Instancing")
	void SetBuildingData(FBuildingStats pBuildingStats);

	void CreatePreviewMesh();

	UFUNCTION(BlueprintCallable, Category = "Mesh Instancing")
	void PlaceBuilding(FTransform pBuildingTransform, FPlacedBuildingParameters pParameters);

	UFUNCTION(BlueprintCallable, Category = "Mesh Instancing")
	void SetAllowedToBuild(bool pAllowedToBuild);

	FORCEINLINE UMaterialInstanceDynamic* GetDynamicMaterial() { return dynamicMaterial; }
	UFUNCTION(BlueprintCallable)
	FORCEINLINE FBuildingStats GetBuildingStats() { return BuildingStats; }
	FORCEINLINE bool GetIsAllowedtoBuild() { return bAllowedToBuild; }
	FORCEINLINE bool GetCanRotate() { return bCanRotate; }
	FORCEINLINE FVector GetRotatedSpawnOffset() { return RotatedSpawnOffset; }
	FORCEINLINE FVector GetLatestDetectedBuildingLocation() { return LatestDetectedBuildingLocation; }
	UFUNCTION(BlueprintGetter)
	TMap<FVector, APlacedBuilding*>& GetStoredBuildings() { return StoredBuildings; }
	/* Only use this when StoredBuildings size is 1. Maps don't store the order of the buildings reliably.*/
	UFUNCTION(BlueprintCallable)
	APlacedBuilding* GetFirstBuilding();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void CreateDynamicPreviewMaterial(int pMaterialIndex, bool pCanPlace);
	
protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh Instancing")
		USceneComponent* SceneRootComponent;

	/*Used in blueprints to add components to it. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh Instancing")
		USceneComponent* TutorialSceneComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Mesh Instancing")
		APlacedBuilding* PreviewMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Mesh Instancing")
		class APlacedBuilding* PlacedBuilding;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Mesh Instancing")
		APlacedBuilding* BuildingBlueprint;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Mesh Instancing")
		UStaticMesh* StaticMesh;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Mesh Instancing")
		TArray<UGridCollisionBox*> CollisionBoxes;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Mesh Instancing")
		TArray<UMaterialInterface*> StaticMeshMaterials;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Mesh Instancing")
		bool bCastShadow = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Mesh Instancing")
		UPollutionAreaDecal* DecalComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building")
		FVector PreviewMaterialCanPlace;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building")
		FVector PreviewMaterialCannotPlace;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = "Building")
		TMap<FVector, APlacedBuilding*> StoredBuildings;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh Instancing")
		bool bCanRotate = true;

protected:
	
		TMultiMap<UHierarchicalInstancedStaticMeshComponent*, int> FlagForDeletionInstances;
	UPROPERTY()
		TArray<ADecorationInstances*> DecorationInstances;

	UPROPERTY(VisibleInstanceOnly)
		ANCD_PlayerController* PlayerController;
	UPROPERTY()
		ACitySystemGameMode* CitySystem;

	UPROPERTY()
		FName Identifier;

	int Rotation = 0;
	bool bAllowedToBuild = true;


	FVector DimensionsInMeters;
	FVector SpawnOffset;
	FVector RotatedSpawnOffset;
	FVector BoundingBoxOffset;
	FVector LastPreviewLocation = FVector(9999,9999,9999);
	FVector LatestDetectedBuildingLocation;
	UPROPERTY(VisibleInstanceOnly)
	APlacedBuilding* LatestCreatedBuilding;

	UPROPERTY()
		UMaterialInstanceDynamic* dynamicMaterial;
	UPROPERTY(VisibleInstanceOnly)
		FBuildingStats BuildingStats;

	int LastRotation = 0;
	int SelectedInstance = -1;
};
