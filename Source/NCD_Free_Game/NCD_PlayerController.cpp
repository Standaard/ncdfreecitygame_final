// Fill out your copyright notice in the Description page of Project Settings.

#include "NCD_PlayerController.h"
#include "BuildableActor.h"
#include "Materials/MaterialInterface.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "NCD_Pawn.h"
#include "Grid.h"
#include "CitySystemGameMode.h"
#include "BuildableRoad.h"
#include "NCD_GameState.h"
#include "BuildingDescriptionActor.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "BuildingManager.h"
#include "TrafficSystem.h"
#include "DrawDebugHelpers.h"
#include <Materials/MaterialParameterCollection.h>
#include <Materials/MaterialParameterCollectionInstance.h>
#include <GameFramework/PlayerInput.h>
#include "MaterialParameters.h"
#include "BuildingStats.h"

ANCD_PlayerController::ANCD_PlayerController()
{
	bShowMouseCursor = true;
	bShowWarning = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ANCD_PlayerController::OnTimeChange(bool pIsNight)
{
	//LightParameterCollection->etScalarParameterValue("")

	if (LightParameterCollection)
	{
		LightParameterCollectionInstance = GetWorld()->GetParameterCollectionInstance(LightParameterCollection);
		LightParameterCollectionInstance->SetScalarParameterValue(FName("bIsLight"), pIsNight);
	}

	for (int i = 0; i < DynamicMaterials.Num();i++)
	{
		if (DynamicMaterials[i]) { DynamicMaterials[i]->SetScalarParameterValue("IsLight", pIsNight); }
	}
}

void ANCD_PlayerController::BeginPlay()
{
	Super::BeginPlay();
	Rereference();
	HandleOnRelease.BindUFunction (CitySystem->GetTutorial (), FName ("OnClickedBuildingTutorial"));
}

/** Setup controls! */
void ANCD_PlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("OnBuildingModeDisabled", IE_Pressed, this, &ANCD_PlayerController::DisableBuildingMode);
	InputComponent->BindAction("OnTap", IE_Pressed, this, &ANCD_PlayerController::OnTap);
	InputComponent->BindAction("OnTap", IE_Released, this, &ANCD_PlayerController::OnTapRelease);
	InputComponent->BindAction("OnRotate", IE_Pressed, this, &ANCD_PlayerController::OnRotate);
	InputComponent->BindAction("OnSwitchEntity", IE_Pressed, this, &ANCD_PlayerController::OnSwitchEntity);
	InputComponent->BindAction("DeleteBuilding", IE_Pressed, this, &ANCD_PlayerController::DeleteBuilding);
}

void ANCD_PlayerController::PlayerTick (float DeltaTime) {
	Super::PlayerTick (DeltaTime);
	//CitySystem->GetTutorial ()->TestTickDebug ();

	if (!bBuildingModeEnabled || !SelectedBuildableActor) return;
	FHitResult CursorHit;

	GetHitResultUnderCursor (ECC_GameTraceChannel4, false, CursorHit);
	if (!CursorHit.bBlockingHit) {
		GetHitResultUnderCursor (ECC_GameTraceChannel3, false, CursorHit);
		if (!CursorHit.bBlockingHit) return;
		SelectedBuildableActor->OnPreviewPlace (FTransform (FRotator (0, SelectedBuildableActor->GetRotation(), 0), CursorHit.Location), bSkipCollisionCheck);
		return;
	} else {
		SelectedBuildableActor->OnPreviewPlace (FTransform (FRotator (0, SelectedBuildableActor->GetRotation(), 0), CursorHit.Location), bSkipCollisionCheck);
		return;
	}
}

void ANCD_PlayerController::UpdateBuildingHighlight () {
	if (!bBuildingMessageActive) {
		FHitResult CursorHitAll;
		GetHitResultUnderCursor (ECC_GameTraceChannel1, false, CursorHitAll);
		HighlightBuilding (CursorHitAll);
	}
}

void ANCD_PlayerController::SetSelectedBuildableActor(ABuildableActor* pBuildableActor)
{
	if (!pBuildableActor) return;
	if (SelectedBuildableActor) { 
		SelectedBuildableActor->ClearPreviewMesh(); 
		SelectedBuildableActor->SetRotation(0);
	}
	SelectedBuildableActor = pBuildableActor;
}

void ANCD_PlayerController::UnbindTutorialFunctions () {
	UE_LOG (LogTemp, Warning, TEXT ("Unbinding handle on release!"));
	HandleOnRelease.Unbind ();
}

ABuildableActor* ANCD_PlayerController::FindBuildableActorByName(FName pName)
{
	ABuildableActor** foundActor = BuildableActorMap.Find(pName);
	return foundActor != nullptr ? *foundActor : nullptr;
}

FVector ANCD_PlayerController::GetSelectedLocation()
{
	return SelectedLocation;
}

FVector ANCD_PlayerController::GetGridLocation (FVector pUnsnappedLocation) {
	return Grid::GetLocationOnGrid (FVector (100.0f, 100.0f, 100.0f), pUnsnappedLocation);
}

ABuildableActor* ANCD_PlayerController::AddBuildableActor(FBuildingStats pBuildingStats)
{
	Rereference();
	if (!GetWorld()) return nullptr;
	if (!CitySystem) return nullptr;
	//Get the BuildableActor from the map, otherwise create one from pBuildingStats
	ABuildableActor* buildableActor = BuildableActorMap.FindRef(pBuildingStats.ID);

	if (buildableActor) return buildableActor;

	switch (pBuildingStats.BuildableActorType) {
		case EBuildableActorType::BA_Road:
		{
			buildableActor = GetWorld()->SpawnActor<ABuildableRoad>(BuildableRoadClass, FVector::ZeroVector, FRotator::ZeroRotator);
			ABuildableRoad* BuildableRoad = Cast<ABuildableRoad>(buildableActor);
			if (!CitySystem->BuildableRoadActor) CitySystem->BuildableRoadActor = BuildableRoad;
		}
		break;
		case EBuildableActorType::BA_ConnectToRoad:
			buildableActor = GetWorld()->SpawnActor<ABuildableActor>(BuildableBuildingClass, FVector::ZeroVector, FRotator::ZeroRotator);
			break;
		case EBuildableActorType::BA_FreePlace:
			buildableActor = GetWorld()->SpawnActor<ABuildableActor>(BuildableActorClass, FVector::ZeroVector, FRotator::ZeroRotator);
			break;
		case EBuildableActorType::BA_Utilities:
			buildableActor = GetWorld ()->SpawnActor<ABuildableActor> (BuildableUtilityClass, FVector::ZeroVector, FRotator::ZeroRotator);
			break;
		default:
			buildableActor = GetWorld()->SpawnActor<ABuildableActor>(BuildableActorClass, FVector::ZeroVector, FRotator::ZeroRotator);
			break;
	}

	if (!buildableActor) return nullptr;

	if (pBuildingStats.BuildingBlueprint.IsNull()) return nullptr;

#if WITH_EDITOR
	buildableActor->SetActorLabel(FString("BuildableActor_") + pBuildingStats.Name);
#endif

	buildableActor->SetBuildingData(pBuildingStats);

	BuildableActorMap.Add(pBuildingStats.ID, buildableActor);
	DynamicMaterials.Add(buildableActor->GetDynamicMaterial());

	///Starter Town
	//find the current building in the StarterTownBuildings
	/*FSpawnedBuildings* foundSpawnedBuilding = StarterTownBuildings.FindByPredicate([&](const FSpawnedBuildings& building)
	{
		return pBuildingStats.ID.IsEqual(building.Building.RowName);
	});

	//check if the current building can be found in StarterTownBuildings
	//Load order: roads -> city hall -> other buildings
	if (foundSpawnedBuilding && foundSpawnedBuilding->Building.RowName.IsEqual(pBuildingStats.ID))
	{
		//if so, then create a FStarterBuilding for every transform
		for (FSpawnBuildingProps& props : foundSpawnedBuilding->BuildingProps)
		{
			switch (pBuildingStats.BuildingType)
			{
			case EBuildingType::EB_ROAD:
			{
				bool isZero = CitySystem->StartRoadLocation.IsZero();
				if (isZero) CitySystem->StartRoadLocation = Grid::GetLocationOnGrid(Grid::GetGridVector(), props.Transform.GetLocation());
				//bool isObscureRoad = transform.GetLocation() == FVector (1550.0f, 2450.0f, 350.0f);
				StarterTownPlacements.Insert(FStarterBuilding(buildableActor, props.Transform, props.Params), 0);
				TotalRoadAmount++;
				break;
			}
			case EBuildingType::EB_CITY_HALL:
				//TODO-SCENARIOS: check for load order: road - city hall - different road
				StarterTownPlacements.Insert(FStarterBuilding(buildableActor, props.Transform, props.Params), TotalRoadAmount);
				break;
			default:
				StarterTownPlacements.Add(FStarterBuilding(buildableActor, props.Transform, props.Params));
				break;
			}
		}
	}*/
	return buildableActor;
}

void ANCD_PlayerController::AddAllBuildableActors()
{
	Rereference();
	if (CitySystem)
	{
		for (auto& Building : CitySystem->GetBuildingsList()) {
			SetSelectedBuildableActor(AddBuildableActor(Building.Value));
		}
	}
}

void ANCD_PlayerController::OnToggleInteractable()
{
	bSetInteractable = !bSetInteractable;
#if WITH_EDITOR
	UE_LOG(LogTemp, Warning, TEXT("Toggled building bInteractable %s"), bSetInteractable ? TEXT("true") : TEXT("false"));
#endif // WITH_EDITOR

}

void ANCD_PlayerController::OnToggleCollisionCheck()
{
	bSkipCollisionCheck = !bSkipCollisionCheck;
#if WITH_EDITOR
	UE_LOG(LogTemp, Warning, TEXT("Toggled building collision check %s"), bSkipCollisionCheck ? TEXT("true") : TEXT("false"));
#endif // WITH_EDITOR
}

void ANCD_PlayerController::BuildStarterTown () {
	UE_LOG (LogTemp, Warning, TEXT ("Placing starter buildings..."));
	//Create the town the player starts off with. 
	//AddAllBuildableActors();
	/*for (int i = 0; i < StarterTownPlacements.Num (); i++) {
		SetSelectedBuildableActor (StarterTownPlacements[i].BuildableActor);
		//Do this so that "CanPlaceBuilding" will return true;
		SelectedBuildableActor->SetRotation(StarterTownPlacements[i].BuildingTransform.Rotator().Yaw);
		SelectedBuildableActor->OnPreviewPlace (StarterTownPlacements[i].BuildingTransform, StarterTownPlacements[i].buildingParams.bSkipCollision); //Do this so that "CanPlaceBuilding" will return true;
		SelectedBuildableActor->PlaceBuilding (StarterTownPlacements[i].BuildingTransform, StarterTownPlacements[i].buildingParams);
	}
	*/
	UE_LOG (LogTemp, Warning, TEXT ("[BuildStarterTown] Starter buildings have been placed!"));
}

ABuildableActor* ANCD_PlayerController::GetBuildableActor (FName pID) {
	return BuildableActorMap[pID];
}

void ANCD_PlayerController::PlaceBuilding (FTransform pBuildingTransform, FPlacedBuildingParameters pParameters) {
	if (SelectedBuildableActor) {
		SelectedBuildableActor->PlaceBuilding(pBuildingTransform, pParameters);
	}
}

void ANCD_PlayerController::OnTap()
{
	Rereference();
	NCD_Pawn->SetDragPan(true);
}

void ANCD_PlayerController::OnTapRelease()
{
	Rereference(); 
	bool bReached = NCD_Pawn->bDragPanTresholdReached;
	NCD_Pawn->SetDragPan(false);

	if (bReached) 
		return;
	
	if (!SelectedBuildableActor) return;
	FHitResult CursorHit;
	FHitResult CursorHitBuilding;
	GetHitResultUnderCursor(ECC_GameTraceChannel1, false, CursorHit); //Changed ECC_VISIBILITY TO ECC_GAMETRACECHANNEL1
	GetHitResultUnderCursor(ECC_GameTraceChannel3, false, CursorHitBuilding);

	APlacedBuilding* HitPlacedBuilding = Cast<APlacedBuilding>(CursorHit.Actor);
	FBuildingStats* stats = nullptr;
	//is the hit BuildableActor valid?
	bool showMessage = true;
	if (HitPlacedBuilding) {
		stats = &HitPlacedBuilding->BuildingStats;
		SelectedLocation = HitPlacedBuilding->GetActorLocation();
	}

	if (HandleOnRelease.IsBound () && stats) {
		HandleOnRelease.Execute (*stats);
		//Special case used in the tutorial due to building succession. --Don't question it--
		showMessage = !(stats->BuildingType == EBuildingType::EB_CLINIC && !CitySystem->GetTutorial ()->HasCompletedUpToStep (ETutorialSteps::BuiltHouse));
	}

	if (!CursorHit.bBlockingHit && !CursorHitBuilding.bBlockingHit) return;

	//only perform a highlight when not in building mode and when the appropriate tutorial step is completed.
	if (CitySystem->GetTutorial ()->HasCompletedUpToStep (ETutorialSteps::OpenedBuildMenu) && !bBuildingModeEnabled) {
		//Highlight selected asset and posssibly show description / demolish message depending on mode and building status.
		if (HitPlacedBuilding) HighlightBuilding (CursorHit, showMessage, CitySystem->GetTutorial ()->HasCompletedTutorial ());
		// Remove asset highlight
		else RemoveHighlight ();
		return;
	}

	//buy building
	if (!CitySystem || !CitySystem->IsAllowedToBuildBuilding (SelectedBuildableActor->GetBuildingStats ())) {
		PlayErrorSound ();
		return;
	}
	FPlacedBuildingParameters params;
	params.bIsInteractable = bSetInteractable;
	SelectedBuildableActor->PlaceBuilding (FTransform (FRotator (0, SelectedBuildableActor->GetRotation(), 0), CursorHitBuilding.Location), params);
}

void ANCD_PlayerController::OnRotate() {
	if (!SelectedBuildableActor) return;
	if (!SelectedBuildableActor->GetCanRotate()) return;
	SelectedBuildableActor->SetRotation((SelectedBuildableActor->GetRotation() + 90) % 360);
}

void ANCD_PlayerController::DeleteBuilding () {
	Rereference();
	if (!CitySystem || !SelectedBuildableActor) return;
	if (SelectedLocation.Equals(FVector::ZeroVector)) return;

	//cannot remove a building if it has a building event
	if (CheckBuildingStatus ()) {
		OnCantDemolishBuilding ();
		return;
	}

	const bool canSell = CitySystem->SellBuilding(SelectedBuildableActor, SelectedLocation);
	if (!canSell) return;
	APlacedBuilding** placedBuildingPointer = SelectedBuildableActor->GetStoredBuildings ().Find (SelectedLocation);
	if (placedBuildingPointer) {
		APlacedBuilding* placedBuilding = *placedBuildingPointer;
		placedBuilding->OnDelete ();
		GetWorld ()->DestroyActor (placedBuilding);
	}
	SelectedBuildableActor->OnDelete(SelectedLocation);
	SelectedBuildableActor->UpdateDecorations();
	RemoveHighlight ();

	SelectedLocation = FVector::ZeroVector;
	OnDeleteBuilding ();
}

void ANCD_PlayerController::OnDeleteBuilding_Implementation () {

}

bool ANCD_PlayerController::CheckBuildingStatus()
{
	if (SelectedLocation.Equals(FVector::ZeroVector)) return false;
	Rereference();
	if (CitySystem) {
		FBuildingStats* foundBuildingStats = CitySystem->GetBuildingByLocation(SelectedLocation);
		if (foundBuildingStats && foundBuildingStats->BuildingStatus != EBuildingStatus::ES_NONE) {
			return true;
		}
	}
	return false;
}

bool ANCD_PlayerController::SelectedBuildingRemovable() {
	FBuildingStats* building;
	if (SelectedLocation != FVector (0.0f, 0.0f, 0.0f)) {
		building = CitySystem->GetBuildingByLocation (SelectedLocation);
		if (building) return building->bIsRemovable;
	}
	return false;
}

void ANCD_PlayerController::OnOpenDescriptionWindow_Implementation (ABuildableActor* pBuildableActor) {

}

ABuildableActor* ANCD_PlayerController::GetSelectedBuildableActor()
{
	return SelectedBuildableActor;
}

void ANCD_PlayerController::DisableBuildingMode()
{
	if (!bBuildingModeEnabled) return;
	bBuildingModeEnabled = false;
	if (SelectedBuildableActor) SelectedBuildableActor->ClearPreviewMesh();
	OnDisableBuildingMode ();
}

void ANCD_PlayerController::EnableBuildingMode()
{
	Rereference();
	if (bBuildingModeEnabled) return;
	bBuildingModeEnabled = true;
	if (SelectedBuildableActor) SelectedBuildableActor->ClearPreviewMesh();

	//Collapse any visible building description window.
	ABuildingDescriptionActor* ABDA = GameState->GetDescriptionActor();
	ABDA->SetVisible(ESlateVisibility::Collapsed);
	OnEnableBuildingMode ();
}

void ANCD_PlayerController::OnDisableBuildingMode_Implementation () {

}

void ANCD_PlayerController::OnEnableBuildingMode_Implementation () {

}

void ANCD_PlayerController::OnSwitchEntity()
{

}

void ANCD_PlayerController::OnResidenceBuilt_Implementation (FBuildingStats pBuildingStats) {

}

void ANCD_PlayerController::OnWorkplaceBuilt_Implementation (FBuildingStats pBuildingStats) {

}

bool ANCD_PlayerController::IsBuildableActor(FHitResult pHitResult)
{
	if (!pHitResult.GetActor()) return false;

	bool bIsBuildableActor = false;

	pHitResult.Actor->IsA(ABuildableActor::StaticClass()) ? bIsBuildableActor = true : bIsBuildableActor = false;

	return bIsBuildableActor;
}

void ANCD_PlayerController::HighlightBuilding(FHitResult pHitResult, bool pAlsoShowMessage /* = false */, bool pShowHighlight /* = true */) {
	Rereference();
	if (!pAlsoShowMessage || (CheckBuildingStatus() && !bShowWarning)) RemoveHighlight (); //Implement some form of "you can't demolish this" message!
	if (bBuildingModeEnabled || !SelectedBuildableActor || !pHitResult.bBlockingHit) return;

	FVector SnappedLocation = Grid::GetLocationOnGrid(Grid::GetGridVector(), pHitResult.Location);
	APlacedBuilding* HitPlacedActor = Cast<APlacedBuilding>(pHitResult.Actor);
	if (!HitPlacedActor) {
		UE_LOG(LogTemp, Error, TEXT("Could not Highlight building at SnappedLocation %s."), *SnappedLocation.ToString());
		return;
	}

	FBuildingStats& stats = HitPlacedActor->BuildingStats;

	//UE_LOG (LogTemp, Warning, TEXT ("Hit object: %s"), *stats.Name);

	ABuildableActor* foundBuildableActor = FindBuildableActorByName(stats.ID);

	//UE_LOG (LogTemp, Warning, TEXT ("Buildable actor: %s"), *foundBuildableActor->GetFName().ToString());

	if (!foundBuildableActor) return;
	/* Added line to make sure the selected buildable actor is set */
	SetSelectedBuildableActor(foundBuildableActor);
	SelectedLocation = HitPlacedActor->GetActorLocation();

	if (!NCD_Pawn) return;
	HighlightMaterials = HitPlacedActor->GetMeshComponent()->GetMaterials();

	FMaterialParameters materialParams;

	if (bUseCustomHighlightMaterials)
	{
		HitPlacedActor->GetPollutionMeshScalars(materialParams);
	}

	//Show highlight around selected asset if it's interactable and it should be highlighted.
	if (pShowHighlight && stats.bIsInteractable) NCD_Pawn->SelectAsset(HitPlacedActor->GetMeshComponent()->GetStaticMesh(), HitPlacedActor->GetMeshComponent()->GetComponentTransform(), HighlightMaterials, false, materialParams);

	if (pAlsoShowMessage) {
		if (bShowWarning || !bDemolishModeActive) {
			bBuildingMessageActive = true;
		}
		if (stats.bIsInteractable) {
			if (GameState) {
				if (bDemolishModeActive) {
					if (bShowWarning) {
						if (CheckBuildingStatus() || !stats.bIsRemovable) OnCantDemolishBuilding();
						else OnOpenDemolishWindow();
					}
					else DeleteBuilding();
				}
				else {
					OnOpenDescriptionWindow(foundBuildableActor);
				}
			}
		}
	}
}

void ANCD_PlayerController::CountOnTapHeld()
{
	FInputActionKeyMapping mapping = PlayerInput->GetKeysForAction(FName(TEXT("OnTap")))[0];
	OnTapHeldDownTime = GetInputKeyTimeDown(mapping.Key);
}

void ANCD_PlayerController::RemoveHighlight () {
	bBuildingMessageActive = false;
	NCD_Pawn->UnselectAsset ();
}

void ANCD_PlayerController::Rereference()
{
	if (!NCD_Pawn) {
		APawn * PossesedPawn = GetPawn();
		NCD_Pawn = Cast<ANCD_Pawn>(PossesedPawn);
	}
	if(!CitySystem)
		CitySystem = Cast<ACitySystemGameMode>(GetWorld()->GetAuthGameMode());
	if(!GameState)
		GameState = Cast<ANCD_GameState>(GetWorld()->GetGameState());
}

void ANCD_PlayerController::SetDemolishModeActive (bool pDemolishModeActive) {
	bDemolishModeActive = pDemolishModeActive;
	OnDemolishModeChanged (pDemolishModeActive);
	OnDemolishModeChanged_Implementation (pDemolishModeActive);
}

bool ANCD_PlayerController::GetDemolishModeActive () {
	return bDemolishModeActive;
}

void ANCD_PlayerController::SetShowWarning (bool pShowWarning) {
	bShowWarning = pShowWarning;
}

bool ANCD_PlayerController::getShowWarning () {
	return bShowWarning;
}

void ANCD_PlayerController::SetBuildingMessageActive (bool pBuildingMessageActive) {
	bBuildingMessageActive = pBuildingMessageActive;
}

void ANCD_PlayerController::CloseDescriptionWindow () {
	ABuildingDescriptionActor* ADA = GameState->GetDescriptionActor ();
	ADA->SetVisible (ESlateVisibility::Collapsed);
}

void ANCD_PlayerController::OnCantDemolishBuilding_Implementation () {

}

void ANCD_PlayerController::OnOpenDemolishWindow_Implementation () {

}

void ANCD_PlayerController::OnDemolishModeChanged_Implementation (bool pNewState) {
	RemoveHighlight ();
}

ANCD_Pawn* ANCD_PlayerController::GetNCDPawn () {
	return NCD_Pawn;
}

void ANCD_PlayerController::SetHighlightMaterials(TArray<UMaterialInterface*>& pMaterials)
{
	HighlightMaterials = pMaterials;
}

void ANCD_PlayerController::SetCollisionSkip(bool pTrue)
{
	bSkipCollisionCheck = pTrue;
}

/*
FBuildingStats ANCD_PlayerController::GetStatsByID(FName pID)
{
	for (int i = 0; i < BuildingStats.Num(); i++)
	{
		if (BuildingStats[i].ID == pID)
		{
			return BuildingStats[i];
		}
	}
	return FBuildingStats();
}*/
