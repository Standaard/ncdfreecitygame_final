// Fill out your copyright notice in the Description page of Project Settings.

#include "BuildableActor.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Materials/MaterialInterface.h"
#include "Grid.h"
#include "GridCollisionBox.h"
#include "Engine/StaticMesh.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "DecorationInstances.h"
#include "Kismet/GameplayStatics.h"
#include "NCD_PlayerController.h"
#include "CitySystemGameMode.h"
#include "BuildableRoad.h"
#include "Engine/DecalActor.h"
#include "Components/DecalComponent.h"
#include "PollutionAreaDecal.h"
#include "Components/InstancedStaticMeshComponent.h"
#include <Private/KismetTraceUtils.h>
#include <Engine/StaticMeshActor.h>
#include "PlacedBuilding.h"
#include "GoalManager.h"

// Sets default values
ABuildableActor::ABuildableActor () {
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SceneRootComponent = CreateDefaultSubobject<USceneComponent> (TEXT ("SceneRootComponent"));
	RootComponent = SceneRootComponent;
	RootComponent->SetMobility (EComponentMobility::Stationary);

	TutorialSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("TutorialSceneComponent"));
	TutorialSceneComponent->SetupAttachment(RootComponent);
	
	DecalComp = CreateDefaultSubobject<UPollutionAreaDecal>(TEXT("DecalComp"));
	DecalComp->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ABuildableActor::BeginPlay () {
	Super::BeginPlay ();
	PlayerController = Cast<ANCD_PlayerController> (UGameplayStatics::GetPlayerController (GetWorld (), 0));
	CitySystem = Cast<ACitySystemGameMode> (GetWorld ()->GetAuthGameMode ());

	if (!StaticMesh) return;

	SetSpawnOffset ();

	DecalComp->SetHiddenInGame(true);
}

AActor* ABuildableActor::SweepForActorInScene (FVector pSnappedLocation, UClass* pRoadClass) {
	FVector BoxSize = FVector (40, 40, 50);
	FCollisionShape BoxShape = FCollisionShape::MakeBox (BoxSize);
	TArray<FHitResult> HitResults;

	FComponentQueryParams params;

	GetWorld ()->SweepMultiByChannel (HitResults, pSnappedLocation, pSnappedLocation, FQuat::Identity, ECC_GameTraceChannel1, BoxShape, params);

	for (int i = 0; i < HitResults.Num (); i++) {
		if (HitResults[i].Actor->IsA (pRoadClass)) {
			bool isGoingBeKilled = HitResults[i].Actor->IsPendingKillPending();
			if (isGoingBeKilled) continue;
			LatestDetectedBuildingLocation = HitResults[i].GetActor()->GetActorLocation();
			return HitResults[i].GetActor();
		}
	}

	return nullptr;;
}

void ABuildableActor::UpdateDecorations () {
	int index = 0;
	for (int i = 0; i < DecorationInstances.Num (); i++) {
		if (!DecorationInstances[i]) continue;
		DecorationInstances[i]->UpdateDecoration (this, index);
		index++;
	}
}

// Called every frame
void ABuildableActor::Tick (float DeltaTime) {
	Super::Tick (DeltaTime);
}

FVector ABuildableActor::GetRotatedSnapLocation(FVector pLocation) {
	return Grid::GetLocationOnGrid(pLocation) - RotatedSpawnOffset;
}

void ABuildableActor::OnPreviewPlace (FTransform pTransform, bool pSkipCollisionCheck /* = false */, bool pIsFreeBuilding /* = false */) {
	if (!PreviewMesh || !StaticMesh) return;

	FVector TargetLocation = pTransform.GetLocation ();
	FVector SnappedLocation = Grid::GetLocationOnGrid (TargetLocation);
	//LETS NOT RUN THE PREVIEW ON THE SAME LOCATION OVER AND OVER AGAIN
	//if (LastPreviewLocation == SnappedLocation && LastRotation == Rotation) return;
	LastPreviewLocation = SnappedLocation;
	LastRotation = Rotation;
	bAllowedToBuild = false;

	RotatedSpawnOffset = SpawnOffset;
	RotatedSpawnOffset = RotatedSpawnOffset.RotateAngleAxis (Rotation, FVector (0, 0, 1));

	FVector RotatedDimensions = DimensionsInMeters;
	RotatedDimensions = RotatedDimensions.RotateAngleAxis (Rotation, FVector (0, 0, 1));
	
	if (!pSkipCollisionCheck) {
		CheckPlacementCollision (SnappedLocation);
		if (bAllowedToBuild) CheckSpecialCollision(SnappedLocation);
	}
	else
	{
		bAllowedToBuild = true;
	}

	if (PreviewMesh->bHidden) { 
		PreviewMesh->SetActorHiddenInGame(false);
		//PreviewMesh->GetMeshComponent()->SetStaticMesh (StaticMesh);
	}
	pTransform.SetLocation (SnappedLocation - RotatedSpawnOffset);

	if (!CitySystem->IsAllowedToBuildBuilding (GetBuildingStats (), pIsFreeBuilding)) {
		bAllowedToBuild = false;
	}

	for (int32 i = 0; i < PreviewMesh->GetMeshComponent()->GetNumMaterials(); i++) {
		CreateDynamicPreviewMaterial(i, bAllowedToBuild);
	}

	PreviewMesh->SetActorTransform (pTransform);

	if (CitySystem->PollutionManager->GetPollutionViewEnabled()) {
		if (BuildingStats.bCanPolluteInArea)
		{
			//DecalComp->SetHiddenInGame(!CitySystem->PollutionManager->GetPollutionViewEnabled());
			//Enable Pollution Area on all polluting buildings
			DecalComp->SetHiddenInGame(false);
			FRotator decalCompRotation = FRotator(-90.0f, Rotation, 0);
			DecalComp->SetWorldRotation(decalCompRotation);
			DecalComp->SetWorldLocation((SnappedLocation - RotatedSpawnOffset) + FVector::UpVector);
		}
	}
	else
	{
		DecalComp->SetHiddenInGame(true);
	}
}


void ABuildableActor::CheckPlacementCollision (FVector pSnappedLocation) {
	//RotatedSpawnOffset = SpawnOffset;
	//RotatedSpawnOffset = RotatedSpawnOffset.RotateAngleAxis (Rotation, FVector (0, 0, 1));
	bAllowedToBuild = true;

	FVector BuildingSize = DimensionsInMeters - FVector (1, 1, 1);

	FVector BoxSize = (FVector (50, 50, 50) + (BuildingSize * 50.f)) - FVector (10.f, 10.f, 0.f);
	BoxSize = BoxSize.RotateAngleAxis (Rotation, FVector (0, 0, 1));
	BoxSize = FVector (FMath::Abs (BoxSize.X), FMath::Abs (BoxSize.Y), FMath::Abs (BoxSize.Z));
	FCollisionShape BoxShape = FCollisionShape::MakeBox (BoxSize);
	FVector checkLocation = pSnappedLocation + FVector(0, 0, 50) - RotatedSpawnOffset;
	CheckPlacementCollisionActor (checkLocation, BoxShape);
	
}

void ABuildableActor::CheckPlacementCollisionActor (FVector pCheckLocation, FCollisionShape pBoxShape) {
	TArray<FHitResult> HitResults;
	FComponentQueryParams params;

	//Perform sweep to see if there are buildings or meshes in the way
	GetWorld ()->SweepMultiByChannel (HitResults, pCheckLocation, pCheckLocation, FQuat::Identity, ECC_Visibility, pBoxShape, params);
	for (int i = 0; i < HitResults.Num (); i++) {
		if (HitResults[i].Actor->IsA (APlacedBuilding::StaticClass ()) || HitResults[i].Actor->IsA (AStaticMeshActor::StaticClass ())) {
			bAllowedToBuild = false;
			return;
		}
	}
}

void ABuildableActor::CheckSpecialCollision(FVector SnappedLocation)
{

}

void ABuildableActor::SetRotation (int pRotation) {
	Rotation = pRotation;
}

int ABuildableActor::GetRotation()
{
	return Rotation;
}

FVector ABuildableActor::OnBuildableActorPlace (FTransform pTransform) {
	if (!bAllowedToBuild) return FVector::ZeroVector;
	FVector placedActorLocation = GetRotatedSnapLocation(pTransform.GetLocation());
	StoredBuildings.Add(placedActorLocation, LatestCreatedBuilding);

	bAllowedToBuild = false;
	return placedActorLocation;
}

void ABuildableActor::OnDelete(FVector pLocation) {
	StoredBuildings.Remove(pLocation);
}

void ABuildableActor::SetBuildingData (FBuildingStats pBuildingStats) {
	if (pBuildingStats.BuildingBlueprint.IsNull()) return;
	BuildingStats = pBuildingStats;
	BuildingBlueprint = ACitySystemGameMode::GetBuildingBlueprintFromStats(pBuildingStats);

	StaticMesh = BuildingBlueprint->GetMeshComponent()->GetStaticMesh();
	Identifier = pBuildingStats.ID;

	SetSpawnOffset();
	if (!PreviewMesh) CreatePreviewMesh();

	DecorationInstances.Init (nullptr, pBuildingStats.Decorations.Num ());

	for (int i = 0; i < pBuildingStats.Decorations.Num (); i++) {
		ADecorationInstances* inst = GetWorld ()->SpawnActor<ADecorationInstances> (ADecorationInstances::StaticClass (), FVector::ZeroVector, FRotator::ZeroRotator);
		if (!inst) return;
#if WITH_EDITOR
		inst->SetActorLabel(BuildingStats.Name);
#endif
		inst->SetDecoration (pBuildingStats.Decorations[i].Mesh, pBuildingStats.Decorations[i].Material, pBuildingStats.Decorations[i].RelativeToParent);
		DecorationInstances.Add (inst);
	}

	if (pBuildingStats.MeshMaterials.Num () > 0) StaticMeshMaterials = pBuildingStats.MeshMaterials;

	for (int i = 0; i < StaticMeshMaterials.Num (); i++) {
		UMaterialInstanceDynamic* DynMaterial = UMaterialInstanceDynamic::Create (StaticMeshMaterials[i], this);
		dynamicMaterial = DynMaterial;
	}

	if (pBuildingStats.bCanPollute) {
		DecalComp->SetPollutionSize(pBuildingStats);
		DecalComp->SetPollutionColorFromStats(pBuildingStats, CitySystem->GetPollutionAreaVisualMin(), CitySystem->GetPollutionAreaVisualMax());
	}
}

void ABuildableActor::CreatePreviewMesh()
{
	UWorld* world = GetWorld();

	FActorSpawnParameters spawnParameters;
	//Use the BuildingBlueprint as a template so SpawnActor can copy all of the values
	spawnParameters.Template = BuildingBlueprint;
	PreviewMesh = world->SpawnActor<APlacedBuilding>(BuildingBlueprint->GetClass(),FVector::ZeroVector,FRotator::ZeroRotator,spawnParameters);
	PreviewMesh->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
	PreviewMesh->SetActorHiddenInGame(false);
#if WITH_EDITOR
	PreviewMesh->SetActorLabel(FString("Preview_" + BuildingStats.Name));
#endif
	UStaticMeshComponent* meshComp = PreviewMesh->GetMeshComponent();
	if (meshComp) {
		meshComp->SetStaticMesh(StaticMesh);
		meshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		meshComp->CastShadow = false;
		meshComp->SetMobility(EComponentMobility::Movable);
		meshComp->SetGenerateOverlapEvents(false);
	}

	//Disable all particle systems
	TArray<UParticleSystemComponent*> particleSystems;
	PreviewMesh->GetComponents<UParticleSystemComponent>(particleSystems,false);
	for (auto* particleSystem : particleSystems)
	{
		if (!particleSystem) continue;
		particleSystem->DeactivateSystem();
	}
}

void ABuildableActor::PlaceBuilding(FTransform pBuildingTransform, FPlacedBuildingParameters pParameters)
{
	if (!bAllowedToBuild) return;
	if (!PlayerController) return;

	FBuildingStats Building = BuildingStats;
	ClearPreviewMesh();
	Building.bIsInteractable = pParameters.bIsInteractable;
	Building.bIsRemovable = pParameters.bIsRemovable;
	Building.ConnectedRoadLocation = GetLatestDetectedBuildingLocation();

	/// Blueprint Check
	FString contextString;
	FBuildingBlueprints* buildingBPClass = BuildingStats.BuildingBlueprint.GetRow<FBuildingBlueprints>(contextString);
	if (!buildingBPClass)
	{
		UE_LOG(LogTemp, Error, TEXT("Building Blueprint not assigned for building %s"), *BuildingStats.Name);
		return;
	}

	APlacedBuilding* placedBuilding = GetWorld()->SpawnActor<APlacedBuilding>(buildingBPClass->PlacedBuildingBlueprint, pBuildingTransform);
	placedBuilding->SetCitySystem(CitySystem);
	LatestCreatedBuilding = placedBuilding;
	//Attach to this BuildableActor to avoid a cluttered World Outliner
	placedBuilding->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);

	Building.Rotation = FRotator(0, Rotation, 0);

	FVector placedBuildingLocation = OnBuildableActorPlace(pBuildingTransform);
	if (pParameters.bSkipLocationOffset)
	{
		Building.Location = pBuildingTransform.GetLocation();
		Building.Rotation = pBuildingTransform.GetRotation().Rotator();
	}
	else
	{
		Building.Location = placedBuildingLocation;
		placedBuilding->SetActorLocation(placedBuildingLocation);
	}

	placedBuilding->SetBuildingStats(Building);

	if (!CitySystem->BuyBuilding(this, placedBuilding, pParameters))
	{
		StoredBuildings.Remove(Building.Location);
		return;
	}
	UpdateDecorations();
	if (Building.BuildingCategory == EBuildingCategory::EBC_RESIDENCE) PlayerController->OnResidenceBuilt(Building);
	else if (Building.JobCapacity > 0) PlayerController->OnWorkplaceBuilt(Building);

	if (!CitySystem->GetTutorial()->HasCompletedTutorial()) CitySystem->GetTutorial()->OnBuildEvent(Building);
	if (!pParameters.bLoadFromScenario) CitySystem->GetGoalManager ()->OnBuildEvent (Building);

	//Only add income and expenses when the building doesn't offer jobs, because that's handled separately in "AddPopToLocation" in PopulationManager.cpp
	if (Building.JobCapacity == 0) {
		CitySystem->AddBuildingIncome (Building.Location, Building.Income);
		CitySystem->AddBuildingExpense (Building.BuildingType, Building.UpkeepCost.MoneyCost, 1);
	}
}

void ABuildableActor::SetAllowedToBuild (bool pAllowedToBuild) {
	bAllowedToBuild = pAllowedToBuild;
}

APlacedBuilding* ABuildableActor::GetFirstBuilding()
{
	if (StoredBuildings.Num() == 0) return nullptr;
	for (auto& building : StoredBuildings)
	{
		return building.Value;
	}
	return nullptr;
}

FName ABuildableActor::GetIdentifier () {
	return Identifier;
}

void ABuildableActor::SetSpawnOffset () {
	FBox BoundingBox = StaticMesh->GetBoundingBox ();
	FVector BoundingBoxExtension = BoundingBox.GetExtent ();
	FVector DifferenceVector = FVector (
		FMath::Fmod (BoundingBoxExtension.X, 50.f),
		FMath::Fmod (BoundingBoxExtension.Y, 50.f),
		FMath::Fmod (BoundingBoxExtension.Z, 50.f)
	);

	if (DifferenceVector.X == 0) { DifferenceVector.X = 50.f; }
	if (DifferenceVector.Y == 0) { DifferenceVector.Y = 50.f; }
	if (DifferenceVector.Z == 0) { DifferenceVector.Z = 50.f; }

	BoundingBoxOffset = (FVector (50.f, 50.f, 50.f) - DifferenceVector);
	BoundingBoxExtension += BoundingBoxOffset;

	(int)BoundingBoxExtension.X % 100 == 0 ? SpawnOffset.X = 50.f : SpawnOffset.X = 0.f;
	(int)BoundingBoxExtension.Y % 100 == 0 ? SpawnOffset.Y = 50.f : SpawnOffset.Y = 0.f;
	//BoundingBoxOffset.Z = 0;

	/*(int)BoundingBoxExtension.X % 100 != 0 ? BoundingBoxOffset.X = 0 : BoundingBoxOffset.X += -50.f;
	(int)BoundingBoxExtension.Y % 100 != 0 ? BoundingBoxOffset.Y = 0 : BoundingBoxOffset.Y += -50.f;*/

	DimensionsInMeters = BoundingBoxExtension / 50.f;
}

void ABuildableActor::ClearPreviewMesh () {
	if (!PreviewMesh) return;
	DecalComp->SetHiddenInGame(true);
	PreviewMesh->SetActorHiddenInGame(true);
	LastPreviewLocation = FVector (9999, 9999, 9999);
}


UStaticMeshComponent* ABuildableActor::GetStaticMeshComponent () {
	UStaticMeshComponent* smComp = (UStaticMeshComponent*)GetComponentsByClass (UStaticMeshComponent::StaticClass ())[0];
	UE_LOG (LogTemp, Warning, TEXT ("TEST"));
	return smComp;
}

void ABuildableActor::CreateDynamicPreviewMaterial(int pMaterialIndex, bool pCanPlace)
{
	PreviewMesh->GetMeshComponent()->CreateAndSetMaterialInstanceDynamicFromMaterial(pMaterialIndex, PreviewMesh->GetMeshComponent()->GetMaterial(pMaterialIndex));
	if(pCanPlace)
		PreviewMesh->GetMeshComponent()->SetVectorParameterValueOnMaterials("Emissive_Color", PreviewMaterialCanPlace);
	else
		PreviewMesh->GetMeshComponent()->SetVectorParameterValueOnMaterials("Emissive_Color", PreviewMaterialCannotPlace);
}