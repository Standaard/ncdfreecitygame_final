// Fill out your copyright notice in the Description page of Project Settings.

#include "Grid.h"
#include <Kismet/KismetMathLibrary.h>

Grid::Grid()
{
}

Grid::~Grid()
{
}

float Grid::GridSize = 100.0f;

FVector Grid::GetGridVector()
{
	return FVector(GridSize, GridSize, GridSize);
}

FVector Grid::GetLocationOnGridXY(FVector& Location)
{
	FVector returnLocation = GetLocationOnGrid(Location);
	returnLocation.Z = Location.Z;
	return returnLocation;
}

FVector Grid::GetLocationOnGrid(FVector GridSize, FVector Location)
{
	FVector DifferenceVector = FVector(FMath::Fmod(Location.X, GridSize.X),
		FMath::Fmod(Location.Y, GridSize.Y),
		FMath::Fmod(Location.Z, GridSize.Z));

	FVector SnapLocation = Location - DifferenceVector;
	FVector GridSizeHalf = GridSize / 2;
	FVector CenteredSnapLocation = FVector(FMath::Sign(Location.X) * GridSizeHalf.X,
		FMath::Sign(Location.Y) * GridSizeHalf.Y,
		FMath::Sign(Location.Z) * GridSizeHalf.Z);

	if (CenteredSnapLocation.X == 0) { CenteredSnapLocation.X = GridSizeHalf.X; }
	if (CenteredSnapLocation.Y == 0) { CenteredSnapLocation.Y = GridSizeHalf.Y; }
	if (CenteredSnapLocation.Z == 0) { CenteredSnapLocation.Z = GridSizeHalf.Z; }

	return CenteredSnapLocation + SnapLocation;

	//return FVector(UKismetMathLibrary::GridSnap_Float(Location.X, GridSize.X), UKismetMathLibrary::GridSnap_Float(Location.Y, GridSize.Y), UKismetMathLibrary::GridSnap_Float(Location.Z, GridSize.Z));
}

FVector Grid::GetLocationOnGrid(FVector Location)
{
	return GetLocationOnGrid(GetGridVector(), Location);
}

FVector Grid::GetOffsetBoundingBoxAndGrid(FVector pGridSize, FVector pInputVector)
{
	FVector GridSizeHalf = pGridSize / 2;

	return FVector(GridSizeHalf.X - FMath::Fmod(pInputVector.X, GridSizeHalf.X),
		GridSizeHalf.Y - FMath::Fmod(pInputVector.Y, GridSizeHalf.Y),
		GridSizeHalf.Z - FMath::Fmod(pInputVector.Z, GridSizeHalf.Z));
}

bool Grid::IsInRange(TArray<FVector> pLocations, FVector pCheckLocation, float pRange /*= 100.f*/)
{
	for (int i = 0; i < pLocations.Num(); i++)
	{
		if (FVector::Distance(pLocations[i], pCheckLocation) <= pRange) { return true; }
	}
	return false;
}
