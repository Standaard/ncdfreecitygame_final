// Fill out your copyright notice in the Description page of Project Settings.

#include "public/ScenarioImporter.h"
#include "public/JsonImporter.h"
#include "public/BuildingStats.h"
#include "Engine/DataTable.h"
#include "JsonImporter.h"
#include "PlatformFilemanager.h"
#include "GenericPlatformFile.h"
#include "Paths.h"
#include "CitySystemGameMode.h"
#include "NCD_PlayerController.h"
#include "Kismet/GameplayStatics.h"

AScenarioImporter::AScenarioImporter () {

}

void AScenarioImporter::BeginPlay () {
	Super::BeginPlay ();
	fromPathBaseString = FPaths::ProjectContentDir () + FolderToImport + "/";
	toPathBaseString = FPaths::ProjectContentDir () + "Assets/";
	JsonImporter = NewObject<UJsonImporter> ();
	JsonImporter->Initialize (this);
}

void AScenarioImporter::OnClickImport () {
	//Import textures
	ImportFolder ("Textures");

	//Import materials
	ImportFolder ("Materials");

	//Import models
	ImportFolder ("Models");

	//Import datatables
	ImportFolder ("Datatables");

	//Import JSON
	ImportFolder ("JSON");

	ImportJsons ();

	//Temporary; The datatables will be read by their respective classes in the game and should be stored here for lookup purposes.
	LoadDatatables ();
}

void AScenarioImporter::ImportJsons () {
	bool success = false;
	success = JsonImporter->ReadBaseJSON ("Base");

	ACitySystemGameMode* gameMode = (ACitySystemGameMode*)GetWorld ()->GetAuthGameMode ();
	gameMode->LoadDatatables ();
	playerController = (ANCD_PlayerController*)UGameplayStatics::GetPlayerController (GetWorld (), 0);
	//ABuildableActor* ba = playerController->FindBuildableActorByName (FName ("Road"));

	success = JsonImporter->ReadSceneJSON ("Scene");
	//else UE_LOG (LogTemp, Warning, TEXT ("Reading one of the JSONs failed!"));
}

void AScenarioImporter::ImportFolder (FString pFolderName) {
	//Local because I couldn't get it to work in the header
	FPlatformFileManager* fileManager = new FPlatformFileManager ();
	fileManager->GetPlatformFile (TEXT ("File Manager"));

	FString fromPathString = fromPathBaseString + pFolderName + "/";
	FString toPathString = toPathBaseString + pFolderName + "/";
	const TCHAR* fromPath = *fromPathString;
	const TCHAR* toPath = *toPathString;
	bool success = fileManager->GetPlatformFile ().CopyDirectoryTree (toPath, fromPath, false);
	//UE_LOG (LogTemp, Warning, TEXT ("Copying %s success: %s"), pFolderName, success ? TEXT ("True") : TEXT ("False"));
}

void AScenarioImporter::LoadDatatables () {
	//LoadDatatable ("DT_Buildings");
}

void AScenarioImporter::LoadDatatable (FString pFileName) {
	//Temporary; The datatables will be read by their respective classes in the game and should be stored here for lookup purposes.
		//Loading the mesh and setting it
	FString contextString;
	FString filePathString = "/Game/Assets/Datatables/" + pFileName;
	const TCHAR* filePath = *filePathString;
	UDataTable* buildingsDT = LoadObject<UDataTable> (nullptr, filePath);
	TArray<FName> rowNames;
	rowNames = buildingsDT->GetRowNames ();

	//Loop through all rows in datatable
	for (auto& name : rowNames) {
		FBuildingStats* buildingStats = buildingsDT->FindRow<FBuildingStats> (name, contextString);
		if (buildingStats) {
			UE_LOG (LogTemp, Warning, TEXT ("Location: %s"), *buildingStats->Location.ToString ());
			UE_LOG (LogTemp, Warning, TEXT ("Rotation: %s"), *buildingStats->Rotation.ToString ());
			UE_LOG (LogTemp, Warning, TEXT ("Name: %s"), *buildingStats->Name);
			UE_LOG (LogTemp, Warning, TEXT ("Visitor capacity: %d"), buildingStats->VisitorCapacity);
			UE_LOG (LogTemp, Warning, TEXT ("Is removable: %s"), buildingStats->bIsRemovable ? TEXT ("True") : TEXT ("False"));
		} else {
			UE_LOG (LogTemp, Warning, TEXT ("Row: %s not found!"), *name.ToString ());
		}
	}
}

UJsonImporter* AScenarioImporter::GetJsonImporter () {
	return JsonImporter;
}

ANCD_PlayerController* AScenarioImporter::GetPlayerController () {
	return playerController;
}