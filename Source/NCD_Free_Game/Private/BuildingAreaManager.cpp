// Fill out your copyright notice in the Description page of Project Settings.

#include "BuildingAreaManager.h"
#include "BuildingArea.h"
#include "BuildingStats.h"
#include "CitySystemGameMode.h"
#include <DrawDebugHelpers.h>
#include <Components/BoxComponent.h>
#include <Kismet/KismetSystemLibrary.h>
#include <Components/HierarchicalInstancedStaticMeshComponent.h>

// Sets default values
ABuildingAreaManager::ABuildingAreaManager()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void ABuildingAreaManager::BeginPlay()
{
	Super::BeginPlay();
	CitySystem = Cast<ACitySystemGameMode>(GetWorld()->GetAuthGameMode());
}

UBuildingArea* ABuildingAreaManager::CreateBuildingArea(APlacedBuilding* pOwner)
{
	UBuildingArea* newBuildingArea = NewObject<UBuildingArea>();
	newBuildingArea->SetOwner(pOwner);
	return newBuildingArea;
}

bool ABuildingAreaManager::GetBuildingsByBoxTrace(FVector pLocation, FVector pExtent, TArray<APlacedBuilding*>& pOutBuildings, bool pIgnoreOwner)
{
	FCollisionShape boxShape = FCollisionShape::MakeBox(pExtent);

	TArray<FHitResult> hitResults;
	bool isHit = UKismetSystemLibrary::BoxTraceMulti(GetWorld(), pLocation, pLocation, pExtent, FRotator::ZeroRotator, UEngineTypes::ConvertToTraceType(ECollisionChannel::ECC_GameTraceChannel1), false, IgnoreBoxTraceActors, EDrawDebugTrace::None, hitResults, pIgnoreOwner,FLinearColor::Red,FLinearColor::Green,2.0f);
	if (!isHit) return false;

	FTransform tempTransform;
	for (auto& hit : hitResults) {
		APlacedBuilding* placedBuilding = Cast<APlacedBuilding>(hit.GetActor());
		if (!placedBuilding) continue;
		pOutBuildings.Add(placedBuilding);
	}
	return true;
}
