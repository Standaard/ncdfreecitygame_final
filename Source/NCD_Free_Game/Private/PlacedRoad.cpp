// Fill out your copyright notice in the Description page of Project Settings.

#include "PlacedRoad.h"
#include "Kismet/KismetSystemLibrary.h"
#include "PlacedUtility.h"

void APlacedRoad::BeginPlay() 
{
	Super::BeginPlay();
	
	if(!RoadStraightMaterial)
		UE_LOG(LogTemp, Error, TEXT("[PlacedRoad] RoadStraightMaterial is not assigned!"));
	if(!RoadTurnMaterial)
		UE_LOG(LogTemp, Error, TEXT("[PlacedRoad] RoadTurnMaterial is not assigned!"));
	if (!RoadTSplitMaterial)
		UE_LOG(LogTemp, Error, TEXT("[PlacedRoad] RoadTSplitMaterial is not assigned!"));
	if (!RoadCrossroadMaterial)
		UE_LOG(LogTemp, Error, TEXT("[PlacedRoad] RoadCrossroadMaterial is not assigned!"));
	if (!MeshComponent)
		UE_LOG(LogTemp, Error, TEXT("[PlacedRoad] MeshComponent is not assigned!"));
}

void APlacedRoad::UpdateRoadMaterial(ERoadType pRoadType)
{
	UMaterialInterface* newMaterial = nullptr;
	switch (pRoadType) {
	case ERoadType::EB_STRAIGHT: 
		newMaterial = RoadStraightMaterial;
		break;
	case ERoadType::EB_TURN: 
		newMaterial = RoadTurnMaterial;
		break;
	case ERoadType::EB_TSPLIT: 
		newMaterial = RoadTSplitMaterial;
		break;
	case ERoadType::EB_CROSS:
		newMaterial = RoadCrossroadMaterial;
		break;
	default:
		newMaterial = RoadStraightMaterial;
		break;
	}
	if (!newMaterial)
	{
		UE_LOG(LogTemp, Error, TEXT("[APlacedRoad] SetRoadMaterial: Material not found!"));
		return;
	}
	MeshComponent->SetMaterial(0, newMaterial);
	RoadType = pRoadType;

	DestroyUtilityOnTop ();
}

void APlacedRoad::OnDelete () {
	Super::OnDelete ();
	DestroyUtilityOnTop ();
}

void APlacedRoad::DestroyUtilityOnTop () {
	FHitResult hitResult;
	bool isHit = UKismetSystemLibrary::BoxTraceSingle (this, this->GetActorLocation (), this->GetActorLocation (), FVector (20.0f, 20.0f, 50.0f), FRotator::ZeroRotator, UEngineTypes::ConvertToTraceType (ECollisionChannel::ECC_GameTraceChannel1), false, IgnoreBoxTraceActors, EDrawDebugTrace::None, hitResult, true);
	//Destroy the any utility actor (such as trashbins) on the road.
	if (isHit && hitResult.Actor->IsA (APlacedUtility::StaticClass ())) hitResult.Actor->Destroy ();
}