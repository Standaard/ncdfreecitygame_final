// Fill out your copyright notice in the Description page of Project Settings.

#include "PlacedBuilding.h"
#include <Components/StaticMeshComponent.h>
#include "PollutionAreaDecal.h"
#include "CitySystemGameMode.h"
#include "MaterialParameters.h"

// Sets default values
APlacedBuilding::APlacedBuilding()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//This scenecomponent is not a staticmeshcomponent, however upon changing it will clear out the blueprints
	//If this becomes a problem, be ready to create all building blueprints again
	SceneComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Scene Component"));
	RootComponent = SceneComponent;
	
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Building Mesh"));
	MeshComponent->SetupAttachment(RootComponent);
	MeshComponent->SetMobility(EComponentMobility::Movable);
	MeshComponent->SetGenerateOverlapEvents(false);
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
	MeshComponent->SetReceivesDecals(false);

	FCollisionResponseContainer container;
	container.SetAllChannels(ECR_Ignore);
	container.SetResponse(ECC_Visibility, ECollisionResponse::ECR_Block);
	container.SetResponse(ECC_Camera, ECollisionResponse::ECR_Block);
	container.SetResponse(ECC_GameTraceChannel1, ECollisionResponse::ECR_Block);
	MeshComponent->SetCollisionResponseToChannels(container);

	DecalPollutionArea = CreateDefaultSubobject<UPollutionAreaDecal>(TEXT("DecalPollutionAreaComp"));
	DecalPollutionArea->SetupAttachment(MeshComponent);
}

void APlacedBuilding::OnDelete () {

}

// Called when the game starts or when spawned
void APlacedBuilding::BeginPlay()
{
	Super::BeginPlay();
	for (int32 i = 0; i < MeshComponent->GetNumMaterials(); i++)
	{
		UMaterialInstanceDynamic* MID = MeshComponent->CreateAndSetMaterialInstanceDynamic(i);
	}
	SetPollutionColor(0.5f);
}

bool APlacedBuilding::SetPollutionColor(float pCurveScalar)
{
	const int materialsNum = MeshComponent->GetNumMaterials();
	if (materialsNum == 0) return false;

	pCurveScalar = FMath::Clamp(pCurveScalar, 0.0f, 1.0f);
	for (int32 i = 0; i < materialsNum; i++)
	{
		MeshComponent->SetScalarParameterValueOnMaterials(PollutionMaterialScalarName, pCurveScalar);
	}
	PollutionMaterialValue = pCurveScalar;
	return true;
}

void APlacedBuilding::GetPollutionMeshScalars(FMaterialParameters& pParams)
{
	pParams.FloatScalars.Add(PollutionMaterialScalarName, PollutionMaterialValue);
}

void APlacedBuilding::SetPollutionDecalVisible(bool pVisible)
{
	if (!bEnableDecal) return;
	DecalPollutionArea->SetHiddenInGame(!pVisible);
}

// Called every frame
void APlacedBuilding::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlacedBuilding::SetBuildingStats(FBuildingStats& pBuildingStats)
{
	BuildingStats = pBuildingStats;
	bEnableDecal = pBuildingStats.bCanPolluteInArea;
	//Only set the decal if the owner can pollute
	if (bEnableDecal)
	{
		DecalPollutionArea->SetPollutionSize(pBuildingStats);
		DecalPollutionArea->SetPollutionColorFromStats(pBuildingStats, CitySystem->PollutionManager->GetPollutionAreaVisualMin(), CitySystem->PollutionManager->GetPollutionAreaVisualMax());
	}
}

void APlacedBuilding::SetCitySystem(ACitySystemGameMode* pCitySystem)
{
	CitySystem = pCitySystem;
}

