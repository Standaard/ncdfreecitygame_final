// Fill out your copyright notice in the Description page of Project Settings.

#include "TrafficNode.h"

int ATrafficNode::GlobalID = 0;


// Sets default values
ATrafficNode::ATrafficNode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

void ATrafficNode::BeginPlay()
{
	Super::BeginPlay();
	ID = GlobalID;
	GlobalID++;
}



