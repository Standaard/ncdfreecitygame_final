// Fill out your copyright notice in the Description page of Project Settings.

#include "BuildableBuilding.h"
#include "BuildableRoad.h"
#include <DrawDebugHelpers.h>
#include "CitySystemGameMode.h"
#include "BuildingAreaManager.h"


void ABuildableBuilding::CheckSpecialCollision(FVector pSnappedLocation) {

	//Obtain the original blueprint
	APlacedBuilding* blueprint = ACitySystemGameMode::GetBuildingBlueprintFromStats(BuildingStats);
	if (!blueprint) return;

	TArray<FVector>& checkLocations = blueprint->GetRoadCollisionHotspot().Locations;
	//used to early out
	AActor* foundRoad = nullptr;
	for (FVector& location : checkLocations)
	{
		//rotate the check locations with Rotation
		FVector rotatedLocation = location.RotateAngleAxis(Rotation, FVector::UpVector);
		//set the rotatedLocation next to the building
		rotatedLocation += pSnappedLocation - RotatedSpawnOffset;
		foundRoad = SweepForActorInScene(rotatedLocation, RoadClass);

		if (bDrawConnectionLocations) DrawDebugBox(GetWorld(), rotatedLocation, FVector::OneVector * 25.0f, foundRoad ? FColor::Green : FColor::Red, false, 2.0f, 0, 2.0f);

		if (foundRoad)
		{
			FrontLocation = rotatedLocation;
			break;
		}
	}
	bAllowedToBuild = foundRoad != nullptr;
}
