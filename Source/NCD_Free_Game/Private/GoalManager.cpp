// Fill out your copyright notice in the Description page of Project Settings.

#include "GoalManager.h"
#include "CitySystemGameMode.h"

UGoalManager::UGoalManager () {

}

UGoalManager::~UGoalManager () {

}

void UGoalManager::Initialize (ACitySystemGameMode* pGameMode) {
	gameMode = pGameMode;
}

void UGoalManager::LoadGoalData (FString pDTName) {
	FString filePathString = "/Game/Assets/Datatables/" + pDTName;
	const TCHAR* filePath = *filePathString;
	UDataTable* datatable = LoadObject<UDataTable> (nullptr, filePath);

	if (datatable) {
		FString ContextString;
		TArray<FName> RowNames = datatable->GetRowNames ();
		for (auto& RowName : RowNames) {
			FScenarioGoal* scenarioGoal = datatable->FindRow<FScenarioGoal> (RowName, ContextString);
			if (scenarioGoal) {
				FScenarioGoal dereferencedGoal = *scenarioGoal;
				dereferencedGoal.ID = *RowName.ToString ();
				scenarioGoals.Add (dereferencedGoal.ID, dereferencedGoal);
				SetGoalTypeActive (scenarioGoal->GoalType);

				switch (scenarioGoal->GoalType) {
					case EGoalTypes::BuildGoal:
						buildGoals.Add (*scenarioGoal);
						break;
					case EGoalTypes::PopulationGoal:
						populationGoals.Add (*scenarioGoal);
						break;
					case EGoalTypes::PollutionGoal:
						pollutionGoals.Add (*scenarioGoal);
						break;
					case EGoalTypes::EmploymentGoal:
						employmentGoals.Add (*scenarioGoal);
						break;
					default:
						break;
				}
			}
			scenarioGoal = nullptr;
		}
	}
}

void UGoalManager::OnBuildEvent (FBuildingStats pBuilding) {
	//Return if there are no build goals in this scenario
	if (!IsGoalTypeActive (EGoalTypes::BuildGoal)) return;

	for (int i = 0; i < buildGoals.Num (); i++) {
		FScenarioGoal* scenarioGoal = &buildGoals[i];
		if (scenarioGoal->buildingToBuild == pBuilding.BuildingType) {
			scenarioGoal->AmountBuilt++;
			if (scenarioGoal->AmountBuilt >= scenarioGoal->AmountToBuild) {
				scenarioGoal->Completed = true;
				UE_LOG (LogTemp, Warning, TEXT ("Specific scenario goal completed: %s"), scenarioGoal->Completed ? TEXT ("True") : TEXT ("False"));
			}
		}
	}
	OnEventHandled ();
}

void UGoalManager::OnEventHandled () {
	bool allGoalsReached = false;
	allGoalsReached = GoalSetCompleted (buildGoals);
	allGoalsReached = GoalSetCompleted (populationGoals);
	allGoalsReached = GoalSetCompleted (pollutionGoals);
	allGoalsReached = GoalSetCompleted (employmentGoals);
	UE_LOG (LogTemp, Warning, TEXT ("Goals completed: %s"), allGoalsReached ? TEXT("True") : TEXT("False"));
}

void UGoalManager::SetGoalTypeActive (EGoalTypes pGoalType) {
	UE_LOG (LogTemp, Warning, TEXT ("Old goaltypes int: %d"), goalTypes);
	if (IsGoalTypeActive (pGoalType)) return;
	int goalType = (int)pGoalType;
	int bit = 1 << goalType;
	goalTypes |= bit;
	UE_LOG (LogTemp, Warning, TEXT ("New goaltypes int: %d"), goalTypes);
}

bool UGoalManager::IsGoalTypeActive (EGoalTypes pGoalType) {
	int goalType = (int)pGoalType;
	int bit = 1 << goalType;
	return ((goalTypes & bit) != 0);
}

bool UGoalManager::GoalSetCompleted (TArray<FScenarioGoal> pGoalSet) {
	bool goalSetCompleted = true;
	for (int i = 0; i < pGoalSet.Num (); i++) {
		if (!pGoalSet[i].Completed) goalSetCompleted = false;
	}
	return goalSetCompleted;
}

TArray<FScenarioGoal> UGoalManager::GetGoalsOfType (EGoalTypes pGoalType) {
	switch (pGoalType) {
		case EGoalTypes::BuildGoal:
			return buildGoals;
			break;
		case EGoalTypes::PopulationGoal:
			return populationGoals;
			break;
		case EGoalTypes::PollutionGoal:
			return pollutionGoals;
			break;
		case EGoalTypes::EmploymentGoal:
			return employmentGoals;
			break;
		default:
			return TArray<FScenarioGoal> ();
			break;
	}
}