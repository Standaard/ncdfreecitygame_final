// Fill out your copyright notice in the Description page of Project Settings.

#include "BuildingHighlightActor.h"


// Sets default values
ABuildingHighlightActor::ABuildingHighlightActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SceneComp = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComp"));
	RootComponent = SceneComp;

	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>("StaticMeshComp");
	StaticMeshComp->SetupAttachment(RootComponent);
	StaticMeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	StaticMeshComp->CastShadow = false;

}

// Called when the game starts or when spawned
void ABuildingHighlightActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABuildingHighlightActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

