// Fill out your copyright notice in the Description page of Project Settings.

#include "TutorialBackend.h"
#include "Tutorial.h"

UTutorialBackend::UTutorialBackend() {

}

UTutorialBackend::~UTutorialBackend() {

}

void UTutorialBackend::Initialize () {
	UE_LOG (LogTemp, Warning, TEXT ("Initializing tutorial backend..."));
	//Calculate how many 32-bit integers we'll need to store all tutorial steps in.
	_bitshifts = &_bitShiftOriginal;
	_tutorialStepCount = (int)ETutorialSteps::TOTAL_STEP_COUNT;
	float totalBitShifts = (float)_tutorialStepCount / 32.0f;
	float partialBitShifts = FMath::Fmod (totalBitShifts, 1);
	totalBitShifts = partialBitShifts > 0 ? totalBitShifts + 1 : totalBitShifts;
	totalBitShifts = FMath::FloorToInt (totalBitShifts);

	for (int i = 0; i < totalBitShifts; i++) {
		_bitshifts->Add (0);
	}
	UE_LOG (LogTemp, Warning, TEXT ("Tutorial backend intialized!"));
}

void UTutorialBackend::SetStepCompleted (ETutorialSteps pStepCompleted, bool pTutorialSkipped /* = false */) {
	//Divide by 31, as that's the max numbers I can shift the 1 to the left (2^31 -1 is max int32)
	int index = FMath::FloorToInt ((int)pStepCompleted / 31);
	int StepCompleted = (int)pStepCompleted;

	//Make sure the incoming step isn't a negative value (should never come in negative anyway) and shift the corresponding bit.
	if (StepCompleted >= 0 && StepCompleted < _tutorialStepCount) {
		int Bit = 1 << (StepCompleted % 31);
		(*_bitshifts)[index] |= Bit; 
		_stepsCompleted++;
	}
}

void UTutorialBackend::SetAllStepsCompleted () {
	for (int i = 0; i < _tutorialStepCount; i++) {
		SetStepCompleted ((ETutorialSteps)i);
	}
}

int UTutorialBackend::TotalStepCount () {
	return _tutorialStepCount;
}

bool UTutorialBackend::HasCompleted (ETutorialSteps pTutorialStep) {
	//Divide by 31, as that's the max numbers I can shift the 1 to the left (2^31 -1 is max int32)
	int index = FMath::FloorToInt((int)pTutorialStep / 31);
	//Check whether the int at 'index' has a '1' in the same position as '(1 << ((int)pTutorialStep % 31))'.
	return ((*_bitshifts)[index] & (1 << ((int)pTutorialStep % 31)));
}

bool UTutorialBackend::HasCompletedUpTo (ETutorialSteps pTutorialStep) {
	//Divide by 31, as that's the max numbers I can shift the 1 to the left (2^31 -1 is max int32)
	int index = FMath::FloorToInt ((int)pTutorialStep / 31);
	//Loop through all the integers before 'index' to make sure they're all max int32 (all steps in them completed).
	for (int i = 0; i < index; i++) {
		if ((*_bitshifts)[i] < (FMath::Pow(2, 31) - 1)) return false;
	}
	int shift = FMath::Pow (2, (((int)pTutorialStep % 31) + 1));
	//int kaas = (FMath::Pow (2, (((int)pTutorialStep % 31) + 1)));
	return ((*_bitshifts)[index] >= shift - 1);
}

bool UTutorialBackend::FinishedTutorial () {
	return _stepsCompleted == _tutorialStepCount;
}

void UTutorialBackend::TestTickDebug () {
	_tick++;
	//UE_LOG (LogTemp, Warning, TEXT ("Tick: %d"), _tick);
	//UE_LOG (LogTemp, Warning, TEXT ("Bitshift pointer: %d"), _bitshifts);
	//UE_LOG (LogTemp, Warning, TEXT ("Steps completed: %d"), _stepsCompleted);
	//UE_LOG (LogTemp, Warning, TEXT ("Tutorial step count: %d"), _tutorialStepCount);
	//UE_LOG (LogTemp, Warning, TEXT ("Floatende henk: %f"), _testhenk);
	//UE_LOG (LogTemp, Warning, TEXT ("Coen: %s"), _coen ? TEXT ("True") : TEXT ("False"));
	//UE_LOG (LogTemp, Warning, TEXT ("Depointered bitshift: %d"), (*_bitshifts));
}