// Fill out your copyright notice in the Description page of Project Settings.

#include "Tutorial.h"
#include "CitySystemGameMode.h"
#include "NCD_PlayerController.h"
#include "NCD_Pawn.h"

UTutorial::UTutorial () {

}

UTutorial::~UTutorial () {

}

void UTutorial::Initialize () {
	UE_LOG (LogTemp, Warning, TEXT ("Initializing tutorial..."));
	_tutorialBackend = NewObject<UTutorialBackend> ();
	_tutorialBackend->Initialize ();
	UE_LOG (LogTemp, Warning, TEXT ("Tutorial intialized!"));
}

void UTutorial::SetStepCompleted (ETutorialSteps pTutorialStep, bool pSkipEvent /* = false */) {
	if (HasCompletedTutorial ()) return;
	//We wouldn't want a step to get completed twice (which would result in a second call to the corresponding completion event)
	if (HasCompletedStep (pTutorialStep)) return;
	//Only continue when we have completed all steps before this one, or we're on the first step.
	if (((int)pTutorialStep % 32) != 0 && !HasCompletedUpToStep ((ETutorialSteps)((int)pTutorialStep - 1))) return;
	_tutorialBackend->SetStepCompleted (pTutorialStep);
	if (!pSkipEvent) OnStepCompleted (pTutorialStep);
	if (_tutorialBackend->FinishedTutorial()) OnTutorialCompleted ();
}

void UTutorial::SkipTutorial () {
	_tutorialBackend->SetAllStepsCompleted ();
	OnSkipTutorial ();
	if (_tutorialBackend->FinishedTutorial ()) OnTutorialCompleted ();
}

bool UTutorial::HasCompletedStep (ETutorialSteps pTutorialStep) {
	return _tutorialBackend->HasCompleted (pTutorialStep);
}

void UTutorial::SetupStarterCity () {
	_gameMode->GetPlayerController ()->BuildStarterTown ();
}

void UTutorial::HighlightBuildingAtLocation (FVector pLocation, bool pRemoveHighlight /* = false */) {
	if (pRemoveHighlight) _gameMode->GetPlayerController ()->GetNCDPawn ()->UnselectAsset (true);
	else {
		APlacedBuilding* Building = _gameMode->GetPlacedBuildingByLocation(pLocation);
		if (Building)
		{
			_gameMode->GetPlayerController ()->GetNCDPawn ()->SelectAsset (Building->GetMeshComponent()->GetStaticMesh(), Building->GetMeshComponent()->GetComponentTransform(), Building->GetMeshComponent()->GetMaterials(), true);
		}
	}
}

bool UTutorial::HasCompletedUpToStep (ETutorialSteps pTutorialStep) {
	return _tutorialBackend->HasCompletedUpTo (pTutorialStep);
}

bool UTutorial::HasCompletedTutorial () {
	return _tutorialBackend->FinishedTutorial ();
}

//This method is used in case the player should be able to figure out what to do, but we want to have an event happen after an x amount of time.
void UTutorial::StartPlayerIdleTimer (int pMaxIdleSeconds, bool pStopTimer /* = false */) {
	UE_LOG (LogTemp, Warning, TEXT ("Idle seconds: %d"), pMaxIdleSeconds);
	if (pStopTimer) GetWorld ()->GetTimerManager ().ClearTimer (_idleTimerHandle);
	else GetWorld ()->GetTimerManager ().SetTimer (_idleTimerHandle, this,  &UTutorial::OnPlayerIdle, pMaxIdleSeconds);
}

void UTutorial::OnConstruct_Implementation (ACitySystemGameMode* pCitySystem) {
	UE_LOG (LogTemp, Warning, TEXT ("Tutorial blueprint constructor called!"));
	_gameMode = pCitySystem;
	Initialize ();
	SetupStarterCity ();
}

void UTutorial::OnStepCompleted_Implementation(ETutorialSteps pTutorialStep) {
	UE_LOG (LogTemp, Warning, TEXT ("Tutorial step completed: %d!"), ((int)pTutorialStep + 1));
}

void UTutorial::OnSkipTutorial_Implementation () {
	UE_LOG (LogTemp, Warning, TEXT ("Tutorial skipped!"));
}

void UTutorial::OnUpdateTutorial_Implementation () {

}

void UTutorial::OnTutorialCompleted_Implementation () {
	UE_LOG (LogTemp, Warning, TEXT ("Tutorial completed!"));
	_gameMode->GetPlayerController ()->UnbindTutorialFunctions ();
}

void UTutorial::OnClickedBuildingTutorial_Implementation (FBuildingStats pBuilding) {
	UE_LOG (LogTemp, Warning, TEXT ("Building clicked"));
}

void UTutorial::OnPlayerIdle_Implementation () {
	UE_LOG (LogTemp, Warning, TEXT ("Player is idle!"));
}

void UTutorial::OnBuildEvent_Implementation (FBuildingStats pBuilding) {
	UE_LOG (LogTemp, Warning, TEXT ("Build event fired"));
}

void UTutorial::TestTickDebug () {
	_tutorialBackend->TestTickDebug ();
}