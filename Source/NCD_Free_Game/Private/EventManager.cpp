// Fill out your copyright notice in the Description page of Project Settings.

#include "EventManager.h"
#include "CitySystemGameMode.h"
#include "Engine/World.h"
#include "EventUI.h"
#include "NCD_PlayerController.h"
#include "HappinessManager.h"
#include <WidgetBlueprintLibrary.h>
#include <TimerManager.h>
#include <Kismet/GameplayStatics.h>
#include <GameFramework/Actor.h>

AEventManager::AEventManager()
{
}

AEventManager::~AEventManager()
{
}

void AEventManager::InitializeEventManager(ACitySystemGameMode* pCitySystemGameMode, TSubclassOf<UEventUI> pEventUI, float pTimeInSeconds)
{
	CitySystemGameMode = pCitySystemGameMode;
	NextEventSearchTimeInSecond = pTimeInSeconds;

	ANCD_PlayerController* pc = Cast<ANCD_PlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	UUserWidget* widget = UWidgetBlueprintLibrary::Create(this, pEventUI, pc);
	//UUserWidget* widget = UWidgetBlueprintLibrary::Create(this, pEventUI, GetWorld()->GetFirstPlayerController());
	EventUI = Cast<UEventUI>(widget);

	if (!EventUI) return;

	UE_LOG(LogTemp, Warning, TEXT("UI does exist"));
	EventUI->SetEventManager(this);
	EventUI->AddToViewport(UIEVENTZORDER);

	//Old timer to start an event after x amount of time
	//GetWorld()->GetTimerManager().SetTimer(SpawnGameEventTimer, this, &AEventManager::PickNewMaintEvent, TimeTillSpawn, false);
	
	/*
	for (int i = 8; i > 0; i--)
	{
		int d = i - 1;
		UE_LOG(LogTemp, Warning, TEXT("%d"), d);
	}*/

	//SetEventManagerActive(true);
	//EnableEventSearch();
	//PickNewEvent(false);
}

//This has to exist, since you can't expose a pointer parameter to blueprint
void AEventManager::ProposeEvent (FNCDEvent pEvent) {
	ProposeEvent (&pEvent);
}

void AEventManager::ProposeEvent(FNCDEvent* pEvent) {
	bCanSearch = false; // Set to false when proposing an event. Stops update function from looking through possible events
	
	ProposedEvent = pEvent;
	if (!EventUI) return;
	if (ProposedEvent->MaxTimesYouCanIgnore != ProposedEvent->TimesIgnored)
	{
		EventUI->SpawnEventWindow(ProposedEvent->Title, ProposedEvent->Description, ProposedEvent->YesOptionText, ProposedEvent->IgnoreOptionText, ProposedEvent->NoOptionText, ProposedEvent->QuestGiver, *ProposedEvent);
	}
	else{
		EventUI->SpawnEventWindow(ProposedEvent->Title, ProposedEvent->Description, ProposedEvent->YesOptionText, false, ProposedEvent->NoOptionText, ProposedEvent->QuestGiver, *ProposedEvent);
	}
	
}

void AEventManager::ProposeEventByName(FName pName)
{
	ProposeEvent(CitySystemGameMode->FindGameEventByID(pName));
}

bool AEventManager::CheckEventEndConditions()
{
	int CompletedCount = 0;
	TMap<EConditionEnum, int> Conditions = CurrentEvent->WinCondition;
	TArray<EConditionEnum> ConditionKeys;
	Conditions.GenerateKeyArray(ConditionKeys);

	for (int i=0; i<Conditions.Num(); i++)
	{
		switch (ConditionKeys[i])
		{
		case EConditionEnum::VE_BuildingAmount:
			for (auto& building : CurrentEvent->BuildingTypeNeeded)
			{
				//if (CitySystemGameMode->GetBuildingsByType(building.Key).Num() < *Conditions.Find(EConditionEnum::VE_BuildingAmount)) return false;		//return false here if building amount is lower than needed.
				//int buildingsOnStart = *BuildingsOnEventStart.Find(building.Key);
				int32 bValue = building.Value;
				UE_LOG(LogTemp, Warning, TEXT("Buildings needed %d"), bValue);
				int32 BuildingsByType = CitySystemGameMode->GetBuildingsByType(building.Key).Num();
				int32 BuildingsFromStartPlusValue = (*BuildingsOnEventStart.Find(building.Key) + bValue);
				UE_LOG(LogTemp, Warning, TEXT("Builiings By Type : %d"), BuildingsByType);
				UE_LOG(LogTemp, Warning, TEXT("Builiings already placed at start of event plus value : %d"), BuildingsFromStartPlusValue);
				if (bValue > 0) {
					if (BuildingsByType < BuildingsFromStartPlusValue) return false;        //return false here if building amount is lower than needed.
				}
				else
				{
					if (BuildingsByType > BuildingsFromStartPlusValue) return false;        //return false here if building amount is lower than needed.
				}
				

				//if (CitySystemGameMode->GetBuildingsByType(building.Key).Num() < (*BuildingsOnEventStart.Find(building.Key) + building.Value)) return false;		//return false here if building amount is lower than needed.
			}
			break;
		case EConditionEnum::VE_People:
			if(CitySystemGameMode->GetPopulationManager()->GetPopulationCount() < *Conditions.Find(EConditionEnum::VE_People)) return false;			//return false here if population amount is lower than needed.
			break;
		case EConditionEnum::VE_WorkingPeople:
			if(CitySystemGameMode->GetPopulationManager()->GetEmployedPeopleCount() < *Conditions.Find(EConditionEnum::VE_WorkingPeople)) return false; //return false here if employed amount is lower than needed.
			break;
		case EConditionEnum::VE_WorkingPeoplePercentage:
			if((CitySystemGameMode->GetPopulationManager()->GetEmployedPeopleCount() / CitySystemGameMode->GetPopulationManager()->GetPopulationCount() * 100) < *Conditions.Find(EConditionEnum::VE_WorkingPeoplePercentage)) return false;
			break;
		case EConditionEnum::VE_PolicyDaysActive:
			//To be implemented
			//Count of days the policy has been active without pause.
			UE_LOG(LogTemp, Warning, TEXT("EConditionEnum::VE_Policy needs implementation!"));
			break;
		case EConditionEnum::VE_Pollution:
			//To be implemented
			UE_LOG(LogTemp, Warning, TEXT("EConditionEnum::VE_Pollution needs implementation!"));
			break;
		default:
			break;
		}

		CompletedCount++;	// if all values where bigger we have met the condition and return true.
	}
	
	if (CompletedCount == Conditions.Num()) return true;
	
	UE_LOG(LogTemp, Warning, TEXT("Something went wrong in EventManager CheckEndConditions")); //should not reach this
	return false;		//should not reach this
}

bool AEventManager::CheckEventSpawnConditions(FNCDEvent* pEvent)
{
	int CompletedCount = 0;
	TMap<EEventSpawnConditionEnum, int> Conditions = pEvent->SpawnCondition;
	TArray<EEventSpawnConditionEnum> ConditionKeys;
	Conditions.GenerateKeyArray(ConditionKeys);

	for (int i = 0; i < Conditions.Num(); i++)
	{
		switch (ConditionKeys[i])
		{
		case EEventSpawnConditionEnum::ESC_BuildingAmount:
			for (auto& building : pEvent->BuildingTypeNeededForSpawn)
			{
				if (CitySystemGameMode->GetBuildingsByType(building.Key).Num() >= building.Value) return false;		//return false here if building amount is lower than needed.
			}
			break;
		case EEventSpawnConditionEnum::ESC_HappinessOverValue:
			if (CitySystemGameMode->GetHappinessManager()->GetHappinessPercentage() < *Conditions.Find(EEventSpawnConditionEnum::ESC_HappinessOverValue)) return false;
			break;
		case EEventSpawnConditionEnum::ESC_HappinessUnderValue:
			if (CitySystemGameMode->GetHappinessManager()->GetHappinessPercentage() > *Conditions.Find(EEventSpawnConditionEnum::ESC_HappinessUnderValue)) return false;
			break;
		case EEventSpawnConditionEnum::ESC_PolicyDaysActive:
			//To be implemented
			//Count of days the policy has been active without pause.
			UE_LOG(LogTemp, Warning, TEXT("EConditionEnum::VE_Policy needs implementation!"));
			break;
		default:
			break;
		}

		CompletedCount++;	// if all values where bigger we have met the condition and return true.
	}

	if (CompletedCount == Conditions.Num()) return true;

	UE_LOG(LogTemp, Warning, TEXT("Something went wrong in EventManager CheckSpawnConditions")); //should not reach this
	return false;
}

void AEventManager::HandleEndConditionActions(FNCDEvent* pEvent, EGameEventAction pEventStatus)
{
	if (!CitySystemGameMode) return;
	if (!pEvent) return;

	FName EventName = pEvent->ID;
	FDateTime CalcCooldownDate = CitySystemGameMode->GetCurrentDateTime();

	bool ChainEventStored = false;

	switch (pEventStatus)
	{
	case EGameEventAction::EGE_COMPLETED:
		if (!CompletedEvents.Contains(EventName)) CompletedEvents.Add(EventName);
		HandleEventCompletionRewardsPenalties(pEvent->RewardsForCompleting);

		//Unlock all stuff for completing
		CitySystemGameMode->UnlockNewBuildings(pEvent->BuildingsToUnlockWhenSucceeded);
		CitySystemGameMode->UnlockNewPolicies(pEvent->PoliciesToUnlockWhenSucceeded);
		CitySystemGameMode->UnlockNewGameEvents(pEvent->EventsToUnlockWhenSucceeded);

		pEvent->bCompletedEvent = true;

		//Check if event has a follow up event
		if(CheckForFollowUpEvent(pEvent, EGameEventAction::EGE_COMPLETED)) ChainEventStored = true;
		break;
	case EGameEventAction::EGE_FAILED:
		if (!FailedEvents.Contains(EventName)) FailedEvents.Add(EventName);
		HandleEventCompletionRewardsPenalties(pEvent->PenaltiesForFailing);

		if (!pEvent->bRepeatableEvent)
		{
			CitySystemGameMode->UnlockNewBuildings(pEvent->BuildingsToUnlockWhenFailed);
			CitySystemGameMode->UnlockNewPolicies(pEvent->PoliciesToUnlockWhenFailed);
			CitySystemGameMode->UnlockNewGameEvents(pEvent->EventsToUnlockWhenFailed);
		}

		pEvent->bCompletedEvent = true;

		//Check if event has a follow up event
		if(CheckForFollowUpEvent(pEvent, EGameEventAction::EGE_FAILED)) ChainEventStored = true;
		break;
	case EGameEventAction::EGE_DECLINED:
		if (!DeclinedEvents.Contains(EventName)) DeclinedEvents.Add(EventName);
		HandleEventCompletionRewardsPenalties(pEvent->PenaltiesForDisagreeing);

		if (!pEvent->bRepeatableEvent)
		{
			CitySystemGameMode->UnlockNewBuildings(pEvent->BuildingsToUnlockWhenFailed);
			CitySystemGameMode->UnlockNewPolicies(pEvent->PoliciesToUnlockWhenFailed);
			CitySystemGameMode->UnlockNewGameEvents(pEvent->EventsToUnlockWhenFailed);
		}

		pEvent->bCompletedEvent = true;

		//Check if event has a follow up event
		if(CheckForFollowUpEvent(pEvent, EGameEventAction::EGE_DECLINED)) ChainEventStored = true;
		break;
	case EGameEventAction::EGE_IGNORED:
		if (!IgnoredEvents.Contains(EventName)) IgnoredEvents.Add(EventName);

		pEvent->TimesIgnored++;

		//Add a cooldown to the ignored event. That way we don't propose it too soon.
		
		CalcCooldownDate += FTimespan(pEvent->IgnoreCooldownInMonths * 30, 0, 0, 0);	//Months x 30 days. will set the date x amount of months further.
		EventsOnCooldown.Add(pEvent, CalcCooldownDate);	// Add to TMap for cooldown tracking
		break;
	default:
		break;
	}

	if (!EventsOnCooldown.Contains(pEvent) && pEvent->bRepeatableEvent)	//if event is repeatable and is not on an ignore cooldown -- add to cooldown list
	{
		CalcCooldownDate += FTimespan(pEvent->MinimumRespawnTimeInMonths * 30, 0, 0, 0);	//Months x 30 days. will set the date x amount of months further.
		EventsOnCooldown.Add(pEvent, CalcCooldownDate);	// Add to TMap for cooldown tracking
	}

	EventIsCurrentlyActive = false;
	ProposedEvent = nullptr;
	CurrentEvent = nullptr;

	if(!ChainEventStored) GetWorld()->GetTimerManager().SetTimer(CanSearchForEventTimer, this, &AEventManager::SetSearchForEventActive, NextEventSearchTimeInSecond, false);
	else SetSearchForEventActive();
}

void AEventManager::HandleEventCompletionRewardsPenalties(TMap<ECompletionReward, int> pCompletionReward)
{
	TArray<ECompletionReward> RewardKeys;
	pCompletionReward.GenerateKeyArray(RewardKeys);

	for (int i = 0; i < RewardKeys.Num(); i++)
	{
		switch (RewardKeys[i])
		{
		case ECompletionReward::ECR_HAPPINESS:
			//Add/Subtract happiness
			CitySystemGameMode->GetHappinessManager()->AddHappiness(*pCompletionReward.Find(ECompletionReward::ECR_HAPPINESS));
			break;
		case ECompletionReward::ECR_MONEY:
			CitySystemGameMode->RewardMoney(*pCompletionReward.Find(ECompletionReward::ECR_MONEY));
			break;
		case ECompletionReward::ECR_PEOPLE:
			//Population Manager add people to city (if possible)
			break;
		case ECompletionReward::ECR_POLLUTION:
			UE_LOG(LogTemp, Warning, TEXT("ECR_POLLUTION needs implementation!"));
			break;
		case ECompletionReward::ECR_NONE:
			//None.
			//UE_LOG(LogTemp, Warning, TEXT("ECR_NOTSPECIFIED needs implementation!"));
			break;
		default:
			break;
		}
	}
}

void AEventManager::OnEventAccepted()
{
	if (!CitySystemGameMode) return;
	if (!ProposedEvent) return;
	if (!bEventManagerActive) return;

	UE_LOG(LogTemp, Warning, TEXT("Event Accepted"));
	CurrentEvent = ProposedEvent;
	FDateTime CurrentTime = CitySystemGameMode->GetCurrentDateTime();
	CurrentTime += FTimespan(CurrentEvent->CompletionTimeLimitInMonths * 30, 0, 0, 0);	//Months x 30 days. will set the date x amount of months further.
	SetEndDateForCurrentEvent(CurrentTime);
	//Store buildings
	BuildingsOnEventStart.Empty();
	StoreBuildingAmountsBeforeEvent(CurrentEvent->BuildingTypeNeeded);

	EventIsCurrentlyActive = true;
}

void AEventManager::OnEventFailed()
{
	if (!CurrentEvent) return;
	UE_LOG(LogTemp, Warning, TEXT("Event Failed"));
	HandleEndConditionActions(CurrentEvent, EGameEventAction::EGE_FAILED);
}

void AEventManager::OnEventIgnore()
{
	UE_LOG(LogTemp, Warning, TEXT("Event Ignored"));
	HandleEndConditionActions(ProposedEvent, EGameEventAction::EGE_IGNORED);
}

void AEventManager::OnEventDeclined()
{
	UE_LOG(LogTemp, Warning, TEXT("Event Declined"));
	HandleEndConditionActions(ProposedEvent, EGameEventAction::EGE_DECLINED);
}


void AEventManager::SetEventManagerActive(bool pActive)
{
	bEventManagerActive = pActive;
}


//Enable the search for an event.
//Can be triggered from outside the manager
void AEventManager::EnableEventSearch()
{
	if (bEventManagerActive)
	{
		UE_LOG(LogTemp, Warning, TEXT("Enable Event searching!"));
		bCanSearch = true;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Can not search for new event, EventManager is not active"));
	}
}


void AEventManager::SetSearchForEventActive()
{
	bCanSearch = true;
	ProposedEvent = nullptr;
	CurrentEvent = nullptr;
}

bool AEventManager::IsOverTime()
{
	if (CurrentEvent->bIgnoreCompletionTimer) return false;
	else if (CitySystemGameMode->GetCurrentDateTime() > EndDateForCurrentEvent)
	{
		OnEventOverTime();
		return true;
	}
	return false;
}

void AEventManager::SetEndDateForCurrentEvent(FDateTime pEndDate)
{
	EndDateForCurrentEvent = pEndDate;
}

void AEventManager::StoreBuildingAmountsBeforeEvent(TMap<EBuildingType, int> pBuildingTypesNeeded)
{
	for (auto& building : pBuildingTypesNeeded)
	{
		int amountOfBuilding = CitySystemGameMode->GetBuildingCountOfType(building.Key);
		BuildingsOnEventStart.Add(building.Key, amountOfBuilding);
	}
}

FDateTime AEventManager::GetEndDateForCurrentEvent(FDateTime pEndDate)
{
	return EndDateForCurrentEvent;
}

TArray<FNCDEvent> AEventManager::GetUnlockedEventQueue()
{
	TArray<FNCDEvent> EventsInQueue;
	TArray<FNCDEvent*> UnlockedEventsArray = GetListOfAvailableEvents();
	
	for (auto& gameEvents : UnlockedEventsArray)
	{
		EventsInQueue.Add(*gameEvents);
	}
	return EventsInQueue;
}

TMap<FString, FDateTime> AEventManager::GetEventsOnCooldown()
{
	TMap<FString, FDateTime> CooldownEventsList;
	
	for (auto& gameEvents : EventsOnCooldown)
	{
		FNCDEvent* eventHolder = gameEvents.Key;
		FString makeEvent = eventHolder->Title;
		CooldownEventsList.Add(makeEvent, gameEvents.Value);
	}
	return CooldownEventsList;
}

void AEventManager::OnEventOverTime()
{
	OnEventFailed();
}

// After an event has been completed or failed and a followup event should be activated
void AEventManager::StoreFollowUpEvent(FNCDEvent * pFollowupEvent, int pSecondsTillPropose)
{
	//Store event
	QueuedEvent = pFollowupEvent;

	//Start Timer counting down for Follow up Event
	GetWorld()->GetTimerManager().SetTimer(QueueEventTimer, this, &AEventManager::SetQueuedEventReady, pSecondsTillPropose, false);
}

void AEventManager::PickFollowUpEvent()
{
	ProposeEvent(QueuedEvent);
}

void AEventManager::SetQueuedEventReady()
{
	bQueuedEventReady = true;
}

bool AEventManager::CheckForFollowUpEvent(FNCDEvent* pEvent, EGameEventAction pEventStatus)
{
	FNCDEvent * FollowUpEvent;
	if (pEvent->FollowUpEvent.Find(pEventStatus) != nullptr)
	{
		FollowUpEvent = CitySystemGameMode->FindGameEventByID(pEvent->FollowUpEvent.Find(pEventStatus)->RowName);

		if (FollowUpEvent == nullptr) return false;

		FollowUpEvent->bUnlockedEvent = true;
		StoreFollowUpEvent(FollowUpEvent, pEvent->TimeTillFollowUpEventInSeconds);
		UE_LOG(LogTemp, Warning, TEXT("Follow up event was found and stored in queue"));

		return true;
	}	
	return false;
}

void AEventManager::Step()
{
	if(!bEventManagerActive) return; //Turned off when manager is inactive
	if(!CitySystemGameMode) return;
	
	if (!bCanSearch) {		//Can not search because event is proposed or active
		if (CurrentEvent == nullptr) return;		//If no event is active return, most likely event is being proposed

		if (IsOverTime()) return;	//Check if current event is over time limit.
		
		//Check Event against its conditions
		if (CheckEventEndConditions()) HandleEndConditionActions(CurrentEvent, EGameEventAction::EGE_COMPLETED);
	}
	else    // No event active or proposed, search for new event!
	{
		if (ProposedEvent || CurrentEvent) return;		//an event is already being proposed.

		//UE_LOG(LogTemp, Warning, TEXT("Before OnCooldownCheck"));
		if (EventsOnCooldown.Num() > 0) CheckIfEventsOffCooldown();

		//Check if there is an event in the follow up queue.
		if (QueuedEvent != nullptr)
		{
			//UE_LOG(LogTemp, Warning, TEXT("Before picking queued event"));

			//if(TimeTillPropose >= 0.f) TimeTillPropose -= GetWorld()->TimerManager->
			PickNewEvent(true);
			return;
		}

		//UE_LOG(LogTemp, Warning, TEXT("Before new event"));

		//Search for new Event.
		PickNewEvent(false);

	}
}

void AEventManager::PickNewEvent(bool pQueuedEvent)
{
	//if (CurrentEvent) return;		//already being done in the step function
	//if (ProposedEvent) return;
	
	if (pQueuedEvent)
	{
		if(bQueuedEventReady) PickFollowUpEvent();
	}
	else
	{
		//UE_LOG(LogTemp, Warning, TEXT("get list of available events"));

		TArray<FNCDEvent*> AvailableEvents;
		AvailableEvents = GetListOfAvailableEvents();
		
		if (AvailableEvents.Num() == 0) return;

		TArray<FNCDEvent*> NonCompletedEvents;		//Store non completed events. (Pick over repeatable)

		for (auto& gameEvent : AvailableEvents)
		{
			if (!gameEvent->bCompletedEvent)
			{
				NonCompletedEvents.Add(gameEvent);
			}
		}
		int rNumber = 0;

		if (NonCompletedEvents.Num() > 0) {
			rNumber = FMath::RandRange(0, NonCompletedEvents.Num() - 1);  //Max - 1 because of index.
			ProposeEvent(NonCompletedEvents[rNumber]);
		}
		else
		{
			rNumber = FMath::RandRange(0, AvailableEvents.Num() - 1);  //Max - 1 because of index.
			ProposeEvent(AvailableEvents[rNumber]);
		}
	}
}

TArray<FNCDEvent*> AEventManager::GetListOfAvailableEvents()
{
	TArray<FNCDEvent*> ListOfEvents;

	for (auto& Events : CitySystemGameMode->GetGameEventsList())
	{
		FNCDEvent* gameEvent = &Events.Value;

		if (gameEvent->bUnlockedEvent)					//Check if event is unlocked
		{
			if (!gameEvent->bCompletedEvent)					//Check if event is completed
			{
				if(CheckEventSpawnConditions(gameEvent)) ListOfEvents.Add(gameEvent);			//add to list
			}
			else if (gameEvent->bCompletedEvent && gameEvent->bRepeatableEvent)		//Check if event is completed and repeatable
			{
				if (!EventsOnCooldown.Contains(gameEvent))		//Check if not on a cooldown
				{
					if (CheckEventSpawnConditions(gameEvent)) ListOfEvents.Add(gameEvent);		//add to list
				}
			}
		}
	}

	return ListOfEvents;
}

void AEventManager::CheckIfEventsOffCooldown()
{
	FDateTime CurrentDate = CitySystemGameMode->GetCurrentDateTime();
	
	TArray<FNCDEvent*> EventArray;
	TArray<FDateTime> DateArray;
	EventsOnCooldown.GenerateKeyArray(EventArray);
	EventsOnCooldown.GenerateValueArray(DateArray);

	//for (auto& Events : EventsOnCooldown)
	//{
	//UE_LOG(LogTemp, Warning, TEXT("Arrays created to loop through"));
	for (int i = EventsOnCooldown.Num(); i > 0; i--)
	{
		FNCDEvent* gameEvent = EventArray[i-1];
		FDateTime DateTime = DateArray[i-1];

		//FNCDEvent* gameEvent = Events.Key;
		
		if (CurrentDate > DateTime) {
			if (IgnoredEvents.Contains(gameEvent->ID)) IgnoredEvents.Remove(gameEvent->ID);
			else if (gameEvent->bRepeatableEvent)
			{
				if (CompletedEvents.Contains(gameEvent->ID)) CompletedEvents.Remove(gameEvent->ID);
				else if (FailedEvents.Contains(gameEvent->ID)) FailedEvents.Remove(gameEvent->ID);
				else if (DeclinedEvents.Contains(gameEvent->ID)) DeclinedEvents.Remove(gameEvent->ID);
				gameEvent->TimesIgnored = 0;
			}
			EventsOnCooldown.Remove(gameEvent);
		}
	}
}
