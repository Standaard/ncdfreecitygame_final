// Fill out your copyright notice in the Description page of Project Settings.

#include "CitySystemGameMode.h"
#include <Kismet/GameplayStatics.h>
#include <Kismet/KismetMathLibrary.h>
#include <Engine/DirectionalLight.h>
#include "Engine/World.h"
#include "NCD_PlayerController.h"
#include "FeedbackNotificationManager.h"
#include "MilestoneManager.h"
#include "PopulationManager.h"
#include "CitySaveGame.h"
#include "MyNCDGameInstance.h"
#include "TrafficSystem.h"
#include "EventManager.h"
#include "EventUI.h"
#include "RoadNode.h"
#include "BuildableRoad.h"
#include "SortByDistance.h"
#include "BuildingAreaManager.h"
#include "PollutionManager.h"
#include "Tutorial.h"
#include "PlacedBuilding.h"
#include "HappinessManager.h"
#include "ScenarioImporter.h"
#include "public/JsonImporter.h"
#include "public/GoalManager.h"


/*
Nothing going on here (yet)
*/
ACitySystemGameMode::ACitySystemGameMode() {
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
}

/*
Deconstructing the arrays that are made
*/
ACitySystemGameMode::~ACitySystemGameMode() {

}

/*
* Beginplay, gametimer started initially here
*/
void ACitySystemGameMode::BeginPlay() {
	UE_LOG(LogTemp, Warning, TEXT("Gamemode is created"));
	Super::BeginPlay();

	PopulationManager = NewObject<UPopulationManager>(this);
	PopulationManager->SetGameMode (this);
	
	GameInstance = Cast<UMyNCDGameInstance>(GetGameInstance());
	
	if (GetWorld()) {
		EventManager = Cast<AEventManager>(GetWorld()->SpawnActor(AEventManager::StaticClass()));

		NotificationManager = GetWorld()->SpawnActor<AFeedbackNotificationManager>(FeedBackNotificationManagerClass, FVector::ZeroVector, FRotator::ZeroRotator);
		MilestonesManager = GetWorld()->SpawnActor<AMilestoneManager>(MilestonesManagerClass, FVector::ZeroVector, FRotator::ZeroRotator);
		BuildingEventsManager = GetWorld()->SpawnActor<ABuildingEvents>(BuildingEventsClass, FVector::ZeroVector, FRotator::ZeroRotator);
		TrafficSystem = GetWorld()->SpawnActor<ATrafficSystem>(TrafficSystemClass, FVector::ZeroVector, FRotator::ZeroRotator);
		BuildingAreaManager = GetWorld()->SpawnActor<ABuildingAreaManager>(ABuildingAreaManager::StaticClass());
		PollutionManager = GetWorld()->SpawnActor<APollutionManager>(PollutionManagerClass);


		if (!EventManager || !NotificationManager || !MilestonesManager || !BuildingEventsManager || !TrafficSystem || !BuildingAreaManager || !PollutionManager) {
			UE_LOG(LogTemp, Warning, TEXT("One of the managers set in BeginPlay of CitySystem is null"));
		}

		PlayerController = Cast<ANCD_PlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
		
		TrafficSystem->SetVehicleSpawnDelay(VehicleSpawnDelay);
	}

	/* Initializations */	
	//InitializePolicies();			//Array of Policies from DataTable to manipulate state of unlock
	//InitializeBuildings();			//Array of Buildings from DataTable to manipulate state of unlock
	//InitializeGameEvents();			//Array of GameEvents from DataTable to manipulate state of unlock

	/* Savegame loading */
	LoadSaveGame();

	/* Adjust with normal values, also adjustable rate by clicking speed up or something similar */
	// Start timelapse day timer

	RestartGameTimer();
	SetGameTimeState(TimelapseHour);
	CreateDelegateBindingMap ();
	BindUpdateFunction (EDelegates::TimeActions, this, EUpdateFunctions::UpdateTime);
	//BindUpdateFunction (EDelegates::TimeActions, this, EUpdateFunctions::UpdateHappiness);
	//RestartTimelapseTimer();

	//Game Events Manager  -- Load After initialization of Game Events & Save Game
	HappinessManager = NewObject<UHappinessManager> ();
	HappinessManager->Init (this);
	GoalManager = NewObject<UGoalManager> ();
	GoalManager->Initialize (this);
	if (EventManager && EventUI) { EventManager->InitializeEventManager(this, EventUI, SecondsTillNextEventSearch); }			//TODO: Rewrite to pass the initiliazed TMap of game events (loaded and (un)locked / completed)
	if (MilestonesManager->bDeveloperUnlockActive) MilestonesManager->DeveloperUnlockMilestone();
	//Development testing
}

void ACitySystemGameMode::LoadDatatables () {
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass (GetWorld (), AScenarioImporter::StaticClass (), FoundActors);
	AScenarioImporter* scenarioImporter = (AScenarioImporter*)FoundActors[0];
	UE_LOG (LogTemp, Warning, TEXT ("[CitySystemGameMode.cpp; line 106]: Scenario importer: %s"), scenarioImporter ? TEXT ("True") : TEXT ("False"));
	if (scenarioImporter) {
		InitializePolicies (scenarioImporter->GetJsonImporter()->DatatableNames.PoliciesDT);			//Array of Policies from DataTable to manipulate state of unlock
		InitializeBuildings (scenarioImporter->GetJsonImporter ()->DatatableNames.BuildingsDT);			//Array of Buildings from DataTable to manipulate state of unlock
		InitializeGameEvents (scenarioImporter->GetJsonImporter ()->DatatableNames.EventsDT);			//Array of GameEvents from DataTable to manipulate state of unlock
		GoalManager->LoadGoalData (scenarioImporter->GetJsonImporter()->DatatableNames.GoalsDT);
	} else UE_LOG (LogTemp, Warning, TEXT ("[CitySystemGameMode.cpp; line 110]: Scenario importer not found!"));
}

void ACitySystemGameMode::CreateDelegateBindingMap () {
	//Add all update delegates. These require a <struct> wrapper, because you are not allowed to put a delegate directly into a map.
	UpdateDelegates.Add (EUpdateFunctions::UpdatePopulation, FDelegateHandleWrap (FDelegateHandle ()));
	UpdateDelegates.Add (EUpdateFunctions::UpdatePolicies, FDelegateHandleWrap (FDelegateHandle ()));
	UpdateDelegates.Add (EUpdateFunctions::UpdateInfoForPlayer, FDelegateHandleWrap (FDelegateHandle ()));
	UpdateDelegates.Add (EUpdateFunctions::UpdateBuildingHighlight, FDelegateHandleWrap (FDelegateHandle ()));
	UpdateDelegates.Add (EUpdateFunctions::UpdateNotificationManager, FDelegateHandleWrap (FDelegateHandle ()));
	UpdateDelegates.Add (EUpdateFunctions::UpdateMilestoneManager, FDelegateHandleWrap (FDelegateHandle ()));
	UpdateDelegates.Add (EUpdateFunctions::UpdateEventManager, FDelegateHandleWrap (FDelegateHandle ()));
	UpdateDelegates.Add (EUpdateFunctions::OnUpdateTutorial, FDelegateHandleWrap (FDelegateHandle ()));
	UpdateDelegates.Add (EUpdateFunctions::UpdateTime, FDelegateHandleWrap (FDelegateHandle ()));
	UpdateDelegates.Add (EUpdateFunctions::UpdateHappiness, FDelegateHandleWrap (FDelegateHandle ()));
	UpdateDelegates.Add (EUpdateFunctions::UpdateIdleTimerTutorial, FDelegateHandleWrap (FDelegateHandle ()));

	//Add all names to a dictionary, so they can't be mistyped later.
	UpdateFunctionNames.Add (EUpdateFunctions::UpdatePopulation, FName ("UpdatePopulation"));
	UpdateFunctionNames.Add (EUpdateFunctions::UpdatePolicies, FName ("UpdatePolicies"));
	UpdateFunctionNames.Add (EUpdateFunctions::UpdateInfoForPlayer, FName ("UpdateInfoForPlayer"));
	UpdateFunctionNames.Add (EUpdateFunctions::UpdateBuildingHighlight, FName ("UpdateBuildingHighlight"));
	UpdateFunctionNames.Add (EUpdateFunctions::UpdateNotificationManager, FName ("UpdateNotificationManager"));
	UpdateFunctionNames.Add (EUpdateFunctions::UpdateMilestoneManager, FName ("UpdateMilestoneManager"));
	UpdateFunctionNames.Add (EUpdateFunctions::UpdateEventManager, FName ("UpdateEventManager"));
	UpdateFunctionNames.Add (EUpdateFunctions::OnUpdateTutorial, FName ("OnUpdateTutorial"));
	UpdateFunctionNames.Add (EUpdateFunctions::UpdateTime, FName ("UpdateTime"));
	UpdateFunctionNames.Add (EUpdateFunctions::UpdateHappiness, FName ("UpdateHappiness"));
	UpdateFunctionNames.Add (EUpdateFunctions::UpdateIdleTimerTutorial, FName ("UpdateIdleTimerTutorial"));

	Delegates.Add (EDelegates::Update, FDelegateTimeHandleWrap (&Update));
	Delegates.Add (EDelegates::TimeActions, FDelegateTimeHandleWrap(&TimeActions));
}

void ACitySystemGameMode::BindAllUpdates () {
	//Bind all update methods to their respective delegates, so they may be executed from now on.
	UE_LOG (LogTemp, Warning, TEXT ("Binding updates..."));
	BindUpdateFunction (EDelegates::TimeActions, this, EUpdateFunctions::UpdatePopulation);
	BindUpdateFunction (EDelegates::Update, this, EUpdateFunctions::UpdatePolicies);
	BindUpdateFunction (EDelegates::Update, this, EUpdateFunctions::UpdateInfoForPlayer);
	BindUpdateFunction (EDelegates::Update, PlayerController, EUpdateFunctions::UpdateBuildingHighlight);
	BindUpdateFunction (EDelegates::Update, this, EUpdateFunctions::UpdateNotificationManager);
	BindUpdateFunction (EDelegates::Update, this, EUpdateFunctions::UpdateMilestoneManager);
	BindUpdateFunction (EDelegates::Update, this, EUpdateFunctions::UpdateEventManager);
	//Remove tutorial update
	RemoveUpdateFunctionBinding (EUpdateFunctions::OnUpdateTutorial);
	UE_LOG (LogTemp, Warning, TEXT ("Binding updates complete!"));
}

void ACitySystemGameMode::PlaceStarterStaticContent () {
	/* Presets loading */
	Tutorial = NewObject<UTutorial> (this, BPTutorialClass);
	Tutorial->OnConstruct (this);
	BindUpdateFunction (EDelegates::Update, Tutorial, EUpdateFunctions::OnUpdateTutorial);
}

//If it crashes on this function, make sure the "pInUserObject" actually contains the function!
void ACitySystemGameMode::BindUpdateFunction (EDelegates pInDelegate, UObject *pInUserObject, EUpdateFunctions pFunction) {
	//Bind pFunction to the inserted pInDelegate so that it can be executed when the delegate is being summoned.
	if (pInUserObject && !boundFunctions.Contains((int)pFunction)) {
		UpdateDelegates[pFunction] = FDelegateHandleWrap (Delegates[pInDelegate].Handle->AddUFunction (pInUserObject, UpdateFunctionNames[pFunction]));
		boundFunctions.Add ((int)pFunction);
	}
}

bool ACitySystemGameMode::RemoveUpdateFunctionBinding (EUpdateFunctions pFunction) {
	//Remove pFunction from its assigned delegate.
	if (UpdateDelegates[pFunction].Handle.IsValid ()) {
		Update.Remove (UpdateDelegates[pFunction].Handle);
		boundFunctions.Remove ((int)pFunction);
		return true;
	} else return false;
}

FCitySystemExport ACitySystemGameMode::TotalExport()
{
	FCitySystemExport Export;
	Export.GameDate = StartDate;
	Export.DayCounter = DayCounter;
	Export.GameTimeState = GameTimeState;
	Export.HappinessCounts = IsHappinessThresholdReached;
	Export.TimelapseHour = TimelapseHour;
	Export.CityName = CityName;
	
	for (auto& GameEvent : GameEventsList)
	{
		if (GameEvent.Value.bUnlockedEvent)
		{
			Export.GameEvents.Add(GameEvent.Value.ID, FNCDEventExport(GameEvent.Value));
		}
	}

	Export.BuildingStatistics = BuildingsExport();
	Export.ResourceStatistics = ResourcesExport();

	return Export;
}

FCitySystemExportBuildings ACitySystemGameMode::BuildingsExport()
{
	FCitySystemExportBuildings Export;
	Export.bIsCityHallPlaced = bIsCityHallPlaced;
	for (auto& BuildingManager: BuildingManagers)
	{
		if (BuildingManager.HasBuildings()) {
			Export.BuildingManagers.Add(FBuildingManagerExport(BuildingManager));
		}
	}
	return Export;
}

FCitySystemExportResources ACitySystemGameMode::ResourcesExport()
{
	FCitySystemExportResources Export;
	Export.Money = Money;
	Export.Support = Support;
	if (PopulationManager) {
		Export.PopulationExport = PopulationManager->Export();
	}
	for (auto& Policy: Policies)
	{
		if (Policy.Value.bMilestoneReached) {
			Export.Policies.Add(Policy.Value.ID, FPolicyExport(Policy.Value));
		}
	}
	if(MilestonesManager) Export.MilestonesList = MilestonesManager->GetUnlockedMilestonesForExport();
	return Export;
}

void ACitySystemGameMode::SaveGameInSlot(int pSlot)
{
	if (GameInstance) {
		GameInstance->SaveGameInSlot(TotalExport(),pSlot);
	}
}

void ACitySystemGameMode::SetCityHallPlaced (bool pPlaced) {	
	bIsCityHallPlaced = pPlaced;
}

/*
Buying a building
- pBuilding = The building that is going to be bought
+ Returns true on sufficient money, false otherwise
*/
bool ACitySystemGameMode::BuyBuilding(ABuildableActor* pBuildableActor, APlacedBuilding* pBuilding, FPlacedBuildingParameters pParams)
{
	if (!pBuildableActor || !pBuilding || !IsAllowedToBuildBuilding(pBuilding->BuildingStats, pParams.bIsFree)) return false;

	/* Withdrawal of money */
	int ManagerIndex = RegisterBuilding (pBuildableActor, pBuilding);
	if (ManagerIndex == -1) return false;

	HandleBuildingValues(pBuilding->BuildingStats, false, pParams);

	float RemovalValue = pParams.bIsFree ? 0 : pBuilding->BuildingStats.Cost * (1.f - GetModifierByType (EModifierType::EMT_DISCOUNT_BUILDING));
	Money -= RemovalValue;
	if (PlayerController) PlayerController->BuyBuildingExpenses(RemovalValue, pBuilding->BuildingStats.Location);
	if (MilestonesManager) MilestonesManager->CheckMilestones();
	if (PollutionManager) PollutionManager->CreatePollutionArea(pBuilding, pParams.bLoadFromSave);
	return true;
}

/*
Registering a building without purchasing it
- pBuilding = The building that is going to be bought
- returns ManagerIndex
*/
int ACitySystemGameMode::RegisterBuilding(ABuildableActor* pBuildableActor, APlacedBuilding* pBuilding)
{
	if (!pBuildableActor || !pBuilding) return -1;
	int ManagerIndex = FindOrCreateBuildingManager(pBuildableActor);
	BuildingManagers[ManagerIndex].AddBuilding(pBuilding);
	if (pBuilding->BuildingStats.BuildingType == EBuildingType::EB_CITY_HALL) bIsCityHallPlaced = true;
	return ManagerIndex;
}

/*
Selling a building, based on the ID that is given in the parameter
- pId = The ID of the building
*/
bool ACitySystemGameMode::SellBuilding(ABuildableActor* pBuildableActor, FVector pLocation, bool pMoneyBack)
{
	if (!pBuildableActor) return false;
	/* Check if Id exists in the buildings, returns otherwise */
	int FoundManagerIndex = FindIndexBuildingMananagerByBuildableActor(pBuildableActor);
	
	// We want a manager to be found
	if (FoundManagerIndex == -1) return false;

	//// We will try to find the index by location
	APlacedBuilding* placedBuilding = GetPlacedBuildingByLocation(pLocation);
	if (!placedBuilding || !placedBuilding->BuildingStats.bIsRemovable) return false;

	FBuildingStats& Building = placedBuilding->BuildingStats;

	if (BuildingManagers[FoundManagerIndex].RemoveBuilding(pLocation)) {
		/* Adds money for selling */
		FPlacedBuildingParameters params;
		HandleBuildingValues(Building, true, params);

		if (pMoneyBack) Money += Building.Cost * (SellPercentage / 100);

		if (PollutionManager)
		{
			PollutionManager->RemovePollutionAreaByPlacedBuilding(placedBuilding);
		}

		GetWorld()->DestroyActor(placedBuilding);

		/* Population related things are not for EBC roads or none. */
		if (Building.BuildingCategory == EBuildingCategory::EBC_ROADS || Building.BuildingCategory == EBuildingCategory::EBC_NONE) return true;

		/* Found building is either residence or the rest, if rest then workers gotta be included in this, otherwise population only */
		Building.BuildingCategory == EBuildingCategory::EBC_RESIDENCE	? PopulationManager->RemovePopulationAtLocation(pLocation, Building.VisitorCapacity, 0, true)
																			: PopulationManager->RemoveEmployeesAtLocation(pLocation, Building.BuildingCategory, 0, true);
		
		return true;
	}
	return false;
}

FBuildingStats ACitySystemGameMode::GetCurrentBuildingStats(FName pBuildingName)
{
	return Buildings.FindRef(pBuildingName);
}
/*
Checks if the player has enough money to buy the building
- pBuilding = Building to check money with
+ Returns true on sufficient money, false on insufficient
*/
bool ACitySystemGameMode::HasSufficientFunds(FBuildingStats pBuilding)
{
	return Money >= pBuilding.Cost * (1.f - GetModifierByType(EModifierType::EMT_DISCOUNT_BUILDING));
}

bool ACitySystemGameMode::HasSufficientFundsMoney(float pMoney)
{
	return Money >= pMoney;
}

/*
Buys the policy
- pIndex = Index of the policy
+ Returns false when Index is not found/ Insufficient funds/ Policy is already unlocked
*/
bool ACitySystemGameMode::BuyPolicy(FName pId)
{
	// Not enough support or not in the list
	FPolicy *FoundPolicy = Policies.Find(pId);
	if (!FoundPolicy || FoundPolicy->bUnlocked || !GetPolicyUnlockedByMilestone(pId)) {
		return false;
	}
	FoundPolicy->bUnlocked = true;

	Policies.Add(pId, *FoundPolicy);
	FoundPolicy = nullptr;
	return true;
}

/*
Toggles a policy between activated and deactivated
If its unlocked and has a valid spot in the policy list, it'll do it, otherwise it won't do anything with the policy
*/
bool ACitySystemGameMode::TogglePolicy(FName pId)
{
	FPolicy *FoundPolicy = Policies.Find(pId);
	if (!FoundPolicy || !FoundPolicy->bUnlocked) return false;
	if (FoundPolicy->bActive && FoundPolicy->PolicyType == EPolicy::EnableTax && !Tutorial->HasCompletedTutorial ()) return true; //Make sure we can't disable the tax policy during the tutorial
	FoundPolicy->bActive = !FoundPolicy->bActive;
	for (auto& Modifier : FoundPolicy->Modifiers)
	{
		Modifiers.Add(Modifier.Key, GetModifierByType(Modifier.Key) + (Modifier.Value * (FoundPolicy->bActive ? 1 : -1)));
	}

	bool found = false;
	for (int i = 0; i < PoliciesDayCounters.Num(); i++)
	{
		if (PoliciesDayCounters[i] == FoundPolicy->Name)
		{
			found = true;
			//PoliciesDayCounters[i].Policy = FoundPolicy;
		}
	}
	if(!found && FoundPolicy->bActive){
		//PoliciesDayCounters.Add(FoundPolicy)
	}

	if (FoundPolicy->bActive)
		PolicyUpkeepCosts.Add(pId, FoundPolicy->PolicyUpkeepCosts);

	Policies.Add(pId, *FoundPolicy);

	bool bActive = FoundPolicy->bActive;
	FoundPolicy = nullptr;
	return bActive;
}

FPolicy ACitySystemGameMode::FindPolicyByID(FName pId)
{
	FPolicy *FoundPolicy = Policies.Find(pId);
	if (FoundPolicy) return *FoundPolicy;
	return FPolicy();
}

void ACitySystemGameMode::SetLightAndHorizon_Implementation(FGameTimeState pGameTimeState) {

}

void ACitySystemGameMode::PushFire(int pBuildingCategory)
{
	if (BuildingEventsManager != nullptr) BuildingEventsManager->PushFireEvent(pBuildingCategory);
}

void ACitySystemGameMode::PushCrime(int pBuildingCategory)
{
	if (BuildingEventsManager != nullptr) BuildingEventsManager->PushCrimeEvent(pBuildingCategory);
}

void ACitySystemGameMode::PushAccident(int pBuildingCategory)
{
	if (BuildingEventsManager != nullptr) BuildingEventsManager->PushAccidentEvent(pBuildingCategory);
}

void ACitySystemGameMode::RemovePop (int pPopToRemove) {
	PopulationManager->RemovePopulation (pPopToRemove);
}

void ACitySystemGameMode::TutorialPartCompleted (ETutorialSteps pTutorialStep) {
	Tutorial->SetStepCompleted (pTutorialStep);
}

void ACitySystemGameMode::TutorialHasCompleted (ETutorialSteps pTutorialStep) {
	Tutorial->HasCompletedStep (pTutorialStep);
}

void ACitySystemGameMode::TutorialHasCompletedUpTo (ETutorialSteps pTutorialStep) {
	UE_LOG(LogTemp, Warning, TEXT("Has completed up to %d: %s"), (int)pTutorialStep, Tutorial->HasCompletedUpToStep (pTutorialStep) ? TEXT("True") : TEXT("False"));
}

void ACitySystemGameMode::AddPopulation (int pAmount) {
	PopulationManager->AddPopulation (pAmount);
}

void ACitySystemGameMode::RemovePopulation (int pAmount) {
	PopulationManager->RemovePopulation (pAmount);
}

void ACitySystemGameMode::HandleTimelapseDay() {
	TimelapseHour + 1 == 24 ? TimelapseHour = 0 : TimelapseHour++;
	SetGameTimeState(TimelapseHour);
}

#pragma region Update
/*
Time based actions, this is called every timer interval tick
*/
void ACitySystemGameMode::HandleTimeActions() {
	if (TimeActions.IsBound()) TimeActions.Broadcast ();
}

void ACitySystemGameMode::Tick (float pDeltaSeconds) {
	Super::Tick (pDeltaSeconds);
	if (Update.IsBound ()) Update.Broadcast ();
}

/*
Generate random amount of population, can also be used to remove
*/
void ACitySystemGameMode::UpdatePopulation () {
	//Get the deviation from the mean
	int deviation = FMath::Abs(HappinessManager->GetHappinessPercentage () - happinessMean);

	float  modifier = HappinessManager->GetHappinessPercentage() >= happinessMean ? 1.0f : -1.0f;

	//Always add one, so the player will get still gain population at the start of the game, and only starts losing when happiness falls below ~35%
	float populationModified = (modifier * (cityGrowthModifier * populationSqrtModifier * FMath::Sqrt (FMath::Pow (deviation, 3.25f)))+ 1.0f) / daysPerPopModification; //Had "modifier *" in front of (cityGrowthModifier
	populationFractionBuffer += populationModified;

	//If a total of 1 person has been accumulated (or -1, if we're losing people), get the fraction and keep that as the new total fraction. Use the whole number to add or subtract to/from current population.
	if (populationFractionBuffer >= 1.0f || populationFractionBuffer <= -1.0f) {
		populationFractionBuffer *= modifier; //Switch back to positive numbers so that the fmod will go correctly
		int populationToModify = (populationFractionBuffer - (FMath::Fmod(populationFractionBuffer, 1.0f))) * modifier;
		populationToModify >= 0 ? PopulationManager->AddPopulation (populationToModify) : PopulationManager->RemovePopulation (populationToModify * modifier);
		cityGrowthModifier = cityGrowthModifierBase * (0.05f * FMath::Sqrt (PopulationManager->GetPopulationCount ()) + 1.0f);
		populationFractionBuffer -= populationToModify * modifier;
	}
}

void ACitySystemGameMode::UpdateTime () {
	CurrentDay = StartDate.GetDay ();
	CurrentMonth = StartDate.GetMonth ();
	CurrentYear = StartDate.GetYear ();

	StartDate += FTimespan (24, 0, 0);
	DayCounter++;

	if (IsDayPassed ()) OnDayPassed ();
	if (IsWeekPassed ()) OnWeekPassed ();
	if (IsMonthPassed ()) OnMonthPassed ();
}

void ACitySystemGameMode::UpdateHappiness () {

}

void ACitySystemGameMode::UpdatePolicies () {

}

void ACitySystemGameMode::UpdateInfoForPlayer () {

}

void ACitySystemGameMode::UpdateNotificationManager () {

}

void ACitySystemGameMode::UpdateMilestoneManager () {
	MilestonesManager->CheckMilestones ();
}

void ACitySystemGameMode::UpdateEventManager () {
	EventManager->Step ();
}

void ACitySystemGameMode::OnDayPassed () {
	//----------Policies----------//
	for (auto& Elem : Policies) {
		FName Name = Elem.Key;
		FPolicy Policy = Elem.Value;
		if (Policy.bActive) {
			Policies.Remove (Name);
			Policy.DaysActive++;
			Policies.Add (Name, Policy);
		} else if (Policy.DaysActive != 0)	{	// If not active and daysactive is greater than 0, reset days active
			Policies.Remove (Name);
			Policy.DaysActive = 0;
			Policies.Add (Name, Policy);
		}
	}
	//----------Policies----------//

	//----------Notifications----------//
	NotificationManager->CheckConditions ();
	//----------Notifications----------//

	//----------Happiness----------//
	if (Tutorial->HasCompletedTutorial ()) {
		HappinessManager->DayPassed ();
	}
	//----------Happiness----------//

	//----------BuildingEvents----------//
	BuildingEventsManager->CheckUndispatchedVehicles();
	//----------BuildingEvents----------//
}

void ACitySystemGameMode::OnWeekPassed () {
	//----------Policies----------//
	RemoveUnusedPolicies ();
	//----------Policies----------//

	//----------Happiness----------//
	HappinessManager->WeekPassed ();
	//----------Happiness----------//
}

void ACitySystemGameMode::OnMonthPassed () {
	//----------PlayerInfo----------//
	if (PlayerController) {
		PlayerController->ShowMonthlyMoneyStatus ();
		PlayerController->ShowMonthlyPopulationStatus ();
	}
	PopulationManager->SetPopulationCountBeginningOfMonth ();
	Money += GetTotalIncome ();
	//----------PlayerInfo----------//
}
#pragma endregion

bool ACitySystemGameMode::IsDayPassed()
{
	/*
		Checks for day to be bigger than last day OR
		Month to be bigger than last one, as well as day to be the 1st of the month (this is due to the equation not working if day is lower than last month's)
	*/
	return StartDate.GetDay() > CurrentDay || (StartDate.GetMonth() > CurrentMonth && StartDate.GetDay() == 1);
}

bool ACitySystemGameMode::IsWeekPassed()
{
	/*
		Checks for day of week to be monday
		At 00:00
	*/
	return IsDayPassed() && StartDate.GetDayOfWeek() == EDayOfWeek::Monday;
	// return StartDate.GetDayOfWeek() == EDayOfWeek::Monday && StartDate.GetHour() == 0 && StartDate.GetMinute() == 0;
}

bool ACitySystemGameMode::IsMonthPassed()
{
	/*
		Checks whether the month is bigger than last OR
		Year bigger than last year's, as well as 1st month as equation doesn't work here as well
	*/
	return StartDate.GetMonth() > CurrentMonth || (StartDate.GetYear() > CurrentYear && StartDate.GetMonth() == 1);
}

/*
Sets the game speed
- pGameSpeed = The game speed multiplier, 1/2/4 is used
*/
void ACitySystemGameMode::SetGameSpeed(int pGameSpeed)
{
	/* Current multiplier is being set here */
	CurrentGameSpeedMultiplier = pGameSpeed;

	if (pGameSpeed == 0) {
		/* Pause */
		GetWorld()->GetTimerManager().PauseTimer(TimeHandler);
		GetWorld()->GetTimerManager().PauseTimer(TimelapseDay);
		return;
	}
	/* Restarts the game timer again */
	RestartGameTimer();
	//RestartTimelapseTimer();

	if (TrafficSystem)
		TrafficSystem->SetUpdatesPerAgentPerTick(pGameSpeed);
}

/*
Custom format for dates/times in game
+ The date in string
*/
FString ACitySystemGameMode::GetDateTimeInString()
{
	/* Date */
	FString Date = AddZeroInFrontOfInt(StartDate.GetDay())
		.Append("/")
		.Append(AddZeroInFrontOfInt(StartDate.GetMonth())
			.Append("/")
			.Append(FString::FromInt(StartDate.GetYear())));

	/* If time is disabled from showing, return date */
	if (!bShowTime) return Date;

	/* Time */
	FString Time =	AddZeroInFrontOfInt(TimelapseHour)
					.Append(":00");

	return Date.Append(" ").Append(Time);
}

UPopulationManager* ACitySystemGameMode::GetPopulationManager() {
	return PopulationManager;
}

AEventManager* ACitySystemGameMode::GetEventManager () {
	return EventManager;
}

AMilestoneManager* ACitySystemGameMode::GetMilestoneManager () {
	return MilestonesManager;
}

UHappinessManager* ACitySystemGameMode::GetHappinessManager () {
	return HappinessManager;
}

UGoalManager* ACitySystemGameMode::GetGoalManager () {
	return GoalManager;
}

void ACitySystemGameMode::ModifyCapacityCount(FBuildingStats pBuilding, bool pRemove)
{
	if (pBuilding.BuildingCategory == EBuildingCategory::EBC_ROADS) return;
	
	// No road, continue
	int Modifier = pRemove ? -1 : 1;

	/* Work capacity */
	int VisitorCapacity = PlacedBuildingsCapacityCount.FindOrAdd(pBuilding.BuildingType);
	PlacedBuildingsCapacityCount.Add(pBuilding.BuildingType, VisitorCapacity + (pBuilding.VisitorCapacity * Modifier));
}

/*
Get total amount of pollution in the city
+ Integer value of the total pollution count
*/
int ACitySystemGameMode::GetTotalPollutionAmount()
{
	return (PollutionManager) ? PollutionManager->GetPollution() : -1;
}

/*
Returns the pollution percentage between 0 and 1. 
*/
float ACitySystemGameMode::GetTotalPollutionPercentage()
{
	return FMath::Clamp(((float)GetTotalPollutionAmount() / (float)PollutionManager->GetPollutionMax()), 0.0f, 1.0f);
}

FBuildingStats ACitySystemGameMode::GetBuildingOnLocation(ABuildableActor* pBuildableActor, FVector pLocation)
{
	if (!pBuildableActor) return FBuildingStats();
	int ManagerIndex = FindIndexBuildingMananagerByBuildableActor(pBuildableActor);
	if (ManagerIndex == -1) return FBuildingStats();
	APlacedBuilding* FoundBuilding = BuildingManagers[ManagerIndex].FindPlacedBuildingByLocation(pLocation);
	return FoundBuilding ? FoundBuilding->BuildingStats : FBuildingStats();
}




/*
Take the RowName of the policy
*/
FPolicy* ACitySystemGameMode::GetPolicyByName(FName pPolicyName)
{
	return Policies.Find(pPolicyName);
}

bool ACitySystemGameMode::IsAllowedToBuildBuilding(FBuildingStats pBuilding, bool pIsFreeBuilding /* = false */)
{
	if (pIsFreeBuilding) return true;
	if (bIsLoadingSavegame) return true; // Override allowing to build
	if (pBuilding.BuildingCategory == EBuildingCategory::EBC_ROADS && HasSufficientFunds(pBuilding)) return true;
	if (!bIsCityHallPlaced && pBuilding.BuildingType != EBuildingType::EB_CITY_HALL ||
		bIsCityHallPlaced && pBuilding.BuildingType == EBuildingType::EB_CITY_HALL) {
		return false;
	}
	return HasSufficientFunds(pBuilding);
}

float ACitySystemGameMode::GetCurrentPolicyUpkeepCosts()
{
	float TotalPolicyUpkeepCost = 0.f;
	for (auto& UpkeepCost : PolicyUpkeepCosts)
	{
		TotalPolicyUpkeepCost += UpkeepCost.Value;
	}
	return TotalPolicyUpkeepCost;
}

TArray<FBuildingManager> ACitySystemGameMode::GetBuildingManagersForJobs(bool pServices)
{
	TArray<FBuildingManager> Managers;
	for (auto& Manager : BuildingManagers)
	{
		if(pServices && Manager.ContainsServices() || !pServices && Manager.ContainsGeneralworkPlaces())
			Managers.Add(Manager);
	}
	return Managers;
}

TMap<FVector, FBuildingStats> ACitySystemGameMode::GetMinimumNotReachedServices()
{
	TMap<FVector, FBuildingStats> Buildings;
	TMap<FVector,FBuildingStats> ServiceBuildings = GetBuildingsByCategory(EBuildingCategory::EBC_SERVICES);
	for (auto& Building: ServiceBuildings)
	{
		if (Building.Value.JobCapacity > 0) {
			int MinimumJobsTaken = FMath::RoundHalfToZero(Building.Value.JobCapacity * (MinimumWorkPercentage / 100));
			if (PopulationManager->GetEmployeeCountByLocation(Building.Key) < MinimumJobsTaken) {
				Buildings.Add(Building.Key,Building.Value);
			}
		}
	}
	return Buildings;
}

void ACitySystemGameMode::LoadSaveGame()
{
	if (!GameInstance || !GameInstance->GetCurrentSaveGame()) return;
	IsNewGame = false;
	bIsLoadingSavegame = true;
	UCitySaveGame* SaveGame = GameInstance->GetCurrentSaveGame();
	
	/* Date */
	StartDate = SaveGame->Data.GameDate;
	TimelapseHour = SaveGame->Data.TimelapseHour;
	GameTimeState = SaveGame->Data.GameTimeState;
	DayCounter = SaveGame->Data.DayCounter;
	IsHappinessThresholdReached = SaveGame->Data.HappinessCounts;
	CityName = SaveGame->Data.CityName;

	LoadPoliciesFromSaveGame(SaveGame);

	MilestonesManager->LoadMilestonesFromSavegame(SaveGame->Data.ResourceStatistics.MilestonesList);

	LoadBuildingsFromSaveGame(SaveGame);

	LoadGameEventsFromSaveGame(SaveGame);

	/* Basic values */
	Support = SaveGame->Data.ResourceStatistics.Support;
	Money = SaveGame->Data.ResourceStatistics.Money;
	PopulationManager->Import(SaveGame->Data.ResourceStatistics.PopulationExport);

	/* Maybe unnecessary nullptr garbage collection*/
	SaveGame = nullptr;
	bIsLoadingSavegame = false;
}

void ACitySystemGameMode::LoadBuildingsFromSaveGame(UCitySaveGame* pSaveGame)
{
	if (!PlayerController || !pSaveGame) return;
	bIsCityHallPlaced = pSaveGame->Data.BuildingStatistics.bIsCityHallPlaced;
	for (auto& ExportedObject : pSaveGame->Data.BuildingStatistics.BuildingManagers)
	{
		ABuildableActor* BuildableActor = PlayerController->AddBuildableActor(ExportedObject.GetFirstBuilding());
		/* Adds building manager with buildable actor */
		FBuildingManager BuildingManager = FBuildingManager(BuildableActor);

		/* Loops through building, handles them the same way like buying buildings, but without money involved*/
		for (auto& Building : ExportedObject.Buildings)
		{
			FTransform buildingTransform = FTransform(Building.Value.Rotation, Building.Value.Location);
			FPlacedBuildingParameters params;
			params.bIsFree = true;
			params.bSkipCollision = true;
			params.bLoadFromSave = true;
			params.bSkipLocationOffset = true;
			params.bIsInteractable = Building.Value.bIsInteractable;
			BuildableActor->OnPreviewPlace(buildingTransform, true);
			BuildableActor->PlaceBuilding(buildingTransform, params);

			if (Building.Value.BuildingStatus != EBuildingStatus::ES_NONE) {
				if(BuildingEventsManager)BuildingEventsManager->LoadEventsForBuilding(Building.Value.BuildingStatus, Building.Value.Location, Building.Value);
			}
		}
		PlayerController->SetSelectedBuildableActor(BuildableActor);
		BuildingManagers.Add(BuildingManager);
	}

	if (BuildingEventsManager)BuildingEventsManager->PushEventsAfterLoadingSave();
}

void ACitySystemGameMode::LoadPoliciesFromSaveGame(UCitySaveGame* pSaveGame)
{
	/* Policies */
	if (!pSaveGame) return;
	for (auto& Policy : pSaveGame->Data.ResourceStatistics.Policies)
	{
		/* Finds the ID */
		FPolicy* FoundPolicy = Policies.Find(Policy.Key);

		/* If found (should always be found, but error checks) */
		if (FoundPolicy) {
			FoundPolicy->bMilestoneReached = Policy.Value.bMilestoneReached;
			FoundPolicy->bUnlocked = Policy.Value.bUnlocked;
			Policies.Add(Policy.Key, *FoundPolicy);
			if (Policy.Value.bActive) {
				TogglePolicy(Policy.Key);
			}
		}
	}
}

void ACitySystemGameMode::LoadGameEventsFromSaveGame(class UCitySaveGame* pSaveGame)
{
	//TODO: Implementation
	/* Game Events */
	if (!pSaveGame) return;
	for (auto& GameEvent : pSaveGame->Data.GameEvents)
	{
		/* Finds the ID */
		FNCDEvent* FoundGameEvent = GameEventsList.Find(GameEvent.Key);

		/* If found (should always be found, but error checks) */
		if (FoundGameEvent) {
			FoundGameEvent->bUnlockedEvent = GameEvent.Value.bUnlocked;
			FoundGameEvent->bCompletedEvent = GameEvent.Value.bCompleted;
			GameEventsList.Add(GameEvent.Key, *FoundGameEvent);
		}
	}
}

int ACitySystemGameMode::FindOrCreateBuildingManager(ABuildableActor* pBuildableActor)
{
	int FoundManagerIndex = FindIndexBuildingMananagerByBuildableActor(pBuildableActor);
	if (FoundManagerIndex == -1) {
		FBuildingManager Manager = FBuildingManager(pBuildableActor);
		return BuildingManagers.Add(Manager);
	}
	return FoundManagerIndex;
}

/*
Gets the amount of buildings per specified type
- pBuildingType = The building type that is looked for
+ Integer value of building count that is found
*/
int ACitySystemGameMode::GetBuildingCountOfType(EBuildingType pBuildingType)
{
	int* FoundBuildingType = PlacedBuildingsCounters.Find(pBuildingType);
	if (!FoundBuildingType) return 0;
	return *FoundBuildingType;
}


TMap<FVector, FBuildingStats> ACitySystemGameMode::GetBuildingsByType(EBuildingType pBuildingType)
{
	TMap<FVector, FBuildingStats> Buildings;
	for (auto& BuildingManager : BuildingManagers)
	{
		if (BuildingManager.BuildingsType == pBuildingType)
		{
			//Buildings.Append(BuildingManager.Buildings);
			BuildingManager.ExportAllBuildings(Buildings);
		}
	}
	return Buildings;
}

TMap<FVector, APlacedBuilding*> ACitySystemGameMode::GetPlacedBuildingsByType(EBuildingType pBuildingType)
{
	TMap<FVector, APlacedBuilding*> Buildings;
	for (auto& BuildingManager : BuildingManagers)
	{
		if (BuildingManager.BuildingsType == pBuildingType)
		{
			Buildings.Append(BuildingManager.Buildings);
			//BuildingManager.ExportAllBuildings(Buildings);
		}
	}
	return Buildings;
}


TMap<FVector, FBuildingStats> ACitySystemGameMode::GetBuildingsByCategory(EBuildingCategory pBuildingCategory)
{
	TMap<FVector, FBuildingStats> Buildings;
	for (auto& BuildingManager: BuildingManagers)
	{
		if (BuildingManager.BuildingsCategory == pBuildingCategory)
		{
			//Buildings.Append(BuildingManager.Buildings);
			BuildingManager.ExportAllBuildings(Buildings);
		}
	}
	return Buildings;
}

FBuildingStats ACitySystemGameMode::GetNearestBuildingByTypes(TArray<EBuildingType> pTypes, FVector pCheckLocation)
{
	TMap<FVector, APlacedBuilding*> Buildings;
	for (auto& buildingType : pTypes)
	{
		Buildings.Append(GetPlacedBuildingsByType(buildingType));
	}
	//Buildings.KeySort(FSortByDistance(pCheckLocation));
	float shortestDistance = 1000000.0f;
	float distance;
	FBuildingStats nearestBuilding = FBuildingStats();
	for (auto& building : Buildings)
	{
		if (!building.Value) continue;
		distance = FVector::DistSquared(pCheckLocation, building.Value->BuildingStats.ConnectedRoadLocation);

		if (distance < shortestDistance)
		{
			shortestDistance = distance;
			nearestBuilding = building.Value->BuildingStats;
		}
	}
	return nearestBuilding;
}

FBuildingStats ACitySystemGameMode::GetNearestBuildingByType(EBuildingType pType, FVector pCheckLocation)
{
	TMap<FVector, APlacedBuilding*> Buildings = GetPlacedBuildingsByType(pType);
	//Buildings.KeySort(FSortByDistance(pCheckLocation));
	float shortestDistance = 1000000.0f;
	float distance;
	FBuildingStats nearestBuilding = FBuildingStats();
	for (auto& building : Buildings)
	{
		if (!building.Value) continue;

		distance = FVector::DistSquared(pCheckLocation, building.Value->BuildingStats.ConnectedRoadLocation);

		if (distance < shortestDistance)
		{
			shortestDistance = distance;
			nearestBuilding = building.Value->BuildingStats;
		}
	}
	//UE_LOG(LogTemp, Warning, TEXT("Invalid building return from GetNearestBuildingByType"));
	return nearestBuilding;
}

bool ACitySystemGameMode::IsCityHallPlaced()
{
	return bIsCityHallPlaced;
}

TMap<FVector, FBuildingStats> ACitySystemGameMode::GetAllJobBuildingsExceptServices()
{
	TMap<FVector, FBuildingStats> Buildings;
	for (auto& BuildingManager : BuildingManagers)
	{
		if (BuildingManager.ContainsGeneralworkPlaces())
		{
			//Buildings.Append(BuildingManager.Buildings);
			BuildingManager.ExportAllBuildings(Buildings);
		}
	}
	return Buildings;
}

TMap<FVector, FBuildingStats> ACitySystemGameMode::GetMinimumAllJobBuildingsExceptServices()
{
	TArray<FBuildingManager> BuildingManagers = GetBuildingManagersForJobs();
	TMap<FVector, FBuildingStats> Buildings;

	for (auto& BuildingManager: BuildingManagers)
	{
		for (auto& Building: BuildingManager.Buildings)
		{
			if (Building.Value && Building.Value->BuildingStats.JobCapacity > 0) {
				int MinimumJobsTaken = FMath::RoundHalfToZero(Building.Value->BuildingStats.JobCapacity * (MinimumWorkPercentage / 100));
				if (PopulationManager->GetEmployeeCountByLocation(Building.Key) < MinimumJobsTaken) {
					Buildings.Add(Building.Key, Building.Value->BuildingStats);
				}
			}
		}
	}
	return Buildings;
}

//Grab the income of all buildings
float ACitySystemGameMode::GetBuildingsIncomeTotal()
{
	float TotalIncome = 0.f;
	for (auto& Building : BuildingsIncomeAtLocation)
	{
		TotalIncome += Building.Value;
	}
	return TotalIncome * (1.f + GetModifierByType(EModifierType::EMT_INCOME_BUILDING));
}

float ACitySystemGameMode::GetTaxIncome()
{
	return PopulationManager->GetPopulationCount() * (MonthlyIncomePerPerson * (VatPercentage / 100));
}

float ACitySystemGameMode::GetTotalIncome(bool pWithoutExpenses) {
	return GetBuildingsIncomeTotal() - (pWithoutExpenses ? 0 : GetMonthlyExpensesTotal());
}

float ACitySystemGameMode::GetMonthlyExpensesTotal()
{
	float TotalBuildingExpenses = 0.f;
	for (auto& Building : BuildingsExpenses)
	{
		float UpkeepToAdd = Building.Value;
		switch (Building.Key) {
			case EBuildingType::EB_POLICE:
				UpkeepToAdd += GetModifierByType(EModifierType::EMT_UPKEEP_POLICE);
			break;
			default:break;
		}
		TotalBuildingExpenses += UpkeepToAdd;
	}
	return TotalBuildingExpenses + GetCurrentPolicyUpkeepCosts();
}

float ACitySystemGameMode::GetBuildingExpensesByType(EBuildingType pBuildingType)
{
	float* FoundExpensesByType = BuildingsExpenses.Find(pBuildingType);
	if (!FoundExpensesByType) return 0.f;
	/* Loops through all the buildings, might want to change to improve performance */
	return *FoundExpensesByType;
}

float ACitySystemGameMode::GetModifierByType(EModifierType pModifierType)
{
	float* FoundModifierByType = Modifiers.Find(pModifierType);
	if (!FoundModifierByType) return 0.f;
	/* Loops through all the buildings, might want to change to improve performance */
	return *FoundModifierByType;
}

int ACitySystemGameMode::GetVisitorCapacityByType(EBuildingType pBuildingType)
{
	int* FoundBuildingsCapacity = PlacedBuildingsCapacityCount.Find(pBuildingType);
	if (!FoundBuildingsCapacity) return 0;
	/* Loops through all the buildings, might want to change to improve performance */
	return *FoundBuildingsCapacity;
}

int ACitySystemGameMode::HasEnoughBuildingsOfTypeByInt(EBuildingType pBuildingType)
{
	return GetVisitorCapacityByType(pBuildingType) >= PopulationManager->GetPopulationCount() ? 1 : -1;
}

/*
	Notification function
*/
float ACitySystemGameMode::HasEnoughBuildingsOfType(EBuildingType pBuildingType)
{
	/* 1.f to avoid notifications from popping up */
	if (PopulationManager->IsPopulationZero()) return 1.f;
	return GetVisitorCapacityByType(pBuildingType) / PopulationManager->GetPopulationCount() * 1.f;
}

/*
	Notification function
*/
float ACitySystemGameMode::HasEnoughJobsForPeople()
{
	/* 1.f to avoid notifications from popping up */
	if (PopulationManager->GetPopulationCount() == 0) return 1.0f;
	return MaxWorkCapacity / PopulationManager->GetPopulationCount();
}

/*
	Notification function
*/
float ACitySystemGameMode::HasEnoughPeopleForJobs()
{
	/* 1.f to avoid notifications from popping up */
	if (MaxWorkCapacity == 0) return 1.0f;
	return PopulationManager->GetPopulationCount() / MaxWorkCapacity;
}

bool ACitySystemGameMode::GetPolicyUnlockedByMilestone( FName pId )
{
	FPolicy *FoundPolicy = Policies.Find(pId);
	if (!FoundPolicy) {
		return false;
	}

	return FoundPolicy->bMilestoneReached;
}

bool ACitySystemGameMode::GetBuildingUnlockedByMilestone(FName pId)
{
	//UE_LOG (LogTemp, Warning, TEXT ("ID: %s"), *pId.ToString ());
	FBuildingStats *FoundBuilding = Buildings.Find(pId);
	if (!FoundBuilding) {
		return false;
	}

	return FoundBuilding->bMilestoneReached;
}

bool ACitySystemGameMode::GetUnlockedGameEvents(FName pId)
{
	FNCDEvent *FoundGameEvent = GameEventsList.Find(pId);
	if (!FoundGameEvent) return false;

	return FoundGameEvent->bUnlockedEvent;
}

//Adds Money to player's money
void ACitySystemGameMode::RewardMoney(int pAmount)
{
	Money += pAmount;
}

ATrafficSystem* ACitySystemGameMode::GetTrafficSystem()
{
	return TrafficSystem;
}

void ACitySystemGameMode::SpawnRandomTrafficToExit(int32 pAmount)
{
	TMap<FVector, FBuildingStats> statsMap = GetBuildingsByType(EBuildingType::EB_HOUSE);
	if (statsMap.Num() == 0) return;
	TArray<FBuildingStats> buildingArray;
	statsMap.GenerateValueArray(buildingArray);
	for (int32 i = 0; i < pAmount; i++)
	{
		int randomCount = FMath::RandRange(0, buildingArray.Num()-1);
		TrafficSystem->SpawnRandomAgentByType(buildingArray[randomCount].ConnectedRoadLocation,StartRoadLocation,EBuildingType::EB_HOUSE);
	}
}

void ACitySystemGameMode::RemoveUnusedPolicies()
{
	TArray<FName> InactivePolicies;
	for (auto& ActivePolicy : PolicyUpkeepCosts)
	{
		FPolicy FoundPolicy = FindPolicyByID(ActivePolicy.Key);
		if (!FoundPolicy.bActive)
			InactivePolicies.Add(ActivePolicy.Key);
	}
	for (auto& InactivePolicy : InactivePolicies)
	{
		PolicyUpkeepCosts.Remove(InactivePolicy);
	}
}

void ACitySystemGameMode::InitializePolicies(FString pDTName)
{
	FString filePathString = "/Game/Assets/Datatables/" + pDTName;
	const TCHAR* filePath = *filePathString;
	UDataTable* datatable = LoadObject<UDataTable> (nullptr, filePath);
	PoliciesDataTable = datatable;

	UE_LOG (LogTemp, Warning, TEXT ("Policies datatable: %s"), PoliciesDataTable ? TEXT ("True") : TEXT ("False"));

	if (PoliciesDataTable) {
		FString ContextString;
		TArray<FName> RowNames = PoliciesDataTable->GetRowNames();
		for (auto& RowName : RowNames)
		{
			FPolicy* Policy = PoliciesDataTable->FindRow<FPolicy>(RowName, ContextString);
			if (Policy) {
				FPolicy DereferencedPolicy = *Policy;
				DereferencedPolicy.ID = *RowName.ToString();
				Policies.Add(DereferencedPolicy.ID, DereferencedPolicy);
			}
			Policy = nullptr;
		}
	}
}

void ACitySystemGameMode::InitializeBuildings(FString pDTName)
{
	FString filePathString = "/Game/Assets/Datatables/" + pDTName;
	const TCHAR* filePath = *filePathString;
	UDataTable* datatable = LoadObject<UDataTable> (nullptr, filePath);
	BuildingsDataTable = datatable;

	if (BuildingsDataTable) {
		FString ContextString;
		TArray<FName> RowNames = BuildingsDataTable->GetRowNames();
		for (auto& RowName : RowNames)
		{
			FBuildingStats* Building = BuildingsDataTable->FindRow<FBuildingStats>(RowName, ContextString);
			if (Building) {
				FBuildingStats DereferencedBuilding = *Building;
				DereferencedBuilding.ID = *RowName.ToString();
				/* For tests, its easier to have everything unlocked */
				if (bUnlockAllBuildings)
				{
					DereferencedBuilding.bMilestoneReached = true;
				}
				if (bSetBuildingBlueprints && BuildingBlueprintsDataTable)
				{
					Building->BuildingBlueprint.DataTable = BuildingBlueprintsDataTable;
					Building->BuildingBlueprint.RowName = RowName;
					FBuildingBlueprints* buildingBP = BuildingBlueprintsDataTable->FindRow<FBuildingBlueprints>(RowName, ContextString);
					if (buildingBP)
					{
						TSubclassOf<APlacedBuilding> buildingClass = buildingBP->PlacedBuildingBlueprint;
						APlacedBuilding* defaultBP = buildingClass->GetDefaultObject<APlacedBuilding>();
						if (defaultBP)
						{
							if (DereferencedBuilding.StaticMesh)
								defaultBP->GetMeshComponent()->SetStaticMesh(DereferencedBuilding.StaticMesh);
							//defaultBP->GetMeshComponent()
						}
					}
				}
				Buildings.Add(DereferencedBuilding.ID, DereferencedBuilding);
			}
			Building = nullptr;
		}
		PlayerController->AddAllBuildableActors ();
		OnBuildingsDTInitialized ();
		UE_LOG (LogTemp, Warning, TEXT ("[CitySystemGameMode.cpp; line 1269]: Buildingstats initialized!"))
	} else {
		UE_LOG(LogTemp, Warning, TEXT("[CitySystemGameMode.cpp; line 1271]: Buildingstats datatable not found!"))
	}
}

void ACitySystemGameMode::InitializeGameEvents(FString pDTName)
{
	FString filePathString = "/Game/Assets/Datatables/" + pDTName;
	const TCHAR* filePath = *filePathString;
	UDataTable* datatable = LoadObject<UDataTable> (nullptr, filePath);
	GameEventsData = datatable;

	if (GameEventsData)
	{
		FString ContextString;
		TArray<FName> RowNames = GameEventsData->GetRowNames();
		for (auto& RowName : RowNames)
		{
			FNCDEvent* GameEvent = GameEventsData->FindRow<FNCDEvent>(RowName, ContextString);
			if (GameEvent)
			{
				FNCDEvent DereferencedGameEvent = *GameEvent;
				//Maybe add ID for lookup functionality like policy & buildings
				DereferencedGameEvent.ID = *RowName.ToString();
				GameEventsList.Add(DereferencedGameEvent.ID, DereferencedGameEvent);
			}
			GameEvent = nullptr;
		}

		UE_LOG( LogTemp, Warning, TEXT("GameEvents DataTable loaded into tmap"));
	}
}

FNCDEvent * ACitySystemGameMode::FindGameEventByID(FName pID)
{
	FNCDEvent *FoundEvent = GameEventsList.Find(pID);
	if (FoundEvent) return FoundEvent;
	return new FNCDEvent();
}

//Add income to the total upon placing a new building.
void ACitySystemGameMode::AddBuildingIncome(FVector pLocation, float Income) {
	if (!MinimumJobsReachedWorkplace.Find(pLocation)) {
		MinimumJobsReachedWorkplace.Add(pLocation, Income);
		BuildingsIncomeAtLocation.Add(pLocation, Income);
	}
}

//Remove a specific building's income
void ACitySystemGameMode::RemoveBuildingIncome (FVector pLocation) {
	if (MinimumJobsReachedWorkplace.Contains(pLocation)) MinimumJobsReachedWorkplace.Remove (pLocation);
	if (BuildingsIncomeAtLocation.Contains(pLocation)) BuildingsIncomeAtLocation.Remove (pLocation);
}

//Add building expenses to the total.
void ACitySystemGameMode::AddBuildingExpense (EBuildingType pBuildingType, float pUpkeep, float pModifier) {
	BuildingsExpenses.Add (pBuildingType, GetBuildingExpensesByType (pBuildingType) + (pUpkeep * pModifier));
}

/*
Adds or removes a building
- pBuilding = The building
- pRemoving = Whether it should be removed or not
- pIndex = Necessary for removal
*/
void ACitySystemGameMode::HandleBuildingValues(FBuildingStats& pBuilding, bool pRemoving, FPlacedBuildingParameters& pParameters)
{
	int Modifier = pRemoving ? -1 : 1;

	/* Handles the capacity */
	ModifyCapacityCount(pBuilding, pRemoving);

	if(pBuilding.bGeneratesFood)
		pBuilding.bIsHealthy ? HealthyFoodChainCount += Modifier : UnhealthyFoodChainCount += Modifier;

	/* Remove off count */
	PlacedBuildingsCounters.Add(pBuilding.BuildingType, GetBuildingCountOfType(pBuilding.BuildingType) + Modifier);

	// Has jobs available TODO:: START USING THIS AGAIN
	/*if(pBuilding.JobCapacity > 0)
		PopulationManager->AddMinimumCapacityWork(pBuilding.Location, FMath::RoundHalfToZero(pBuilding.JobCapacity * (MinimumWorkPercentage / 100)),pRemoving);*/
	
	// For income
	if (!pParameters.bIsFree) {
		if (pBuilding.BuildingType == EBuildingType::EB_CITY_HALL) {
			bIsCityHallPlaced = !pRemoving;
			if (!pRemoving) AddBuildingIncome (pBuilding.Location, pBuilding.Income);
			else RemoveBuildingIncome (pBuilding.Location);
		}

		/* Expenses */
		if (pRemoving) AddBuildingExpense (pBuilding.BuildingType, GetBuildingExpensesByType (pBuilding.BuildingType) + (pBuilding.UpkeepCost.MoneyCost), Modifier);
		//BuildingsExpenses.Add (pBuilding.BuildingType, GetBuildingExpensesByType (pBuilding.BuildingType) + (pBuilding.UpkeepCost.MoneyCost * Modifier));
	}
}

FBuildingStats* ACitySystemGameMode::GetBuildingByLocation(FVector pLocation)
{
	APlacedBuilding* foundBuilding = GetPlacedBuildingByLocation(pLocation);
	return foundBuilding ? &foundBuilding->BuildingStats : nullptr;
}

APlacedBuilding* ACitySystemGameMode::GetPlacedBuildingByLocation(FVector pLocation)
{
	for (auto& BuildingManager : BuildingManagers)
	{
		APlacedBuilding* FoundBuilding = BuildingManager.FindPlacedBuildingByLocation(pLocation);
		if (FoundBuilding) return FoundBuilding;
	}	
	UE_LOG(LogTemp, Error, TEXT("Could not find building at location %s"), *pLocation.ToCompactString());
	return nullptr;
}

FBuildingManager& ACitySystemGameMode::GetBuildingManagerByID(FName pBuildingIdentifier)
{
	return GetBuildingManagerByIndex(FindIndexBuildingMananagerByBuildableActorID(pBuildingIdentifier));
}

FBuildingStats ACitySystemGameMode::GetBuildingByLocationBP (FVector pLocation) {
	FBuildingStats* foundStats = GetBuildingByLocation(pLocation);
	return foundStats != nullptr ? *foundStats : FBuildingStats();
}

int ACitySystemGameMode::FindIndexBuildingMananagerByBuildableActor(ABuildableActor* pBuildableActor)
{
	for (int i = 0; i < BuildingManagers.Num(); i++)
	{
		if (BuildingManagers[i].BuildableActor == pBuildableActor) return i;
	}
	return -1;
}

int ACitySystemGameMode::FindIndexBuildingMananagerByBuildableActorID(FName pBuildableActorIdentifier)
{
	for (int i = 0; i < BuildingManagers.Num(); i++)
	{
		if (BuildingManagers[i].BuildableActor->GetIdentifier() == pBuildableActorIdentifier) return i;
	}
	return -1;
}

APlacedBuilding* ACitySystemGameMode::GetBuildingBlueprintFromStats(FBuildingStats& pBuildingStats)
{
	FString contextString;
	FBuildingBlueprints* blueprintStruct = pBuildingStats.BuildingBlueprint.GetRow<FBuildingBlueprints>(contextString);
	if (!blueprintStruct || !blueprintStruct->PlacedBuildingBlueprint) return nullptr;
	APlacedBuilding* defaultObject = blueprintStruct->PlacedBuildingBlueprint->GetDefaultObject<APlacedBuilding>();
	return defaultObject;
}

/*
Sets the state game time, whether its morning etc.
*/
void ACitySystemGameMode::SetGameTimeState(int pHour)
{
	if (StartDate.GetMinute() > 0) { return; }

	if (GameStates.Find(pHour)) {
		GameTimeState = *GameStates.Find(pHour);
		SetLightAndHorizon(GameTimeState);
	}

	ANCD_PlayerController* PController = Cast<ANCD_PlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));

	if (!PController) return;
	PController->OnTimeChange((pHour < 11 || pHour > 22));
}

UTutorial* ACitySystemGameMode::GetTutorial () {
	return Tutorial;
}

ANCD_PlayerController* ACitySystemGameMode::GetPlayerController () {
	return PlayerController;
}

void ACitySystemGameMode::SetCityName (FString pName) {
	CityName = pName;
}

FString ACitySystemGameMode::GetCityname () {
	return CityName;
}

void ACitySystemGameMode::OnBuildingsDTInitialized_Implementation () {
	UE_LOG (LogTemp, Warning, TEXT ("Buildings DT init event called!"));
}

bool ACitySystemGameMode::ProcessConsoleExec(const TCHAR* Cmd, FOutputDevice& Ar, UObject* Executor)
{
	bool handled = Super::ProcessConsoleExec(Cmd, Ar, Executor);
	if (!handled)
	{
		if (PollutionManager)
			handled &= PollutionManager->ProcessConsoleExec(Cmd, Ar, Executor);
		if (EventManager)
			handled &= EventManager->ProcessConsoleExec(Cmd, Ar, Executor);
		if (TrafficSystem)
			handled &= EventManager->ProcessConsoleExec(Cmd, Ar, Executor);
	}
	return handled;
}

void ACitySystemGameMode::UnlockNewPolicies(TArray<FDataTableRowHandle> pPoliciesToUnlock)
{
	for (const auto& policyUnlock : pPoliciesToUnlock)
	{
		TMap<FName, FPolicy> *PolicyList = &GetPolicyList();

		for (auto& _policy : *PolicyList)
		{
			FPolicy * Policy = &_policy.Value;
			if (policyUnlock.RowName == Policy->ID)
			{
				Policy->bMilestoneReached = true;
				UE_LOG(LogTemp, Warning, TEXT("Policy to be unlocked : %s"), *policyUnlock.RowName.ToString());
				UE_LOG(LogTemp, Warning, TEXT("Policy that is going to be unlocked : %s"), *Policy->ID.ToString());
			}
		}
	}
}

void ACitySystemGameMode::UnlockNewBuildings(TArray<FDataTableRowHandle> pBuildingsToUnlock)
{
	for (const auto& buildingUnlock : pBuildingsToUnlock)
	{
		TMap<FName, FBuildingStats> *BuildingList = &GetBuildingsList();

		for (auto& _building : *BuildingList)
		{
			FBuildingStats * Building = &_building.Value;
			if (buildingUnlock.RowName == Building->ID)
			{
				Building->bMilestoneReached = true;
				UE_LOG(LogTemp, Warning, TEXT("Building to be unlocked : %s"), *buildingUnlock.RowName.ToString());
				UE_LOG(LogTemp, Warning, TEXT("Building that is going to be unlocked : %s"), *Building->ID.ToString());
			}
		}
	}
}

void ACitySystemGameMode::UnlockNewGameEvents(TArray<FDataTableRowHandle> pGameEventToUnlock)
{
	//UE_LOG(LogTemp, Warning, TEXT("UnlockNewGameEvent Check 1"));
	for (const auto& eventToUnlock : pGameEventToUnlock)
	{
		//UE_LOG(LogTemp, Warning, TEXT("UnlockNewGameEvent Check 2"));
		TMap<FName, FNCDEvent> *EventList = &GetGameEventsList();

		for (auto& _gameEvent : *EventList)
		{


			FNCDEvent * GameEvent = &_gameEvent.Value;
			if (eventToUnlock.RowName == GameEvent->ID)
			{
				//UE_LOG(LogTemp, Warning, TEXT("UnlockNewGameEvent Check 3"));

				GameEvent->bUnlockedEvent = true;
				//GameEvent->bCompletedEvent = true;	// For testing purposes

				UE_LOG(LogTemp, Warning, TEXT("GameEvent to be unlocked : %s"), *eventToUnlock.RowName.ToString());
				UE_LOG(LogTemp, Warning, TEXT("GameEvent that is going to be unlocked : %s"), *GameEvent->ID.ToString());
			}
		}
	}
}