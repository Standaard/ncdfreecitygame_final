// Fill out your copyright notice in the Description page of Project Settings.

#include "DecorationInstances.h"
#include "BuildableActor.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "PlacedBuilding.h"
#include "PlacedRoad.h"
#include <DrawDebugHelpers.h>


// Sets default values
ADecorationInstances::ADecorationInstances()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SceneRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRootComponent"));
	RootComponent = SceneRootComponent;
	RootComponent->SetMobility(EComponentMobility::Stationary);

	Decorations = CreateDefaultSubobject<UHierarchicalInstancedStaticMeshComponent>(TEXT("Decorations"));
	Decorations->SetupAttachment(RootComponent);
	Decorations->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Decorations->SetMobility(EComponentMobility::Stationary);
	Decorations->SetGenerateOverlapEvents(false);
}

// Called when the game starts or when spawned
void ADecorationInstances::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ADecorationInstances::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADecorationInstances::UpdateDecoration(ABuildableActor* pBuildableActor, int pDecorationIndex) {
	//-----Workaround for "AddInstance" crash-----//
	Decorations->ReleasePerInstanceRenderData();
	Decorations->InitPerInstanceRenderData(true);
	//---------------------------------------------------------//

	Decorations->ClearInstances();
	//UHierarchicalInstancedStaticMeshComponent* MeshInstances = pBuildableActor->GetMeshInstancesComponent();
	TMap<FVector, APlacedBuilding*> storedMap = pBuildableActor->GetStoredBuildings();
	APlacedBuilding* building = nullptr;

	for (auto& pair : storedMap)
	{
		building = pair.Value;
		if (!building) continue;
		FTransform CopyTransform = building->GetActorTransform();

		APlacedRoad* castRoad = Cast<APlacedRoad>(building);
		if (castRoad && castRoad->GetRoadType() != ERoadType::EB_STRAIGHT) continue;
		//TODO: Use PlacedBuilding hotspots for decorations
		//FTransform CopyTransform = FTransform(building->BuildingStats.Rotation, building->BuildingStats.Location, FVector::OneVector);
		//MeshInstances->GetInstanceTransform(i, CopyTransform);

		FTransform newTransform = FTransform::Identity;
		FVector Rotator = relativeTransformToParent.GetLocation().RotateAngleAxis(CopyTransform.GetRotation().Rotator().Yaw, FVector(0, 0, 1));
		newTransform.SetLocation(Rotator + CopyTransform.GetLocation());
		//FVector rotationCopy = (FVector::ForwardVector * 50.0f).RotateAngleAxis(CopyTransform.GetRotation().Rotator().Yaw, FVector::UpVector);
		//DrawDebugDirectionalArrow(GetWorld(), CopyTransform.GetLocation(), CopyTransform.GetLocation() + rotationCopy, 6.0f, FColor::Red, false, 2.0f, 0, 12.f);
		FVector newRotation = relativeTransformToParent.GetRotation().RotateVector(CopyTransform.GetRotation().Vector());
		newTransform.SetRotation(newRotation.Rotation().Quaternion());
		FVector newScale = building->BuildingStats.Decorations[pDecorationIndex].RelativeToParent.GetScale3D();
		newTransform.SetScale3D(newScale);

		Decorations->AddInstance(newTransform);
		//i > Decorations->GetInstanceCount() ? Decorations->UpdateInstanceTransform(i, newTransform, false) : Decorations->AddInstance(newTransform);
	}
}

void ADecorationInstances::SetDecoration(UStaticMesh* pStaticMesh, UMaterialInterface* pMaterial, FTransform pRelativeTransform)
{
	StaticMesh = pStaticMesh;
	Material = pMaterial;
	relativeTransformToParent = pRelativeTransform;
	Decorations->SetStaticMesh(pStaticMesh);
	Decorations->SetMaterial(0, pMaterial);
}

