// Fill out your copyright notice in the Description page of Project Settings.

#include "BuildingDemolishActor.h"
#include "BuildableActor.h"
#include "Components/SceneComponent.h"
#include "ActorWidgetComponent.h"
#include <Kismet/GameplayStatics.h>
#include "NCD_PlayerController.h"

// Sets default values
ABuildingDemolishActor::ABuildingDemolishActor () {
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneRootComponent = CreateDefaultSubobject<USceneComponent> (TEXT ("SceneRootComponent"));
	RootComponent = SceneRootComponent;
	RootComponent->SetMobility (EComponentMobility::Movable);

	ActorWidgetComp = CreateDefaultSubobject<USActorWidgetComponent> (TEXT ("ActorWidgetComp"));
	ActorWidgetComp->SetupAttachment (RootComponent);

	PlayerController = Cast<ANCD_PlayerController> (UGameplayStatics::GetPlayerController (GetWorld (), 0));

}

void ABuildingDemolishActor::SetBuildableActor (ABuildableActor* pActor) {
	BuildableActor = pActor;
}

// Called when the game starts or when spawned
void ABuildingDemolishActor::BeginPlay () {
	Super::BeginPlay ();

}

// Called every frame
void ABuildingDemolishActor::Tick (float DeltaTime) {
	Super::Tick (DeltaTime);

}