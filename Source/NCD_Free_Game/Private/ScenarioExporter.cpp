// Fill out your copyright notice in the Description page of Project Settings.

#include "public/ScenarioExporter.h"
#include "public/JsonExporter.h"
#include "Engine/DataTable.h"
#include "Kismet/KismetSystemLibrary.h"
#include "PlatformFilemanager.h"
#include "GenericPlatformFile.h"
#include "Paths.h"

// Sets default values
AScenarioExporter::AScenarioExporter () {

}

void AScenarioExporter::BeginPlay () {
	Super::BeginPlay ();
	fromPathBaseString = FPaths::ProjectContentDir () + AssetsFolder + "/";
	toPathBaseString = FPaths::ProjectContentDir () + "Exports/";
	JsonExporter = NewObject<UJsonExport> ();
	JsonExporter->Initialize (this, AssetsFolder);
}

void AScenarioExporter::OnClickExport () {
	//Acknowledge that we received a click event!
	UE_LOG (LogTemp, Warning, TEXT ("Click received!"));

	if (CheckFolders ()) {
		//Export JSONs
		ExportJsons ();

		//Export the JSONs
		ExportFolder ("JSON");

		//Export Materials, textures, models and datatables
		ExportFolder ("Textures");
		ExportFolder ("Materials");
		ExportFolder ("Models");
		ExportFolder ("Datatables");
	} else {
		UE_LOG (LogTemp, Warning, TEXT ("Export aborted! See log for errors!"));
	}

	UE_LOG (LogTemp, Warning, TEXT ("Exporting finished!"));
}

void AScenarioExporter::ExportJsons () {
	JsonExporter->ExportBaseJSON (GetFileNameFromObject (BuildingsDT), GetFileNameFromObject (EventsDT), GetFileNameFromObject (MilestonesDT), GetFileNameFromObject (PoliciesDT), GetFileNameFromObject (VehiclesDT), GetFileNameFromObject (GoalsDT), InitialValues);
	//JsonExporter->ExportSceneJSON (); -> Now called from ExportBaseJson in JsonExport.cpp
}

void AScenarioExporter::ExportFolder (FString pFolderName) {
	//Local because I can't get it to work in the header
	FPlatformFileManager* fileManager = new FPlatformFileManager ();
	fileManager->GetPlatformFile (TEXT ("File Manager"));

	FString fromPathString = fromPathBaseString + pFolderName + "/";
	FString toPathString = toPathBaseString + pFolderName + "/";
	const TCHAR* fromPath = *fromPathString;
	const TCHAR* toPath = *toPathString;
	bool success = fileManager->GetPlatformFile ().CopyDirectoryTree (toPath, fromPath, false);
	//UE_LOG (LogTemp, Warning, TEXT ("Copying %s success: %s"), pFolderName, success ? TEXT ("True") : TEXT ("False"));
}

void AScenarioExporter::ExportFile (FString pFileName) {
	////Local because I can't get it to work in the header
	FPlatformFileManager* fileManager = new FPlatformFileManager ();
	fileManager->GetPlatformFile (TEXT ("File Manager"));

	FString fromPathString = fromPathBaseString + pFileName;
	FString toPathString = toPathBaseString + pFileName;
	const TCHAR* fromPath = *fromPathString;
	const TCHAR* toPath = *toPathString;
	bool success = fileManager->GetPlatformFile ().CopyFile (toPath, fromPath);
	//UE_LOG (LogTemp, Warning, TEXT ("Copying %s success: %s"), pFolderName, success ? TEXT ("True") : TEXT ("False"));
}

FString AScenarioExporter::GetFileNameFromObject (UObject* pObject) {
	return FPaths::GetBaseFilename (UKismetSystemLibrary::GetPathName (pObject));
}

bool AScenarioExporter::CheckFolders () {
	bool flag = true;
	if (!FPaths::DirectoryExists (FPaths::ProjectContentDir () + AssetsFolder + "/")) {
		UE_LOG (LogTemp, Error, TEXT ("Assets folder doesn't exist or is not assigned. Please set your asset folder nam in the 'ScenarioExporter' in the scene hierarchy or check if you misspelled it."));
		flag = false;
	}
	if (flag) {
		if (!FPaths::DirectoryExists (FPaths::ProjectContentDir () + AssetsFolder + "/Datatables/")) {
			UE_LOG (LogTemp, Error, TEXT ("'Datatables' folder doesn't exist. Please create a folder named 'Datatables' in your assets directory."));
			flag = false;
		}
		if (!FPaths::DirectoryExists (FPaths::ProjectContentDir () + AssetsFolder + "/Models/")) {
			UE_LOG (LogTemp, Error, TEXT ("'Models' folder doesn't exist. Please create a folder named 'Models' in your assets directory."));
			flag = false;
		}
		if (!FPaths::DirectoryExists (FPaths::ProjectContentDir () + AssetsFolder + "/Textures/")) {
			UE_LOG (LogTemp, Error, TEXT ("'Textures' folder doesn't exist. Please create a folder named 'Textures' in your assets directory."));
			flag = false;
		}
		if (!FPaths::DirectoryExists (FPaths::ProjectContentDir () + AssetsFolder + "/JSON/")) {
			UE_LOG (LogTemp, Error, TEXT ("'JSON' folder doesn't exist. Please create a folder named 'JSON' in your assets directory."));
			flag = false;
		}
		if (!FPaths::DirectoryExists (FPaths::ProjectContentDir () + AssetsFolder + "/Materials/")) {
			UE_LOG (LogTemp, Error, TEXT ("'Materials' folder doesn't exist. Please create a folder named 'Materials' in your assets directory."));
			flag = false;
		}
	}
	if (!flag) UE_LOG (LogTemp, Error, TEXT ("Aborting export! One or multiple folders are non-existant!"));
	return flag;
}