// Fill out your copyright notice in the Description page of Project Settings.

#include "MilestoneManager.h"
#include "Policy.h"
#include "BuildingStats.h"
#include "CitySystemGameMode.h"
#include "NCD_PlayerController.h"
#include "HappinessManager.h"
#include <Kismet/GameplayStatics.h>

// Sets default values
AMilestoneManager::AMilestoneManager()
{
	PrimaryActorTick.bCanEverTick = false;
}

AMilestoneManager::~AMilestoneManager()
{
}

// Called when the game starts or when spawned
void AMilestoneManager::BeginPlay()
{
	Super::BeginPlay();
	CitySystem = GetWorld()->GetAuthGameMode<ACitySystemGameMode>();
	if (!CitySystem) return;
	if (MilestoneList.Num() == 0) LoadMilestones();
}

void AMilestoneManager::LoadMilestones()
{
	if (!MilestonesDataTable) return;

	FString ContextString;
	TArray<FName> RowNames = MilestonesDataTable->GetRowNames();
	for (auto& RowName : RowNames)
	{
		FMilestones* Milestone = MilestonesDataTable->FindRow<FMilestones>(RowName, ContextString);
		if (Milestone) {
			MilestoneList.Add(*RowName.ToString(), *Milestone);
		}
		Milestone = nullptr;
	}
}

void AMilestoneManager::DeveloperUnlockMilestone()
{
	
	UE_LOG(LogTemp, Warning, TEXT("Developer Unlock for Milestones called"));
	for (auto& milestone : MilestoneList)
	{
		FMilestones* _milestone = &milestone.Value;
		if (_milestone->bDeveloperUnlockMilestone) MilestoneReached(_milestone, false);
	}
}

void AMilestoneManager::LoadMilestonesFromSavegame(TArray<FName> pUnlockedMilestonesList)
{
	if (MilestoneList.Num() == 0) LoadMilestones();
	for (auto& MilestoneKey: pUnlockedMilestonesList)
	{
		MilestoneReached(MilestoneList.Find(MilestoneKey),false);
	}
}

void AMilestoneManager::CheckMilestones()
{
	if (MilestoneList.Num() == 0) return;

	bool bCitizensRequired;
	bool bBuildingsRequired;
	bool bWorkingPeopleRequired;
	bool bCompletedEventsRequired;
	//bool bHappinessRequired;
	//bool bMoneyRequired;
	//bool bPollutionRequired;

	for (auto& _milestone : MilestoneList)
	{
		FMilestones * Milestone = &_milestone.Value;
		if (Milestone->bUnlocked) continue;

		int ConditionsDidMeetCriteria = 0;
		int ConditionsNeededToMeet = 0;

		bCitizensRequired = Milestone->bPopulationCondition;
		bBuildingsRequired = Milestone->bBuildingCondition;
		bWorkingPeopleRequired = Milestone->bWorkingPopulationCondition;
		bCompletedEventsRequired = Milestone->bEventsCondition;
		//bHappinessRequired = Milestone->bHappinessCondition;
		//bMoneyRequired = Milestone->bMoneyCondition;
		//bPollutionRequired = Milestone->bPollutionCondition;

		if (bCitizensRequired)
		{
			ConditionsNeededToMeet++;
			if (CheckAmountOfCitizens(Milestone->RequiredCitizens)) ConditionsDidMeetCriteria++;
		}
		if (bBuildingsRequired)
		{
			ConditionsNeededToMeet++;
			if (CheckAmountOfBuildings(Milestone->AmountOfBuildingNeeded, Milestone->OfBuildingType)) ConditionsDidMeetCriteria++;
		}
		if (bWorkingPeopleRequired)
		{
			ConditionsNeededToMeet++;
			if (CheckAmountOfWorkingPeople(Milestone->AmountOfWorkingPeople)) ConditionsDidMeetCriteria++;
		}
		if (bCompletedEventsRequired)
		{
			ConditionsNeededToMeet++;
			if(CheckCompletedGameEvent(Milestone->EventsThatNeedToBeCompleted)) ConditionsDidMeetCriteria++;
		}

		/*if (bHappinessRequired)
		{
			ConditionsNeededToMeet++;
			if (CheckAmountOfHappiness(Milestone->AmountOfHappiness)) ConditionsDidMeetCriteria++;
		}
		if (bMoneyRequired)
		{
			ConditionsNeededToMeet++;
			if (CheckAmountOfMoney(Milestone->AmountOfMoney)) ConditionsDidMeetCriteria++;
		}
		if (bPollutionRequired)
		{
			ConditionsNeededToMeet++;
			if (CheckAmountOfPollution(Milestone->AmountOfPollution)) ConditionsDidMeetCriteria++;
		}*/
		if (ConditionsDidMeetCriteria == ConditionsNeededToMeet) MilestoneReached(Milestone); // Milestone reached, unlock it
	}
}

TArray<FName> AMilestoneManager::GetUnlockedMilestonesForExport()
{
	TArray<FName> ReturnList;
	for (auto& Milestone : MilestoneList)
	{
		if (Milestone.Value.bUnlocked)
			ReturnList.Add(Milestone.Key);
	}
	return ReturnList;
}

void AMilestoneManager::MilestoneReached( FMilestones * pMilestone , bool bShowNotification)
{
	if (!pMilestone) return;

	pMilestone->bUnlocked = true;
	UE_LOG(LogTemp, Warning, TEXT("This Milestone has been reached : %s"), *pMilestone->Title);

	if (bShowNotification)	// First time you unlock this. Not after loading from save
	{
		if (pMilestone->MoneyReward != 0) CitySystem->RewardMoney(pMilestone->MoneyReward);
		if (pMilestone->RawHappinessReward != 0.0f) HappinessReward(pMilestone->RawHappinessReward);

		AddMilestoneNotification(*pMilestone);
	}

	//Set new building or policy on unlocked
	if (pMilestone->PoliciesToUnlock.Num() > 0) CitySystem->UnlockNewPolicies(pMilestone->PoliciesToUnlock);
	if (pMilestone->BuildingsToUnlock.Num() > 0) CitySystem->UnlockNewBuildings (pMilestone->BuildingsToUnlock);
	if (pMilestone->EventsToUnlock.Num() > 0) CitySystem->UnlockNewGameEvents(pMilestone->EventsToUnlock);

	ANCD_PlayerController* pc = Cast<ANCD_PlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	if(pc) pc->UpdateUIWindows();
}

void AMilestoneManager::HappinessReward(float pAmount)
{
	CitySystem->GetHappinessManager()->AddHappiness (pAmount);
}

bool AMilestoneManager::CheckAmountOfCitizens(int pAmountNeeded)
{ 
	if (CitySystem && CitySystem->GetPopulationManager())
		return CitySystem->GetPopulationManager()->GetPopulationCount() >= pAmountNeeded;
	return false;
}

bool AMilestoneManager::CheckAmountOfBuildings( int pAmountNeeded, TArray<EBuildingType> pOfBuildingType)
{
	if (pOfBuildingType.Num() == 0) return false;

	int conditionsMet = 0;

	for (EBuildingType buildingType : pOfBuildingType)
	{
		if (CitySystem->GetBuildingCountOfType(buildingType) >= pAmountNeeded)
		{
			conditionsMet++;
			continue;
		}
		return false;
	}
	return conditionsMet == pOfBuildingType.Num();
}

bool AMilestoneManager::CheckAmountOfWorkingPeople(int pAmountNeeded)
{
	if (CitySystem && CitySystem->GetPopulationManager())
		return CitySystem->GetPopulationManager()->GetEmployedPeopleCount();
	return false;
}

bool AMilestoneManager::CheckCompletedGameEvent(TArray<FDataTableRowHandle> pGameEvents)
{
	int ConditionsNeededToMeet = 0;
	int ConditionsMet = 0;
	
	//UE_LOG(LogTemp, Warning, TEXT("CheckCompletedGameEvent Check 1"));

	for (auto& GameEventToCheck : pGameEvents)
	{
		TMap<FName, FNCDEvent> *EventList = &CitySystem->GetGameEventsList();
		ConditionsNeededToMeet++;
		//UE_LOG(LogTemp, Warning, TEXT("CheckCompletedGameEvent Check 2"));
		for (auto& _gameEvent : *EventList)
		{
			FNCDEvent * GameEvent = &_gameEvent.Value;
			if (GameEventToCheck.RowName == GameEvent->ID)
			{
				//UE_LOG(LogTemp, Warning, TEXT("CheckCompletedGameEvent Check 3"));
				if (GameEvent->bCompletedEvent) {
					ConditionsMet++;
				}
				else return false;
			}
		}
	}
	
	if (ConditionsMet == ConditionsNeededToMeet) return true;
	return false;
}

/*
bool AMilestoneManager::CheckAmountOfHappiness(int pAmountNeeded)
{
	if (CitySystem) {
		return CitySystem->GetHappiness() >= pAmountNeeded;
	}
	return false;
}

bool AMilestoneManager::CheckAmountOfMoney(int pAmountNeeded)
{
	if (CitySystem) {
		return CitySystem->GetMoneyAmount() >= pAmountNeeded;
	}
	return false;
}

bool AMilestoneManager::CheckAmountOfPollution(int pAmountNeeded, EPollutionType pPollutionType)
{
	//if (CitySystem-> >= pAmountNeeded) return true;
	return false;
}
*/

void AMilestoneManager::AddMilestoneNotification(FMilestones pMilestoneNotification)
{	
	MilestoneNotifications.Push(pMilestoneNotification);
}

FMilestones AMilestoneManager::CheckForMilestoneNotification()
{
	if (MilestoneNotifications.Num() > 0) {
		FMilestones _Milestone = MilestoneNotifications[0];
		MilestoneNotifications.RemoveAt(0, 1, true);
		return _Milestone;
	}
	return FMilestones();
}

TMap<FName, FMilestones> AMilestoneManager::GetMilestoneList()
{
	return MilestoneList;
}

TArray<FMilestones> AMilestoneManager::GetNotificationsList()
{
	return MilestoneNotifications;
}
