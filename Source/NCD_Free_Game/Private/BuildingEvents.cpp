// Fill out your copyright notice in the Description page of Project Settings.

#include "BuildingEvents.h"
#include "Kismet/GameplayStatics.h"
#include "CitySystemGameMode.h"
#include "NCD_PlayerController.h"
#include "TrafficSystem.h"
ABuildingEvents::ABuildingEvents()
{
	PrimaryActorTick.bCanEverTick = false;
}

ABuildingEvents::~ABuildingEvents()
{
}


void ABuildingEvents::BeginPlay()
{
	Super::BeginPlay();
	CitySystem = GetWorld()->GetAuthGameMode<ACitySystemGameMode>();
	if (!CitySystem) return;

	LoadBuildingEventChances();

	//Set Timers
	GetWorld()->GetTimerManager().SetTimer(UpdateEventTimeTimer, this, &ABuildingEvents::UpdateEventTimePassed, UpdateEventTimeDelay, true);
	if (!bBuildingEventsUnlocked) GetWorld()->GetTimerManager().SetTimer(CheckForEachServiceBuildings, this, &ABuildingEvents::CheckIfServicesAvailable, CheckForServiceBuildingsDelay, true);
	else GetWorld()->GetTimerManager().SetTimer(SpawnBuildingEventTimer, this, &ABuildingEvents::SpawnBuildingEvent, EventTimeTillSpawn, false);		// TODO: Change looping to random intervals
}

//Loads spawn chance of an event type from datatable for each buildingcategory
void ABuildingEvents::LoadBuildingEventChances()
{
	if (!BuildingEventChancesDataTable) return;

	FString ContextString;
	TArray<FName> RowNames = BuildingEventChancesDataTable->GetRowNames();
	for (auto& RowName : RowNames)
	{
		FBuildingEventsChances* EventChance = BuildingEventChancesDataTable->FindRow<FBuildingEventsChances>(RowName, ContextString);
		if (EventChance) {
			BuildingEventChanceList.Add(EventChance->BuildingCategory, *EventChance);
		}
		EventChance = nullptr;
	}
}

//Check whether services have been unlocked through milestones.
//true = buildingevents will now be able to spawn
void ABuildingEvents::CheckIfServicesAvailable()
{
	if (CheckIfServiceBuildingIsUnlocked(0)) //Blueprint functionality
	{
		bBuildingEventsUnlocked = true;
		//Clear timer
		GetWorld()->GetTimerManager().ClearTimer(CheckForEachServiceBuildings);
		//Set new Timer for spawning events
		GetWorld()->GetTimerManager().SetTimer(SpawnBuildingEventTimer, this, &ABuildingEvents::SpawnBuildingEvent, EventTimeTillSpawn, false);		// TODO: Change looping to random intervals
		UE_LOG(LogTemp, Warning, TEXT("New Timer has started for building events, after services were built."));
	}
}

void ABuildingEvents::SetTimerForNextEvent()
{
	float RandomTime = FMath::RandRange(MINIMUMTIMEBETWEENEVENTSPAWN, MAXIMUMTIMEBETWEENEVENTSPAWN);   // 20 - 50

	GetWorld()->GetTimerManager().SetTimer(SpawnBuildingEventTimer, this, &ABuildingEvents::SpawnBuildingEvent, RandomTime, false);
}

void ABuildingEvents::SpawnBuildingEvent()
{
	SetTimerForNextEvent(); // Make random time for next event call

	EBuildingCategory BuildingCategoryType;

	int RandomNumber = FMath::RandRange(0, 100); // Between 0-100    // Define Ratios for which kind of building gets the event

	if (RandomNumber >= 0 && RandomNumber <= 60) BuildingCategoryType = EBuildingCategory::EBC_RESIDENCE;			// Residential
	else if (RandomNumber >= 61 && RandomNumber <= 80) BuildingCategoryType = EBuildingCategory::EBC_INDUSTRY;		// Industrial
	else if (RandomNumber >= 81 && RandomNumber <= 100)	BuildingCategoryType = EBuildingCategory::EBC_COMMERCE;		// Commercial
	else return;

	SpawnEvent(BuildingCategoryType);
}

//void ABuildingEvents::SpawnEvent(FBuildingManager* pBuildingManager, EBuildingCategory pBuildingCategory)
void ABuildingEvents::SpawnEvent(EBuildingCategory pBuildingCategory)
{
	//Check which services should be available to build.
	if (!bFireEventsUnlocked) bFireEventsUnlocked = CheckIfServiceBuildingIsUnlocked(1);
	if (!bCrimeEventsUnlocked) bCrimeEventsUnlocked = CheckIfServiceBuildingIsUnlocked(2);
	if (!bAccidentEventsUnlocked) bAccidentEventsUnlocked = CheckIfServiceBuildingIsUnlocked(3);
	
	UE_LOG(LogTemp, Warning, TEXT("Trying to spawn a building event!"));

	//Calculate which type of event we will spawn
	EBuildingEventType EventType = CalculateTypeOfEvent(pBuildingCategory);
	//Check if there is enough service capacity to handle the current amount of events.
	CheckEventCapacity(EventType);

	// Select Building
	TMap<FVector, FBuildingStats> BuildingsFromCategory;
	BuildingsFromCategory = CitySystem->GetBuildingsByCategory(pBuildingCategory);
	if (BuildingsFromCategory.Num() == 0) return;				// If there are no buildings of this category  RETURN

	TArray<FVector> LocationKeys;					// Holds Locations of BuildingType
	TArray<FBuildingStats> BuildingValues;			// Holds BuildingStats of BuildingType
	BuildingsFromCategory.GenerateKeyArray(LocationKeys);
	BuildingsFromCategory.GenerateValueArray(BuildingValues);

	FVector Location;
	FBuildingStats Building;
	int RandomBuilding;
	// Why was this outside the loop :/
	// int RandomBuilding = FMath::RandRange(0, BuildingsFromCategory.Num() - 1);
	
	int MaxLoops = 0;

	do 
	{
		if (MaxLoops == 10) return;
		RandomBuilding = FMath::RandRange(0, BuildingsFromCategory.Num() - 1);	// Inside loop should try to check new numbers (if possible)
		Location = LocationKeys[RandomBuilding];
		Building = BuildingValues[RandomBuilding];
		MaxLoops++;
	} while (Building.bCanHaveBuildingEvent == false);
	
	PushEventToBuilding(EventType, Location, Building);
}

bool ABuildingEvents::PushEventToBuilding(EBuildingEventType pEventType, FVector pLocation, FBuildingStats pBuilding)
{
	FBuildingEventsData NewEventForBuilding;										// New struct to set values for

	NewEventForBuilding.BuildingLocation = pLocation;								// Location of Building
	NewEventForBuilding.BuildingStats = pBuilding;									// Stats of Building

	if (BuildingsWithFireEvents.Contains(pLocation) || BuildingsWithCrimeEvents.Contains(pLocation) || BuildingsWithAccidentEvents.Contains(pLocation)) return false;		//If building holds an event, don't spawn another on top

	//ABuildingStatusActor* ABSA = CreateOrFindInactiveStatusWidget(pLocation);
	
	FBuildingStats* stats = CitySystem->GetBuildingByLocation(pLocation);	// Get pointer to original

	//ABSA->SetStatus(pEventType);
	//ABSA->SetLocation(pLocation);
	//ABSA->SetVisible(ESlateVisibility::Visible);

	switch (pEventType)
	{
	case EBuildingEventType::ET_FIRE:
		NewEventForBuilding.EventType = EBuildingEventType::ET_FIRE;
		NewEventForBuilding.BuildingStats.BuildingStatus = EBuildingStatus::ES_ONFIRE;
		//NewEventForBuilding.BuildingStatusActor = ABSA;
		NewEventForBuilding.ParticleComponent = SpawnParticleForBuilding(EBuildingEventType::ET_FIRE, pLocation, stats);
		if (stats)
		{
			stats->BuildingStatus = EBuildingStatus::ES_ONFIRE;
			FBuildingStats nearestBuilding = CitySystem->GetNearestBuildingByType(EBuildingType::EB_FIRE_DEPARTMENT, stats->ConnectedRoadLocation);
			NewEventForBuilding.bVehicleDispatched = (nearestBuilding.ID != NAME_None)
				&& CitySystem->GetPopulationManager()->IsOperational(nearestBuilding) ? true : false;
			if (NewEventForBuilding.bVehicleDispatched)
			{
				CitySystem->GetTrafficSystem()->SpawnRandomAgentByType(nearestBuilding.ConnectedRoadLocation, stats->ConnectedRoadLocation, nearestBuilding.BuildingType, pLocation,true);
			}
			
		}
		BuildingsWithFireEvents.Add(pLocation, NewEventForBuilding);
		break;
	case EBuildingEventType::ET_CRIME:
		NewEventForBuilding.BuildingStats.BuildingStatus = EBuildingStatus::ES_ROBBED;
		NewEventForBuilding.EventType = EBuildingEventType::ET_CRIME;
		//NewEventForBuilding.BuildingStatusActor = ABSA;
		NewEventForBuilding.ParticleComponent = SpawnParticleForBuilding(EBuildingEventType::ET_CRIME, pLocation, stats);

		if (stats)
		{
			stats->BuildingStatus = EBuildingStatus::ES_ROBBED;
			FBuildingStats nearestBuilding = CitySystem->GetNearestBuildingByType(EBuildingType::EB_POLICE, stats->ConnectedRoadLocation);
			NewEventForBuilding.bVehicleDispatched = (nearestBuilding.ID != NAME_None) 
				&& CitySystem->GetPopulationManager()->IsOperational(nearestBuilding) ? true : false;
			if (NewEventForBuilding.bVehicleDispatched)
			{
				CitySystem->GetTrafficSystem()->SpawnRandomAgentByType(nearestBuilding.ConnectedRoadLocation, stats->ConnectedRoadLocation, nearestBuilding.BuildingType, pLocation,true);
			}

		}
		BuildingsWithCrimeEvents.Add(pLocation, NewEventForBuilding);
		break;
	case EBuildingEventType::ET_ACCIDENT:
		NewEventForBuilding.EventType = EBuildingEventType::ET_ACCIDENT;
		NewEventForBuilding.BuildingStats.BuildingStatus = EBuildingStatus::ES_ACCIDENT;
		//NewEventForBuilding.BuildingStatusActor = ABSA;
		NewEventForBuilding.ParticleComponent = SpawnParticleForBuilding(EBuildingEventType::ET_ACCIDENT, pLocation, stats);
		if (stats)
		{
			stats->BuildingStatus = EBuildingStatus::ES_ACCIDENT;
			TArray<EBuildingType> types;
			types.Add(EBuildingType::EB_HOSPITAL);
			types.Add(EBuildingType::EB_CLINIC);
			FBuildingStats nearestBuilding = CitySystem->GetNearestBuildingByTypes(types, stats->ConnectedRoadLocation);
			NewEventForBuilding.bVehicleDispatched = (nearestBuilding.ID != NAME_None)
				&&	CitySystem->GetPopulationManager()->IsOperational(nearestBuilding) ? true : false;
			if (NewEventForBuilding.bVehicleDispatched)
			{
				CitySystem->GetTrafficSystem()->SpawnRandomAgentByType(
					nearestBuilding.ConnectedRoadLocation,
					stats->ConnectedRoadLocation,
					nearestBuilding.BuildingType,
					pLocation,
					true
				);
			}
		}
		BuildingsWithAccidentEvents.Add(pLocation, NewEventForBuilding);
		break;
	case EBuildingEventType::ET_NONE:
		UE_LOG(LogTemp, Warning, TEXT("No Event Pushed"));
		return false;
		break;
	default:
		return false;
		break;
	}

	UE_LOG(LogTemp, Warning, TEXT("Event Pushed"));
	return true;
	// ANCD_PlayerController* pc = Cast<ANCD_PlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	// if (pc) pc->ShowStatusAboveBuilding(pLocation);
	// else UE_LOG(LogTemp, Warning, TEXT(" AB"));
}

EBuildingEventType ABuildingEvents::CalculateTypeOfEvent(EBuildingCategory pBuildingCategory)
{
	CheckEventConditions();

	int EventSpawnRandom = FMath::RandRange(0, 100);

	FBuildingEventsChances* EventChancesForCategory = BuildingEventChanceList.Find(pBuildingCategory);
	int ChanceIfBalanced = FMath::RandRange(0, 100);		// 15% Chance		// Additional check to see if it will spawn even when service is balanced

	if (EventSpawnRandom <= EventChancesForCategory->ChanceForFire)
	{
		if (bFireEventsUnlocked) {
			if (bFireNotBalanced) return EBuildingEventType::ET_FIRE;
			else if (ChanceIfBalanced <= CHANCEIFBALANCEDCHANCE) return EBuildingEventType::ET_FIRE;
		}
	}
	else if (EventSpawnRandom > EventChancesForCategory->ChanceForFire && EventSpawnRandom <= (EventChancesForCategory->ChanceForFire + EventChancesForCategory->ChanceForCrime))
	{
		if (bCrimeEventsUnlocked) {
			if (bCrimeNotBalanced) return EBuildingEventType::ET_CRIME;
			else if (ChanceIfBalanced <= CHANCEIFBALANCEDCHANCE) return EBuildingEventType::ET_CRIME;
		}
	}
	else if (EventSpawnRandom > (EventChancesForCategory->ChanceForFire + EventChancesForCategory->ChanceForCrime) && EventSpawnRandom <= (EventChancesForCategory->ChanceForFire + EventChancesForCategory->ChanceForCrime + EventChancesForCategory->ChanceForAccident))
	{
		if (bAccidentEventsUnlocked)
		{
			if (bHealthCareNotBalanced) return EBuildingEventType::ET_ACCIDENT;
			else if (ChanceIfBalanced <= CHANCEIFBALANCEDCHANCE) return EBuildingEventType::ET_ACCIDENT;
		}
	}

	return EBuildingEventType::ET_NONE;
}

void ABuildingEvents::CheckEventConditions()
{
	//Reset Booleans
	bFireNotBalanced = false;
	bCrimeNotBalanced = false;
	bHealthCareNotBalanced = false;

	float FireRatio;					//Fire Stations on Citizens/buildings
	float CrimeRatio;					//Police Stations on Citizens/buildings
	float AccidentRatio;				//Health care on Citizens/buildings

	FireRatio = CitySystem->HasEnoughBuildingsOfType(EBuildingType::EB_FIRE_DEPARTMENT);
	CrimeRatio = CitySystem->HasEnoughBuildingsOfType(EBuildingType::EB_POLICE);
	AccidentRatio = (CitySystem->HasEnoughBuildingsOfType(EBuildingType::EB_CLINIC) + CitySystem->HasEnoughBuildingsOfType(EBuildingType::EB_HOSPITAL));

	if (FireRatio < UNBALANCETHRESHOLD) bFireNotBalanced = true;					// More citizens than fire departments capacity	
	if (CrimeRatio < UNBALANCETHRESHOLD) bCrimeNotBalanced = true;					// More citizens than police stations capacity	
	if (AccidentRatio < UNBALANCETHRESHOLD)	bHealthCareNotBalanced = true;			// More citizens than health care capacity	
}


// 
// This function has the formula for the max amount of available building events possible based on the amount of services available
//
bool ABuildingEvents::CheckEventCapacity(EBuildingEventType pEventType)
{
	int EXTRAEVENTPOSSIBLE = 0;
	switch (pEventType)	// Check each type whether the capacity can handle it, otherwise early return because we don't want too many buildingevents happening.
	{
	case EBuildingEventType::ET_NONE:
		return false;
		break;
	case EBuildingEventType::ET_FIRE:
		if (bFireNotBalanced) EXTRAEVENTPOSSIBLE = EXTRAEVENTADDMODIFIER;
		if (GetLengthOfEventsInList(pEventType) >= (CitySystem->GetBuildingCountOfType(EBuildingType::EB_FIRE_DEPARTMENT) * MAXEVENTSMULTIPLIER) + EXTRAEVENTPOSSIBLE) return false;
		break;
	case EBuildingEventType::ET_CRIME:
		if (bCrimeNotBalanced) EXTRAEVENTPOSSIBLE = EXTRAEVENTADDMODIFIER;
		if (GetLengthOfEventsInList(pEventType) >= (CitySystem->GetBuildingCountOfType(EBuildingType::EB_POLICE) * MAXEVENTSMULTIPLIER) + EXTRAEVENTPOSSIBLE) return false;
		break;
	case EBuildingEventType::ET_ACCIDENT:
		if (bHealthCareNotBalanced) EXTRAEVENTPOSSIBLE = EXTRAEVENTADDMODIFIER;
		if (GetLengthOfEventsInList(pEventType) >= ((CitySystem->HasEnoughBuildingsOfType(EBuildingType::EB_CLINIC) + CitySystem->HasEnoughBuildingsOfType(EBuildingType::EB_HOSPITAL)) * MAXEVENTSMULTIPLIER) + EXTRAEVENTPOSSIBLE) return false;
		break;
	default:
		return false;
		break;
	}

	return true;
}

int ABuildingEvents::GetLengthOfEventsInList(EBuildingEventType pEventType)
{
	switch (pEventType)
	{
	case EBuildingEventType::ET_FIRE:
		return BuildingsWithFireEvents.Num();
		break;
	case EBuildingEventType::ET_CRIME:
		return BuildingsWithCrimeEvents.Num();
		break;
	case EBuildingEventType::ET_ACCIDENT:
		return BuildingsWithAccidentEvents.Num();
		break;
	default:
		return 0;
		break;
	}
}

bool ABuildingEvents::CheckTimePassedOnBuilding(FBuildingEventsData BuildingData, EBuildingEventType EventType)
{
	if (BuildingData.TimePassed >= BuildingData.BuildingEventMaxTime)
	{
		//Follow up event
		//CitySystem->GetBuildingByLocation(BuildingData.BuildingLocation)->BuildingStatus = EBuildingStatus::ES_NONE;
		//RemoveBuildingFromEventList(BuildingData.BuildingLocation);			// Now returns true; to add for later removal.
		switch (EventType)
		{
		case EBuildingEventType::ET_FIRE:
			//Event Function ??   BURNED DOWN HOUSE?
			UE_LOG(LogTemp, Warning, TEXT("Fire Event reached max time. Fire Truck failed to reach destination in time"));
			break;
		case EBuildingEventType::ET_CRIME:
			//Event Function ??
			UE_LOG(LogTemp, Warning, TEXT("Crime Event reached max time. Police failed to reach destination in time"));
			break;
		case EBuildingEventType::ET_ACCIDENT:
			//Event Function ??   PERSON DIED?
			UE_LOG(LogTemp, Warning, TEXT("Accident Event reached max time. Ambulance failed to reach destination in time"));
			break;
		default:
			break;
		}
		return true;
	}
	return false;
}

void ABuildingEvents::UpdateEventTimePassed()
{
	float CurrentTimePassed = GetWorld()->GetTimeSeconds();
	float NewTimePassed = CurrentTimePassed - LastTimeInSeconds;
	LastTimeInSeconds = CurrentTimePassed;

	TArray<FVector> LocationsToRemoveFromList;

	if (BuildingsWithFireEvents.Num() != 0)
	{
		//FIRE EVENTS
		for (auto& buildingEventData : BuildingsWithFireEvents)
		{
			buildingEventData.Value.TimePassed += NewTimePassed;
			if (CheckTimePassedOnBuilding(buildingEventData.Value, EBuildingEventType::ET_FIRE)) LocationsToRemoveFromList.Add(buildingEventData.Value.BuildingLocation);
		}
	}
	if (BuildingsWithCrimeEvents.Num() != 0)
	{
		//CRIME EVENTS
		for (auto& buildingEventData : BuildingsWithCrimeEvents)
		{
			buildingEventData.Value.TimePassed += NewTimePassed;
			if (CheckTimePassedOnBuilding(buildingEventData.Value, EBuildingEventType::ET_CRIME)) LocationsToRemoveFromList.Add(buildingEventData.Value.BuildingLocation);
		}
	}
	if (BuildingsWithAccidentEvents.Num() != 0)
	{
		//ACCIDENT EVENTS
		for (auto& buildingEventData : BuildingsWithAccidentEvents)
		{
			buildingEventData.Value.TimePassed += NewTimePassed;
			if (CheckTimePassedOnBuilding(buildingEventData.Value, EBuildingEventType::ET_ACCIDENT)) LocationsToRemoveFromList.Add(buildingEventData.Value.BuildingLocation);
		}
	}

	if (LocationsToRemoveFromList.Num() > 0)
	{
		for (FVector _location : LocationsToRemoveFromList)
		{
			RemoveBuildingFromEventList(_location);
		}
	}
}

UParticleSystemComponent * ABuildingEvents::SpawnParticleForBuilding(EBuildingEventType pBuildingEventType, FVector pLocation, FBuildingStats* pBuildingStats)
{
	UParticleSystemComponent * particleSpawned = nullptr;

	if (pBuildingStats == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("pbuildingstats is nullptr."));
		return particleSpawned;
	}
	FVector particleOffset = FVector::ZeroVector;

	APlacedBuilding* foundBuilding = CitySystem->GetPlacedBuildingByLocation(pLocation);
	if (foundBuilding)
	{
		particleOffset = foundBuilding->GetFirstParticleHotspot();
		FRotator rotator = foundBuilding->GetActorRotation(); 
		particleOffset.RotateAngleAxis(rotator.Yaw, FVector::UpVector);
		particleOffset += pLocation;
	}

	switch (pBuildingEventType)
	{
	case EBuildingEventType::ET_FIRE:
		if (!pBuildingStats->bBigParticle) particleSpawned = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BuildingFireParticle, particleOffset);
		else particleSpawned = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BigBuildingFireParticle, particleOffset);
		break;
	case EBuildingEventType::ET_CRIME:
		if (!pBuildingStats->bBigParticle)particleSpawned = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BuildingCrimeParticle, particleOffset);
		else particleSpawned = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BigBuildingCrimeParticle, particleOffset);
		UE_LOG(LogTemp, Warning, TEXT("Particle Spawned for Crime"));
		break;
	case EBuildingEventType::ET_ACCIDENT:
		if (!pBuildingStats->bBigParticle)particleSpawned = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BuildingAccidentParticle, particleOffset);
		else particleSpawned = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BigBuildingAccidentParticle, particleOffset);
		UE_LOG(LogTemp, Warning, TEXT("Particle Spawned for Accident"));
		break;
	default:
		break;
	}

	return particleSpawned;
}

/*
void ABuildingEvents::SetBuildingStatusActiveStatus(FVector pLocation, bool pActive)
{
	for (auto& Widget : BuildingStatusActorPool)
	{
		if (Widget.Key->GetActorLocation() == pLocation + Widget.Key->GetOffsetLocation()) {
			Widget.Value = pActive;
			Widget.Key->SetVisible(pActive ? ESlateVisibility::Visible : ESlateVisibility::Collapsed);
			return;
		}
	}
}

ABuildingStatusActor* ABuildingEvents::CreateOrFindInactiveStatusWidget(FVector pLocation)
{
	for (auto& Widget : BuildingStatusActorPool)
	{
		// Inactive
		if (Widget.Value == false) {
			// Set as active
			Widget.Value = true;
			Widget.Key->SetLocation(pLocation);
			Widget.Key->SetVisible(ESlateVisibility::Visible);
			return Widget.Key;
		}
	}

	ABuildingStatusActor* BuildingStatus = GetWorld()->SpawnActor<ABuildingStatusActor>(BuildingStatusActorClass, FVector::ZeroVector, FRotator::ZeroRotator);
	if (BuildingStatus) {
		BuildingStatusActorPool.Add(BuildingStatus, true);
	}
	return BuildingStatus;
}
*/

// This function can be called from the AI when they succeeded in getting to the building
void ABuildingEvents::RemoveBuildingFromEventList(FVector pLocation)
{
	if (pLocation.Equals(FVector::ZeroVector)) return;
	//Remove status effect from Building
	CitySystem->GetBuildingByLocation(pLocation)->BuildingStatus = EBuildingStatus::ES_NONE;

	if (BuildingsWithFireEvents.Contains(pLocation))
	{
		FBuildingEventsData* BuildingEvent = BuildingsWithFireEvents.Find(pLocation);
		if (BuildingEvent) {
			//SetBuildingStatusActiveStatus(pLocation, false);
			//BuildingEvent->BuildingStatusActor = nullptr;
			if(BuildingEvent->ParticleComponent != nullptr) BuildingEvent->ParticleComponent->DestroyComponent();

			BuildingsWithFireEvents.Remove(pLocation);
		}
	}
	else if (BuildingsWithCrimeEvents.Contains(pLocation))
	{
		FBuildingEventsData* BuildingEvent = BuildingsWithCrimeEvents.Find(pLocation);
		if (BuildingEvent) {
			//SetBuildingStatusActiveStatus(pLocation, false);
			//BuildingEvent->BuildingStatusActor = nullptr;
			if (BuildingEvent->ParticleComponent != nullptr) BuildingEvent->ParticleComponent->DestroyComponent();

			BuildingsWithCrimeEvents.Remove(pLocation);
		}
	}
	else if (BuildingsWithAccidentEvents.Contains(pLocation))
	{
		FBuildingEventsData* BuildingEvent = BuildingsWithAccidentEvents.Find(pLocation);
		if (BuildingEvent) {
			//SetBuildingStatusActiveStatus(pLocation, false);
			//BuildingEvent->BuildingStatusActor = nullptr;
			if (BuildingEvent->ParticleComponent != nullptr) BuildingEvent->ParticleComponent->DestroyComponent();

			BuildingsWithAccidentEvents.Remove(pLocation);
		}
	}
}

void ABuildingEvents::LoadEventsForBuilding(EBuildingStatus pBuildingEventType, FVector pLocation, FBuildingStats pBuildingStats)
{
	FBuildingEventsData loadedBuilding;
	loadedBuilding.BuildingLocation = pLocation;
	loadedBuilding.BuildingStats = pBuildingStats;

	switch (pBuildingEventType)
	{
	case EBuildingStatus::ES_ONFIRE:
		loadedBuilding.EventType = EBuildingEventType::ET_FIRE;
		break;
	case EBuildingStatus::ES_ROBBED:
		loadedBuilding.EventType = EBuildingEventType::ET_CRIME;
		break;
	case EBuildingStatus::ES_ACCIDENT:
		loadedBuilding.EventType = EBuildingEventType::ET_ACCIDENT;
		break;
	default:
		break;
	}

	LoadedBuildingEvents.Add(loadedBuilding);
}

void ABuildingEvents::PushEventsAfterLoadingSave()
{
	for (auto& buildingWithEvent : LoadedBuildingEvents)
	{
		PushEventToBuilding(buildingWithEvent.EventType, buildingWithEvent.BuildingLocation, buildingWithEvent.BuildingStats);
	}

	LoadedBuildingEvents.Empty();
}

void ABuildingEvents::CheckUndispatchedVehicles()
{
	for (auto& fireEvent : BuildingsWithFireEvents)
	{
		if (fireEvent.Value.bVehicleDispatched) continue;
		FBuildingStats nearestBuilding = CitySystem->GetNearestBuildingByType(EBuildingType::EB_FIRE_DEPARTMENT, fireEvent.Value.BuildingStats.ConnectedRoadLocation);
		bool bIsBuildingFound = (nearestBuilding.ID != NAME_None) ? true : false;
		if (!bIsBuildingFound) continue;
		if (!CitySystem->GetPopulationManager()->IsOperational(nearestBuilding)) continue;
		fireEvent.Value.bVehicleDispatched = true;
		CitySystem->GetTrafficSystem()->SpawnRandomAgentByType(nearestBuilding.ConnectedRoadLocation, fireEvent.Value.BuildingStats.ConnectedRoadLocation, nearestBuilding.BuildingType, fireEvent.Value.BuildingLocation,true);
	}

	for (auto& crimeEvent : BuildingsWithCrimeEvents)
	{
		if (crimeEvent.Value.bVehicleDispatched) continue;
		FBuildingStats nearestBuilding = CitySystem->GetNearestBuildingByType(EBuildingType::EB_POLICE, crimeEvent.Value.BuildingStats.ConnectedRoadLocation);
		bool bIsBuildingFound = (nearestBuilding.ID != NAME_None) ? true : false;
		if (!bIsBuildingFound) continue;
		if (!CitySystem->GetPopulationManager()->IsOperational(nearestBuilding)) continue;
		crimeEvent.Value.bVehicleDispatched = true;
		CitySystem->GetTrafficSystem()->SpawnRandomAgentByType(nearestBuilding.ConnectedRoadLocation, crimeEvent.Value.BuildingStats.ConnectedRoadLocation, nearestBuilding.BuildingType, crimeEvent.Value.BuildingLocation,true);
	}

	TArray<EBuildingType> accidentEventTypes;
	accidentEventTypes.Add(EBuildingType::EB_HOSPITAL);
	accidentEventTypes.Add(EBuildingType::EB_CLINIC);
	for (auto& accidentEvent : BuildingsWithAccidentEvents)
	{
		if (accidentEvent.Value.bVehicleDispatched) continue;
		FBuildingStats nearestBuilding = CitySystem->GetNearestBuildingByTypes(accidentEventTypes, accidentEvent.Value.BuildingStats.ConnectedRoadLocation);
		bool bIsBuildingFound = (nearestBuilding.ID != NAME_None) ? true : false;
		if (!bIsBuildingFound) continue;
		if (!CitySystem->GetPopulationManager()->IsOperational(nearestBuilding)) continue;
		accidentEvent.Value.bVehicleDispatched = true;
		CitySystem->GetTrafficSystem()->SpawnRandomAgentByType(nearestBuilding.ConnectedRoadLocation, accidentEvent.Value.BuildingStats.ConnectedRoadLocation, nearestBuilding.BuildingType, accidentEvent.Value.BuildingLocation,true);
	}
}

