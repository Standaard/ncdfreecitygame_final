// Fill out your copyright notice in the Description page of Project Settings.

#include "FeedbackNotificationManager.h"
#include "CitySystemGameMode.h"
#include "Misc/DateTime.h"
#include "Engine/DataTable.h"
#include "NotificationHandling.h"
AFeedbackNotificationManager::AFeedbackNotificationManager()
{
}

AFeedbackNotificationManager::~AFeedbackNotificationManager()
{
}

void AFeedbackNotificationManager::BeginPlay()
{
	Super::BeginPlay();
	CitySystem = GetWorld()->GetAuthGameMode<ACitySystemGameMode>();
	if (!CitySystem) return;
	LoadConditions();
}

void AFeedbackNotificationManager::LoadConditions()
{
	if (!NotificationDataTable) return;

	FString ContextString;
	TArray<FName> RowNames = NotificationDataTable->GetRowNames();
	for (auto& RowName : RowNames)
	{
		FFeedbackNotifications* Condition = NotificationDataTable->FindRow<FFeedbackNotifications>(RowName, ContextString);
		if (Condition) {
			Conditions.Add(*RowName.ToString(), *Condition);
		}
		Condition = nullptr;
	}
}

void AFeedbackNotificationManager::CheckConditions()
{
	if (Conditions.Num() == 0) return;
	FDateTime currentDateTime = CitySystem->GetCurrentDateTime();
	float FeedbackValueThreshold;

	for (const auto& notification : Conditions) {
		FFeedbackNotifications feedbackNotification = notification.Value;

		int conditionCount = feedbackNotification.Conditions.Num();
		int conditionsMet = 0;

		for (const auto& condition : feedbackNotification.Conditions) {
			EFeedbackRatioType ratioType = condition.Key;
			FFeedbackCondition FC = condition.Value;

			FeedbackValueThreshold = ReturnFeedbackValue(ratioType);
			if (FC.bBiggerThanOne)
			{
				if (FC.FeedbackValue > FeedbackValueThreshold && currentDateTime >= notification.Value.CooldownDateEnd) {
					conditionsMet++;
				}
			}
			else
			{
				if (FC.FeedbackValue < FeedbackValueThreshold && currentDateTime >= notification.Value.CooldownDateEnd) {
					conditionsMet++;
				}
			}
		}
		//checks for all of the conditions met
		if (conditionsMet == conditionCount)
		{
			RaiseNotification(feedbackNotification.Notification);
			feedbackNotification.CooldownDateEnd = currentDateTime + notification.Value.Cooldown;	// Set new End Date of cooldown
		}
	}
}

void AFeedbackNotificationManager::RaiseNotification(FDataTableRowHandle pHandle)
{
	if (!pHandle.DataTable) return;

	Notifications.Push(pHandle.RowName);
	FString ContextString;
	FNotification* FoundNotification = pHandle.DataTable->FindRow<FNotification>(pHandle.RowName, ContextString);

	if (FoundNotification) 
		AddNotification(*FoundNotification);
}

void AFeedbackNotificationManager::AddNotification(FNotification pNotification)
{
	if (NotificationWidget)
		NotificationWidget->AddNotification(pNotification);
}

FName AFeedbackNotificationManager::CheckForNotification()
{
	if (Notifications.Num() > 0)
		return Notifications.Pop();
	else
		return "";
}



float AFeedbackNotificationManager::ReturnFeedbackValue(EFeedbackRatioType pRatioType)
{
	switch (pRatioType)
	{
	case EFeedbackRatioType::EF_UNEMPLOYMENT:
		return CitySystem->HasEnoughJobsForPeople();
		break;
	case EFeedbackRatioType::EF_HOSPITALVISITOR:
		return (CitySystem->HasEnoughBuildingsOfType(EBuildingType::EB_CLINIC) + CitySystem->HasEnoughBuildingsOfType(EBuildingType::EB_HOSPITAL));
		break;
	case EFeedbackRatioType::EF_FIREDEPARTMENTVISITOR:
		return CitySystem->HasEnoughBuildingsOfType(EBuildingType::EB_FIRE_DEPARTMENT);
		break;
	case EFeedbackRatioType::EF_POLICEVISITOR:
		return CitySystem->HasEnoughBuildingsOfType(EBuildingType::EB_POLICE);
		break;
	case EFeedbackRatioType::EF_COMMERCEVISITOR:
		return 1.f;
		break;
	default:
		return 1.f;
		break;
	}
}