// Fill out your copyright notice in the Description page of Project Settings.

#include "PlaceableObject.h"
#include "Components/StaticMeshComponent.h"

APlaceableObject::APlaceableObject () {
	meshComp = CreateDefaultSubobject<UStaticMeshComponent> (TEXT ("Mesh component"));
	//meshComp->SetStaticMesh (baseMesh);
	RootComponent = meshComp;
}

void APlaceableObject::BeginPlay () {
	Super::BeginPlay ();

}

void APlaceableObject::SetMesh (UStaticMesh* pMesh) {
	meshComp->SetStaticMesh (pMesh);
}
