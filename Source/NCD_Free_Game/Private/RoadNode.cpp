// Fill out your copyright notice in the Description page of Project Settings.

#include "RoadNode.h"
int URoadNode::GlobalID = 0;

void URoadNode::Initialize(FVector pLocation)
{
	Position = pLocation;
}

FVector URoadNode::GetLocation()
{
	return Position;
}

void URoadNode::AddNeighbour(URoadNode* pNode)
{
	Neighbours.Add(pNode);
}

void URoadNode::RemoveNode()
{
	//for (int i = 0; i < Neighbours.Num(); i++)
	//{
	//	Neighbours[i]->RemoveFromNeighbours(this);
	//}
	bEnabled = false;
}

void URoadNode::RemoveFromNeighbours(URoadNode* pNode)
{
	Neighbours.Remove(pNode);
}

TArray<URoadNode*> URoadNode::GetNeighbours()
{
	return Neighbours;
}

int URoadNode::GetID()
{
	return ID;
}

float URoadNode::CalculateDistance(URoadNode* pOther) {
	return FVector::Distance(GetLocation(), pOther->GetLocation());
}
