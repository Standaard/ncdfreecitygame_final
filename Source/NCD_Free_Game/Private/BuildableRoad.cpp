// Fill out your copyright notice in the Description page of Project Settings.

#include "BuildableRoad.h"
#include "Grid.h"
#include <WorldCollision.h>
#include "Engine/World.h"
#include "Components/StaticMeshComponent.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "RoadNode.h"
#include <DrawDebugHelpers.h>
#include "BuildableActor.h"
#include "CitySystemGameMode.h"
#include "TrafficSystem.h"
#include "PlacedBuilding.h"
#include "PlacedRoad.h"
#define print(text) UE_LOG(LogTemp, Warning, text);
#define printint(text, textint) UE_LOG(LogTemp, Warning, text, textint);


ABuildableRoad::ABuildableRoad()
{

}

ABuildableRoad::~ABuildableRoad()
{

}

void ABuildableRoad::BeginPlay()
{
	Super::BeginPlay();
	//cache the directional checked locations
	checkLocations.Init(FVector::ZeroVector, 4);
	checkLocations[0] = FVector(0, -100, 0);
	checkLocations[1] = FVector(0, 100, 0);
	checkLocations[2] = FVector(-100, 0, 0);
	checkLocations[3] = FVector(100, 0, 0);

	RoadTransforms.Init(FTransform::Identity, 5);
	RoadTypes.Init(ERoadType::EB_STRAIGHT, 5);
	HasCollision.Init(false, 5);
	bCanRotate = false;
}

void ABuildableRoad::OnPreviewPlace(FTransform pTransform, bool pSkipCollisionCheck, bool pIsFreeBuilding)
{
	//obtain the snapped to grid impact location
	FVector SnappedLocation = Grid::GetLocationOnGrid(pTransform.GetLocation());
	PreviewMesh->SetActorLocation(SnappedLocation);
	
	UpdateRoad(SnappedLocation + FVector(0, -100, 0), true, false, true, true, ERoadPosition::EB_LEFT);
	UpdateRoad(SnappedLocation + FVector(0, 100, 0), true, true, false, true, ERoadPosition::EB_RIGHT);
	UpdateRoad(SnappedLocation + FVector(-100, 0, 0), false, true, true, true, ERoadPosition::EB_BOTTOM);
	UpdateRoad(SnappedLocation + FVector(100, 0, 0), true, true, true, false, ERoadPosition::EB_TOP);
	UpdateCenterRoad(SnappedLocation);
	bUpdateMesh = false;

	Super::OnPreviewPlace(pTransform, pSkipCollisionCheck);
}


void ABuildableRoad::CheckSpecialCollision(FVector SnappedLocation)
{
	int32 collisionNum = 0;
	for (int32 i = 0; i < checkLocations.Num(); i++)
	{	
		if (SweepForActorInScene(SnappedLocation + checkLocations[i], RoadClass)) collisionNum++;
	}
	if (collisionNum == 0)
	{
		bAllowedToBuild = false;
	}
}

FVector ABuildableRoad::OnBuildableActorPlace(FTransform pTransform)
{
	if (!bAllowedToBuild || !LatestCreatedBuilding) return FVector::ZeroVector;
	APlacedRoad* latestPlacedRoad = Cast<APlacedRoad>(LatestCreatedBuilding);
	if (!latestPlacedRoad) return FVector::ZeroVector;

	FVector returnLocation = Grid::GetLocationOnGrid(pTransform.GetLocation());
	StoredBuildings.Add(returnLocation, LatestCreatedBuilding);

	bUpdateMesh = true;

	for (int i = 0; i < RoadTypes.Num(); i++)
	{
		if (!HasCollision[i]) continue;
		APlacedBuilding** findbuilding = StoredBuildings.Find(RoadTransforms[i].GetLocation());
		if (findbuilding)
		{
			APlacedRoad* foundRoad = Cast<APlacedRoad>(*findbuilding);
			if (foundRoad)
			{
				foundRoad->UpdateRoadMaterial(RoadTypes[i]);
				foundRoad->SetActorRotation(RoadTransforms[i].GetRotation());
				foundRoad->BuildingStats.Rotation = RoadTransforms[i].GetRotation().Rotator();
			}
		}
	}


	if (HasCollision[4]) { CreateNode(returnLocation); }

	for (int i = 0; i < RoadTransforms.Num(); i++)
	{
		RoadTypes[i] = ERoadType::EB_STRAIGHT;
		RoadTransforms[i] = FTransform::Identity;
		HasCollision[i] = false;
	}

	PreviewMesh->GetMeshComponent()->SetStaticMesh(latestPlacedRoad->GetMeshComponent()->GetStaticMesh());
	PreviewMesh->SetActorHiddenInGame(false);

	bAllowedToBuild = false;

	return returnLocation;
}

void ABuildableRoad::OnDelete(FVector pLocation)
{
	Super::OnDelete	(pLocation);
	DeleteNode(pLocation);
	bUpdateMesh = true;
	UpdateRoad(pLocation + FVector(-100, 0, 0), true, true, true, true, ERoadPosition::EB_BOTTOM);
	UpdateRoad(pLocation + FVector(0, -100, 0), true, true, true, true, ERoadPosition::EB_LEFT);
	UpdateRoad(pLocation + FVector(0, 100, 0), true, true, true, true, ERoadPosition::EB_RIGHT);
	UpdateRoad(pLocation + FVector(100, 0, 0), true, true, true, true, ERoadPosition::EB_TOP);
	bUpdateMesh = false;
}

void ABuildableRoad::UpdateCenterRoad(FVector pSnappedLocation)
{
	TArray<bool> ContainsRoads;
	ContainsRoads.Init(false, 4);
	ContainsRoads[0] = SweepForActorInScene(pSnappedLocation + FVector(0, -100, 50), RoadClass);
	ContainsRoads[1] = SweepForActorInScene(pSnappedLocation + FVector(0, 100, 50), RoadClass);
	ContainsRoads[2] = SweepForActorInScene(pSnappedLocation + FVector(-100, 0, 50), RoadClass);
	ContainsRoads[3] = SweepForActorInScene(pSnappedLocation + FVector(100, 0, 50), RoadClass);

	HasCollision[4] = true;
	int Count = 0;
	for (int i = 0; i < ContainsRoads.Num(); i++)
	{
		HasCollision[i] = ContainsRoads[i];
		if (ContainsRoads[i]) Count++;
	}

	switch (Count) {
	case 0: SetStraightRoad(ContainsRoads, pSnappedLocation, ERoadPosition::EB_CENTER); 
		break;
	case 1: SetStraightRoad(ContainsRoads, pSnappedLocation, ERoadPosition::EB_CENTER); 
		break;
	case 2: SetTurnRoad(ContainsRoads, pSnappedLocation, ERoadPosition::EB_CENTER); 
		break;
	case 3: SetTSplit(ContainsRoads, pSnappedLocation, ERoadPosition::EB_CENTER); 
		break;
	case 4: SetCrossRoad(ContainsRoads, pSnappedLocation, ERoadPosition::EB_CENTER); 
		break;
	}
}

void ABuildableRoad::UpdateRoad(FVector pSnappedLocation, bool pTop /*= false*/, bool pRight /*= false*/, bool pLeft /*= false*/, bool pBottom /*= false*/, ERoadPosition pPosition)
{
	TArray<bool> ContainsRoads;
	ContainsRoads.Init(false, 4);

	pLeft ? ContainsRoads[0] = SweepForActorInScene(pSnappedLocation + FVector(0, -100, 50), RoadClass) : ContainsRoads[0] = true;
	pRight ? ContainsRoads[1] = SweepForActorInScene(pSnappedLocation + FVector(0, 100, 50), RoadClass) : ContainsRoads[1] = true;
	pBottom ? ContainsRoads[2] = SweepForActorInScene(pSnappedLocation + FVector(-100, 0, 50), RoadClass) : ContainsRoads[2] = true;
	pTop ? ContainsRoads[3] = SweepForActorInScene(pSnappedLocation + FVector(100, 0, 50), RoadClass) : ContainsRoads[3] = true;

	int Count = 0;
	for (int i = 0; i < ContainsRoads.Num(); i++)
	{
		if (ContainsRoads[i] == true) Count++;
	}

	switch (Count) {
		case 0: SetStraightRoad(ContainsRoads, pSnappedLocation, pPosition); 
			break;
		case 1: SetStraightRoad(ContainsRoads, pSnappedLocation, pPosition); 
			break;
		case 2: SetTurnRoad(ContainsRoads, pSnappedLocation, pPosition); 
			break;
		case 3: SetTSplit(ContainsRoads, pSnappedLocation, pPosition); 
			break;
		case 4: SetCrossRoad(ContainsRoads, pSnappedLocation, pPosition);
			break;
	}
}

void ABuildableRoad::UpdateRoadMesh(ERoadType pRoadType, FVector pSnappedLocation, int pRotation, ERoadPosition pPosition)
{
	uint8 arrindex = (uint8)pPosition;
	RoadTypes[arrindex] = pRoadType;
	FRotator newRotation = FRotator(0, pRotation, 0);
	RoadTransforms[arrindex] = FTransform(newRotation, pSnappedLocation);

	PreviewMesh->SetActorRotation(newRotation.Quaternion());

	APlacedBuilding** foundBuilding = StoredBuildings.Find(pSnappedLocation);

	if (foundBuilding && *foundBuilding)
	{
		APlacedRoad* foundRoad = Cast<APlacedRoad>(*foundBuilding);
		if (!foundRoad) return;
		PreviewMesh->GetMeshComponent()->SetStaticMesh(foundRoad->GetMeshComponent()->GetStaticMesh());
		PreviewMesh->SetActorHiddenInGame(false);

		if (!bUpdateMesh) return;
		foundRoad->UpdateRoadMaterial(pRoadType);
		foundRoad->SetActorRotation(newRotation);
		foundRoad->BuildingStats.Rotation = newRotation;
	}
}

void ABuildableRoad::SetStraightRoad(TArray<bool> Neighbours, FVector pSnappedLocation, ERoadPosition pPosition)
{
	int newRot = 0;

	if (Neighbours[2] || Neighbours[3]) { newRot = 0; }
	if (Neighbours[0] || Neighbours[1]) { newRot = 90; }

	UpdateRoadMesh(ERoadType::EB_STRAIGHT, pSnappedLocation, newRot, pPosition);
}

void ABuildableRoad::SetTurnRoad(TArray<bool> Neighbours, FVector pSnappedLocation, ERoadPosition pPosition)
{
	int newRot = 0;

	if (Neighbours[0] && Neighbours[1]) { SetStraightRoad(Neighbours, pSnappedLocation, pPosition); return; }
	if (Neighbours[2] && Neighbours[3]) { SetStraightRoad(Neighbours, pSnappedLocation, pPosition); return; }
	
	if (Neighbours[3] && Neighbours[1]) { newRot = -90; }
	if (Neighbours[0] && Neighbours[2]) { newRot = 90; }
	if (Neighbours[0] && Neighbours[3]) { newRot = 180; }

	UpdateRoadMesh(ERoadType::EB_TURN, pSnappedLocation, newRot, pPosition);
}

void ABuildableRoad::SetTSplit(TArray<bool> Neighbours, FVector pSnappedLocation, ERoadPosition pPosition)
{
	int newRot = 0;

	if (!Neighbours[0]) { newRot = 270; }
	if (!Neighbours[1]) { newRot = 90; }
	if (!Neighbours[2]) { newRot = 180; }
	//if (!Neighbours[3]) { newRot = 180; }

	UpdateRoadMesh(ERoadType::EB_TSPLIT, pSnappedLocation, newRot, pPosition);
}

void ABuildableRoad::SetCrossRoad(TArray<bool> Neighbours, FVector pSnappedLocation, ERoadPosition pPosition)
{
	UpdateRoadMesh(ERoadType::EB_CROSS, pSnappedLocation, 0, pPosition);
}

void ABuildableRoad::DeleteNode(FVector pLocation)
{
	for (int i = 0; i < RoadNodes.Num(); i++)
	{
		if(!RoadNodes[i]) continue;

		if (pLocation == RoadNodes[i]->GetLocation())
		{  
			RoadNodes[i]->RemoveNode();
			////RoadNodes[i]->ConditionalBeginDestroy();
			//delete RoadNodes[i];
			//RoadNodes[i] = nullptr;
		}
	}

}

void ABuildableRoad::LinkNodes(URoadNode* pNode1, URoadNode* pNode2)
{
	pNode1->AddNeighbour(pNode2);
	pNode2->AddNeighbour(pNode1);
}

URoadNode* ABuildableRoad::FindRoadNodeByLocation(FVector pLocation)
{
	for (auto* node : RoadNodes)
	{
		if (!node || !node->GetLocation().Equals(pLocation)) continue;
		return node;
	}
	return nullptr;
}

void ABuildableRoad::CreateNode(FVector pLocation)
{
	URoadNode* foundNode = FindRoadNodeByLocation(pLocation);
	if (foundNode)
	{
		foundNode->bEnabled = true;
		return;
	}

	URoadNode* NewNode = NewObject<URoadNode>();
	NewNode->Initialize(pLocation);

	for (int i = 0; i < RoadNodes.Num(); i++)
	{
		if(!RoadNodes[i]) continue;

		if ((NewNode->GetLocation() + FVector(100, 0, 0)) == RoadNodes[i]->GetLocation()) { LinkNodes(RoadNodes[i], NewNode); }
		if ((NewNode->GetLocation() + FVector(-100, 0, 0)) == RoadNodes[i]->GetLocation()) { LinkNodes(RoadNodes[i], NewNode); }
		if ((NewNode->GetLocation() + FVector(0, 100, 0)) == RoadNodes[i]->GetLocation()) { LinkNodes(RoadNodes[i], NewNode); }
		if ((NewNode->GetLocation() + FVector(0, -100, 0)) == RoadNodes[i]->GetLocation()) { LinkNodes(RoadNodes[i], NewNode); }
	}

	RoadNodes.Add(NewNode);
}

URoadNode* ABuildableRoad::GetNodeByLocation(FVector pLocation)
{
	for (auto* node : RoadNodes) {
		if (node && node->GetLocation().Equals(pLocation))
		{
			return node;
		}
	}
	return nullptr;
}
