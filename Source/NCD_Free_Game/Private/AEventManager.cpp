// Fill out your copyright notice in the Description page of Project Settings.
//
//#include "EventManager.h"
//#include "../Public/CitySystemGameMode.h"
//#include "DateTime.h"
//#include <GameFramework/Actor.h>
//#include "Engine/World.h"
//#include "EventUI.h"
//#include <WidgetBlueprintLibrary.h>
//
//AEventManager::AEventManager()
//{
//
//}
//
//AEventManager::~AEventManager()
//{
//}
//
//void AEventManager::InitializeEventManager(ACitySystemGameMode* pCitySystemGameMode, UDataTable* pNCDEvents, TSubclassOf<UEventUI> pEventUI)
//{
//	CitySystemGameMode = pCitySystemGameMode;
//	NCDEvents = pNCDEvents;
//	UUserWidget* widget = UWidgetBlueprintLibrary::Create(this, pEventUI, GetWorld()->GetFirstPlayerController());
//	EventUI = Cast<UEventUI>(widget);
//
//	if (!EventUI) return;
//	EventUI->SetEventManager(this);
//	EventUI->AddToViewport(9999);
//
//	Step();
//
//	//EventUI->AddToViewport(9999);
//	//EventUI->SpawnEventWindow("test", "description", "yes", "no");
//
//	//PickNewEvent(true);
//}
//
//void AEventManager::Step()
//{
//	if (!CitySystemGameMode) return;
//
//	if (ProposedEvent && !CurrentEvent) return;
//	if (!ProposedEvent && !CurrentEvent)
//	{
//		return;
//	}
//
//	if(IsEventFinished(CurrentEvent->Condition, CurrentEvent)) 
//	{
//		CompletedEvents.Add(CurrentEvent);
//
//		for (int i = 0; i < CompletedEvents.Num(); i++)
//		{
//			UE_LOG(LogTemp, Warning, TEXT("Event title %s"), *CompletedEvents[i]->Title);
//		}
//		
//		if (CurrentEvent->NextEvent.Num() > 0)
//		{
//			PickFollowUpEvent();
//		}
//		else
//		{
//			//;
//			CitySystemGameMode->GetWorld()->GetTimerManager().SetTimer(TimeToNextEvent, this, &AEventManager::PickNewMaintEvent, 0.f, false, TimeToNextEventInSec);
//		}
//
//		CurrentEvent = nullptr;
//	}
//}
//
//void AEventManager::PickNewEvent(bool pMainEvent)
//{
//	if (CurrentEvent) return;
//	if (ProposedEvent) return;
//	if (!NCDEvents) return;
//
//	for (auto it : NCDEvents->RowMap)
//	{
//		FNCDEvent* NCDEventsRow = (FNCDEvent*)it.Value;
//
//		bool found = false;
//		for (int i = 0; i < CompletedEvents.Num(); i++)
//		{
//			if (CompletedEvents[i] == NCDEventsRow) { 
//				found = true; 
//				break;
//			}
//		}
//		if (found) { continue; }
//
//		if (!NCDEventsRow) continue;
//
//		if (pMainEvent) {
//			if (NCDEventsRow->Priority == EEventPriority::VE_Main && IsEventTriggered(NCDEventsRow->Condition, NCDEventsRow)) { 
//				ProposeEvent(NCDEventsRow);
//				break;
//			}
//			continue;
//		}
//	}
//
//	if (ProposedEvent) return;
//	if (pMainEvent) {
//		PickNewEvent(false);
//		return;
//	}
//
//	for (auto it : NCDEvents->RowMap)
//	{
//		FNCDEvent* NCDEventsRow = (FNCDEvent*)it.Value;
//
//		if (!NCDEventsRow) continue;
//
//		if (NCDEventsRow->Condition == EConditionEnum::VE_Random)
//		{
//			ProposeEvent(NCDEventsRow);
//			return;
//		}
//	}
//}
//
//void AEventManager::IsOverTime(FDateTime pCurrentDate)
//{
//	if(pCurrentDate >= TimeLimit) {  }
//}
//
//bool AEventManager::IsEventTriggered(EConditionEnum pCondition, FNCDEvent* pEvent)
//{
//	if (!CitySystemGameMode) return false;
//
//	if (pCondition == EConditionEnum::VE_BuildingAmount)
//	{
//		TMap<EBuildingType, int> BuildingTypes = CitySystemGameMode->GetPlacedBuildingsCounters();
//		int ConditionCount = CitySystemGameMode->GetBuildingsByType(pEvent->BuildingType).Num();
//
//		return (ConditionCount >= pEvent->TriggerCount);
//	}
//
//	if (pCondition == EConditionEnum::VE_People)
//	{
//		return false; //return (CitySystemGameMode->GetPopulation() > pEvent->TriggerCount);
//	}
//
//	if (pCondition == EConditionEnum::VE_Polution)
//	{
//		return CitySystemGameMode->IsPollutionTooHigh();
//	}
//
//	if (pCondition == EConditionEnum::VE_WorkingPeople)
//	{
//		// will be implemented soon
//		return false;
//	}
//
//	return false;
//}
//
//bool AEventManager::IsEventFinished(EConditionEnum pConditions, FNCDEvent* pEvent)
//{
//	if (!CitySystemGameMode) return false;
//
//	if (pConditions == EConditionEnum::VE_BuildingAmount)
//	{
//		int ConditionCount = CitySystemGameMode->GetBuildingsByType(pEvent->BuildingType).Num();
//
//		return (ConditionCount >= pEvent->AcceptenceCount);
//	}
//
//	if (pConditions == EConditionEnum::VE_People)
//	{
//		return false; //(CitySystemGameMode->GetPopulation() > pEvent->AcceptenceCount);
//	}
//
//	if (pConditions == EConditionEnum::VE_Polution)
//	{
//		return !CitySystemGameMode->IsPollutionTooHigh();
//	}
//
//	if (pConditions == EConditionEnum::VE_WorkingPeople)
//	{
//		// will be implemented soon
//		return false;
//	}
//
//	return false;
//}
//
//void AEventManager::ProposeEvent(FNCDEvent* pEvent)
//{
//	UE_LOG(LogTemp, Warning, TEXT("Propose Main Event"));
//	ProposedEvent = pEvent;
//
//	if (!EventUI) return;
//	UE_LOG(LogTemp, Warning, TEXT("Is Legit"));
//
//	EventUI->SpawnEventWindow(pEvent->Title, pEvent->Description, pEvent->OptionOneText, pEvent->OptionTwoText);
//}
//
//void AEventManager::OnEventAccepted()
//{
//	if (!ProposedEvent) return;
//
//	TimeToNextEventInSec = ProposedEvent->TimeToNextEvent;
//
//	CurrentEvent = ProposedEvent;
//	ProposedEvent = nullptr;
//}
//
//void AEventManager::OnEventFailed()
//{
//	UE_LOG(LogTemp, Warning, TEXT("Failed"));
//	// Remove Hapiness
//
//	ProposedEvent = nullptr;
//	CurrentEvent = nullptr;
//}
//
//void AEventManager::PickNewMaintEvent()
//{
//	PickNewEvent(true);
//}
//
//void AEventManager::PickFollowUpEvent()
//{
//
//}
