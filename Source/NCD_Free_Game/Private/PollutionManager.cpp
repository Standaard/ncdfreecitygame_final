// Fill out your copyright notice in the Description page of Project Settings.

#include "PollutionManager.h"
#include "BuildingStats.h"
#include "BuildingArea.h"
#include "CitySystemGameMode.h"
#include "BuildingAreaManager.h"
#include <DrawDebugHelpers.h>
#include <Kismet/GameplayStatics.h>
#include <Engine/PostProcessVolume.h>
#include <Materials/MaterialParameterCollection.h>
#include <Kismet/KismetMathLibrary.h>
#include <Materials/MaterialParameterCollectionInstance.h>
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "NCD_PlayerController.h"
#include "PlacedBuilding.h"

// Sets default values
APollutionManager::APollutionManager()
{
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void APollutionManager::BeginPlay()
{
	Super::BeginPlay();
	CitySystem = Cast<ACitySystemGameMode>(GetWorld()->GetAuthGameMode());
	if (!PollutionMPC)
	{
		UE_LOG(LogTemp, Error, TEXT("PollutionMPC is not assigned in PollutionManager!"));
	}
	else
	{
		PollutionMPCInstance = GetWorld()->GetParameterCollectionInstance(PollutionMPC);
		SetPollutionFogScalar(0.0f);
	}
	PollutionAreas.Reserve(500);
}

void APollutionManager::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	for (auto* area : PollutionAreas) {
		if (!area) continue;
		area->RemoveAllBuildings();
	}
	PollutionAreas.Empty();
}

void APollutionManager::AddGlobalPollution(float pValue)
{
	Pollution += pValue;
}

void APollutionManager::GetPollutionInConsole()
{
	if (!CitySystem) return;
	FString pollutionToString = FString::FromInt(GetPollution());
	CitySystem->GetPlayerController()->ClientMessage(pollutionToString);
}

void APollutionManager::AddAreaPollutionToBuilding(APlacedBuilding* pOwner, int pValueToAdd)
{
	if (!pOwner) return;
	FBuildingStats& ownerStats = pOwner->BuildingStats;
	ownerStats.UnclampedCurrentPollution += pValueToAdd;
	//clamp the lower bound of TotalPollution to zero
	ownerStats.TotalPollution = ownerStats.UnclampedCurrentPollution >= 0 ? ownerStats.UnclampedCurrentPollution : 0;
}

bool APollutionManager::CreatePollutionArea(APlacedBuilding* pOwner, bool pLoadGame /*= false*/)
{
	if (!CitySystem || !CitySystem->BuildingAreaManager || !pOwner) return false;
	
	FBuildingStats& ownerStats = pOwner->BuildingStats;
	if (((!ownerStats.bCanPollute || !ownerStats.bCanPolluteInArea) && !ownerStats.bCanBePolluted)) return false;
	UBuildingArea* newArea = CitySystem->BuildingAreaManager->CreateBuildingArea(pOwner);
	
	if (!newArea) return false;

	//determine if the building can add pollution to Pollution
	if (ownerStats.bCanPollute)
	{
		AddGlobalPollution(ownerStats.Pollution);
	}

	if (ownerStats.bCanPolluteInArea)
	{
		//skip adding values to other buildings, as they are already saved
		if (!pLoadGame) AddAreaPollutionToBuilding(pOwner, ownerStats.Pollution);

		TArray<APlacedBuilding*> buildingsToPollute;
		//Retrieve ALL surrounding buildings
		CitySystem->BuildingAreaManager->GetBuildingsByBoxTrace(ownerStats.Location, ownerStats.PollutionExtent, buildingsToPollute);

		for (APlacedBuilding* BTP : buildingsToPollute)
		{
			//check if the building is valid and can be polluted
			if (!BTP || !BTP->BuildingStats.bCanBePolluted) continue;
			UBuildingArea* foundArea = GetPollutionAreaByOwner(BTP);
			//add the building to the new area
			if (!foundArea || !newArea->AddBuilding(BTP)) continue;
			//add the building's area to the affecting areas (used for removal)
			if (!foundArea->AddAffectingArea(newArea)) continue;
			//skip the addition of pollution when loading buildingstats from a save file
			if (!pLoadGame) AddAreaPollutionToBuilding(BTP, ownerStats.PollutionInArea);
			//Update BTP's mesh as its value is changed
			UpdatePollutionMeshByPlacedBuilding(BTP);
		}
		pOwner->SetPollutionDecalVisible(bPollutionViewEnabled);
	}

	if (ownerStats.bCanBePolluted)
	{
		TArray<APlacedBuilding*> buildingsThatPolluteOwner;
		//Look for the buildings around pOwner in MaxPollutionAreaExtent
		CitySystem->BuildingAreaManager->GetBuildingsByBoxTrace(ownerStats.Location, MaxPollutionAreaExtent, buildingsThatPolluteOwner);
		TArray<APlacedBuilding*> buildingsCheck;

		for (APlacedBuilding* BTPO : buildingsThatPolluteOwner)
		{
			if (!BTPO || !BTPO->BuildingStats.bCanPollute) continue;
			UBuildingArea* foundArea = GetPollutionAreaByOwner(BTPO);
			if (!foundArea) continue;
			buildingsCheck.Empty();

			//determine if newArea->Owner is in range of BTPO
			CitySystem->BuildingAreaManager->GetBuildingsByBoxTrace(BTPO->BuildingStats.Location, BTPO->BuildingStats.PollutionExtent, buildingsCheck);
			bool bNewOwnerFound = false;
			for (auto* building : buildingsCheck)
			{
				if (building == pOwner)
				{
					bNewOwnerFound = true;
					break;
				}
			}
			//continue if newArea->Owner is not affected by BTPO
			if (!bNewOwnerFound) continue;
			if (!foundArea->AddBuilding(pOwner)) continue;
			if (!newArea->AddAffectingArea(foundArea)) continue;

			AddAreaPollutionToBuilding(pOwner, BTPO->BuildingStats.PollutionInArea);
		}
	}

	PollutionAreas.Add(newArea);
	UpdatePollutionMeshByPlacedBuilding(pOwner);

	const float fogScalar = UKismetMathLibrary::MapRangeClamped(GetPollution(), 0.f, PollutionMaxValue, 0.f, 1.f);
	SetPollutionFogScalar(fogScalar);
	return true;
}

UBuildingArea* APollutionManager::GetPollutionAreaByLocation(FVector pLocation)
{
	for (auto* area : PollutionAreas)
	{
		if (!area || !area->Owner)
			continue;

		if (area->Owner->BuildingStats.Location.Equals(pLocation)) return area;
	}
	return nullptr;
}

UBuildingArea* APollutionManager::GetPollutionAreaByOwner(APlacedBuilding* pPlacedBuilding)
{
	for (UBuildingArea* area : PollutionAreas)
	{
		if (!area) continue;
		if (area->Owner && area->Owner == pPlacedBuilding) return area;
	}
	return nullptr;
}

bool APollutionManager::RemovePollutionAreaByPlacedBuilding(APlacedBuilding* pBuilding)
{
	if (!pBuilding) return false;
	UBuildingArea* foundPollutionArea = GetPollutionAreaByOwner(pBuilding);
	if (!foundPollutionArea) return false;

	//Remove owner from the areas that affect the owner
	for (auto* area : foundPollutionArea->AreasThatAffectOwner)
	{
		if (!area) continue;
		area->RemoveBuilding(foundPollutionArea->Owner);
	}

	//Remove both base pollution and the area pollution from the other areas in one go
	AddGlobalPollution(-pBuilding->BuildingStats.Pollution);
	AddAreaPollutionToBuilding(pBuilding, -pBuilding->BuildingStats.Pollution);

	//Remove the buildings that are affected by the owner
	for (APlacedBuilding* affectedBuilding : foundPollutionArea->Buildings)
	{
		if (!affectedBuilding) continue;
		//Remove the area pollution
		AddAreaPollutionToBuilding(affectedBuilding, -pBuilding->BuildingStats.PollutionInArea);
		//Update the mesh accordingly
		UpdatePollutionMeshByPlacedBuilding(affectedBuilding);
	}
	//Clear the BuildingArea
	foundPollutionArea->RemoveAllBuildings();
	PollutionAreas.Remove(foundPollutionArea);

	const float fogScalar = UKismetMathLibrary::MapRangeClamped(GetPollution(), 0.f, PollutionMaxValue, 0.f, 1.f);
	SetPollutionFogScalar(fogScalar);

	return true;
}

void APollutionManager::SetPollutionFogScalar(float pScalar)
{
	if (PollutionMPCInstance)
		PollutionMPCInstance->SetScalarParameterValue(PollutionFogScalarName, pScalar);
}

void APollutionManager::SetPollutionView(bool pEnabled)
{
	if (!PollutionMPCInstance) return;
	bPollutionViewEnabled = pEnabled;
	//Enable the pollution scalar in the material collection
	PollutionMPCInstance->SetScalarParameterValue(PollutionEnabledScalarName, (pEnabled) ? 1.0f : 0.0f);
	CitySystem->GetPlayerController()->bUseCustomHighlightMaterials = pEnabled;

	for (auto* area : PollutionAreas)
	{
		if (!area || !area->Owner) continue;
		area->Owner->SetPollutionDecalVisible(pEnabled);
	}
}

bool APollutionManager::UpdatePollutionMeshByPlacedBuilding(APlacedBuilding* pOwner)
{
	if (!pOwner) return false;
	//use the clamped pollution for pollutable buildings
	float pollutionValue = pOwner->BuildingStats.TotalPollution;
	//use the unclamped pollution for polluting buildings (negative pollution is only shown on polluting buildings)
	if (pOwner->BuildingStats.bCanPollute)
		pollutionValue = pOwner->BuildingStats.UnclampedCurrentPollution;

	const float pollutionScalar = UKismetMathLibrary::MapRangeClamped(pollutionValue, PollutionBuildingVisualMin, PollutionBuildingVisualMax, 0.0f, 1.0f);
	return pOwner->SetPollutionColor(pollutionScalar);
}

