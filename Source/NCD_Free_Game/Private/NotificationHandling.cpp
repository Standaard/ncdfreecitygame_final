// Fill out your copyright notice in the Description page of Project Settings.

#include "NotificationHandling.h"




void UNotificationHandling::AddNotification(FNotification pNotification)
{
	Notifications.Push(pNotification);
}

FNotification UNotificationHandling::CheckForNotification()
{
	if (Notifications.Num() > 0)
		return Notifications.Pop();
	else
		return FNotification();
}
