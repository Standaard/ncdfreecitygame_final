// Fill out your copyright notice in the Description page of Project Settings.

#include "BuildingStatusActor.h"
#include <Kismet/GameplayStatics.h>
#include <RotationMatrix.h>


// Sets default values
ABuildingStatusActor::ABuildingStatusActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRootComponent"));
	RootComponent = SceneRootComponent;
	RootComponent->SetMobility(EComponentMobility::Movable);

	ActorWidgetComp = CreateDefaultSubobject<USActorWidgetComponent>(TEXT("ActorWidgetComp"));
	ActorWidgetComp->SetupAttachment(RootComponent);

	

}

void ABuildingStatusActor::SetStatus(EBuildingEventType pStatus)
{
	StatusBuilding = pStatus;
}

FVector ABuildingStatusActor::GetOffsetLocation()
{
	return OffsetLocation;
}

void ABuildingStatusActor::SetLocation(FVector pLocation)
{
	SetActorLocation(pLocation += OffsetLocation);
}

// Called when the game starts or when spawned
void ABuildingStatusActor::BeginPlay()
{
	Super::BeginPlay();

	if(GetWorld())
		CameraManager = UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0);
}

// Called every frame
void ABuildingStatusActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (CameraManager) {
		SetActorRotation(FRotationMatrix::MakeFromX(CameraManager->GetCameraLocation() - GetActorLocation()).Rotator());
	}

}

