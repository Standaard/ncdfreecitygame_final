// Fill out your copyright notice in the Description page of Project Settings.

#include "HappinessManager.h"
#include "CitySystemGameMode.h"
#include <Kismet/KismetMathLibrary.h>

UHappinessManager::UHappinessManager () {

}

UHappinessManager::~UHappinessManager () {

}

void UHappinessManager::Init (ACitySystemGameMode* pGameMode) {
	gameMode = pGameMode;
	factorWeights = TMap<FString, float> ();
	InitializeWeights ();
}

//Add the different weight factors to the list for later use.
void UHappinessManager::InitializeWeights () {
	factorWeights.Add (UnemploymentWeight, unemploymentWeightVal);
	factorWeights.Add (PollutionWeight, pollutionWeightVal);
	factorWeights.Add (HealthcareWeight, healthcareWeightVal);
	factorWeights.Add (EducationWeight, educationWeightVal);
	factorWeights.Add (PoliceWeight, policeWeightVal);
	factorWeights.Add (FireDepartmentWeight, fireDepartmentWeightVal);
	factorWeights.Add (FoodWeight, foodWeightVal);
}

//Called from CitySystem when a day has passed
void UHappinessManager::DayPassed () {
	weekHappinessAddition += CalculateHappiness (); //Add the calculated daily happiness to the total that will be added to the current happiness after a week has a passed
}

//Called from CitySystem when a week has passed
void UHappinessManager::WeekPassed () {
	happiness += weekHappinessAddition;
	happiness = FMath::Clamp (happiness, 0, 10000);
	weekHappinessAddition = 0;
}

float UHappinessManager::CalculateHappiness () {
	float happinessAddition = 0.0f;
	happinessAddition += GetJobHappiness () + GetServiceHappiness () + GetPollutionHappiness() + GetFoodAndGoodsHappiness();
	return happinessAddition * happinessIncreaseMultiplier * INTERNAL_HAPPINESS_MULTIPLIER;
}

float UHappinessManager::GetJobHappiness () {
	//Get the difference between max unemployment rate and our current unemployment rate.
	int unemploymentOverCap = gameMode->GetPopulationManager ()->GetUnemploymentRatePercentage () - gameMode->GetPopulationManager ()->GetMaxUnemploymentRate ();
	unemploymentOverCap = unemploymentOverCap < 0 ? 0 : unemploymentOverCap;
	//Clamp the resulting value between the negative weight and positive weight.
	return FMath::Clamp(factorWeights[UnemploymentWeight] - (1.2f * unemploymentOverCap), -factorWeights[UnemploymentWeight], factorWeights[UnemploymentWeight]);
}

float UHappinessManager::GetServiceHappiness () {
	float totalServiceHappiness = 0.0f;

	//----------Healthcare----------//
	//If there's more people than coverage that means the number goes negative and if there's more coverage than people, we'll cap it at 0 because we don't want to add an excess of happiness.
	if (unlockedClinic || unlockedHospital) {
		float healthcareUnderMinimum = FMath::Min ((gameMode->GetVisitorCapacityByType (EBuildingType::EB_CLINIC) + gameMode->GetVisitorCapacityByType (EBuildingType::EB_HOSPITAL)) - gameMode->GetPopulationManager ()->GetPopulationCount (), 0);
		float healthcareCoverageUnderCap = gameMode->GetPopulationManager()->GetPopulationCount() == 0 ? 100 : FMath::Max (((FMath::Abs (healthcareUnderMinimum) / gameMode->GetPopulationManager ()->GetPopulationCount ()) * 100.0f) - maxPercentagePopulationWithoutHealthcare, 0.0f);
		totalServiceHappiness += FMath::Clamp (factorWeights[HealthcareWeight] - (0.1f * healthcareCoverageUnderCap), -factorWeights[HealthcareWeight], factorWeights[HealthcareWeight]);
	} else {
		unlockedClinic = gameMode->GetBuildingUnlockedByMilestone (FName ("Clinic"));
		unlockedHospital = gameMode->GetBuildingUnlockedByMilestone (FName ("Hospital"));
	}
	//----------Healthcare----------//
	//----------Police----------//
	if (unlockedPoliceStation) {
		float policeUnderMinimum = FMath::Min (gameMode->GetVisitorCapacityByType (EBuildingType::EB_POLICE) - gameMode->GetPopulationManager ()->GetPopulationCount (), 0);
		float policeCoverageUnderCap = gameMode->GetPopulationManager ()->GetPopulationCount () == 0 ? 100 : FMath::Max (((FMath::Abs (policeUnderMinimum) / gameMode->GetPopulationManager ()->GetPopulationCount ()) * 100.0f) - maxPercentagePopulationWithoutPolice, 0.0f);
		totalServiceHappiness += FMath::Clamp (factorWeights[PoliceWeight] - (0.08f * policeCoverageUnderCap), -factorWeights[PoliceWeight], factorWeights[PoliceWeight]);
	} else unlockedPoliceStation = gameMode->GetBuildingUnlockedByMilestone (FName ("PoliceStation"));
	//----------Police----------//
	//----------Fire Department----------//
	if (unlockedFireDepartment) {
		float FireDepUnderMinimum = FMath::Min (gameMode->GetVisitorCapacityByType (EBuildingType::EB_FIRE_DEPARTMENT) - gameMode->GetPopulationManager ()->GetPopulationCount (), 0);
		float FireDepCoverageUnderCap = gameMode->GetPopulationManager ()->GetPopulationCount () == 0 ? 100 : FMath::Max (((FMath::Abs (FireDepUnderMinimum) / gameMode->GetPopulationManager ()->GetPopulationCount ()) * 100.0f) - maxPercentagePopulationWithoutFireDep, 0.0f);		totalServiceHappiness += FMath::Clamp (factorWeights[FireDepartmentWeight] - (0.05f * FireDepCoverageUnderCap), -factorWeights[FireDepartmentWeight], factorWeights[FireDepartmentWeight]);
	} else unlockedFireDepartment = gameMode->GetBuildingUnlockedByMilestone (FName ("FireDepartment"));
	//----------Fire Department----------//
	//----------Education----------//
	if (unlockedJuniorHigh || unlockedSchool) {
		float educationUnderMinimum = FMath::Min (gameMode->GetVisitorCapacityByType (EBuildingType::EB_BASESCHOOL) + gameMode->GetVisitorCapacityByType(EBuildingType::EB_HIGHSCHOOL) - gameMode->GetPopulationManager ()->GetPopulationCount (), 0);
		float educationCoverageUnderCap = gameMode->GetPopulationManager ()->GetPopulationCount () == 0 ? 100 : FMath::Max (((FMath::Abs (educationUnderMinimum) / gameMode->GetPopulationManager ()->GetPopulationCount ()) * 100.0f) - maxPercentagePopulationWithoutEducation, 0.0f);		totalServiceHappiness += FMath::Clamp (factorWeights[EducationWeight] - (0.1f * educationCoverageUnderCap), -factorWeights[EducationWeight], factorWeights[EducationWeight]);
	} else {
		unlockedJuniorHigh = gameMode->GetBuildingUnlockedByMilestone (FName ("Junior High"));
		unlockedSchool = gameMode->GetBuildingUnlockedByMilestone (FName ("School"));
	}
	//----------Education----------//

	return totalServiceHappiness;
}

float UHappinessManager::GetPollutionHappiness () {
	float totalPollutionHappiness = 0.0f;

	if (unlockedPollution) {
		float pollutionOverMaximum = FMath::Min (gameMode->PollutionManager->GetPollution() - maxPollutionWithoutIssues, 0);
		//float pollution = FMath::Max (((FMath::Abs (educationUnderMinimum) / gameMode->GetPopulationManager ()->GetPopulationCount ()) * 100.0f) - maxPercentagePopulationWithoutEducation, 0.0f);
		totalPollutionHappiness += FMath::Clamp (factorWeights[PollutionWeight] + (0.11f * pollutionOverMaximum), -factorWeights[PollutionWeight], factorWeights[PollutionWeight]);
		UE_LOG (LogTemp, Warning, TEXT ("Pollution over maximum: %f"), pollutionOverMaximum);
	} else unlockedPollution = gameMode->GetBuildingUnlockedByMilestone (FName ("SmallFactory"));

	return totalPollutionHappiness;
}


float UHappinessManager::GetFoodAndGoodsHappiness () {
	float totalFoodAndGoodsHappiness = 0.0f;
	int totalStoreCoverage = gameMode->GetVisitorCapacityByType (EBuildingType::EB_GROCERYSTORE) + gameMode->GetVisitorCapacityByType (EBuildingType::EB_MARKETPLACE) + gameMode->GetVisitorCapacityByType(EBuildingType::EB_SUPERMARKETSMALL) + gameMode->GetVisitorCapacityByType(EBuildingType::EB_SUPERMARKETBIG);

	if (unlockedGroceryStore) {
		float foodAndGoodsUnderMinimum = FMath::Min (totalStoreCoverage - gameMode->GetPopulationManager ()->GetPopulationCount (), 0);
		float foodAndGoodsCoverageUnderCap = FMath::Max (((FMath::Abs (foodAndGoodsUnderMinimum) / gameMode->GetPopulationManager ()->GetPopulationCount ()) * 100.0f) - maxPercentagePopulationWithoutFood, 0.0f);
		//UE_LOG (LogTemp, Warning, TEXT ("[HappinessManager, line 125] food coverage under cap: %f"), foodAndGoodsCoverageUnderCap);
		totalFoodAndGoodsHappiness += FMath::Clamp (factorWeights[FoodWeight] - (0.1f * foodAndGoodsCoverageUnderCap), -factorWeights[FoodWeight], factorWeights[FoodWeight]);
	} else unlockedGroceryStore = gameMode->GetBuildingUnlockedByMilestone (FName ("GroceryStore"));	

	return totalFoodAndGoodsHappiness;
}

//Add a percentage of happiness to the current value
//- pAmountPercentage = a value in the range of 0 - 100 (percentage)
void UHappinessManager::AddHappiness (float pAmountPercentage) {
	happiness += (FMath::Clamp (pAmountPercentage, 0.0f, 100.0f) * INTERNAL_HAPPINESS_MULTIPLIER);
}

int UHappinessManager::GetHappinessPercentage () {
	//Part divided by whole. Then times 100 to get a proper percentage.
	return ((float)happiness / MAX_HAPPINESS) * 100.0f;
}