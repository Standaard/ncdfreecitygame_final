// Fill out your copyright notice in the Description page of Project Settings.

#include "BuildableUtility.h"
#include "BuildableRoad.h"
#include "WorldCollision.h"
#include "Engine/StaticMeshActor.h"
#include "BuildableBuilding.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "NCD_PlayerController.h"
#include "PlacedUtility.h"
#include <DrawDebugHelpers.h>
#include "PlacedBuilding.h"

void ABuildableUtility::CheckSpecialCollision (FVector pSnappedLocation) {
	//FVector BoxSize = DimensionsInMeters * 50.f;
	//BoxSize = BoxSize.GetAbs ();
	//FVector directionVector = FRotator (0, Rotation, 0).Vector ();
	///*	Building location center = pSnappedLocation - RotatedSpawnOffset.
	//*	From the center, add half of the depth (BoxSize.X) multiplied by the direction to get the front extent x.
	//*	Then check one tile further.
	//*/
	//FrontLocation = (pSnappedLocation - RotatedSpawnOffset) + (directionVector * (BoxSize.X/* / 2*/)) + (directionVector * (Grid::GridSize / 2.0f));
	////FrontLocation -= (directionVector * 50.0f);
	//// DrawDebugSphere(GetWorld(), FrontLocation, 50, 16, FColor::White);
	//if (bDrawDebugDirection) {
	//	DrawDebugBox (GetWorld (), FrontLocation, FVector (50, 50, 50), FColor::White, false, -1.0f, 0, 5.0f);
	//	DrawDebugBox (GetWorld (), pSnappedLocation - RotatedSpawnOffset, BoxSize, directionVector.ToOrientationQuat (), FColor::Blue, false, -1.0f, 0, 5.0f);
	//}
	//bool hasRoadInFront = ContainsBuildableActor (FrontLocation, ABuildableRoad::StaticClass (), true);
	//if (!hasRoadInFront) {
	//	bAllowedToBuild = false;
	//	for (int32 i = 0; i < PreviewMesh->GetNumMaterials (); i++) {
	//		//PreviewMesh->SetMaterial (i, PreviewErrorMaterial);
	//		CreateDynamicPreviewMaterial (i, false);
	//	}
	//}
}

void ABuildableUtility::CheckPlacementCollisionActor (FVector pCheckLocation, FCollisionShape pBoxShape) {
	TArray<FHitResult> HitResults;
	FComponentQueryParams params;
	HitResultImpact = FVector::ZeroVector;
	FHitResult hitResult;
	//Perform sweep to see if there are buildings or meshes in the way
	//GetWorld ()->SweepMultiByChannel (HitResults, pCheckLocation, pCheckLocation, FQuat::Identity, ECC_Visibility, pBoxShape, params);

	PlayerController->GetHitResultUnderCursor (ECC_GameTraceChannel4, false, hitResult);
	if (!hitResult.bBlockingHit || hitResult.Actor->IsA (APlacedUtility::StaticClass ())) {
		bAllowedToBuild = false;
		return;
	}

	PlayerController->GetHitResultUnderCursor (ECC_Visibility, false, hitResult);
	if (hitResult.Actor->IsA (APlacedRoad::StaticClass ())) {
		APlacedRoad* placedRoad = Cast<APlacedRoad>(hitResult.Actor);
		if (!placedRoad) return;
		FTransform transform = placedRoad->GetMeshComponent()->GetComponentTransform();
		if (!FMath::IsNearlyEqual((double)transform.GetRotation().Euler().Z, (double)Rotation, 0.005) && !FMath::IsNearlyEqual ((double)transform.GetRotation ().Euler ().Z + 180.0, (double)Rotation, 0.005)) {
			bAllowedToBuild = false;
			return;
		}
		HitResultImpact = Grid::GetLocationOnGridXY(hitResult.ImpactPoint);
	} else {
		bAllowedToBuild = false;
		return;
	}
}

FVector ABuildableUtility::OnBuildableActorPlace(FTransform pTransform)
{
	pTransform.SetLocation(HitResultImpact);
	Super::OnBuildableActorPlace(pTransform);
	return HitResultImpact;
}
