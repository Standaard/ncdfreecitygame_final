// Fill out your copyright notice in the Description page of Project Settings.

#include "TrafficAgent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"
#include "RoadNode.h"
#include "TrafficSystem.h"
#include "CitySystemGameMode.h"

UTrafficAgent::UTrafficAgent()
{
}

bool UTrafficAgent::Initialize()
{
	if (!StartNode || !EndNode || Path.Num() == 0 || !TrafficSystem) return false;

	
	BoundingBoxExtentX = AgentData.Mesh->GetBounds().BoxExtent.X + AgentData.LineTraceOffsetFromExtent;
	CurrentNode = Path[0];
	NextNode = Path[1];
	PathIndex = 0;
	
	bIsActive = true;
	bIsRotating = false;

	Transform.SetLocation(StartNode->GetLocation());
	Transform.SetRotation(UKismetMathLibrary::FindLookAtRotation(CurrentNode->GetLocation(), NextNode->GetLocation()).Quaternion());
	DrawTransform = Transform;
	DrawTransform.AddToTranslation(DrawTransform.GetRotation().GetRightVector() * AgentData.HorizontalLocationOffset);
	return true;
}



void UTrafficAgent::CheckCollision(FName pCollisionProfile, const UWorld * pWorld)
{
	if (!pWorld) return;
	FCollisionQueryParams collisionParams;
	collisionParams.bTraceComplex = false;
	FHitResult hit;
	FVector forward = Transform.GetRotation().GetForwardVector();
	FVector right = Transform.GetRotation().GetRightVector();
	FVector start = DrawTransform.GetLocation() + (forward * BoundingBoxExtentX);
	FCollisionShape boxShape = FCollisionShape::MakeBox(FVector(AgentData.LineTraceLength,10.0f,5.0f));
	FVector end = start + (forward * AgentData.LineTraceLength);
	//set the center of the collision check in front of start
	FVector newLoc = start + ((end - start) / 2.0f);

	bDetectedVehicle = pWorld->SweepSingleByProfile(hit, newLoc, newLoc, (end - start).ToOrientationQuat(), pCollisionProfile, boxShape, collisionParams);
	if (!bDetectedVehicle) return;

	UHierarchicalInstancedStaticMeshComponent* hitComp = Cast<UHierarchicalInstancedStaticMeshComponent>(hit.GetComponent());
	if (hitComp)
	{
		UTrafficAgent* OtherAgent = TrafficSystem->GetVehicleFromComponent(hitComp, hit.Item);
		if (OtherAgent)
		{
			//Let this vehicle pass through the other one, if it detects this agent
			if (OtherAgent->bDetectedVehicle)
			{
				bDetectedVehicle = false;
				return;
			}
		}
	}
}

bool UTrafficAgent::Move(float DeltaTime, const UWorld* pWorld)
{
	if (!bIsActive || bDetectedVehicle) return false;
	FVector dirToNext = NextNode->GetLocation() - Transform.GetLocation();
	if (!bIsRotating && dirToNext.Size() <= PivotPoint) 
	{
		//increase except for the final time
		if (PathIndex < Path.Num()-1) 
		{
			FVector storedCurrent = CurrentNode->GetLocation();
			PathIndex++;
			CurrentNode = NextNode;
			NextNode = Path[PathIndex];
			if (!NextNode->bEnabled) return true;
			if (!CurrentNode->bEnabled) return true;

			//calculate if nextnode is left or right of the previous node
			FVector dirCurrentToNext = NextNode->GetLocation() - storedCurrent;
			dirCurrentToNext.Normalize();
			float dot = FVector::DotProduct(dirCurrentToNext, Transform.GetRotation().GetRightVector());
			//Is the next node straight? Do not rotate.
			if (FMath::IsNearlyZero(dot,0.1f))
			{
				//exit as rotating is not required
				return false;
			}
			bIsRight = dot > 0 ? true : false;
			bIsRotating = true;
			FIntVector pivotDirection;
			//get direction vector from entrance to center
			GetPivotPosition(dirToNext, bIsRight, pivotDirection);
			//whether you have to go left or right
			CurrentRotationIndex = RotationStartIndex;

			CurrentPivot = FVector(pivotDirection.X * PivotPoint, pivotDirection.Y * PivotPoint, Transform.GetLocation().Z);
			CurrentPivot += FVector(CurrentNode->GetLocation().X, CurrentNode->GetLocation().Y, 0);

		} else if (PathIndex == Path.Num() - 1)
		{
			bIsActive = false;
			if (!BuildingLocation.IsZero() && TrafficSystem->GetCitySystem())
			{
				TrafficSystem->GetCitySystem()->GetBuildingEventManager()->RemoveBuildingFromEventList(BuildingLocation);
			}
			return true;
		}
	}
	if (!bIsRotating)
	{
		dirToNext.Normalize();
		Transform.AddToTranslation(dirToNext * AgentData.TranslationSpeed * DeltaTime);
	}
	else
	{
		RotationDegreesPerTick = FGenericPlatformMath::CeilToInt(AgentData.TranslationSpeed * (bIsRight ? 2.0f : 1.0f) * DeltaTime);

		if (bIsRight)
		{
			//go clockwise
			//RotationDegreesPerTick = FGenericPlatformMath::CeilToInt(AgentData.ArcLengthSmall * AgentData.TranslationSpeed * DeltaTime);

			CurrentRotationIndex -= RotationDegreesPerTick;
			if (CurrentRotationIndex < RotationStartIndex - RotationDegrees)
			{
				CurrentRotationIndex = RotationStartIndex - RotationDegrees;
				bIsRotating = false;
			}
			Transform.SetLocation(TrafficSystem->CircleLUT[CurrentRotationIndex] + CurrentPivot);
			FRotator newRot = UKismetMathLibrary::FindLookAtRotation(Transform.GetLocation(), CurrentPivot);
			newRot.Yaw -= 90.0f;
			Transform.SetRotation(newRot.Quaternion());
		}
		else
		{
			//go counterclockwise
			//RotationDegreesPerTick = FGenericPlatformMath::CeilToInt(AgentData.ArcLengthLarge * AgentData.TranslationSpeed * DeltaTime);
			CurrentRotationIndex += RotationDegreesPerTick;
			if (CurrentRotationIndex > RotationStartIndex + RotationDegrees)
			{
				CurrentRotationIndex = RotationStartIndex + RotationDegrees;
				bIsRotating = false;
			}
			Transform.SetLocation(TrafficSystem->CircleLUT[CurrentRotationIndex] + CurrentPivot);
			FRotator newRot = UKismetMathLibrary::FindLookAtRotation(Transform.GetLocation(), CurrentPivot);
			newRot.Yaw += 90.0f;
			Transform.SetRotation(newRot.Quaternion());
		}
	}
	DrawTransform = Transform;
	DrawTransform.AddToTranslation(DrawTransform.GetRotation().GetRightVector() * AgentData.HorizontalLocationOffset);
	return false;
}
void UTrafficAgent::GetPivotPosition(FVector& pInDirection, bool pPivotIsRight, FIntVector& pUnitDirection)
{
	if (FMath::IsNearlyZero(pInDirection.X, 1 - 0.707f))
	{
		if (pInDirection.Y > 0)
		{
			//right
			if (pPivotIsRight)
			{
				//bottom left
				pUnitDirection = FIntVector(-1, -1, 0);
				//start rotation
				RotationStartIndex = 90;
			}
			else
			{
				//top left
				pUnitDirection = FIntVector(1, -1, 0);
				RotationStartIndex = 270;
			}
		}
		else {
			//left
			if (pPivotIsRight)
			{
				//top right
				pUnitDirection = FIntVector(1, 1, 0);
				RotationStartIndex = 270;

			}
			else
			{
				//bottom right
				pUnitDirection = FIntVector(-1, 1, 0);
				RotationStartIndex = 90;
			}
		}
	}
	else if (pInDirection.X > 0.707f)
	{
		//up
		if (pPivotIsRight)
		{
			//bottom right
			pUnitDirection = FIntVector(-1, 1, 0);
			RotationStartIndex = 180;
		}
		else
		{
			//bottom left
			pUnitDirection = FIntVector(-1, -1, 0);
			RotationStartIndex = 0;
		}
	}
	else if (pInDirection.X < 0.707f)
	{
		//down
		if (pPivotIsRight)
		{
			//top left
			pUnitDirection = FIntVector(1, -1, 0);
			RotationStartIndex = 360;
		}
		else
		{
			//top right
			pUnitDirection = FIntVector(1, 1, 0);
			RotationStartIndex = 180;
		}
	}
}

void UTrafficAgent::SetBuildingLocation(FVector pLocation)
{
	BuildingLocation = pLocation;
}

void UTrafficAgent::SetStartNode(URoadNode* pStartNode)
{
	StartNode = pStartNode;
	CurrentNode = pStartNode;
}

void UTrafficAgent::SetEndNode(URoadNode* pEndNode)
{
	EndNode = pEndNode;
}

void UTrafficAgent::SetTransform(const FTransform& pTransform)
{
	Transform = pTransform;
}

void UTrafficAgent::SetTrafficSystem(ATrafficSystem* pSystem)
{
	TrafficSystem = pSystem;
}

bool UTrafficAgent::GetDetectedVehicle()
{
	return bDetectedVehicle;
}
