// Fill out your copyright notice in the Description page of Project Settings.

#include "EventUI.h"

void UEventUI::SpawnEventWindow_Implementation(const FString& pTitle, const FString& pDescription, bool pAccepted, bool pIgnore, bool pDeclined, EQuestGiver pQuestGiver, FNCDEvent pEvent)
{

}

void UEventUI::OnEventAnswer(bool pAccept)
{
	if (!EventManager) return;

	pAccept ? EventManager->OnEventAccepted() : EventManager->OnEventDeclined();
}

void UEventUI::OnEventIgnore()
{
	if (!EventManager) return;

	EventManager->OnEventIgnore();
}

void UEventUI::SetEventManager(AEventManager* pManager)
{
	EventManager = pManager;
}

