// Fill out your copyright notice in the Description page of Project Settings.

#include "TrafficSystem.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "RoadNode.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Algo/Reverse.h"
#include "ArraySortByNodeCost.h"
#include <DrawDebugHelpers.h>
#include <Kismet/GameplayStatics.h>
#include "CitySystemGameMode.h"
#include "BuildableRoad.h"
#include "NCD_PlayerController.h"

const float ATrafficSystem::two_pi = 6.283185f;
const float ATrafficSystem::one_pi = 3.1415925f;
const float ATrafficSystem::pi_over_two = 1.5707963f;
// Sets default values
ATrafficSystem::ATrafficSystem()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void ATrafficSystem::Destroyed()
{
	AgentsByHISM.Empty();
	AgentDataMap.Empty();
	OpenSet.Empty();
	ClosedSet.Empty();
	CachedNodesSet.Empty();
	AgentQueue.Empty();
	AgentQueueHandle.Invalidate();

}

void ATrafficSystem::BeginPlay()
{
	Super::BeginPlay();
	if (GetWorld())
	{
		CitySystem = Cast<ACitySystemGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	}
	RoadPivotPoint = FMath::Abs(RoadPivotPoint);
	AgentPercentageUpdatedPerTick = FMath::Clamp(AgentPercentageUpdatedPerTick, 0.0f, 1.0f);
	DilatedVehicleSpawnDelay = VehicleSpawnDelay;
	if (!VehicleDataTable) UE_LOG(LogTemp, Error, TEXT("Could not find the VehicleDataTable! Assign it in BP_TrafficSystem!"));
	SetupLUT();
	LoadAgentDataFromTable();
}

void ATrafficSystem::LoadAgentDataFromTable()
{
	if (!VehicleDataTable) return;
	FString ContextString;
	TArray<FName> RowNames = VehicleDataTable->GetRowNames();
	for (auto& RowName : RowNames)
	{
		FTrafficAgentData* LoadedAgentData = VehicleDataTable->FindRow<FTrafficAgentData>(RowName, ContextString);
		if (LoadedAgentData) {
			LoadedAgentData->ID = RowName;
			AgentDataMap.Add(RowName, *LoadedAgentData);
			GetHISMComponentByAgentData(*LoadedAgentData);
		}
	}
}

void ATrafficSystem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//early out if the gamespeed is set to zero
	if (CitySystem->GetGameSpeed() == 0) return;

	bool bCanUpdateAgents = false;
	if (CurrentAgentNum > 0) bCanUpdateAgents = true;

	//Calculate the amount vehicles updated for this tick
	AgentsUpdatedThisTick = FGenericPlatformMath::CeilToInt(AgentPercentageUpdatedPerTick * (float)CurrentAgentNum);
	//Upper limit cannot exceed CurrentAgentNum
	if (CurrentUpperLimit == CurrentAgentNum) CurrentUpperLimit = AgentsUpdatedThisTick;
	CurrentUpperLimit += AgentsUpdatedThisTick;
	//Make sure the upper limit doesn't go above the max nr of agents
	if (CurrentUpperLimit > CurrentAgentNum) CurrentUpperLimit = CurrentAgentNum;
	//Set CurrentLowerLimit to UpperLimit minus amount of updated agents
	CurrentLowerLimit = CurrentUpperLimit - AgentsUpdatedThisTick - 1;
	//Clamp to zero
	if (CurrentLowerLimit < 0) CurrentLowerLimit = 0;
	//Reset counters
	CurrentAgentNum = 0;
	CurrentAgentIndex = 0;
	UWorld* world = GetWorld();

	for (auto& Entry : AgentsByHISM)
	{
		for (int32 i = 0; i < Entry.Agents.Num(); i++)
		{
			UTrafficAgent* agent = Entry.Agents[i];
			if (!agent || !agent->bIsActive) continue;
			bool canUpdateThisAgent = bCanUpdateAgents && CurrentAgentIndex >= CurrentLowerLimit && CurrentAgentIndex < CurrentUpperLimit && CurrentAgentIndex < MaxAgentNum;

			for (int32 i = 0; i < UpdatesPerAgentPerTick; i++)
			{
				//break if the agent became inactive in the previous iteration
				if (!agent->bIsActive) break;
				if (canUpdateThisAgent) {
					agent->CheckCollision(VehicleCollisionPresetName, world);
				}
				bool isInactive = agent->Move(DeltaTime, world);
				//Teleport the vehicle away if it's made inactive this update.
				if (isInactive)
				{
					agent->Transform.SetLocation(VehicleTeleportLocation);
					agent->DrawTransform.SetLocation(VehicleTeleportLocation);
				}
			}
			HISMComponents[Entry.HISMIndex]->UpdateInstanceTransform(i, agent->DrawTransform, true);
			if (!agent->bIsActive) continue;
			CurrentAgentIndex++;
			if (!agent->bSkippedCap)
			{
				CurrentAgentNum++;
			}
		}
	}
}



void ATrafficSystem::SetVehicleSpawnDelay(float pDelay)
{
	VehicleSpawnDelay = pDelay;
}

void ATrafficSystem::SpawnAgent(URoadNode* pStartNode, URoadNode* pEndNode, FName pVehicle, FVector pEndBuildingLocation, bool pOverrideCap)
{
	AgentQueue.Enqueue(FTrafficQueueAgent(pStartNode, pEndNode, pVehicle, pOverrideCap, pEndBuildingLocation));
	if (!AgentQueueHandle.IsValid())
	{
		GetWorldTimerManager().SetTimer(AgentQueueHandle, this, &ATrafficSystem::SpawnVehicleFromQueue, DilatedVehicleSpawnDelay / (float)UpdatesPerAgentPerTick, true, 1.0f);
	}
}

void ATrafficSystem::SpawnRandomAgentByType(APlacedBuilding* pStartBuilding, APlacedBuilding* pEndBuilding, EBuildingType pType, bool pSkipOverride /*= EBuildingType::EB_HOUSE*/)
{
	if (!CitySystem || !pStartBuilding || !pEndBuilding) return;
	URoadNode* startNode = CitySystem->BuildableRoadActor->GetNodeByLocation(pStartBuilding->BuildingStats.ConnectedRoadLocation);
	URoadNode* endNode = CitySystem->BuildableRoadActor->GetNodeByLocation(pEndBuilding->BuildingStats.ConnectedRoadLocation);
	if (!startNode || !endNode) return;
	SpawnRandomAgentByType(startNode, endNode, pType, pEndBuilding->BuildingStats.Location,pSkipOverride);
}

void ATrafficSystem::SpawnRandomAgentByType(FVector nodeLocationOne, FVector nodeLocationTwo, EBuildingType pType, FVector pEndBuildingLocation, bool pOverrideCap)
{
	if (!CitySystem || !CitySystem->BuildableRoadActor) return;
	SpawnRandomAgentByType(CitySystem->BuildableRoadActor->GetNodeByLocation(nodeLocationOne), CitySystem->BuildableRoadActor->GetNodeByLocation(nodeLocationTwo),pType, pEndBuildingLocation, pOverrideCap);
}

void ATrafficSystem::SpawnRandomAgentByType(URoadNode* pStartNode, URoadNode* pEndNode, EBuildingType pType /*= EBuildingType::EB_HOUSE*/, FVector pEndBuildingLocation /*= FVector::ZeroVector*/, bool pOverrideCap)
{
	if (!pStartNode || !pEndNode) {
		UE_LOG(LogTemp, Warning, TEXT("Could not spawn a random agent as a node was invalid."));
		return;
	}

	TArray<FName> StoredVehicles;
	for (auto& agentData : AgentDataMap)
	{
		for (auto& buildingType : agentData.Value.Types)
		{
			if (buildingType == pType)
			{
				StoredVehicles.Add(agentData.Value.ID);
			}
		}

	}
	if (StoredVehicles.Num() == 0) {
		UE_LOG(LogTemp, Warning, TEXT("Couldn't find a random vehicle from the Data Table."));
		return;
	}
	SpawnAgent(pStartNode, pEndNode, StoredVehicles[FMath::RandRange(0, StoredVehicles.Num() - 1)], pEndBuildingLocation,pOverrideCap);
}

UTrafficAgent* ATrafficSystem::GetVehicleFromComponent(UHierarchicalInstancedStaticMeshComponent* pHISMComp, int pVehicleIndex)
{
	int hismIndex;
	bool isFound = HISMComponents.Find(pHISMComp, hismIndex);
	if (!isFound) return nullptr;
	return AgentsByHISM[hismIndex].Agents[pVehicleIndex];
}

void ATrafficSystem::SpawnVehicleFromQueue()
{
	if (!GetWorld()) return;

	if (AgentQueue.IsEmpty())
	{
		AgentQueueHandle.Invalidate();
		return;
	}


	FTrafficQueueAgent queueAgent;
	bool agentFound = AgentQueue.Peek(queueAgent);
	if (!agentFound) return;

	AgentQueue.Dequeue(queueAgent);

	if (!queueAgent.bOverrideMaxAgent)
	{
		if (CurrentAgentNum >= MaxAgentNum)
		{
			AgentQueue.Enqueue(queueAgent);
			return;
		}
	}
	
	FCollisionShape boxShape = FCollisionShape::MakeBox(FVector(25, 25, 25));
	//Detect if there is a vehicle present on StartNode
	TArray<FHitResult> HitResult;
	bool hit = GetWorld()->SweepMultiByProfile(
		HitResult,
		queueAgent.StartNode->GetLocation(),
		queueAgent.StartNode->GetLocation() + FVector(100, 100, 100),
		FQuat::Identity,
		VehicleCollisionPresetName,
		boxShape
	);
	//if a vehicle is detected, add it to the end to the queue
	if (hit) {
		//re-insert the agent
		AgentQueue.Enqueue(queueAgent);
		return;
	}

	//obtain the hismComp based on the AgentData
	UHierarchicalInstancedStaticMeshComponent* hismComp = GetHISMComponentByAgentData(AgentDataMap[queueAgent.AgentName]);
	int hismIndex;
	//Retrieve the index of the hismComp
	if (!hismComp || !HISMComponents.Find(hismComp, hismIndex)) return;

	UTrafficAgent* agent = nullptr;

	TArray<UTrafficAgent*>& containerAgents = AgentsByHISM[hismIndex].Agents;
	bool found = false;
	//Attempt to obtain an inactive vehicle from the pool
	for (auto* Vehicle : containerAgents)
	{
		if (!Vehicle->bIsActive)
		{
			agent = Vehicle;
			found = true;
			break;
		}
	}
	if (!found) {
		//assign the constant agent values
		agent = NewObject<UTrafficAgent>(this);
		agent->AgentData = AgentDataMap[queueAgent.AgentName];
		agent->SetTrafficSystem(this);
		agent->PivotPoint = RoadPivotPoint;
	}
	agent->bSkippedCap = queueAgent.bOverrideMaxAgent;
	agent->SetBuildingLocation(queueAgent.BuildingLocation);
	agent->SetTransform(FTransform(queueAgent.StartNode->GetLocation()));
	agent->SetStartNode(queueAgent.StartNode);
	agent->SetEndNode(queueAgent.EndNode);
	if (GetPath(agent, agent->Path))
	{
		//if not found in the pool and is successfully initialized
		if (agent->Initialize())
		{
			if (!found)
			{
				hismComp->AddInstance(agent->Transform);
				containerAgents.Add(agent);
			}
		}
	}

}

UHierarchicalInstancedStaticMeshComponent* ATrafficSystem::GetHISMComponentByAgentData(FTrafficAgentData pAgentData)
{
	//Loop through all components to see if there is one for pMesh
	for (auto* HISMComp : HISMComponents)
	{
		//If so, do not create a new HISMComponent
		if (pAgentData.ID.IsEqual(HISMComp->GetFName()))
		{
			return HISMComp;
		}
	}
	UHierarchicalInstancedStaticMeshComponent* HISM = NewObject<UHierarchicalInstancedStaticMeshComponent>(this, pAgentData.ID);
	HISM->RegisterComponent();
	HISM->SetStaticMesh(pAgentData.Mesh);
	HISM->SetCollisionProfileName(VehicleCollisionPresetName);
	if (pAgentData.Materials.Num() > 0)
	{
		for (int32 i = 0; i < pAgentData.Materials.Num(); i++)
		{
			HISM->SetMaterial(i, pAgentData.Materials[i]);
		}
	}
	HISM->CastShadow = 0;
	AgentsByHISM.Add(FTrafficContainer(HISMComponents.Add(HISM)));
	return HISM;
}

bool ATrafficSystem::GetPath(UTrafficAgent* pAgent, TArray<URoadNode*> &pAgentPath)
{
	float currentToNeighbourDistance = 0.0f;
	float neighbourToEndDistance = 0.0f;
	bool endFound = false;

	URoadNode* StartNode = pAgent->StartNode;
	URoadNode* CurrentNode = pAgent->CurrentNode;
	URoadNode* EndNode = pAgent->EndNode;
	if (StartNode == EndNode) return false;
	pAgentPath.Empty();
	OpenSet.Empty();
	ClosedSet.Empty();
	CachedNodesSet.Empty();

	OpenSet.Push(CurrentNode);
	while (endFound == false && OpenSet.Num() > 0)
	{
		CurrentNode = OpenSet[0];
		OpenSet.Remove(CurrentNode);

		ClosedSet.Add(CurrentNode);

		if (CurrentNode == EndNode) {
			endFound = true;
			break;
		}

		for (URoadNode* Neighbour : CurrentNode->GetNeighbours()) {
			CachedNodesSet.Add(Neighbour);
			if (!Neighbour->bEnabled) continue;
			if (ClosedSet.Contains(Neighbour)) continue;
			else
			{
				//cache the distances
				currentToNeighbourDistance = CurrentNode->CalculateDistance(Neighbour);
				neighbourToEndDistance = Neighbour->CalculateDistance(EndNode);

				if (!OpenSet.Contains(Neighbour))
				{
					//Dijkstra
					Neighbour->CostCurrent = CurrentNode->CostCurrent + currentToNeighbourDistance;
					//Greedy
					Neighbour->CostEstimate = neighbourToEndDistance;

					//Set the parent of the node - used to backtrack later
					Neighbour->Parent = CurrentNode;
					OpenSet.Add(Neighbour);
				}
				else
				{
					//check if current cost + distance is cheaper than neighbour's cost
					if (CurrentNode->CostCurrent + currentToNeighbourDistance < Neighbour->CostCurrent)
					{
						//Update the costs
						//Dijkstra
						Neighbour->CostCurrent = CurrentNode->CostCurrent + currentToNeighbourDistance;
						//Greedy
						Neighbour->CostEstimate = neighbourToEndDistance;
						//Set the parent of the node - used to backtrack later
						Neighbour->Parent = CurrentNode;
					}
				}
			}

			// (one.costcurrent + one.costestimate) < (two.costCurrent + two.costestimate)
			//Sort the OpenSet by the Costs - next iteration = better node
			OpenSet.Sort(FArraySortByNodeCost());
		}
	}

	if (!endFound)
	{
		UE_LOG(LogTemp, Warning, TEXT("No path could be found!"));
		return false;
	}

	//path found, now add them backwards to the list
	URoadNode* reverseNode = EndNode;

	while (reverseNode->Parent)
	{
		pAgentPath.Add(reverseNode);
		reverseNode = reverseNode->Parent;
		if (reverseNode == StartNode)
		{
			pAgentPath.Add(reverseNode);
			break;
		}
	}
	Algo::Reverse(pAgentPath);

	for (URoadNode* Node : CachedNodesSet) {
		Node->CostCurrent = 0;
		Node->CostEstimate = 0;
	}
	return true;

}

void ATrafficSystem::SetUpdatesPerAgentPerTick(int pAmount)
{
	UpdatesPerAgentPerTick = pAmount;
	DilatedVehicleSpawnDelay = VehicleSpawnDelay / (float)pAmount;
	AgentQueueHandle.Invalidate();
	GetWorldTimerManager().SetTimer(AgentQueueHandle, this, &ATrafficSystem::SpawnVehicleFromQueue, DilatedVehicleSpawnDelay / (float)UpdatesPerAgentPerTick, true, 1.0f);
}

void ATrafficSystem::SetupLUT()
{
	CircleLUT.Empty();
	float section = two_pi / 360.0f;
	for (int i = 0; i <= 360; i++)
	{
		CircleLUT.Add(FVector(FMath::Sin(section*i), FMath::Cos(section*i), 0) * RoadPivotPoint);
	}
}
