// Fill out your copyright notice in the Description page of Project Settings.

#include "PollutionAreaDecal.h"
#include "Grid.h"
#include <Kismet/KismetMathLibrary.h>

UPollutionAreaDecal::UPollutionAreaDecal(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) 
{
	//SetComponentTickEnabled(false);
	SetHiddenInGame(true);
	SetWorldRotation(FRotator(-90.0f, 0.0f, 0.0f));
	bDestroyOwnerAfterFade = false;
	DecalSize = NormalizedPollutionDecalSize * Grid::GridSize;
	bTickInEditor = false;
}

void UPollutionAreaDecal::SetPollutionSize(FBuildingStats& pBuildingStats)
{
	//turn the extent into the size by multiplying by 2
	FVector polExtent = pBuildingStats.PollutionExtent;
	polExtent *= 2.0f;
	DecalSize = FVector(Grid::GridSize, polExtent.Y, polExtent.X) * NormalizedPollutionDecalSize;
}

void UPollutionAreaDecal::SetPollutionColorFromStats(FBuildingStats& pBuildingStats, float pMinPollution, float pMaxPollution)
{
	//Clamp the pollution to be within the range
	SetPollutionColor(pBuildingStats.PollutionInArea, pMinPollution, pMaxPollution);
}

void UPollutionAreaDecal::SetPollutionColor(int pPollution, float pMinPollution, float pMaxPollution)
{
	//Clamp the pollution to be within the range
	const float clampedPollution = FMath::Clamp((float)pPollution, pMinPollution, pMaxPollution);
	const float rangeClampedScalar = UKismetMathLibrary::MapRangeClamped(clampedPollution, pMinPollution, pMaxPollution, 0.0f, 1.0f);
	PollutionDynamicMaterialInstance = CreateDynamicMaterialInstance();
	PollutionDynamicMaterialInstance->SetScalarParameterValue(PollutionColorScalar, rangeClampedScalar);
}