// Fill out your copyright notice in the Description page of Project Settings.

#include "public/JsonExporter.h"
#include "public/ScenarioExporter.h"
#include "Runtime/JsonUtilities/Public/JsonUtilities.h"
#include "PrettyJsonPrintPolicy.h"
#include "EngineUtils.h"
#include "Engine/StaticMeshActor.h"
#include "BuildingStats.h"
#include "BuildableActor.h"

UJsonExport::UJsonExport () {

}

UJsonExport::~UJsonExport () {

}

void UJsonExport::Initialize (AScenarioExporter* pScenarioExporter, FString pAssetPath) {
	scenarioExporter = pScenarioExporter;
	assetPath = pAssetPath;
}

void UJsonExport::ExportBaseJSON (FString pBuildingsDTName, FString pEventsDTName, FString pMilestoneDTName, FString pPoliciesDTName, FString pVehiclesDTName, FString pGoalDTName, FInitialValues pInitialValues) {
	UE_LOG (LogTemp, Warning, TEXT ("Exporting base json..."));

	FString JsonFilePath = FPaths::ProjectContentDir () + assetPath + "/JSON/" + "Base" + ".json";
	FString outputString = "";

	TSharedPtr<FJsonObject> JsonObject = MakeShareable (new FJsonObject ());
	TSharedRef<TJsonWriter<>> jsonWriter = TJsonWriterFactory<>::Create (&outputString);

	jsonWriter->WriteObjectStart ();

	jsonWriter->WriteObjectStart ("ScenarioData");
	jsonWriter->WriteValue ("Name", ScenarioName);
	jsonWriter->WriteValue ("MapModelName", MapModelName);
	jsonWriter->WriteObjectEnd ();

	jsonWriter->WriteObjectStart ("PathNames");
	jsonWriter->WriteValue ("DatatableFolder", "Datatables/");
	jsonWriter->WriteValue ("ModelFolder", "Models/");
	jsonWriter->WriteValue ("TextureFolder", "Textures/");
	jsonWriter->WriteValue ("JSONFolder", "JSONS/");
	jsonWriter->WriteValue ("MaterialFolder", "Materials/");
	jsonWriter->WriteValue ("BuildingsDatatableName", pBuildingsDTName);
	jsonWriter->WriteValue ("EventsDatatableName", pEventsDTName);
	jsonWriter->WriteValue ("MilestonesDatatableName", pMilestoneDTName);
	jsonWriter->WriteValue ("PoliciesDatatableName", pPoliciesDTName);
	jsonWriter->WriteValue ("VehiclesDatatableName", pVehiclesDTName);
	jsonWriter->WriteValue ("GoalsDatatableName", pGoalDTName);
	jsonWriter->WriteObjectEnd ();

	jsonWriter->WriteObjectStart ("InitialValues");
	jsonWriter->WriteValue ("InitialPollution", pInitialValues.InitialPollution);
	jsonWriter->WriteValue ("InitialHappiness", pInitialValues.InitialHappiness);
	jsonWriter->WriteValue ("InitialPopulation", pInitialValues.InitialPopulation);
	jsonWriter->WriteValue ("InitialEmploymentRate", pInitialValues.InitialEmploymentRate);
	jsonWriter->WriteObjectEnd ();

	jsonWriter->WriteObjectStart ("Goals");
	jsonWriter->WriteObjectEnd ();

	jsonWriter->WriteObjectEnd ();
	jsonWriter->Close ();

	//Writing the string to an actual JSON file
	FFileHelper fileHelper = FFileHelper ();
	const TCHAR* filePath = *JsonFilePath;
	fileHelper.SaveStringToFile (outputString, filePath, FFileHelper::EEncodingOptions::AutoDetect);

	UE_LOG (LogTemp, Warning, TEXT ("Exporting base json completed!"));

	ExportSceneJSON ();
}

//This method basically does the same for ABuildableActors and AStaticMeshActors. Clean up if there is time!
void UJsonExport::ExportSceneJSON () {
	UE_LOG (LogTemp, Warning, TEXT ("Exporting scene json..."));

	const FString JsonFilePath = FPaths::ProjectContentDir () + assetPath + "/JSON/" + "Scene" + ".json";
	FString outputString = "";

	TSharedPtr<FJsonObject> JsonObject = MakeShareable (new FJsonObject ());
	TSharedRef<TJsonWriter<>> jsonWriter = TJsonWriterFactory<>::Create (&outputString);

	FString lastActorName = "";
	int iteration = 0;
	bool isFirstObject = true;

	jsonWriter->WriteObjectStart ();

	//Grabbing all actors in the current scene
	for (TActorIterator<AStaticMeshActor> ActorItr (scenarioExporter->GetWorld ()); ActorItr; ++ActorItr) {
		//AStaticMeshActor *Mesh = *ActorItr;
		//UE_LOG (LogTemp, Warning, TEXT ("Itr name: %s"), *ActorItr->GetName ());
		UStaticMeshComponent* meshComp = ActorItr->GetStaticMeshComponent ();
		if (meshComp->GetStaticMesh ()) {
			//UE_LOG (LogTemp, Warning, TEXT ("Itr mesh name: %s"), *meshComp->GetStaticMesh ()->GetName ());
			//UE_LOG (LogTemp, Warning, TEXT ("Itr location: %s"), *ActorItr->GetActorLocation ().ToString ());

			FString meshName = meshComp->GetStaticMesh ()->GetName ();

			//Don't export the planes.
			if (meshName.Contains ("Plane")) continue; 
			if (!differingActors.Contains (meshName)) {
				TArray<AStaticMeshActor*> actors;
				actors.Add (*ActorItr);
				differingActors.Add (meshName, actors);
			} else {
				differingActors[meshName].Add (*ActorItr);
			}
		}
	}

	//Iterate over the static mesh actors
	for (TPair<FString, TArray<AStaticMeshActor*>>& actors : differingActors) {
		//Write the start of a JSON block
		jsonWriter->WriteObjectStart (actors.Key);
		for (int i = 0; i < actors.Value.Num (); i++) {
			jsonWriter->WriteObjectStart (actors.Value[i]->GetName ());
			jsonWriter->WriteValue ("ActorName", actors.Value[i]->GetName ());

			//Convert the position to a better format for importing purposes
			FVector actorLocation = actors.Value[i]->GetActorLocation ();
			FVector actorRotation = actors.Value[i]->GetActorRotation ().Euler();
			FVector actorScale = actors.Value[i]->GetActorScale ();
			FString locationString = FString::SanitizeFloat (actorLocation.X) + "," + FString::SanitizeFloat (actorLocation.Y) + "," + FString::SanitizeFloat (actorLocation.Z);
			FString rotationString = FString::SanitizeFloat (actorRotation.X) + "," + FString::SanitizeFloat (actorRotation.Y) + "," + FString::SanitizeFloat (actorRotation.Z);
			FString scaleString = FString::SanitizeFloat (actorScale.X) + "," + FString::SanitizeFloat (actorScale.Y) + "," + FString::SanitizeFloat (actorScale.Z);
			jsonWriter->WriteValue ("ActorPosition", locationString);
			jsonWriter->WriteValue ("ActorRotation", rotationString);
			jsonWriter->WriteValue ("ActorScale", scaleString);
			jsonWriter->WriteValue ("ID", GetBuildingStatsRowName (actors.Key));
			jsonWriter->WriteObjectEnd ();
		}
		jsonWriter->WriteObjectEnd ();
	}

	jsonWriter->WriteObjectEnd ();
	jsonWriter->Close ();

	//Clear map in case of another export so we don't add non-existant buildings to the JSON
	differingActors.Empty ();
	differingBActors.Empty ();

	//Writing the string to an actual JSON file
	FFileHelper fileHelper = FFileHelper ();
	const TCHAR* filePath = *JsonFilePath;
	fileHelper.SaveStringToFile (outputString, filePath, FFileHelper::EEncodingOptions::AutoDetect);

	UE_LOG (LogTemp, Warning, TEXT ("Exporting scene json completed!"));
}

FString UJsonExport::GetBuildingStatsRowName (FString pModelName) {
	FString filePathString = "/Game/Assets/Datatables/DT_BuildingStats";
	const TCHAR* filePath = *filePathString;
	UDataTable* datatable = LoadObject<UDataTable> (nullptr, filePath);

	if (datatable) {
		FString ContextString;
		TArray<FName> RowNames = datatable->GetRowNames ();
		for (auto& RowName : RowNames) {
			FBuildingStats* Building = datatable->FindRow<FBuildingStats> (RowName, ContextString);
			if (Building->StaticMesh->GetName () == pModelName) {
				return RowName.ToString();
			}
		}
	}
	return "";
}