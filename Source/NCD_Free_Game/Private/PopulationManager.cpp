﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "PopulationManager.h"
#include "CitySystemGameMode.h"
#include "BuildableRoad.h"
#include "NCD_PlayerController.h"
#include "TrafficSystem.h"

UPopulationManager::UPopulationManager()
{

}

UPopulationManager::~UPopulationManager()
{

}

//Called from CitySystem; Used to add population to the city.
void UPopulationManager::AddPopulation(int pPopToAdd) {
	int TotalPopToAdd = pPopToAdd;
	while (TotalPopToAdd > 0) {
		if ((ResidencesWithAvailableSpace.Num () > 0) == 😄) { //If there are houses with space for new residents
			FVector CurrentResidencePos = ResidencesWithAvailableSpace[0];
			FBuildingStats* CurrentResidence = GameMode->GetBuildingByLocation (CurrentResidencePos);
			if (CurrentResidence) {
				bool wasEmpty = ResidencePopulationByLocation[CurrentResidencePos] == 0;
				//If the amount of people to add exceeds the amount of residential spots the selected residence has, reduce it to the amount of residential spots available.
				int NewPopToAdd = TotalPopToAdd > (CurrentResidence->VisitorCapacity - ResidencePopulationByLocation[CurrentResidencePos]) ?
					(CurrentResidence->VisitorCapacity - ResidencePopulationByLocation[CurrentResidencePos]) : TotalPopToAdd;
				//If people are already living in the currently selected house, simply add the new population to it, otherwise add it to the list with residences containing people.
				if (!ResidencePopulationByLocation.Contains (CurrentResidencePos)) ResidencePopulationByLocation.Add (CurrentResidencePos, NewPopToAdd);
				else ResidencePopulationByLocation[CurrentResidencePos] += NewPopToAdd;
				//If the max amount of people for this residence has been reached, remove it from the list with residences that have spots available.
				if (ResidencePopulationByLocation[CurrentResidencePos] == CurrentResidence->VisitorCapacity) RemoveResidenceWithAvailableSpace (CurrentResidencePos);
				//Add to the total population in the city and remove it from the total we're adding in this loop.
				TotalPopulation += NewPopToAdd;
				TotalPopToAdd -= NewPopToAdd;

				if (GameMode->BuildableRoadActor && wasEmpty) {
					GameMode->GetTrafficSystem ()->SpawnRandomAgentByType (
						GameMode->StartRoadLocation,
						CurrentResidence->ConnectedRoadLocation,
						EBuildingType::EB_HOUSE
					);
				}
			} else {
				UE_LOG (LogTemp, Error, TEXT ("[PopulationManager.cpp, line 42]: Why is the residence null? Positioning wrong?"));
				break;
			}
			//UE_LOG (LogTemp, Warning, TEXT ("TotalPopToAdd: %d, NewPopToAdd: %d, Population at location: %d"), TotalPopToAdd, NewPopToAdd, ResidencePopulationByLocation[CurrentResidencePos]);
		} else {
			//UE_LOG (LogTemp, Warning, TEXT ("No more residences to fit people in!"));
			return;
		}
	}
}

//Called from CitySystem; Used to remove people from the city.
void UPopulationManager::RemovePopulation (int pPopToRemove) {
	//If we are removing more people than there are in the city, take the amount of people currently in the city
	pPopToRemove = FMath::Min (pPopToRemove, TotalPopulation);

	int TotalPopToRemove = pPopToRemove;
	int index = 0;
	TArray<FVector> Residences = TArray<FVector> ();
	ResidencePopulationByLocation.GenerateKeyArray (Residences);
	while (TotalPopToRemove > 0) {
		if (index < ResidencePopulationByLocation.Num ()) {
			FVector CurrentResidencePos = Residences[index];

			//Iterate to the next residence as this one has no people left
			if (ResidencePopulationByLocation[CurrentResidencePos] <= 0) {
				index++;
				continue;
			}

			//Grab the residence we're iterating over and remove population from it. Also remove it from the "without available space" list so that it will be considered for new population.
			FBuildingStats CurrentResidence = *GameMode->GetBuildingByLocation (CurrentResidencePos);
			int NewPopToRemove = FMath::Abs (TotalPopToRemove) > ResidencePopulationByLocation[CurrentResidencePos] ? ResidencePopulationByLocation[CurrentResidencePos] : FMath::Abs (TotalPopToRemove); //Using Absolute value to make sure nobody passes in a negative value.
			if (!ResidencePopulationByLocation.Contains (CurrentResidencePos)) {
				UE_LOG (LogTemp, Warning, TEXT ("How did this happen? It should never loop over items not in the tmap!"));
			} else ResidencePopulationByLocation[CurrentResidencePos] -= FMath::Abs (NewPopToRemove);
			if (ResidencePopulationByLocation[CurrentResidencePos] <= CurrentResidence.VisitorCapacity) RemoveResidenceWithoutAvailableSpace (CurrentResidencePos);

			//Remove people from a random workplace
			RemoveEmployeesFromRandomWorkplace (NewPopToRemove - (TotalPopulation - EmployedPeople));

			//Grab the absolute values just to make sure we are subtracting, and not accidentally adding
			TotalPopulation -= FMath::Abs(NewPopToRemove);
			TotalPopToRemove -= FMath::Abs(NewPopToRemove);

			//Spawn vehicles
			if (ResidencePopulationByLocation.Contains (CurrentResidencePos)) {
				if (GameMode->BuildableRoadActor && ResidencePopulationByLocation[CurrentResidencePos] == 0) {
					GameMode->GetTrafficSystem()->SpawnRandomAgentByType (
						CurrentResidence.ConnectedRoadLocation,
						GameMode->StartRoadLocation,
						EBuildingType::EB_HOUSE
					);
				}
			}

			index++;
		} else {
			if (TotalPopulation == 0) {
				//UE_LOG (LogTemp, Warning, TEXT ("No more people in city!"));
			} else {  }//UE_LOG (LogTemp, Warning, TEXT ("Removing people done!"));
			return;
		}
	}
}

void UPopulationManager::RemoveEmployeesFromRandomWorkplace (int pEmployeesToRemove) {
	TArray<FVector> Workplaces = TArray<FVector> ();
	EmployeesByLocation.GenerateKeyArray (Workplaces);
	int TotalToRemove = pEmployeesToRemove;
	while (TotalToRemove > 0) {
		//Grab a random workplace and get the amount of people currently working there.
		FVector RandomWorkplacePos = Workplaces[FMath::RandRange (0, Workplaces.Num () - 1)];
		FBuildingStats RandomWorkPlace = *GameMode->GetBuildingByLocation (RandomWorkplacePos);
		int EmployeesAtLocation = EmployeesByLocation[RandomWorkplacePos];

		//Nobody's working there, onto the next!
		if (EmployeesAtLocation == 0) {
			Workplaces.Remove (RandomWorkplacePos);
			return; 
		}

		//Check whether we are trying to remove more people than are working in the workplace.
		int NewEmployeesToRemove = TotalToRemove > EmployeesAtLocation ? EmployeesAtLocation : TotalToRemove;
		NewEmployeesToRemove = NewEmployeesToRemove > EmployedPeople ? EmployedPeople : NewEmployeesToRemove;
		RemoveEmployeesAtLocation (RandomWorkplacePos, RandomWorkPlace.BuildingCategory, NewEmployeesToRemove, false);
		TotalToRemove -= NewEmployeesToRemove;
	}
}

//0 pop = empty building because it got destroyed
void UPopulationManager::RemovePopulationAtLocation (FVector pResidencePos, int pBuildingCapacity, int pPopToRemove /* = 0 */, bool pDemolished /* = false */) {
	int popToRemove = pPopToRemove == 0 ? ResidencePopulationByLocation[pResidencePos] : pPopToRemove;
	ResidencePopulationByLocation[pResidencePos] -= popToRemove;
	//if (ResidencePopulationByLocation[pResidencePos] < GameMode->GetBuildingByLocation (pResidencePos)->VisitorCapacity) RemoveResidenceWithoutAvailableSpace (pResidencePos);
	if (pDemolished) RemoveBuildingData (pResidencePos, EBuildingCategory::EBC_RESIDENCE, pBuildingCapacity);
	TotalPopulation -= popToRemove;
}

//0 employees = empty building because it got destroyed
void UPopulationManager::RemoveEmployeesAtLocation(FVector pWorkplacePos, EBuildingCategory pCategory, int pEmployeesToRemove /* = 0 */, bool pDemolished /* = false */) {
	if (!EmployeesByLocation.Contains (pWorkplacePos)) return;
	int EmployeesToRemove = pEmployeesToRemove == 0 ? EmployeesByLocation[pWorkplacePos] : pEmployeesToRemove;
	EmployeesByLocation[pWorkplacePos] -= EmployeesToRemove;
	EmployedPeople -= EmployeesToRemove;
	if (pDemolished) RemoveBuildingData (pWorkplacePos, pCategory, EmployeesToRemove);
}

void UPopulationManager::RemoveBuildingData (FVector pBuildingPos, EBuildingCategory pCategory, int pPopulationCount) {
	if (pCategory == EBuildingCategory::EBC_RESIDENCE) {
		RemoveResidenceWithAvailableSpace (pBuildingPos, true);
		RemoveResidenceWithoutAvailableSpace (pBuildingPos, true);
		TotalResidentSpace -= pPopulationCount;
		RemoveAvailableResidence (pBuildingPos);
	} else {
		TotalEmployeeSpace -= pPopulationCount;
		TArray<FVector> Residences = TArray<FVector> ();
		ResidenceLocationsWithAvailablePop.GenerateKeyArray (Residences);
		AddAvailableResidenceValue (Residences, pPopulationCount);
		if (EmployeesByLocation.Contains (pBuildingPos)) EmployeesByLocation.Remove (pBuildingPos);
	}
}


int UPopulationManager::AssignPopToWorkPlace (FBuildingStats pBuilding, FBuildingStats pResidence, int pPopToAdd) {
	int PopAdded = 0;
	//Break when nothing is available
	if (ResidenceLocationsWithAvailablePop.Num () > 0) { 
		TArray<FVector> Residences = TArray<FVector> ();
		ResidenceLocationsWithAvailablePop.GenerateKeyArray (Residences);
		int TotalPopToAdd = pPopToAdd;
		int index = 0;
		while (TotalPopToAdd != 0) {
			if (index < Residences.Num ()) {
				FVector CurrentResidencePos = Residences[index];
				FBuildingStats CurrentResidence = *GameMode->GetBuildingByLocation (CurrentResidencePos);
				int NewPopToAdd = TotalPopToAdd > ResidenceLocationsWithAvailablePop[CurrentResidencePos] ? ResidenceLocationsWithAvailablePop[CurrentResidencePos] : TotalPopToAdd;
				if (NewPopToAdd > 0 && GetEmployeeCountByLocation (pBuilding.Location) == pBuilding.JobCapacity) {
					return 0;
				} else if (NewPopToAdd < 0 && GetEmployeeCountByLocation (pBuilding.Location) == 0) {
					return 0;
				}
				//Negative because if we add workers we want to subtract available people and vice versa
				AddAvailableResidenceValue (Residences, -NewPopToAdd);
				AddPopToLocation (pBuilding, CurrentResidence, NewPopToAdd);
				TotalPopToAdd -= NewPopToAdd;
				PopAdded += NewPopToAdd;
				index++;
			} else {
				//UE_LOG (LogTemp, Warning, TEXT ("No more residences to fill workplaces!"));
				break;
			}
		}
	}
	return PopAdded;
}

void UPopulationManager::AddPopToLocation (FBuildingStats pBuilding, FBuildingStats pResidence, int pPopToAdd) {
	if (pPopToAdd == 0) return;
	int oldEmployeesAtLocation = GetEmployeeCountByLocation (pBuilding.Location);
	AddEmployeesAtLocation (pResidence.Location, pBuilding.Location, pPopToAdd);

	if (pPopToAdd > 0) {
		//If the building is now operational, subscribe it to the income generation.
		if (IsOperational(pBuilding)) GameMode->AddBuildingIncome (pBuilding.Location, pBuilding.Income);
		//If the building now has at least 1 employee (where it previously had 0), it will generate expenses.
		if (oldEmployeesAtLocation == 0) GameMode->AddBuildingExpense (pBuilding.BuildingType, pBuilding.UpkeepCost.MoneyCost, 1);
		if (GameMode->BuildableRoadActor) {
			GameMode->GetTrafficSystem()->SpawnRandomAgentByType(
				pResidence.ConnectedRoadLocation,
				pBuilding.ConnectedRoadLocation,
				EBuildingType::EB_HOUSE
			);
		}
	} else {
		//If the amount of workers is now below 100% capacity, unsubscribe it from income generation.
		if (GetEmployeeCountByLocation (pBuilding.Location) < pBuilding.JobCapacity) GameMode->RemoveBuildingIncome(pBuilding.Location);
		//If the amount of employees lands below 1 (0 if everything's correct), remove it from the building expenses.
		if (GetEmployeeCountByLocation (pBuilding.Location) < 1) GameMode->AddBuildingExpense (pBuilding.BuildingType, pBuilding.UpkeepCost.MoneyCost, -1);
		if (GameMode->BuildableRoadActor) {
			GameMode->GetTrafficSystem()->SpawnRandomAgentByType(
				pBuilding.ConnectedRoadLocation,
				pResidence.ConnectedRoadLocation,
				EBuildingType::EB_HOUSE
			);
		}
	}
}

//Called in blueprint. Adds a newly built residence and readies it for filling.
void UPopulationManager::AddAvailableResidence (FVector pResidencePos, int pPopCapacity) {
	if (!ResidenceLocationsWithAvailablePop.Contains(pResidencePos)) ResidenceLocationsWithAvailablePop.Add (pResidencePos, pPopCapacity);
	if (!ResidencePopulationByLocation.Contains (pResidencePos)) ResidencePopulationByLocation.Add (pResidencePos, 0);
	if (!ResidencesWithAvailableSpace.Contains (pResidencePos)) ResidencesWithAvailableSpace.Add (pResidencePos);
	TotalResidentSpace += pPopCapacity;
}

void UPopulationManager::RemoveAvailableResidence (FVector pResidencePos) {
	if (ResidenceLocationsWithAvailablePop.Contains (pResidencePos)) ResidenceLocationsWithAvailablePop.Remove (pResidencePos);
	if (ResidencePopulationByLocation.Contains (pResidencePos)) ResidencePopulationByLocation.Remove (pResidencePos);
}

//Called from blueprint; Adds a newly built workplace and readies it for new workers.
void UPopulationManager::AddWorkplace (FVector pWorkplacePos) {
	if (!EmployeesByLocation.Contains (pWorkplacePos)) EmployeesByLocation.Add (pWorkplacePos, 0);
	TotalEmployeeSpace += GameMode->GetBuildingByLocation (pWorkplacePos)->JobCapacity;
}

TMap<FVector, int> UPopulationManager::GetAvailableResidenceLocations () {
	return ResidenceLocationsWithAvailablePop;
}

//Method used to add to/subtract from a residence's population spots.
void UPopulationManager::AddAvailableResidenceValue(TArray<FVector> pResidencePositions, int pValue) {
	int pTotalValue = pValue;
	int index = 0;
	while (pTotalValue != 0) {
		if (index < pResidencePositions.Num ()) {
			FVector ResidencePos = pResidencePositions[index];
			FBuildingStats Residence = *GameMode->GetBuildingByLocation (ResidencePos);
			if ((ResidenceLocationsWithAvailablePop[ResidencePos] == Residence.VisitorCapacity && pValue > 0) || (ResidenceLocationsWithAvailablePop[ResidencePos] == 0 && pValue < 0)) {
				index++;
				continue;
			} else {
				int ValueToAdd = pTotalValue > (Residence.VisitorCapacity - ResidenceLocationsWithAvailablePop[ResidencePos]) ?
					(Residence.VisitorCapacity - ResidenceLocationsWithAvailablePop[ResidencePos]) : pTotalValue;
				ResidenceLocationsWithAvailablePop[ResidencePos] += ValueToAdd;
				pTotalValue -= ValueToAdd;
				index++;
			}
		} else UE_LOG (LogTemp, Warning, TEXT ("Doc, how have we gotten this far? Index shouldn't be able to get above position num!"));
	}
}

void UPopulationManager::AddResidenceWithAvailableSpace (FVector pResidencePos, bool pRemoveFromOtherList) {
	if (!ResidencesWithAvailableSpace.Contains (pResidencePos)) {
		ResidencesWithAvailableSpace.Add (pResidencePos);
		return;
	}
	if (pRemoveFromOtherList) RemoveResidenceWithoutAvailableSpace (pResidencePos);
}

void UPopulationManager::RemoveResidenceWithAvailableSpace (FVector pResidencePos, bool pDemolished /* = false */) {
	if (ResidencesWithAvailableSpace.Contains (pResidencePos)) {
		ResidencesWithAvailableSpace.Remove (pResidencePos);
		return;
	}
	if (!pDemolished) AddResidenceWithoutAvailableSpace (pResidencePos, false);
}

void UPopulationManager::AddResidenceWithoutAvailableSpace (FVector pResidencePos, bool pRemoveFromOtherList) {
	if (!ResidencesWithoutAvailableSpace.Contains (pResidencePos)) {
		ResidencesWithoutAvailableSpace.Add (pResidencePos);
		return;
	}
	if (pRemoveFromOtherList) RemoveResidenceWithAvailableSpace (pResidencePos);
}

void UPopulationManager::RemoveResidenceWithoutAvailableSpace (FVector pResidencePos, bool pDemolished /* = false */) {
	if (ResidencesWithoutAvailableSpace.Contains (pResidencePos)) {
		ResidencesWithoutAvailableSpace.Remove (pResidencePos);
		return;
	}
	if (!pDemolished) AddResidenceWithAvailableSpace (pResidencePos, false);
}

void UPopulationManager::AddEmployeesAtLocation(FVector pResidencePos, FVector pWorkPos, int pEmployeesToAdd) {
	if (EmployeesByLocation.Contains (pWorkPos)) {
		EmployeesByLocation[pWorkPos] += pEmployeesToAdd; //It's safe to just add, because the check already happens in CitySystemGameMode's AssignPopulationToWorkPlaceNew to see whether the company is full
		EmployedPeople += pEmployeesToAdd;
	} else UE_LOG (LogTemp, Warning, TEXT ("Why did this workplace not get added to the tmap?"));
}

int UPopulationManager::GetPopulationCount() {
	return TotalPopulation;
}

int UPopulationManager::GetEmployedPeopleCount () {
	return EmployedPeople;
}

int UPopulationManager::GetUnemployedPeopleCount() {
	return TotalPopulation - EmployedPeople;
}

void UPopulationManager::SetPopulationCountBeginningOfMonth() {
	PopulationBeginningMonth = TotalPopulation;
}

void UPopulationManager::SetPopulationCountBeginningOfMonth(int pPopulation) {
	PopulationBeginningMonth = pPopulation;
}

int UPopulationManager::GetPopulationMonth() {
	return PopulationBeginningMonth;
}

bool UPopulationManager::IsPopulationZero() {
	return TotalPopulation == 0;
}

void UPopulationManager::SetGameMode (ACitySystemGameMode* pGameMode) {
	GameMode = pGameMode;
}

int UPopulationManager::GetTotalResidentSpace() {
	return TotalResidentSpace;
}

int UPopulationManager::GetTotalEmployeeSpace () {
	return TotalEmployeeSpace;
}

int UPopulationManager::GetAvailableResidenceSpacesForLocation (FVector pLocation) {
	return GameMode->GetBuildingByLocation (pLocation)->VisitorCapacity - ResidencePopulationByLocation[pLocation];
}

int UPopulationManager::GetAvailableJobSpacesForLocation(FVector pLocation) {
	return GameMode->GetBuildingByLocation (pLocation)->JobCapacity - EmployeesByLocation[pLocation];
}

int UPopulationManager::GetEmployeeCountByLocation(FVector pLocation) {
	if (EmployeesByLocation.Contains(pLocation)) return EmployeesByLocation[pLocation];
	else return 0;
}

int UPopulationManager::GetResidenceCountByLocation(FVector pLocation) {
	if (ResidencePopulationByLocation.Contains(pLocation)) return ResidencePopulationByLocation[pLocation];
	else return 0;
}

int UPopulationManager::GetUnemploymentRatePercentage () {
	//We subtract from 100 here because we want the UNemployed people, not the employed ones.
	//Standard unemployment rate of 100%, because otherwise happiness would continuously bounce between 30 and 50
	return IsPopulationZero() ? 100 : 100 - (((float)EmployedPeople / (float)TotalPopulation) * 100); 
}

int UPopulationManager::GetMaxUnemploymentRate () {
	return MaxUnemploymentRatePercentage;
}

bool UPopulationManager::IsOperational (FBuildingStats pBuilding) {
	if (!EmployeesByLocation.Contains (pBuilding.Location)) return false;
	else return EmployeesByLocation[pBuilding.Location] == pBuilding.JobCapacity;
}

bool UPopulationManager::UnemploymentRateTooHigh () {
	return GetUnemploymentRatePercentage () > MaxUnemploymentRatePercentage;
}

//TODO:: Fix all the import and export stuff related to population; Related to save games.
void UPopulationManager::Import (FPopulationExport pExport) {

	//ResidenceWorkPopulation = pExport.PopulationResidenceWork;

	//ResidencePopulation = pExport.PopulationResidence;
	//for (auto& ResidenceBuilding : pExport.PopulationResidence)
	//{
	//	Population += ResidenceBuilding.Value;
	//}

	//WorkPlacePopulation = pExport.PopulationJobs;
	//for (auto& JobBuilding : pExport.PopulationJobs) 
	//{
	//	PopulationWithJob += JobBuilding.Value;
	//}

	//WorkCapacities = pExport.WorkCapacities;
	//MaxPopulationCapacity = pExport.MaxPopulationCapacity;
	//ResidenceWorkPercentage = pExport.ResidenceWorkPercentage;
	//LeavingPeople = pExport.LeavingPeople;
	//PopulationBeginningMonth = pExport.PopulationMonth;
}

FPopulationExport UPopulationManager::Export () {
	//return FPopulationExport(ResidencePopulation, WorkPlacePopulation, ResidenceWorkPopulation, 
	//							PopulationBeginningMonth, LeavingPeople, ResidenceWorkPercentage, MaxPopulationCapacity, WorkCapacities);
	return FPopulationExport ();
}