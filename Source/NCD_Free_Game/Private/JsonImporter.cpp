// Fill out your copyright notice in the Description page of Project Settings.

#include "JsonImporter.h"
#include "ScenarioImporter.h"
#include "PlaceableObject.h"
#include "Runtime/JsonUtilities/Public/JsonUtilities.h"
#include "Engine/World.h"
#include "BuildableActor.h"
#include "BuildableRoad.h"
#include "BuildableBuilding.h"
#include "Kismet/GameplayStatics.h"
#include "NCD_PlayerController.h"

UJsonImporter::UJsonImporter () {

}

UJsonImporter::~UJsonImporter () {

}

void UJsonImporter::Initialize (AScenarioImporter* pScenarioImporter) {
	scenarioImporter = pScenarioImporter;
}

bool UJsonImporter::ReadBaseJSON (FString pRelativePath) {
	const FString JsonFilePath = FPaths::ProjectContentDir () + "Assets/JSON/" + pRelativePath + ".json";
	FString JsonString;

	FFileHelper::LoadFileToString (JsonString, *JsonFilePath);
	TSharedPtr<FJsonObject> JsonObject = MakeShareable (new FJsonObject ());
	TSharedRef<TJsonReader<>> JsonReader = TJsonReaderFactory<>::Create (JsonString);

	if (FJsonSerializer::Deserialize (JsonReader, JsonObject) && JsonObject.IsValid ()) {
		UE_LOG (LogTemp, Warning, TEXT ("Reading base json..."));

		//The object that's retrieved from the JSON
		TSharedPtr<FJsonObject> ScenarioDataObject = JsonObject->GetObjectField ("ScenarioData");
		TSharedPtr<FJsonObject> PathsObject = JsonObject->GetObjectField ("PathNames");
		TSharedPtr<FJsonObject> InitialValuesObject = JsonObject->GetObjectField ("InitialValues");
		TSharedPtr<FJsonObject> Goals = JsonObject->GetObjectField ("Goals");
		/**/
		//Data properties
		UE_LOG (LogTemp, Warning, TEXT ("Name: %s"), *ScenarioDataObject->GetStringField ("Name"));
		UE_LOG (LogTemp, Warning, TEXT ("Map model name: %s"), *ScenarioDataObject->GetStringField ("MapModelName"));

		//Paths
		UE_LOG (LogTemp, Warning, TEXT ("Datatable folder: %s"), *PathsObject->GetStringField ("DatatableFolder"));
		UE_LOG (LogTemp, Warning, TEXT ("Model folder: %s"), *PathsObject->GetStringField ("ModelFolder"));
		UE_LOG (LogTemp, Warning, TEXT ("Texture folder: %s"), *PathsObject->GetStringField ("TextureFolder"));
		UE_LOG (LogTemp, Warning, TEXT ("JSON folder: %s"), *PathsObject->GetStringField ("JSONFolder"));
		UE_LOG (LogTemp, Warning, TEXT ("Material folder: %s"), *PathsObject->GetStringField ("MaterialFolder"));
		UE_LOG (LogTemp, Warning, TEXT ("Buildings datatable: %s"), *PathsObject->GetStringField ("BuildingsDatatableName"));
		UE_LOG (LogTemp, Warning, TEXT ("Events datatable: %s"), *PathsObject->GetStringField ("EventsDatatableName"));
		UE_LOG (LogTemp, Warning, TEXT ("Milestones datatable: %s"), *PathsObject->GetStringField ("MilestonesDatatableName"));
		UE_LOG (LogTemp, Warning, TEXT ("Policies datatable: %s"), *PathsObject->GetStringField ("PoliciesDatatableName"));
		UE_LOG (LogTemp, Warning, TEXT ("Vehicles datatable: %s"), *PathsObject->GetStringField ("VehiclesDatatableName"));

		DatatableNames = FDatatables (*PathsObject->GetStringField ("BuildingsDatatableName"), *PathsObject->GetStringField ("EventsDatatableName"), *PathsObject->GetStringField ("MilestonesDatatableName"), 
														  *PathsObject->GetStringField ("PoliciesDatatableName"), *PathsObject->GetStringField ("VehiclesDatatableName"), *PathsObject->GetStringField("GoalsDatatableName"));

		//Initial values
		UE_LOG (LogTemp, Warning, TEXT ("Pollution: %d"), InitialValuesObject->GetIntegerField ("InitialPollution"));
		UE_LOG (LogTemp, Warning, TEXT ("Happiness: %d"), InitialValuesObject->GetIntegerField ("InitialHappiness"));
		UE_LOG (LogTemp, Warning, TEXT ("Population: %d"), InitialValuesObject->GetIntegerField ("InitialPopulation"));
		UE_LOG (LogTemp, Warning, TEXT ("EmploymentRate: %d"), InitialValuesObject->GetIntegerField ("InitialEmploymentRate"));

		//Goals

		/**/
		UE_LOG (LogTemp, Warning, TEXT ("Reading base json finished!"));
		return true;
	} else return false;
}

bool UJsonImporter::ReadSceneJSON (FString pRelativePath) {
	UE_LOG (LogTemp, Warning, TEXT ("Reading scene json..."));

	//Read JSON model names
	//Create lists of models + objects/position/etc...
	//Figure out how to add actor to level/scene and set position and other properties
	const FString JsonFilePath = FPaths::ProjectContentDir () + "Assets/JSON/" + pRelativePath + ".json";
	FString JsonString;

	FFileHelper::LoadFileToString (JsonString, *JsonFilePath);
	TSharedPtr<FJsonObject> JsonObject = MakeShareable (new FJsonObject ());
	TSharedRef<TJsonReader<>> JsonReader = TJsonReaderFactory<>::Create (JsonString);

	if (FJsonSerializer::Deserialize (JsonReader, JsonObject) && JsonObject.IsValid ()) {
		//UE_LOG (LogTemp, Warning, TEXT ("Reading scene json..."));
		for (auto& currentObject : JsonObject->Values) {
			//UE_LOG (LogTemp, Warning, TEXT ("Current object field key: %s"), *currentObject.Key);
			TSharedPtr<FJsonObject> actorType = currentObject.Value->AsObject ();
			for (auto& currentInnerObject : actorType->Values) {
				TSharedPtr<FJsonObject> actor = currentInnerObject.Value->AsObject ();

				//Set spawn parameters such as name
				FActorSpawnParameters spawnParams;
				FString nameBase = *actor->GetStringField ("ActorName");
				spawnParams.Name = FName (*FString (nameBase + "_Spawned"));

				if (nameBase.Contains ("Tunnel")) UE_LOG (LogTemp, Warning, TEXT ("Tunnel!!"));
				//if (nameBase.Contains ("Cube")) continue;

				//Creating the actor
				TArray<FString> locationStrings;
				actor->GetStringField ("ActorPosition").ParseIntoArray (locationStrings, TEXT (","), true);
				TArray<FString> rotationStrings;
				actor->GetStringField ("ActorRotation").ParseIntoArray (rotationStrings, TEXT (","), true);
				TArray<FString> scaleStrings;
				actor->GetStringField ("ActorScale").ParseIntoArray (scaleStrings, TEXT (","), true);
				FTransform* transform = new FTransform (FVector (FCString::Atof (*locationStrings[0]), FCString::Atof (*locationStrings[1]), FCString::Atof (*locationStrings[2])));
				transform->SetRotation (FQuat::MakeFromEuler (FVector (FCString::Atof (*rotationStrings[0]), FCString::Atof (*rotationStrings[1]), FCString::Atof (*rotationStrings[2]))));
				transform->SetScale3D (FVector (FCString::Atof (*scaleStrings[0]), FCString::Atof (*scaleStrings[1]), FCString::Atof (*scaleStrings[2])));

				//UE_LOG (LogTemp, Warning, TEXT ("ID: %s"), *actor->GetStringField ("ID"));
				if (actor->GetStringField ("ID") == "") {
					APlaceableObject* spawnedActor = scenarioImporter->GetWorld ()->SpawnActor<APlaceableObject> (transform->GetLocation (), transform->GetRotation ().Rotator (), spawnParams);
					FString filePathString = "/Game/Assets/Models/" + currentObject.Key;
					const TCHAR* filePath = *filePathString;
					UStaticMesh* mesh = LoadObject<UStaticMesh> (nullptr, filePath);
					spawnedActor->SetMesh (mesh);
					spawnedActor->SetActorTransform (*transform);
					spawnedActor->Rename (*actor->GetStringField ("ActorName"));
				} else {
					FPlacedBuildingParameters params;
					params.bIsInteractable = true;
					params.bIsFree = true;
					params.bSkipLocationOffset = true;
					params.bLoadFromScenario = true;
					//params.bSkipLocationOffset = true;
					ABuildableActor* buildableActor = scenarioImporter->GetPlayerController ()->FindBuildableActorByName (FName (*actor->GetStringField ("ID"))); //FName (*actor->GetStringField("ID"))
					int rotation = transform->GetRotation ().Euler ().Z;
					buildableActor->SetRotation (transform->GetRotation ().Euler().Z);
					buildableActor->OnPreviewPlace (*transform, true, true);
					buildableActor->PlaceBuilding (*transform, params);
				}
			}
		}
		UE_LOG (LogTemp, Warning, TEXT ("Reading scene json finished!"));
		return true;
	} else return false;
}
