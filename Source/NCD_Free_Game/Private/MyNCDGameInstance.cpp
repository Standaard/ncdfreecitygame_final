// Fill out your copyright notice in the Description page of Project Settings.

#include "MyNCDGameInstance.h"
// #include "MoviePlayer.h"
#include <Kismet/GameplayStatics.h>

void UMyNCDGameInstance::Init()
{
	Super::Init();
	FCoreUObjectDelegates::PreLoadMap.AddUObject(this, &UMyNCDGameInstance::BeginLoadingScreen);
	FCoreUObjectDelegates::PostLoadMapWithWorld.AddUObject(this, &UMyNCDGameInstance::EndLoadingScreen);
}

void UMyNCDGameInstance::BeginLoadingScreen(const FString& InMapName)
{
	/*
	if (!IsRunningDedicatedServer())
	{
		FLoadingScreenAttributes LoadingScreen;

		LoadingScreen.bAutoCompleteWhenLoadingCompletes = false;
		LoadingScreen.WidgetLoadingScreen = FLoadingScreenAttributes::NewTestLoadingScreenWidget();

		GetMoviePlayer()->SetupLoadingScreen(LoadingScreen);
	}*/
}

void UMyNCDGameInstance::EndLoadingScreen(UWorld* InLoadedWorld)
{

}

bool UMyNCDGameInstance::LoadGameInSlot(int pSlot)
{
	SaveGame = Cast<UCitySaveGame>(UGameplayStatics::LoadGameFromSlot(GetSaveNameWithSlot(pSlot), pSlot));
	return SaveGame != nullptr;
}

bool UMyNCDGameInstance::SaveGameInSlot(FCitySystemExport pData, int pSlot)
{
	SaveGame = Cast<UCitySaveGame>(UGameplayStatics::CreateSaveGameObject(UCitySaveGame::StaticClass()));
	SaveGame->Data = pData;
	SaveGame->SaveDate = FDateTime::Now();
	bool Success = UGameplayStatics::SaveGameToSlot(SaveGame, GetSaveNameWithSlot(pSlot), pSlot);
	if (Success) RefreshSavegames();
	return Success;
}

UCitySaveGame* UMyNCDGameInstance::GetCurrentSaveGame()
{
	return SaveGame;
}


void UMyNCDGameInstance::SetSaveMode(bool pMode)
{
	SavingMode = pMode;
}

bool UMyNCDGameInstance::IsSaveModeOn()
{
	return SavingMode;
}

FString UMyNCDGameInstance::GetSaveNameWithSlot(int pSlot /*= 0*/)
{
	if (pSlot == 0) return DefaultSaveName;
	return DefaultSaveName + FString::FromInt(pSlot);
}

void UMyNCDGameInstance::ClearSaveGame()
{
	SaveGame = nullptr;
}

bool UMyNCDGameInstance::CheckIfSaveGameExists(int pSlot /*= 0*/)
{
	return UGameplayStatics::DoesSaveGameExist(GetSaveNameWithSlot(pSlot),pSlot);
}

void UMyNCDGameInstance::RefreshSavegames()
{
	SaveGamesAvailable.Empty();
	int SaveGameSlots = 3;
	for (size_t i = 0; i < SaveGameSlots; i++)
	{
		SaveGamesAvailable.Add(Cast<UCitySaveGame>(UGameplayStatics::LoadGameFromSlot(GetSaveNameWithSlot(i), i)));
	}
}

TArray<UCitySaveGame*> UMyNCDGameInstance::GetSaveGamesAvailable()
{
	return SaveGamesAvailable;
}

void UMyNCDGameInstance::SaveToConfig()
{
	SaveConfig();
}
