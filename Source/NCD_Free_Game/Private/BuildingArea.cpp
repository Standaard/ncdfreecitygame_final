// Fill out your copyright notice in the Description page of Project Settings.

#include "BuildingArea.h"
#include <DrawDebugHelpers.h>
#include "PlacedBuilding.h"

void UBuildingArea::SetOwner(APlacedBuilding* pOwner)
{
	Owner = pOwner;
}

bool UBuildingArea::AddBuilding(APlacedBuilding* pBuilding)
{
	if (!Owner || !pBuilding || Buildings.Contains(pBuilding)) return false;
	Buildings.Add(pBuilding);
	return true;
}

bool UBuildingArea::AddAffectingArea(UBuildingArea* pArea)
{
	if (!Owner || !pArea || AreasThatAffectOwner.Contains(pArea)) return false;
	AreasThatAffectOwner.Add(pArea);
	return true;
}

bool UBuildingArea::RemoveAffectingArea(UBuildingArea* pArea)
{
	if (!Owner || !pArea || !AreasThatAffectOwner.Contains(pArea)) return false;
	AreasThatAffectOwner.Remove(pArea);
	return true;
}


bool UBuildingArea::RemoveBuilding(APlacedBuilding* pBuilding)
{
	if (!pBuilding || !Buildings.Contains(pBuilding)) return false;
	Buildings.Remove(pBuilding);
	return true;
}

bool UBuildingArea::RemoveBuildingByLocation(FVector pLocation)
{
	APlacedBuilding* foundBuilding = nullptr;
	for (auto* building : Buildings)
	{
		if (building && building->BuildingStats.Location.Equals(pLocation))
		{
			foundBuilding = building;
			break;
		}
	}
	if (foundBuilding)
	{
		Buildings.Remove(foundBuilding);
		return true;
	} 
	return false;
}

void UBuildingArea::RemoveAllBuildings()
{
	if (Buildings.Num() == 0) return;
	Buildings.Empty();
	AreasThatAffectOwner.Empty();
}
