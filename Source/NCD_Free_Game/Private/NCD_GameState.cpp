// Fill out your copyright notice in the Description page of Project Settings.

#include "NCD_GameState.h"
#include "BuildingDescriptionActor.h"
#include "BuildingDemolishActor.h"

ABuildingDescriptionActor * ANCD_GameState::GetDescriptionActor () {
	if (DescriptionActor) return DescriptionActor;
	else {
		if (DescriptionActorClass && GetWorld ()) {
			DescriptionActor = GetWorld ()->SpawnActor<ABuildingDescriptionActor> (DescriptionActorClass, FVector::ZeroVector, FRotator::ZeroRotator);
		}
	}
	return DescriptionActor;
}

ABuildingDemolishActor* ANCD_GameState::GetDemolishActor () {
	if (DemolishActor) return DemolishActor;
	else {
		if (DemolishActorClass && GetWorld ()) {
			DemolishActor = GetWorld ()->SpawnActor<ABuildingDemolishActor> (DemolishActorClass, FVector::ZeroVector, FRotator::ZeroRotator);
		}
	}
	return DemolishActor;
}
