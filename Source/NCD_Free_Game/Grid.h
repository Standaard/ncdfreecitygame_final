// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class NCD_FREE_GAME_API Grid
{
public:
	Grid();
	~Grid();
public:
	static float GridSize;

	static FVector GetGridVector();
	static FVector GetLocationOnGridXY(FVector& Location);
	static FVector GetLocationOnGrid(FVector GridSize, FVector Location);
	static FVector GetLocationOnGrid(FVector Location);

	static FVector GetOffsetBoundingBoxAndGrid(FVector GridSize, FVector Location);
	static bool IsInRange(TArray<FVector> pLocations, FVector pCheckLocation, float pRange = 100.f);
};
