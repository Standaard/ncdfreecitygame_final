// Fill out your copyright notice in the Description page of Project Settings.

#include "NCD_Pawn.h"
#include "Grid.h"
#include <Components/DecalComponent.h>
#include <GameFramework/PlayerController.h>
#include <Components/StaticMeshComponent.h>
#include <Camera/CameraComponent.h>
#include <GameFramework/SpringArmComponent.h>
#include "BuildableActor.h"
#include <Classes/Components/HierarchicalInstancedStaticMeshComponent.h>
#include "DrawDebugHelpers.h"
#include <Unrealclient.h>
#include "BuildingHighlightActor.h"
#include <Kismet/KismetSystemLibrary.h>
#include <Kismet/GameplayStatics.h>
#include "BuildingPollutionActor.h"
#include "NCD_PlayerController.h"


// Sets default values
ANCD_Pawn::ANCD_Pawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	SceneComp = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComp"));
	RootComponent = SceneComp;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComp"));
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->bDoCollisionTest = false;
	SpringArm->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, DefaultCameraHeight),
		FRotator(DefaultCameraYRotation, 0.0f, 0.0f));
	SpringArm->TargetArmLength = DefaultArmLength;

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
	FViewport::ViewportResizedEvent.AddUObject(this, &ANCD_Pawn::OnViewportSizeChanged);

	 // Create a decal in the world to show the cursor's location
	//CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld2");
	//CursorToWorld->SetupAttachment(RootComponent);
	//CursorToWorld->SetDecalMaterial(DecalMaterial);
	//CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	//CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	GridMesh = CreateDefaultSubobject<UStaticMeshComponent>("GridMesh");
	GridMesh->SetupAttachment(RootComponent);
	GridMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GridMesh->CastShadow = false;

	//SelectedMesh = CreateDefaultSubobject<UStaticMeshComponent>("SelectedMesh");
	////SelectedMesh->SetupAttachment(RootComponent);
	//SelectedMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	//SelectedMesh->CastShadow = false;

	SquaredAutoPanMargin = FMath::Sqrt(AutoPanMargin);
	
}

void ANCD_Pawn::BeginPlay()
{
	Super::BeginPlay();
	PlayerController = Cast<ANCD_PlayerController>(GetController());
	if (!PlayerController)
	{
		UE_LOG(LogTemp, Error, TEXT("[NCD_Pawn] PlayerController is not of type ANCD_PlayerController!"));
	}

	PlayerController->GetViewportSize(ViewportSize.X, ViewportSize.Y);
	//set the default value of StartLocation
	StartLocation = GetActorLocation();
	if (!LocationBoundingBox.IsInside(StartLocation))
	{
		//Put it inside of the LocationBoundingBox
		StartLocation = LocationBoundingBox.GetCenter();
	}
	SetActorLocation(StartLocation);

	NewArmLength = DefaultArmLength;
	InitializeSpringArm();

	//set the squared treshold for panning the pawn with LMB
	DragPanMoveTresholdSqrd = FMath::Square(DragPanMoveTreshold);

	DefaultCameraHeight = StartLocation.Z;
	if (!GridStaticMesh || !GridMaterial)
	{
		UE_LOG(LogTemp, Error, TEXT("[NCD_Pawn] GridStaticMesh or GridMaterial are not assigned!"));
		return;
	}
	GridMesh->SetStaticMesh(GridStaticMesh);
	GridMesh->SetMaterial(0, GridMaterial);

	HighlightMeshActor = Cast<ABuildingHighlightActor>(GetWorld()->SpawnActor<ABuildingHighlightActor>(ABuildingHighlightActor::StaticClass(), FTransform::Identity));
	HighlightMeshActorTutorial = Cast<ABuildingHighlightActor> (GetWorld ()->SpawnActor<ABuildingHighlightActor> (ABuildingHighlightActor::StaticClass (), FTransform::Identity));
	if (!HighlightMeshActor || !HighlightMeshActor->GetMeshComponent()) return;
	SelectedMesh = HighlightMeshActor->GetMeshComponent();
	SelectedMeshTutorial = HighlightMeshActorTutorial->GetMeshComponent ();
}

// Called every frame
void ANCD_Pawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (bIsAutoPanning) {
		//Perform the automatic panning; doesn't allow the use any further input
		PerformAutoPan(DeltaTime);
	}
	else
	{
		//perform panning via axis
		if (bCanPan && !PanDirection.IsZero())
		{
			//normalize the axis input
			PanDirection.Normalize();
			//scale it correctly (ZoomPanPercentage is calculated by how far you are zoomed out)
			PanDirection *= PanScalar * DeltaTime * (1.0f + ZoomPanPercentage);
			PanCamera(FVector2D(PanDirection));
			PanDirection = FVector2D::ZeroVector;
			if (bCanDragPan)
			{
				MousePositionToGround(PreviousDragLocation);
			}

		}

		//perform panning via mouse drag
		if (bCanDragPan && !DragPanDirection.IsZero())
		{
			FVector currentDragLocation;
			//cast a ray from the mouse position to the ground, 
			//then store the position where the ray hits the ground
			MousePositionToGround(currentDragLocation);
			
			//only perform the mouse distance check when the treshold isn't reached
			if (!bDragPanTresholdReached)
			{
				PanDragDistanceSqrd = FVector2D::DistSquared(StartingMouseDragPosition, MousePosition);
				if (PanDragDistanceSqrd >= DragPanMoveTresholdSqrd)
					bDragPanTresholdReached = true;
			}

			//only perform the drag panning when the player moved PanDragMoveTreshold distance
			//and when the mouse has moved
			if (bDragPanTresholdReached && !currentDragLocation.IsZero())
			{
				FVector actorLocation = GetActorLocation();
				//subtract the distance from the actor location
				FVector newLocation = actorLocation - (currentDragLocation - PreviousDragLocation);
				ClampLocationInBounds(newLocation);
				//restore the Z location
				newLocation.Z = actorLocation.Z;
				SceneComp->SetWorldLocation(newLocation);
				PreviousDragLocation = currentDragLocation;
			}
			DragPanDirection = FVector2D::ZeroVector;
		}
	}
	ZoomCamera(DeltaTime);


	if (!GridMesh) return;
	FHitResult TraceHitResult;
	PlayerController->GetHitResultUnderCursor(ECC_GameTraceChannel3, false, TraceHitResult);
	GridMesh->SetWorldLocation(Grid::GetLocationOnGrid(FVector(100.f,100.f,100.f), TraceHitResult.Location));
}

void ANCD_Pawn::PerformAutoPan(float pDeltaTime)
{
	FVector currentActorLocation = GetActorLocation();
	if (FVector::DistSquared2D(currentActorLocation, AutoPanLocation) < SquaredAutoPanMargin)
	{
		bIsAutoPanning = false;
		bCanPan = true;
	}
	else
	{
		FVector a = FMath::Lerp(currentActorLocation, AutoPanLocation, pDeltaTime * AutoPanSpeed);
		SetActorLocation(FVector(a.X, a.Y, currentActorLocation.Z));
	}
}

void ANCD_Pawn::ClampLocationInBounds(FVector& pLocationToClamp)
{
	pLocationToClamp.X = FMath::Clamp(pLocationToClamp.X, LocationBoundingBox.Min.X, LocationBoundingBox.Max.X);
	pLocationToClamp.Y = FMath::Clamp(pLocationToClamp.Y, LocationBoundingBox.Min.Y, LocationBoundingBox.Max.Y);
}

void ANCD_Pawn::SelectAsset(UStaticMesh* pMesh, FTransform pTransform, TArray<UMaterialInterface*> pMaterials, bool pTutorial /* = false */, FMaterialParameters pParams /*= FMaterialParameters::Empty*/) {
	UnselectAsset(pTutorial);
	if (!pTutorial) {
		SelectedMesh->SetStaticMesh (pMesh);
		for (int32 i = 0; i < pMaterials.Num(); i++)
		{
			SelectedMesh->SetMaterial(i, pMaterials[i]);
		}

		for (auto& param : pParams.FloatScalars)
		{
			SelectedMesh->SetScalarParameterValueOnMaterials(param.Key, param.Value);
		}

		SelectedMesh->SetWorldTransform (pTransform);
		SelectedMesh->SetWorldScale3D (FVector (0.995, 0.995f, 0.995f));
		SelectedMesh->SetRenderCustomDepth (true);
	} else {
		SelectedMeshTutorial->SetStaticMesh (pMesh);
		SelectedMeshTutorial->SetWorldTransform (pTransform);
		SelectedMeshTutorial->SetWorldScale3D (FVector (0.995, 0.995f, 0.995f));
		SelectedMeshTutorial->SetRenderCustomDepth (true);
	}
}

void ANCD_Pawn::UnselectAsset(bool pTutorial /* = false */) {
	if (!pTutorial) {
		SelectedMesh->SetStaticMesh (nullptr);
		SelectedMesh->SetRenderCustomDepth (false);
	} else {
		SelectedMeshTutorial->SetStaticMesh (nullptr);
		SelectedMeshTutorial->SetRenderCustomDepth (false);

	}
}

UStaticMeshComponent* ANCD_Pawn::GetSelectedMesh(bool pTutorial /* = false */)
{
	if (!pTutorial)
	{
		return SelectedMesh;
	}
	else
	{
		return SelectedMeshTutorial;
	}
}

void ANCD_Pawn::SetDragPan(bool pEnabled)
{
	bCanDragPan = pEnabled;
	if (pEnabled) {
		MousePositionToGround(PreviousDragLocation);
		StartingMouseDragPosition = MousePosition;
	}
	else
	{
		bDragPanTresholdReached = false;
	}
}

void ANCD_Pawn::InitializeSpringArm()
{
	SpringArm->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, DefaultCameraHeight),
		FRotator(DefaultCameraYRotation, 0.0f, 0.0f));
	SpringArm->TargetArmLength = DefaultArmLength;
	SpringArm->CameraLagSpeed = CameraLag;
	SpringArm->CameraRotationLagSpeed = CameraRotationLag;
	SpringArm->bEnableCameraLag = bEnableCameraLag;
	SpringArm->bEnableCameraRotationLag = bEnableCameraRotationLag;
}

void ANCD_Pawn::OnViewportSizeChanged(FViewport* pViewport, uint32 pSomeIntValue)
{
	ViewportSize = pViewport->GetSizeXY();
	PanMarginHorizontal = ViewportSize.Y * PanMarginHorizontalPercentage;
	PanMarginVertical = ViewportSize.X * PanMarginVerticalPercentage;
}

void ANCD_Pawn::MousePositionToGround(FVector& pGroundLocation)
{
	if (!PlayerController) return;
	//TODO: Look into DPI scaling
	PlayerController->GetMousePosition(MousePosition.X, MousePosition.Y);

	FVector worldDirection = FVector::ZeroVector;
	FVector screenWorldLocation = FVector::ZeroVector;
	//Convert the mouse position in the viewport space to the world space, together with the direction
	UGameplayStatics::DeprojectScreenToWorld(PlayerController, MousePosition, screenWorldLocation, worldDirection);
	//multiply the direction by a large number; large enough to be able to rotate low + reach far
	worldDirection *= DragPanScreenDirectionScalar;
	FHitResult out;
	//perform a line trace against an invisible plane with PanProfileName
	GetWorld()->LineTraceSingleByProfile(out, screenWorldLocation, screenWorldLocation + worldDirection, PanProfileName);
	pGroundLocation = out.Location;
}

void ANCD_Pawn::PanEdges(bool pPanX)
{
	if (bDebug) return;
	if (bCanRotate || !PlayerController) return;

	PlayerController->GetMousePosition(MousePosition.X, MousePosition.Y);

	if (pPanX)
	{
		if (MousePosition.X < PanMarginVertical) EdgePanDirection.Y = -1.0f;
		else if (MousePosition.X > ViewportSize.X - PanMarginVertical) EdgePanDirection.Y = 1.0f;
	}
	else
	{
		if (MousePosition.Y < PanMarginHorizontal) EdgePanDirection.X = 1.0f;
		else if (MousePosition.Y > ViewportSize.Y - PanMarginHorizontal) EdgePanDirection.X = -1.0f;
	}
}

void ANCD_Pawn::PanCamera(FVector2D pPanDirection) {

	//Execute only if there is movement
	if (!bCanPan || pPanDirection.IsZero()) return;
	//Cache the magnitude of the direction (direction is normalized later)
	float DirMagnitude = pPanDirection.Size();
	
	//The idea is to move forward and right with the rotation of the SpringArm

	//Create a Rotator without a pitch
	FRotator zeroPitchRotator = SpringArm->RelativeRotation;
	zeroPitchRotator.Pitch = 0;

	//create a transform where there is no pitch applied
	FTransform zeroPitchTransform = SpringArm->GetComponentTransform();
	zeroPitchTransform.SetRotation(zeroPitchRotator.Quaternion());

	FVector newDirection = FVector::ZeroVector;
	//Calculate forward direction
	newDirection += zeroPitchTransform.GetUnitAxis(EAxis::X) * pPanDirection.X;
	//Calculate right direction
	newDirection += zeroPitchTransform.GetUnitAxis(EAxis::Y) * pPanDirection.Y;

	//Normalize the new direction
	newDirection.Normalize();
	FVector newVelocity = newDirection * DirMagnitude;
	FVector newLocation = GetActorLocation() + newVelocity;
	ClampLocationInBounds(newLocation);
	SceneComp->SetWorldLocation(newLocation);
}


void ANCD_Pawn::RotateCamera(FVector2D pDirection)
{
	//early out if there's no change
	if (pDirection.IsNearlyZero()) return;
	
	FRotator springArmRotation = SpringArm->RelativeRotation;

	if (pDirection.X != 0.0f) {
		springArmRotation.Yaw += pDirection.X * (bInvertHorizontalAxis ? -1 : 1);
	}
	if (pDirection.Y != 0.0f) {
		springArmRotation.Pitch += pDirection.Y * (bInvertVerticalAxis ? -1 : 1),
		springArmRotation.Pitch = FMath::Clamp(springArmRotation.Pitch, MinimumPitch, MaximumPitch);
	}
	SpringArm->SetRelativeRotation(springArmRotation);
}

void ANCD_Pawn::ZoomCamera(float pDeltaTime)
{
	SpringArm->TargetArmLength = FMath::FInterpTo(SpringArm->TargetArmLength, NewArmLength, pDeltaTime, ZoomLerpScalar);
	ZoomPanPercentage = (SpringArm->TargetArmLength - MinArmLength) / (MaxArmLength - MinArmLength);
}


void ANCD_Pawn::AutoPanToLocation(FVector pPanLocation)

{
	AutoPanLocation = pPanLocation;
	bIsAutoPanning = true;
	bCanPan = false;
}

void ANCD_Pawn::InputMoveRight(float pInput)
{
	if (FMath::IsNearlyZero(pInput)) return;
	PanDirection.Y = pInput;
}

void ANCD_Pawn::InputMoveForward(float pInput)
{

	if (FMath::IsNearlyZero(pInput)) return;
	//UE_LOG(LogTemp, Warning, TEXT("moveforward"));
	PanDirection.X = pInput;
}

void ANCD_Pawn::InputStartCameraRotation()
{
	bCanRotate = true;
}

void ANCD_Pawn::InputStopCameraRotation()
{
	bCanRotate = false;
}

void ANCD_Pawn::InputZoom(float pInput)
{
	if (!FMath::IsNearlyZero(pInput))
	{
		CurrentArmLength = SpringArm->TargetArmLength;
		NewArmLength = CurrentArmLength - (pInput * ZoomScalar);
		NewArmLength = FMath::Clamp<float>(NewArmLength, MinArmLength, MaxArmLength);
	}
}

void ANCD_Pawn::InputRotateYaw(float pInput)
{
	if (GetWorld())
		RotateCamera(FVector2D(pInput * RotateScalar * GetWorld()->GetDeltaSeconds(),0));
}

void ANCD_Pawn::InputMouseX(float pInput)
{
	if (bCanDragPan)
	{
		DragPanDirection.X = pInput;
	} 
	else if (bCanRotate)
	{
		float newYaw = pInput * RotateScalar * (bInvertHorizontalAxis ? -1 : 1) * GetWorld()->GetDeltaSeconds();
		RotateCamera(FVector2D(newYaw, 0));
	}
	PanEdges(true);
}

void ANCD_Pawn::InputMouseY(float pInput)
{
	if (bCanDragPan)
	{
		DragPanDirection.Y = pInput;
	}
	else if (bCanRotate)
	{
		float newPitch = pInput * RotateScalar * (bInvertVerticalAxis ? -1 : 1) * GetWorld()->GetDeltaSeconds();
		RotateCamera(FVector2D(0, newPitch));
	}
	PanEdges(false);
}

// Called to bind functionality to input
void ANCD_Pawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MouseAxis", this, &ANCD_Pawn::InputZoom);
	PlayerInputComponent->BindAxis("MouseX", this, &ANCD_Pawn::InputMouseX);
	PlayerInputComponent->BindAxis("MouseY", this, &ANCD_Pawn::InputMouseY);
	PlayerInputComponent->BindAxis("MoveForward", this, &ANCD_Pawn::InputMoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ANCD_Pawn::InputMoveRight);
	PlayerInputComponent->BindAxis("RotateYaw", this, &ANCD_Pawn::InputRotateYaw);

	PlayerInputComponent->BindAction("MouseRightClick", IE_Pressed, this, &ANCD_Pawn::InputStartCameraRotation);
	PlayerInputComponent->BindAction("MouseRightClick", IE_Released, this, &ANCD_Pawn::InputStopCameraRotation);


}




