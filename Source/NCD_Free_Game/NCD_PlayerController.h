// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "BuildingStats.h"
#include "Milestones.h"
#include "BuildableRoad.h"
#include "BuildableBuilding.h"
#include "SpawnedBuildings.h"
#include "BuildableUtility.h"
#include "BuildableActor.h"
#include "NCD_PlayerController.generated.h"

DECLARE_DELEGATE_OneParam (FHandleOnRelease, FBuildingStats);

/**
 *
 */
USTRUCT ()
struct FStarterBuilding {
	GENERATED_USTRUCT_BODY ()

public:
	int32 ID;
	ABuildableActor* BuildableActor;
	FTransform BuildingTransform;
	int AmountToPlace;
	FPlacedBuildingParameters buildingParams;

	FStarterBuilding () {}

	FStarterBuilding (ABuildableActor* pBuildableActor, FTransform pBuildingTransform, FPlacedBuildingParameters& pParams) {
		ID = FMath::RandRange (0, 2000000000);
		BuildableActor = pBuildableActor;
		BuildingTransform = pBuildingTransform;
		buildingParams = pParams;
	}

	friend bool operator== (const FStarterBuilding& pFirst, const FStarterBuilding& pSecond) {
		return (pFirst.ID == pSecond.ID);
	}
};

UCLASS()
class NCD_FREE_GAME_API ANCD_PlayerController : public APlayerController {
	GENERATED_BODY()

public:
	ANCD_PlayerController ();

public:
	FHandleOnRelease HandleOnRelease;

	UFUNCTION (BlueprintCallable)
		void OnTimeChange (bool pIsNight);

	UFUNCTION (BlueprintCallable)
		void DisableBuildingMode ();

	UFUNCTION (BlueprintCallable)
		void EnableBuildingMode ();

	UFUNCTION (BlueprintNativeEvent)
		void OnDisableBuildingMode ();

	UFUNCTION (BlueprintNativeEvent)
		void OnEnableBuildingMode ();

	UFUNCTION (BlueprintCallable)
		void SetSelectedBuildableActor (ABuildableActor* pBuildableActor);

	UFUNCTION(BlueprintCallable)
		ABuildableActor* FindBuildableActorByName(FName pName);

	UFUNCTION (BlueprintCallable)
		FVector GetSelectedLocation ();

	UFUNCTION (BlueprintCallable)
		FVector GetGridLocation (FVector pUnsnappedLocation);

	UFUNCTION (BlueprintCallable)
		void DeleteBuilding (); // Delete that nasty building!

	UFUNCTION (BlueprintNativeEvent)
		void OnDeleteBuilding ();

	UFUNCTION (BlueprintCallable)
		bool CheckBuildingStatus ();

	UFUNCTION (BlueprintCallable)
		bool SelectedBuildingRemovable ();

	UFUNCTION (BlueprintCallable)
		ABuildableActor* AddBuildableActor (struct FBuildingStats pBuildingStats);

	UFUNCTION(BlueprintCallable)
		void AddAllBuildableActors();

	UFUNCTION (BlueprintCallable)
		void OnToggleInteractable ();

	UFUNCTION (BlueprintCallable)
		void OnToggleCollisionCheck ();

	UFUNCTION (BlueprintImplementableEvent, Category = "Money")
		void ShowMonthlyMoneyStatus ();
	UFUNCTION (BlueprintImplementableEvent, Category = "Population")
		void ShowMonthlyPopulationStatus ();

	UFUNCTION (BlueprintImplementableEvent, Category = "Money")
		void BuyBuildingExpenses (float pExpenses, FVector pLocation);
	//const FString & pMessage

	UFUNCTION (BlueprintImplementableEvent, Category = "Building status")
		void ShowStatusAboveBuilding (FVector pLocation);

	UFUNCTION (BlueprintImplementableEvent, Category = "Sounds")
		void PlayErrorSound ();
	//const FString & pMessage

	UFUNCTION (BlueprintImplementableEvent, Category = "UpdateUI")
		void UpdateUIWindows ();
	UFUNCTION ()
		void UpdateBuildingHighlight ();
	UFUNCTION ()
		void UnbindTutorialFunctions ();

	UFUNCTION ()
		void Rereference ();
	UFUNCTION ()
		ABuildableActor* GetBuildableActor (FName pID);

	UFUNCTION (BlueprintCallable, Category = "DemolishMode")
		void SetDemolishModeActive (bool pDemolishModeActive);
	UFUNCTION (BlueprintCallable, Category = "DemolishMode")
		bool GetDemolishModeActive ();
	UFUNCTION (BlueprintNativeEvent, Category = "DemolishMode")
		void OnOpenDemolishWindow ();
	UFUNCTION (BluePrintNativeEvent, Category = "DemolishMode")
		void OnDemolishModeChanged (bool pNewState);
	UFUNCTION (BlueprintCallable, Category = "DemolishMode")
		void SetShowWarning (bool pShowWarning);
	UFUNCTION (BlueprintCallable, Category = "DemolishMode")
		bool getShowWarning ();
	UFUNCTION (BluePrintCallable, Category = "DemolishMode")
		void SetBuildingMessageActive (bool pBuildingMessageActive);
	UFUNCTION (BluePrintCallable, Category = "DemolishMode")
		void CloseDescriptionWindow ();
	UFUNCTION (BlueprintNativeEvent, Category = "DemolishMode")
		void OnCantDemolishBuilding ();

	UFUNCTION (BlueprintNativeEvent, Category = "BuildMode")
		void OnResidenceBuilt (FBuildingStats pBuildingStats);
	UFUNCTION (BlueprintNativeEvent, Category = "BuildMode")
		void OnWorkplaceBuilt (FBuildingStats pBuildingStats);
	UFUNCTION()
		void PlaceBuilding(FTransform pBuildingTransform, FPlacedBuildingParameters pParameters);
	UFUNCTION (BlueprintNativeEvent, Category = "Description")
		void OnOpenDescriptionWindow (ABuildableActor* pBuildableActor);

	UFUNCTION ()
		class ANCD_Pawn* GetNCDPawn ();

	UFUNCTION(BlueprintCallable)
		void SetHighlightMaterials(TArray<UMaterialInterface*>& pMaterials);

	UFUNCTION(Exec)
		void SetCollisionSkip(bool pTrue);

#pragma region Starter town
	UFUNCTION ()
		void BuildStarterTown ();

	UPROPERTY ()
		TArray< FStarterBuilding> StarterTownPlacements;
	UPROPERTY (EditDefaultsOnly, BlueprintReadWrite, Category = "StarterTown")
		TArray<FTransform> StarterRoadTransforms;
	UPROPERTY (EditDefaultsOnly, BlueprintReadWrite, Category = "StarterTown")
		FTransform StarterCityHallTransform;
	UPROPERTY (EditDefaultsOnly, BlueprintReadWrite, Category = "StarterTown")
		TArray<FTransform> StarterHouseTransforms;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "StarterTown")
		TArray<FSpawnedBuildings> StarterTownBuildings;

#pragma endregion

public:
	ABuildableActor* GetSelectedBuildableActor ();

	bool bDemolishModeActive;
	bool bShowWarning;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bUseCustomHighlightMaterials;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay () override;
	// Begin PlayerController interface
	virtual void PlayerTick (float DeltaTime) override;
	virtual void SetupInputComponent () override;
	// End PlayerController interface
protected:

	void OnTap (); // On Click or Tap!
	void OnTapRelease();
	void OnRotate (); // When the rotation button is pressed
	void OnSwitchEntity (); // Switch selection!
	UFUNCTION(BlueprintCallable, Category = "Player Controller|Raycast")
	bool IsBuildableActor (FHitResult pHitResult);

	void HighlightBuilding (FHitResult pHitResult, bool pAlsoShowMessage = false, bool pShowHighlight = true);
	void CountOnTapHeld();
	void RemoveHighlight ();

	// FBuildingStats GetStatsByID(FName pID);
protected:

	UPROPERTY (EditDefaultsOnly, BlueprintReadOnly, Category = "Building")
		class UMaterialParameterCollection* LightParameterCollection;

	UPROPERTY (VisibleDefaultsOnly, BlueprintReadOnly, Category = "Building")
		class ABuildableActor* SelectedBuildableActor;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building")
		TSubclassOf<ABuildableActor> BuildableActorClass = ABuildableActor::StaticClass();

	UPROPERTY (EditDefaultsOnly, BlueprintReadOnly, Category = "Building")
		TSubclassOf<ABuildableActor> BuildableRoadClass = ABuildableRoad::StaticClass();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Building")
		TSubclassOf<ABuildableActor> BuildableBuildingClass = ABuildableBuilding::StaticClass();

	UPROPERTY (EditDefaultsOnly, BlueprintReadOnly, Category = "Building")
		TSubclassOf<ABuildableActor> BuildableUtilityClass = ABuildableUtility::StaticClass ();

	UPROPERTY (VisibleInstanceOnly, BlueprintReadOnly, Category = "Building")
		TMap<FName, ABuildableActor*> BuildableActorMap;

	UPROPERTY()
		FTimerHandle OnTapTimerHandle;

private:
	UPROPERTY()
		class UMaterialParameterCollectionInstance* LightParameterCollectionInstance;
	UPROPERTY()
		class ACitySystemGameMode* CitySystem;
	UPROPERTY()
		class ANCD_Pawn* NCD_Pawn;
	UPROPERTY()
		FVector SelectedLocation;
	UPROPERTY()
		class ANCD_GameState* GameState;
	UPROPERTY()
		TArray<UMaterialInstanceDynamic*> DynamicMaterials;
	UPROPERTY()
		TArray<UMaterialInterface*> HighlightMaterials;

	int SelectedBuildableActorBuildingID = 0;
	int TotalRoadAmount = 0;

	bool bBuildingModeEnabled = false;
	bool bSkipCollisionCheck = false;
	bool bSetInteractable = true;
	bool bBuildingMessageActive = false;

	
	float OnTapHeldDownTime = 0.0f;
};
